<?php
return [
    'adminEmail' => 'erik@eset.ee',
    'currency' => 'EUR',

    // site language
    'languages' => array(
        'en' => 'English',
        'ru' => 'Русский',
        'et' => 'Eesti',
        'lv' => 'Lietuvių',
    ),

    'defaultResellerId' => 1,
];
