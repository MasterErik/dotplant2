<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    'viewPath' => '@app/theme/mail',
    'useFileTransport' => false,
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'safertechnology.eu',
        'username' => 'noreply@safertechnology.eu',
        'password' => 'noreply123456',
        'port' => '25',
    ],
    //'sendMail' => '',
    'messageConfig' => [
        'from' => 'noreplay@eset.ee',
        'charset' => 'UTF-8',
    ]
];
