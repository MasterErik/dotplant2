<?php
namespace app\theme\tests\codeception\fixtures;

use yii\test\ActiveFixture;

class EmailConfirmCodeFixture extends ActiveFixture
{
    public $modelClass = 'app\theme\models\EmailConfirmCode';
}
