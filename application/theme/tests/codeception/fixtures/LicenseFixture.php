<?php
namespace app\theme\tests\codeception\fixtures;

use app\models\OrderItem;
use app\theme\models\License;
use yii\test\ActiveFixture;

class LicenseFixture extends ActiveFixture
{
    public $modelClass;
    public $depends;

    public function __construct($config = [])
    {
        $this->modelClass = License::className();
        $this->depends = [
            OrderItemsFixture::className()
        ];
        parent::__construct($config);
    }


}
