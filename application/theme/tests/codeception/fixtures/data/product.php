<?php
return [
    'product1' => [
        'id' => 1,
        'main_category_id' => 1,
        'name' => 'ESET ENDPOINT ANTIVIRUS',
        'slug' => 'ENDPOINT ANTIVIRUS',
        'price' => '153.05',
        'currency_id' => 3,
        'eset_product_code' => 107,
        'eset_quantity_params' => '5-25',
        'eset_period_params' => '12 => 1 year, 24 => 2 years, 36 => 3 years'
    ],
    'product2' => [
        'id' => 2,
        'main_category_id' => 1,
        'name' => 'ESET Endpoint Security',
        'slug' => 'Endpoint Security',
        'price' => '61',
        'currency_id' => 3,
        'eset_product_code' => 307,
        'eset_quantity_params' => '1-4',
        'eset_period_params' => '12 => 1 year, 24 => 2 years, 36 => 3 years'
    ]
];
