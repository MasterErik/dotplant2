<?php
return [
    'order1_item1' => [
        'id' => 1,
        'order_id' => 1,
        'product_id' => 1,
        'quantity' => 1,
        'additional_options' => json_encode([
            "additionalPrice" => 0,
            "eset_quantity" => 1,
            "eset_period" => 12,
            "eset_purchase_type" => "New",
            "eset_note" => null,
            "eset_price" => "0.31"
        ])
    ],
    'order1_item2' => [
        'id' => 2,
        'order_id' => 1,
        'product_id' => 2,
        'quantity' => 1,
        'additional_options' => json_encode([
            "additionalPrice" => 0,
            "eset_quantity" => 1,
            "eset_period" => 12,
            "eset_purchase_type" => "New",
            "eset_note" => null,
            "eset_price" => "0.31"
        ])
    ],
];
