<?php
namespace app\theme\tests\codeception\fixtures;

use app\models\OrderItem;
use yii\test\ActiveFixture;

class OrderItemsFixture extends ActiveFixture
{
    public $modelClass;
    public $depends;

    public function __construct($config = [])
    {
        $this->modelClass = OrderItem::className();
        $this->depends = [
            ProductFixture::className(),
            OrderFixture::className()
        ];
        parent::__construct($config);
    }


}
