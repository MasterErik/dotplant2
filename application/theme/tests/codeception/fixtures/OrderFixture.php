<?php
namespace app\theme\tests\codeception\fixtures;

use app\models\Order;
use yii\test\ActiveFixture;

class OrderFixture extends ActiveFixture
{
    public $modelClass;
    public $depends;

    public function __construct($config = [])
    {
        $this->modelClass = Order::className();
        $this->depends = [
            ProductFixture::className()
        ];
        parent::__construct($config);
    }


}
