<?php

$_config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../../../config/console.php'),
    require(__DIR__ . '/config.php'),
    [
        'class' => 'yii\console\Application',
        'components' => [
            'db' => [
                'dsn' => 'mysql:host=localhost;dbname=dotplant2_unit',
                'username' => 'dotplant2_unit',
                'password' => 'dr9rfTjYdHE9UNTc',
            ],
        ],
    ]
);

Yii::setAlias('@tests', dirname(dirname(__DIR__)));

return $_config;