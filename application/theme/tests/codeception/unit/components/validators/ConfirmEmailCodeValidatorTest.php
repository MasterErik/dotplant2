<?php
namespace models;

use app\theme\models\RegistrationForm;
use app\theme\tests\codeception\fixtures\EmailConfirmCodeFixture;
use yii\codeception\DbTestCase;

class ConfirmEmailCodeValidatorTest extends DbTestCase
{
    public function fixtures()
    {
        return [
            'email_code' => EmailConfirmCodeFixture::className(),
        ];
    }
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testValidator()
    {

        $model = new RegistrationForm([
            'email' => $this->email_code['email1']['email'],
            'confirmEmailCode' => $this->email_code['email1']['code'],
        ]);
        $this->assertTrue($model->validate(['confirmEmailCode']), print_r($model->getErrors(),1));


        $model = new RegistrationForm([
            'email' => 'bad_email@example.com',
            'confirmEmailCode' => $this->email_code['email1']['code'],
        ]);
        $this->assertFalse($model->validate(['confirmEmailCode']));


        $model = new RegistrationForm([
            'email' => $this->email_code['email1']['email'],
            'confirmEmailCode' => 'BAD_CODE',
        ]);
        $this->assertFalse($model->validate(['confirmEmailCode']));
   }

}