<?php
namespace models;

//TODO Надо проверить как отрабатывает смена статуса Order. При регистрации ордера, оплате, выдачи лицензии.

use app\theme\models\License;
use app\theme\models\Order;
use app\theme\tests\codeception\fixtures\LicenseFixture;
use app\theme\tests\codeception\fixtures\OrderFixture;
use yii\codeception\DbTestCase;
use Codeception\Util\Debug;


class OrderTest extends DbTestCase
{
    public function fixtures()
    {
        return [
            'orders' => OrderFixture::className(),
            'licenses' => LicenseFixture::className(),
        ];
    }
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testStatusAfterLicenseRegistered()
    {
        //Debug::debug(\Yii::$app->className());

        // проверим входные параметры
        $license1 = License::findOne($this->licenses['unregistered_license1']['id']);
        $this->assertNotNull($license1, 'Step1. License not found in the database');

        $license2 = License::findOne($this->licenses['unregistered_license2']['id']);
        $this->assertNotNull($license2, 'Step2. License not found in the database');

        $order = Order::findOne($this->orders['new_order1']['id']);
        $this->assertNotNull($order, 'Order not fount in the database');

        $this->assertTrue($order->order_status_id == 2, 'Order status is not 2');

        $license1->register_state = License::REGISTER_STATE_REGISTERED;
        $license1->save();

        $this->assertTrue($order->order_status_id == 2, 'Order status is not 2');

        $license2->register_state = License::REGISTER_STATE_REGISTERED;
        $license2->save();

        $order = Order::findOne($this->orders['new_order1']['id']);
        $this->assertTrue($order->order_status_id == 4, 'Order status is not 4');

    }

}