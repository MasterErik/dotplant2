<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');


// the entry script URL (without host info) for functional and acceptance tests
// PLEASE ADJUST IT TO THE ACTUAL ENTRY SCRIPT URL
defined('YII_TEST_ENTRY_URL') or define('YII_TEST_ENTRY_URL', '/index-test.php');

// the entry script file path for functional and acceptance tests
defined('YII_TEST_ENTRY_FILE') or define('YII_TEST_ENTRY_FILE', dirname(__DIR__) . '/index-test.php');


require_once(__DIR__ . '/../../../vendor/autoload.php');
require_once(__DIR__ . '/../../../vendor/yiisoft/yii2/Yii.php');

// set correct script paths
//$_SERVER['SCRIPT_FILENAME'] = YII_TEST_ENTRY_FILE;
//$_SERVER['SCRIPT_NAME'] = YII_TEST_ENTRY_URL;
//$_SERVER['SERVER_NAME'] = 'localhost';
//$_SERVER['SERVER_PORT'] = '80';

Yii::setAlias('@tests', dirname(__DIR__));
//echo dirname(__DIR__)."\n"; exit;

