<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 06.09.15
 * Time: 8:34
 */

namespace app\theme\components\invoice;


use app\theme\models\Discount;
use app\theme\models\Invoice;
use app\theme\models\Order;
use app\theme\models\UserProfile;
use yii\base\Model;

class Generator extends Model
{
    /**
     * @var Invoice[]
     */
    private $invoices = [];

    /**
     * Create invoices for resellers
     * @param $order
     * @return Invoice|bool
     */
    public function addOrder($order)
    {
        $reseller = UserProfile::getParentReseller($order->user_id);

        if (!isset($this->invoices[$reseller->user_id])) {
            
            
            //Invoice::TYPE_INVOICE
            $this->invoices[$reseller->user_id] = Invoice::create($reseller);
        }

        $invoice = &$this->invoices[$reseller->user_id];
        if ($invoice->hasErrors()) {
            $this->addError('invoice', 'Error create invoice: ' . print_r($invoice->getErrors(), 1));

            return false;
        }

        $invoice_detail = $invoice->addOrder($order);
        if ($invoice_detail->hasErrors()) {
            $this->addError('invoice_detail', 'Error create invoice detail: ' . print_r($invoice_detail->getErrors(), 1));

            return false;
        }

//        foreach ($order->items as $item) {
//            // лучшая скидка по продукту для ресселера
//            // TODO согласовать
//            $discount = Discount::find()
//                ->product($item->product_id)
//                ->user($reseller->user_id)
//                ->date($order->start_date)
//                ->best()
//                ->one();
//            if ($discount) {
//                $invoice_detail = $invoice->addDiscount($discount, 'bonus', $item);
//                if ($invoice_detail->hasErrors()) {
//                    $this->addError('invoice_bonus', 'Error create invoice detail: ' . print_r($invoice_detail->getErrors(), 1));
//
//                    return false;
//                }
//
//            }
//        }

        // лучшая скидка для ресселера
        foreach ($order->items as $item) {
            $discount = Discount::find()
                ->user($reseller->user_id)
                ->date($order->start_date)
                ->best()
                ->one();
            if ($discount) {
                $invoice_detail = $invoice->addDiscount($discount, 'partner', $item);
                if ($invoice_detail->hasErrors()) {
                    $this->addError('invoice_partner', 'Error create invoice detail: ' . print_r($invoice_detail->getErrors(), 1));

                    return false;
                }
            }
        }

        return $invoice;
    }

    /**
     * Create Invoice for single user order
     * @param $order Order
     * @param string $type
     * @return Invoice|bool
     */
    public function makeInvoice($order, $type = Invoice::TYPE_INVOICE)
    {
        $invoice = Invoice::create($order->clientUserProfile, $type);
        if ($invoice->hasErrors()) {
            $this->addError('invoice', 'Error create invoice: ' . print_r($invoice->getErrors(), 1));

            return false;
        }

        $invoice_detail = $invoice->addOrder($order);
        if ($invoice_detail->hasErrors()) {
            $this->addError('invoice_detail', 'Error create invoice detail: ' . print_r($invoice_detail->getErrors(), 1));

            return false;
        }

        return $invoice;
    }


}

