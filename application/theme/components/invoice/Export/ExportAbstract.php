<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 10.09.15
 * Time: 20:30
 */

namespace app\theme\components\invoice\Export;


use app\theme\models\query\InvoiceQuery;

abstract class ExportAbstract
{
    /**
     * @param InvoiceQuery $invoices
     * @return string content
     */
    abstract public function run(InvoiceQuery $invoices);
}