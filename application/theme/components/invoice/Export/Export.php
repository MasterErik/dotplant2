<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 10.09.15
 * Time: 22:00
 */

namespace app\theme\components\invoice\Export;

use app\theme\models\query\InvoiceQuery;
use app\theme\module\helpers\StyleCase;
use yii\base\Model;

/**
 * Class Export
 * @package app\theme\components\invoice\Export
 *
 * @property string $data
 */
class Export extends Model
{
    /**
     * @var $invoices InvoiceQuery
     */
    public $invoices;

    public $format;

    private static $formats = [
        'hansa_world' => 'Hansa World SBE',
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['format', 'in', 'range' => array_keys(self::$formats)],
        ];
    }

    public function getData()
    {
        $className = $this->getClassName();
        if( !$className ) {
            return false;
        }

        /** @var $class  ExportAbstract */
        $class = new $className;
        return $class->run($this->invoices);
    }

    private function getClassName()
    {
        $className = __NAMESPACE__ . '\\' . StyleCase::toCamelCase($this->format) . 'Export';
        if (!class_exists($className)) {
            return false;
        }

        return $className;
    }

    public static function getFormats()
    {
        return self::$formats;
    }
}