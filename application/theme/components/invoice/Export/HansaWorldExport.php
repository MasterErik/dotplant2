<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 10.09.15
 * Time: 20:31
 */

namespace app\theme\components\invoice\Export;


use app\theme\models\Invoice;
use app\theme\models\query\InvoiceQuery;

class HansaWorldExport extends ExportAbstract
{
    /**
     * @param InvoiceQuery $invoices
     * @return string
     */
    public function run(InvoiceQuery $invoices)
    {
        //Iterator
//        $dataProvider = new ActiveDataProvider([
//            'query' => $invoices,
//
//        ]);

        $content = '';
        foreach( $invoices->all() as $invoice ) {
            /**
             * @var $invoice Invoice
             */
            //$invoice->

            $quantity = 0;
            $items = [];
            $content = '';

            foreach (array() /*$invoice->invoiceDetails*/ as $item) {

                $item['total_sum'] = ($item['discount'] > 0 ? ($item['item_price'] - ($item['item_price'] * ($item['discount'] / 100))) * $item['item_count'] : $item['item_price'] * $item['item_count']);
                $quantity += $item['item_count'];

                $items[$item->id]['stp'] = '1';//sisu v2li, alati 1
                $items[$item->id]['ArtCode'] = '';//artiklikood
                $items[$item->id]['Quant'] = intval(trim($item['item_count']));//kogus
                $items[$item->id]['Price'] = $this->nrFormat($item['item_price']); //yhiku hind
                $items[$item->id]['Sum'] = $this->nrFormat($item['total_sum'], " ");//summa
                $items[$item->id]['vRebate'] = $item['discount'];//ale %
                $items[$item->id]['SalesAcc'] = '3010';//myygikonto, Allan Kinsigo OTRS , October 05, 2011 4:09 Kui andmed imporditakse Books'i v?liss?steemist, tuleb selle s?steemi ?mber seadistada nii, et m??gikontoks oleks "3010" ning KM koodiks "5".
                $items[$item->id]['Objects'] = '';
                $items[$item->id]['OrdRow'] = '';
                $items[$item->id]['BasePrice'] = '0,00';//artikli ostuhind
                $items[$item->id]['rowGP'] = '0,00';//artikli myygikate
                $items[$item->id]['FIFO'] = '';
                $items[$item->id]['Spec'] = $this->replace_newline(trim($item['item_name']));//artikli nimetus
                $items[$item->id]['VATCode'] = '5';//KM kood
                $items[$item->id]['Recepy'] = '';
                $items[$item->id]['SerialNr'] = '';//seeria nr
                $items[$item->id]['PriceFactor'] = '';
                $items[$item->id]['VARList'] = '';
                $items[$item->id]['CUPNr'] = '';
                $items[$item->id]['FIFORowVal'] = '';
                $items[$item->id]['Coefficient'] = '';
                $items[$item->id]['CuAccCode'] = '';
                $items[$item->id]['ExciseNr'] = '';
                $items[$item->id]['PeriodCode'] = '';
                $items[$item->id]['UnitCode'] = trim($item['item_unit']);//yhik - tk, kast jne
                $items[$item->id]['UnitFactQuant'] = '0';
                $items[$item->id]['UnitFactPrice'] = '0,00';
                $items[$item->id]['UnitXval'] = '';
                $items[$item->id]['UnitYval'] = '';
                $items[$item->id]['UnitZval'] = '';
                $items[$item->id]['VECode'] = '';
                $items[$item->id]['CreditCard'] = '';
                $items[$item->id]['AuthorizationCode'] = '';
                $items[$item->id]['PosCode'] = '';
                $items[$item->id]['CurncyCode'] = '';
                $items[$item->id]['FrRate'] = '';
                $items[$item->id]['ToRateB1'] = '';
                $items[$item->id]['ToRateB2'] = '';
                $items[$item->id]['BaseRate1'] = '';
                $items[$item->id]['BaseRate2'] = '';
                $items[$item->id]['PayMode'] = '';
                $items[$item->id]['GCNr'] = '';
                $items[$item->id]['CustOrdNr'] = '';
                $items[$item->id]['RepaExVAT'] = '';
                $items[$item->id]['BasePriceB2'] = '';
            }
            //$header['items'] = $items;

            $header['SerNr'] = $invoice->number;
            $header['InvDate'] = date('d.m.Y', strtotime($invoice->created_at));
            $header['CustCode'] = $invoice->user_id;#'1003';

            //todo помоему PayDate я неверно вычисляю
            $header['PayDate'] = '??'; //($invoice['paid'] == '1' ? date('d.m.Y', strtotime($invoice->due_date)) : '');

            //todo ???
            $header['Addr0'] = trim($invoice->userProfile->client_name);
            $header['Addr1'] = trim($invoice->userProfile->address);
            $header['Addr2'] = '';
            $header['Addr3'] = '';
            $header['OurContact'] = 'Allan Kinsigo';
            $header['ClientContact'] = '';//kliendi esindaja
            $header['ExportFlag'] = '0';
            $header['PayDeal'] = round(abs(strtotime($invoice->due_date) - strtotime($invoice->created_at)) / 60 / 60 / 24);//tasumistingimus
            $header['OrderNr'] = '';
            $header['Prntdf'] = '';
            $header['OKFlag'] = '0';
            $header['pdays'] = round(abs(strtotime($invoice->due_date) - strtotime($invoice->created_at)) / 60 / 60 / 24);//tasumistingimus
            $header['pdvrebt'] = '0';
            $header['pdrdays'] = '0';
            $header['CustCat'] = 'EKSP';//kliendiklass
            $header['pdComment'] = '';
            $header['x1'] = '';
            $header['InvType'] = '1';//arvetyyp, 1=tavaline, 2=sularahaarve
            $header['xStatFlag'] = '0';
            $header['PriceList'] = 'EUR';//hinnakiri
            $header['Objects'] = '';//objekt

            //todo Сумма включает налог ?
            $header['InclVAT'] = '1'; //$invoice['include_wat'];//sis.KM ??
            $header['ARAcc'] = $this->nrFormat($invoice->tax);//myygiv6lakonto, arve summa ilma KM-ta

            $header['InvComment'] = '';
            $header['CredInv'] = '';
            $header['CredMark'] = '';
            $header['SalesMan'] = 'A';//myygimees
            $header['ToRateB1'] = '1';//kurss baasvaluutaga
            $header['TransDate'] = date('d.m.Y', strtotime($invoice->created_at));//kande kuupv
            $header['CurncyCode'] = 'EUR'; //(array_key_exists($invoice[currency_id], $rates_2010) ? $invoice[currency_id] : (intval(date('Y', strtotime($invoice[out_date]))) < 2011 ? 'EEK' : 'EUR'));//valuuta		date('d.m.Y', strtotime($invoice[out_date]));$rates_2010
            $header['LangCode'] = 'EST';//keelekood
            $header['UpdStockFlag'] = '1';//muuda ladu
            $header['LastRemndr'] = '';
            $header['LastRemDate'] = '';
            $header['Sign'] = 'A';//myygimees
            $header['FrPrice'] = '';
            $header['FrBase'] = '';
            $header['FrItem'] = '';
            $header['FrVATCode'] = '';
            $header['FrObjects'] = '';
            $header['OrgCust'] = '';
            $header['FrGP'] = '0,00';
            $header['FrGPPercent'] = '';
            $header['Sum0'] = '0,00';

            //todo ???
            $header['Sum1'] = $this->nrFormat($invoice->sum);//summa KM-ta
            $header['Sum2'] = '';
            $header['Sum3'] = $this->nrFormat($invoice->tax);//KM
            $header['Sum4'] = $this->nrFormat($invoice->sum);//summa KM-ga
            $header['VATNr'] = '???'; //trim($invoice['client_kmkr']);//KMK nr

            $header['ShipDeal'] = 'FOB';//lahetustingimus

            //todo ???
            $header['ShipAddr0'] = trim($invoice->userProfile->client_name);//lahetusaadress
            $header['ShipAddr1'] = trim($invoice->userProfile->address);//lahetusaadress

            $header['ShipAddr2'] = '';//lahetusaadress
            $header['ShipAddr3'] = '';//lahetusaadress
            $header['ShipMode'] = '';
            $header['Location'] = '';//ladu
            $header['PRCode'] = '';
            $header['FrSalesAcc'] = '';
            $header['Tax1Sum'] = '0';
            $header['CustVATCode'] = '1';//1-eesti, 2-EU, 3-nonEU
            $header['RebCode'] = '';
            $header['CalcFinRef'] = '';
            $header['Phone'] = '';//kliendi tel
            $header['Fax'] = '';//kliendi fax
            $header['IntCode'] = '0';
            $header['ARonTR'] = '1';
            $header['CustOrdNr'] = '';
            $header['ExportedFlag'] = '0';

            //todo ???
            $header['BaseSum4'] = $this->nrFormat($invoice->sum);//summa KM-ga

            $header['FrRate'] = '1';
            $header['ToRateB2'] = '';
            $header['BaseRate1'] = '1'; //(intval(date('Y', strtotime($invoice[out_date]))) < 2011 ? $rates_2010[$header['CurncyCode']] : $rates_2011[$header['CurncyCode']]);//baasvaluutade 1 ja 2 suhe, pre 2011
            $header['BaseRate2'] = '1';#pre 2011 EEK, hilisemad EUR,
            $header['InvoiceNr'] = '';
            $header['DiscPerc'] = '';
            $header['DiscSum'] = '';
            $header['TotGP'] = '';//arve myygikate kokku
            $header['LocOKNr'] = '';
            $header['Invalid'] = '0';
            $header['CreditCard'] = '';
            $header['AuthorizationCode'] = '';
            $header['RecValue'] = '';
            $header['RetValue'] = '';
            $header['FromBUQT'] = '0';
            $header['Sorting'] = '';
            $header['NoInterestFlag'] = '0';
            $header['NoRemndrFlag'] = '0';
            $header['SVONr'] = '';
            $header['InstallmentInv'] = '0';
            $header['OfficialSerNr'] = '';
            $header['LegalInvNr'] = '';
            $header['TotQty'] = trim($quantity);//toodete arv, xlsis 3 ??
            $header['TotWeight'] = '0';
            $header['TotVolume'] = '0';
            $header['Commision'] = '0';

            //todo
            $header['SumIncCom'] = $this->nrFormat($invoice->sum);//summa KM-ga

            $header['InvAddr3'] = '';//riik
            $header['InvAddr4'] = '';
            $header['DelAddr3'] = '';//riik
            $header['DelAddr4'] = '';
            $header['DelAddrCode'] = '';
            $header['AutoGiro'] = '0';
            $header['SalesGroup'] = 'TLN';//osakond
            $header['DisputedFlag'] = '0';
            $header['NoColectionFlag'] = '0';
            $header['QTNr'] = '';
            $header['FiscalFlag'] = '0';
            $header['JobNr'] = '';
            $header['RetnValue'] = '';
            $header['MachineName'] = '1';
            $header['TransTime'] = '00:00:00';//transp kellaaeg ??
            $header['DrawerCode'] = '';
            $header['Site'] = '';
            $header['colnr'] = '0';
            $header['StatVal'] = '';
            $header['EInvFunc'] = '';
            $header['EInvExpFlag'] = '';
            $header['EInvExpDate'] = '';
            $header['EInvExpQty'] = '';
            $header['ServiceDelDate'] = '';


            $content .= $this->hansaExport($header, $items);



        }


        return $content;
    }


    #hansaworld sbe export, parsimine allpool, arvete nimekirja koostamise juures
    protected function hansaExport($invoice, $items)
    {
        $str = '';
        #write invoice rows
        if (is_array($invoice)) {
            $str .= $this->toCsv($invoice, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
            if (is_array($items)) {
                foreach ($items as $item) {
                    #utf8_decode($col);
                    $str .= $this->toCsv($item, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
                }
            }
            $str .= $this->toCsv(array(), chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
        }
        return $str;
    }

    private function toCsv($fields = array(), $delimiter = ',', $enclosure = '"')
    {
        $str = '';
        $escape_char = '\\';
        foreach ($fields as $value) {
            if (strpos($value, $delimiter) !== false ||
                strpos($value, $enclosure) !== false ||
                strpos($value, "\n") !== false ||
                strpos($value, "\r") !== false ||
                strpos($value, "\t") !== false ||
                strpos($value, ' ') !== false) {
                $str2 = $enclosure;
                $escaped = 0;
                $len = strlen($value);
                for ($i = 0; $i < $len; $i++) {
                    if ($value[$i] == $escape_char) {
                        $escaped = 1;
                    } else if (!$escaped && $value[$i] == $enclosure) {
                        $str2 .= $enclosure;
                    } else {
                        $escaped = 0;
                    }
                    $str2 .= $value[$i];
                }
                $str2 .= $enclosure;
                $str .= $str2 . $delimiter;
            } else {
                $str .= $value . $delimiter;
            }
        }
        $str = substr($str, 0, -1);
        $str .= "\n";
        return $str;
    }

    private function replace_newline($string)
    {
        return (string)str_replace(array("\r", "\r\n", "\n"), '', $string);
    }

    /**
     * @param $value integer
     * @param string $space
     * @return string
     */
    private function nrFormat($value, $space = '')
    {
        return number_format($value, 2, ',', $space);
    }

}