<?php

namespace app\theme\components\restClient\entity;

class UpdateLicense extends CreateLicense
{
    protected static $command = 'update_license';
}
