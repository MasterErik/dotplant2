<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\response\Response;


class SearchLicense extends CreateLicense
{
    protected static $command = 'search_license';
}