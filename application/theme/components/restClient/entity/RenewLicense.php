<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\response\Response;


class RenewLicense extends CreateLicense
{
    protected static $command = 'renew_license';
}