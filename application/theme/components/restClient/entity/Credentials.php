<?php

namespace app\theme\components\restClient\entity;

class Credentials extends RestModel
{
    public $Username;
    public $Password;
    public $EPLI;
    public $LicenseKey;
    public $PublicLicenseKey;
    public $CommonTag;

}
