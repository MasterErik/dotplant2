<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 30.3.2016
 * Time: 14:24
 */

namespace app\theme\components\restClient\entity;


class LicenseByEpli  extends CreateLicense
{
    protected static $command = 'get_license_details_by_epli';
}