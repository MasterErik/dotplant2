<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\response\Response;


class DowngradeLicense extends CreateLicense
{
    protected static $command = 'upgrade_license';
}