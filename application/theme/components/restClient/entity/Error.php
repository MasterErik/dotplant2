<?php

namespace app\theme\components\restClient\entity;

class Error extends RestModel
{
    public $ErrorId;

    public $Message;

    public $ModelState;

    public function isError()
    {
        return true;
    }

    public function toString()
    {
        $msg = $this->Message;
        $a = [];
        if (isset($this->ModelState)) {
            foreach ($this->ModelState as $key => $value) {
                $a[] = $key . ': ' . implode(', ', $value);
            }
        }
        if ($a) {
            $msg .= ': ' . implode('; ', $a);
        }

        return $msg;
    }

}
