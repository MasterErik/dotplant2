<?php

namespace app\theme\components\restClient\entity;


class PageInfo
{
    public $pageNumber;

    public $pageSize;

    public $totalRows;

    public $totalPages;
}
