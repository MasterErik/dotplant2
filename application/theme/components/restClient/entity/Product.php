<?php

namespace app\theme\components\restClient\entity;

class Product extends RestModel
{
    protected static $uri = '/products';

    public $code;

    public $name;

    public $productType;
}