<?php

namespace app\theme\components\restClient\entity;

class UpgradePrice extends Price
{
    protected static $command = 'get_product_price_for_upgrade';
}
