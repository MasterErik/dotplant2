<?php

namespace app\theme\components\restClient\entity;

class Price extends RestModel
{
    protected static $command = 'get_product_price';

    public $DistributorPrice;

    public $SystemPrice;

    public $Currency;

    public $DiscountCode;
}
