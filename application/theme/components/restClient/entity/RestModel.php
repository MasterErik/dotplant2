<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\query\Query;
use app\theme\components\restClient\response\Response;
use app\theme\components\restClient\restClient;
use app\theme\components\restClient\RestDescriptionApi;
use yii\base\Model;

//use ekstazi\crud\helpers\Model;

abstract class RestModel extends Model
{
    protected static $command;

    /**
     * @param array $params
     * @param null $user_id
     * @return RestModel | Error
     */
    public static function one($params = [], $user_id = null)
    {
        return static::query($params, $user_id)
            ->setResponse(Response::entity(get_called_class()))
            ->run();
    }

    /**
     * @param array $params
     * @param null $user_id
     * @return RestModel[] | Error
     */
    public static function all($params = [], $user_id = null)
    {
        return static::query($params, $user_id)
            ->setResponse(Response::collection(Response::entity(get_called_class())))
            ->run();
    }

    /**
     * @param array $params
     * @param null $user_id
     * @return Query
     */
    public static function query($params = [], $user_id = null)
    {
        return static::restClient()
            ->createQuery(static::$command, $user_id)
            ->setParams($params);
    }

    /**
     * @return restClient
     */
    public static function restClient()
    {
        return \Yii::$app->restClient;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return false;
    }

    public static function getParams()
    {
        $api = RestDescriptionApi::$methodArray[static::$command];
        return array_keys($api['fieldlist']);
    }
}
