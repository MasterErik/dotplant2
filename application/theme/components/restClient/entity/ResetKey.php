<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 23.3.2016
 * Time: 15:17
 */

namespace app\theme\components\restClient\entity;


use app\theme\components\restClient\response\Response;

class ResetKey extends RestModel
{
    protected static $command = 'reset_license_keys';

    /* @var Credentials $Credentials */
    public $Credentials;

    public $Username;
    public $Password;
    public $EPLI;
    public $LicenseKey;
    public $PublicLicenseKey;
    public $CommonTag;
    /**
     * @param array $params
     * @param null $user_id
     * @return CreateLicense[]
     */
    public static function all($params = [], $user_id = null)
    {
        $result = static::query($params, $user_id)
            ->setResponse(
                Response::collection(Response::entity(Credentials::className()))
            )
            ->run();

        if (static::restClient()->isError()) {
            return $result;
        }

        return $result;
    }

    /**
     * @param array $params
     * @param null $user_id
     * @return CreateLicense
     */
    public static function one($params = [], $user_id = null)
    {
        $result = self::all($params, $user_id);

        if (is_array($result)) {
            $result = $result[0];
        }

        return $result;
    }


}