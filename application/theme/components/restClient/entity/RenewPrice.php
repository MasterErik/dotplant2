<?php

namespace app\theme\components\restClient\entity;

class RenewPrice extends Price
{
    protected static $command = 'get_product_price_for_renewals';
}
