<?php

namespace app\theme\components\restClient\entity;

class LicenseInfo extends RestModel
{
    public $Product;

    public $Quantity;

    public $PurchaseType;

    public $ExpirationDate;

    public $State;

    public $PublicResellerId;

    public $PartnerId;

    public $Note;

    public $ChannelId;

    public $BatchBased;

    public $UpdateTypeId;

    public $DealCode;

    public $ActivationToken;

}
