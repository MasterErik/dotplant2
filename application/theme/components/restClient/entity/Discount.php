<?php

namespace app\theme\components\restClient\entity;


class Discount extends RestModel
{
    public $code;

    public $name;

    public $discountPercentage;

    public $note;

    public $state;
}
