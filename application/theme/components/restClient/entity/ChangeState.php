<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 23.3.2016
 * Time: 13:52
 */

namespace app\theme\components\restClient\entity;


class ChangeState extends RestModel
{
    protected static $command = 'change_license_state';

    public $state;

    public $note;
}