<?php

namespace app\theme\components\restClient\entity;


use app\theme\components\restClient\Response;

class Customer extends RestModel
{
    public $Name;

    public $Email;

    public $CountryId;

    public $Company;

    public $SuppressEmail;

    public $PartnerId;
}
