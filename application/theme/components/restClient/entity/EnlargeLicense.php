<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\response\Response;


class EnlargeLicense extends CreateLicense
{
    protected static $command = 'enlarge_license';
}