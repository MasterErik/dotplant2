<?php

namespace app\theme\components\restClient\entity;

use app\theme\components\restClient\response\Response;


class CreateLicense extends RestModel
{
    protected static $command = 'create_license';

    /* @var Customer $Customer */
    public $Customer;

    /* @var Credentials $Credentials */
    public $Credentials;

    /* @var LicenseInfo $LicenseInfo */
    public $LicenseInfo;

    /* @var Price $Price */
    public $Price;

    public $BundleProduct;

    /* @var ParentLicense $ParentLicense */
    public $ParentLicense;

    public $ProductInfo;

    /* @var SystemInfo $SystemInfo */
    public $SystemInfo;

    public $AuxilliaryParams;

    public $LicenseFiles;

    public $LicenseAdministrator;

    /**
     * @param array $params
     * @param null $user_id
     * @return CreateLicense[]
     */
    public static function all($params = [], $user_id = null)
    {
        $result = static::query($params, $user_id)
            ->setResponse(
                Response::composite([
                    'License' => Response::collection(Response::entity(CreateLicense::className())),
                    'Version' => Response::entity(Version::className())
                ])
            )
            ->run();

        if (static::restClient()->isError()) {
            return $result;
        }

        return $result->License;
    }

    /**
     * @param array $params
     * @param null $user_id
     * @return CreateLicense
     */
    public static function one($params = [], $user_id = null)
    {
        $result = self::all($params, $user_id);

        if (is_array($result)) {
            $result = $result[0];
        }

        return $result;
    }
}