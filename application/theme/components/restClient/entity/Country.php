<?php

namespace app\theme\components\restClient\entity;

class Country extends RestModel
{
    public $id;

    public $name;

    public $alpha2Code;

    public $alpha3Code;
}
