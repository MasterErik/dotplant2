<?php

namespace app\theme\components\restClient\entity;

class SystemInfo extends RestModel
{
    public $CreatedDate;

    public $CreatedUserId;

    public $CanceledDate;

    public $CanceledUserId;

    public $ModifiedDate;

    public $ModifiedUserId;
}
