<?php

namespace app\theme\components\restClient\request;


class PostRequest extends Request
{
    protected function prepare($curl, $params)
    {
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
    }

} 