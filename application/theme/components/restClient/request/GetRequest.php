<?php

namespace app\theme\components\restClient\request;


class GetRequest extends Request
{
    protected function prepare($curl, $params)
    {
        curl_setopt($curl, CURLOPT_HTTPGET, true);
    }

    /**
     * @param string $uri
     * @param array $params
     * @return string
     */
    protected function prepareUrl($uri, &$params)
    {
        //$url = parent::prepareUrl($uri, $params);
        $filter = function ($item) {
            return !empty($item);
        };

        if (!empty($params)) {
            $params = (strpos($uri, '?') ? '&' : '?') . http_build_query(array_filter($params, $filter));
        } else {
            $params = '';
        }

        return $this->client->getConfig()->Url . $uri . $params;

    }
}
