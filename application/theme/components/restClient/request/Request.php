<?php

namespace app\theme\components\restClient\request;

use app\theme\components\restClient\RestClient;
use yii\base\ErrorException;
use yii\base\InvalidCallException;

abstract class Request
{
    public static $messages = [];

    public $api;
    public $client;

    /**
     * @param RestClient $client
     * @param array $api
     */
    public function __construct(RestClient $client, $api)
    {
        $this->client = $client;
        $this->api = $api;
    }

//    private static function convertMethodType($type)
//    {
//        $methods = Array(
//            'put' => CURLOPT_PUT,
//            'post' => CURLOPT_POST,
//            'get' => CURLOPT_HTTPGET,
//            'delete' => CURLOPT_RETURNTRANSFER
//        );
//
//        return $methods[$type];
//    }


    /**
     * @return array
     */
    private function genHeader()
    {
        $curl_header = array();
        foreach ($this->client->headers as $key => $value) {
            array_push($curl_header, $key . ': ' . $value);
        }

        return $curl_header;
    }

    /**
     * @param string $uri
     * @return resource
     */
    protected function initCurl($uri)
    {
        $header = $this->genHeader();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, YII_ENV ? 20 : 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Yii Framework 2');
        //curl_setopt($ch, self::convertMethodType($this->api["method"]), true);

        return $ch;
    }

    /**
     * @param string $uri
     * @param array $params
     * @return string
     */
    protected function prepareUrl($uri, &$params)
    {
        foreach ($params as $key => $param) {
            if (in_array(gettype($params[$key]), ["string", "integer", "boolean", "double"])) {

                if( strpos($uri, '{' . $key . '}') !== false ) {
                    $uri = str_replace('{' . $key . '}', urlencode($params[$key]), $uri);
                    unset($params[$key]);
                }
            }
        }

        return $this->client->getConfig()->Url . $uri;
    }

    public function process($params)
    {
        $uri = $this->api["uri"];
        $url = $this->prepareUrl($uri, $params);
  /*
        if (YII_DEBUG) {
            \Yii::info("url:\n" . $url, 'eset');
        }
  */
        $curl = $this->initCurl($url);
        $this->prepare($curl, $params);

        return $this->execute($curl);
    }

    /**
     * @param resource $curl
     * @param array $params
     * @return Request
     */
    abstract protected function prepare($curl, $params);

    protected function execute($curl)
    {
        $result = curl_exec($curl);
/*
        if (YII_DEBUG) {
            \Yii::info("curl result:\n" . $result, 'eset');
            \Yii::info("curl getinfo:\n" . print_r(curl_getinfo($curl),1), 'eset');
        }
*/
        $this->client->code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if (empty($this->client->code)) {
            $this->client->code = curl_errno($curl);
            $this->client->error = curl_error($curl);
        } elseif ($this->client->code >= 400) {
            $this->client->error = $result;
        }

        curl_close($curl);
        if (!empty($this->client->error)) {
            throw new ErrorException($this->client->error);
        }

        return $result;
    }

    /**
     * @param RestClient $client
     * @param array $api
     * @return Request
     */
    public static function ident(RestClient $client, $api)
    {
        switch (strtolower($api["method"])) {
            case 'get':
                return self::get($client, $api);
            case 'post':
                return self::post($client, $api);
            case 'put':
                return self::put($client, $api);
            case 'delete':
                return self::delete($client, $api);
            default:
                throw new InvalidCallException('Unknown request type');
        }

    }

    /**
     * @param RestClient $client
     * @param array $api
     * @return Request
     */
    public static function get(RestClient $client, $api)
    {
        return new GetRequest($client, $api);
    }

    /**
     * @param RestClient $client
     * @param array $api
     * @return Request
     */
    public static function post(RestClient $client, $api)
    {
        return new PostRequest($client, $api);
    }

    /**
     * @param RestClient $client
     * @param array $api
     * @return Request
     */
    public static function put(RestClient $client, $api)
    {
        return new PutRequest($client, $api);
    }

    /**
     * @param RestClient $client
     * @param array $api
     * @return Request
     */
    public static function delete(RestClient $client, $api)
    {
        return new DeleteRequest($client, $api);
    }

}
