<?php

namespace app\theme\components\restClient\request;

class PutRequest extends Request
{
    protected function prepare($curl, $params)
    {
        $params = json_encode($params);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
    }
}
