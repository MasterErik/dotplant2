<?php

namespace app\theme\components\restClient\request;


class DeleteRequest extends Request
{
    protected function prepare($curl, $params)
    {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
    }

} 