<?php
namespace app\theme\components\restClient\query;

class PagedQuery extends Query
{
    protected $params = [];

    protected $_pageSize = 100;

    protected $_offset = 0;

    public function pageSize($size)
    {
        $this->_pageSize = $size;
        return $this;
    }

    public function offset($page)
    {
        $this->_offset = $page;
        return $this;
    }

    public function run()
    {
        $this->params['pageSize'] = $this->_pageSize;
        $this->params['offset'] = $this->_offset;
        return parent::run();
    }
}