<?php

namespace app\theme\components\restClient\query;

use app\theme\components\restClient\entity\Error;
use app\theme\components\restClient\response\Response;
use app\theme\components\restClient\RestClient;


class Query
{
    protected $client;

    protected $command;

    protected $params;

    protected $response;


    public function __construct(RestClient $client, $command)
    {
        $this->client = $client;
        $this->command = $command;
    }

    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    public function run()
    {
        $result = $this->client->execute($this->command, $this->params);

        if ($this->client->isError()) {
            $object = json_decode($result);

            if (is_null($object)) {
                return $result;
            } else {
                return Response::entity(Error::className())->parse($object);
            }
        } else {
            return $this->response->parse($result);
        }
    }
}
