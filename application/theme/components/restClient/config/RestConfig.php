<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 23.03.2015
 * Time: 16:02
 */

namespace app\theme\components\restClient\config;

use Yii;
use  yii\base\Object;

abstract class RestConfig extends Object
{
    const   reseller = 'reseller';
    public $id;
    public $server;


    /**
     * @return string
     */
    abstract public function getUsername();

    /**
     * @return string
     */
    abstract public function getPassword();

    /**
     * @return string
     */
    abstract public function getUrl();

    public static function setReseller()
    {
        $id = Yii::$app->request->get(self::reseller);
        if (!empty($id)) {
            Yii::$app->session->set(self::reseller, $id);
        }

    }

    public static function getReseller()
    {
        return \Yii::$app->session->get(self::reseller, Yii::$app->params['defaultResellerId']);
    }
}