<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 23.03.2015
 * Time: 14:37
 */

namespace app\theme\components\restClient\config;

class StaticConfig extends RestConfig
{
    public $partner_id;

    public static $configArray = [
        1 => [
            'dev' =>
                [
                    'Url' => "https://dextertest.eset.com/dxapidev/v2",
                    "username" => "initec_kinsigo",
                    "password" => "yaSWefR7"
                ],
            'work' =>
                [
                    'Url' => "https://dexter.eset.com/dxapi/v2",
                    "username" => "initec_kinsigo",
                    "password" => "h8fSq&9L14dcV|K"
                ]
        ],
        2 => [
            'dev' =>
                [
                    'Url' => "https://dextertest.eset.com/dxapidev/v2",
                    "username" => "initecla_kinsigo",
                    "password" => "Wr6n8RuN"
                ],
            'work' =>
                [
                    'Url' => "https://dexter.eset.com/dxapi/v2",
                    "username" => "initecla_kinsigo",
                    "password" => "Wr6n8RuN"
                ]
        ],

    ];

    public function init()
    {
        $this->id = $this->partner_id ? $this->partner_id : self::getReseller();
        if (!array_key_exists($this->id, self::$configArray))
            throw new \Exception('No rest configuration for the key: ' . $this->id);

    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return self::$configArray[$this->id][$this->server]["username"];
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return self::$configArray[$this->id][$this->server]["password"];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return self::$configArray[$this->id][$this->server]["Url"];
    }

}