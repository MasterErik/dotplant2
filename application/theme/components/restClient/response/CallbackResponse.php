<?php

namespace app\theme\components\restClient\response;

class CallbackResponse extends StandardResponse
{
    public $callable;

    public function __construct($callable)
    {
        $this->callable = $callable;
    }

    public function parse($data)
    {
        $data = parent::parse($data);
        return call_user_func($this->callable, $data);
    }
}
