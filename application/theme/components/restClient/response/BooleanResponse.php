<?php

namespace app\theme\components\restClient\response;

class BooleanResponse extends Response
{
    public function parse($data)
    {
        return true;
    }
}
