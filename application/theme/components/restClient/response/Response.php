<?php

namespace app\theme\components\restClient\response;

abstract class Response
{
    public static function entity($className)
    {
        return new EntityResponse($className);
    }

    public static function standard()
    {
        return new StandardResponse();
    }

    public static function callback($callable)
    {
        return new CallbackResponse($callable);
    }

    public static function boolean()
    {
        return new BooleanResponse();
    }

    public static function collection($itemParser)
    {
        return new CollectionResponse($itemParser);
    }

    public static function composite($parsers)
    {
        return new CompositeResponse($parsers);
    }

    abstract public function parse($data);
}
