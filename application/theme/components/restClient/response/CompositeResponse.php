<?php

namespace app\theme\components\restClient\response;

class CompositeResponse extends Response
{
    /** @var  Response[] */
    protected $parsers;

    public function __construct($parsers)
    {
        $this->parsers = $parsers;
    }

    public function parse($data)
    {
        //$data = (array) $data;
        foreach ($this->parsers as $key => $parser) {
            if( isset($data->$key) ) {
                $data->$key = $parser->parse($data->$key);
            }
        }
        return $data;
    }
}
