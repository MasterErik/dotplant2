<?php

namespace app\theme\components\restClient\response;

class StandardResponse extends Response
{
    public function parse($data)
    {
        return json_decode($data);
    }
}
