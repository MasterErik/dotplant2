<?php

namespace app\theme\components\restClient\response;

use yii\base\Model;

class EntityResponse extends Response
{
    public $className;

    public function __construct($className)
    {
        $this->className = $className;
    }

    public function parse($data)
    {
        /** @var Model $model */
        $model = new $this->className;
        $model->setAttributes((array) $data, false);
        return $model;
    }

}
