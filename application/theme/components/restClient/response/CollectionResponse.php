<?php

namespace app\theme\components\restClient\response;

class CollectionResponse extends Response
{
    /**
     * @var Response
     */
    public $parser;

    public function __construct(Response $parser)
    {
        $this->parser = $parser;
    }

    public function parse($data)
    {
        $result = [];
        foreach ($data as $item)
            $result[] = $this->parser->parse($item);

        return $result;
    }
}
