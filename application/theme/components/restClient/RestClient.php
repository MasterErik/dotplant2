<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 13.03.2015
 * Time: 14:38
 */

namespace app\theme\components\restClient;

use app\theme\components\restClient\config;
use app\theme\components\restClient\config\StaticConfig;
use app\theme\components\restClient\query\Query;
use app\theme\components\restClient\request\Request;
use app\theme\components\restClient\response\Response;
use app\theme\models\UserProfile;
use yii\base\Component;

class RestClient extends Component
{

    public $headers = [
        'Content-Type' => 'application/json',
    ];

    public $rest_object;
    public $code;
    public $error;

    const token_name = 'eset.token.';
    const token_wait = 3600; //120;
    const token_save = 43200; //86400;

    private $config;

    public $configuration = ['class' => 'app\theme\components\restClient\StaticConfig'];

    private function getToken()
    {
        return self::token_name . md5($this->getConfig()->id . $this->getConfig()->server);
    }

    public function createQuery($uri, $user_id = null)
    {
        if ($user_id) {
            $this->configuration['partner_id'] = UserProfile::getParentPartner($user_id)->user_id;
        }
        return new Query($this, $uri);
    }

    /**
     * @return StaticConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    private function createConfig($user_id = null)
    {
        if ($user_id) {
            $this->configuration['partner_id'] = UserProfile::getParentPartner($user_id)->user_id;
        }
        $this->config = new StaticConfig($this->configuration);
    }

    public function execute($method, $params, $user_id = null)
    {
        $this->createConfig($user_id);

        try {
            $this->login();
            $this->rest_object = $this->internal_exec($method, $params);
            return $this->rest_object;
        } catch (\Exception $e) {
            \Yii::error('api:' . $method . ' error:' . $e->getMessage(), 'dexter');
            if ($this->code == 401) {   //$error 401 Unauthorized
                $this->logout();
            }
            return $e->getMessage();
        }
    }

    /**
     * @param string $method
     * @param array $params
     * @return Response
     * @throws \Exception
     */
    private function internal_exec($method, $params)
    {
        $this->code = null;
        $this->error = null;

        $api = RestDescriptionApi::$methodArray[$method];
        if (empty($api))
            throw new \Exception('Not found rest api function: ' . $method);

        $result = Request::ident($this, $api)->process($params);
        return Response::standard()->parse($result);
    }

//    private function logout()
//    {
//        try {
//            if (\Yii::$app->cache->exists($this->getToken())) {
//                $this->internal_exec('logout', ['token' => \Yii::$app->cache->get($this->getToken())["token"]]);
//                \Yii::$app->cache->delete($this->getToken());
//            }
//            $this->headers['Authorization'] = '';
//
//        } catch (\Exception $e) {
//            \Yii::$app->cache->delete($this->getToken());
//        }
//    }
//
//    private function login()
//    {
//        try {
//            if (\Yii::$app->cache->exists($this->getToken())) {
//                if (time() > \Yii::$app->cache->get($this->getToken())["time"] + self::token_wait)
//                    $this->logout();
//            }
//
//            if (!\Yii::$app->cache->exists($this->getToken())) {
//                $result = $this->internal_exec('login', ["username" => $this->getConfig()->username, "password" => $this->getConfig()->password]);
//                if (YII_DEBUG) \Yii::info('rest login: ' . print_r($result, 1), 'eset');
//                \Yii::$app->cache->set($this->getToken(), ["token" => $result->token, "time" => time()], self::token_save);
//            }
//
//            $this->headers['Authorization'] = \Yii::$app->cache->get($this->getToken())["token"];
//
//            if (empty($this->headers['Authorization'])) {
//                throw new \Exception('token empty.');
//            }
//
//        } catch (\Exception $e) {
//            throw new \Exception('Cannot authorize on rest server. ' . $e->getMessage());
//        }
//    }


    private function getLoginFilename()
    {
        return \Yii::$app->runtimePath . '/login/' . $this->getToken();
    }

    private function isLogin()
    {
        return file_exists($this->getLoginFilename());
    }

    private function isExpiredLogin()
    {
        return time() > filemtime($this->getLoginFilename()) + self::token_wait;
    }

    private function getLoginToken()
    {
        return file_get_contents($this->getLoginFilename());
    }

    private function setLoginToken($token)
    {
        $dir = \Yii::$app->runtimePath . '/login';
        if( !file_exists($dir)) {
            mkdir($dir, 0770);
        }
        return file_put_contents($this->getLoginFilename(), $token);
    }

    private function logout()
    {
        try {
            if ($this->isLogin()) {
                $this->internal_exec('logout', ['token' => $this->getLoginToken()]);
                @unlink($this->getLoginFilename());
            }
            $this->headers['Authorization'] = '';
        } catch (\Exception $e) {
            @unlink($this->getLoginFilename());
        }
    }

    private function login()
    {
        if( $this->isLogin() && !$this->isExpiredLogin() ) {
            $this->headers['Authorization'] = $this->getLoginToken();
            return;
        }


        try {
            //if( $this->isLogin() && $this->isExpiredLogin() ) {
                $this->logout();
            //}

            $result = $this->internal_exec('login', ["username" => $this->getConfig()->username, "password" => $this->getConfig()->password]);
            //if (YII_DEBUG) \Yii::info('rest login: ' . print_r($result, 1), 'dexter');
            $this->setLoginToken($result->token);

            $this->headers['Authorization'] = $result->token;

            if (empty($this->headers['Authorization'])) {
                throw new \Exception('token empty.');
            }

        } catch (\Exception $e) {
            throw new \Exception('Cannot authorize on rest server. ' . $e->getMessage());
        }
    }

    public function isError()
    {
        return $this->code < 100 || $this->code >= 400;
    }
}