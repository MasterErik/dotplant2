<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 13.03.2015
 * Time: 16:20
 */

namespace app\theme\components\restClient;

use app\theme\components\restClient\entity\Price;

//sections={parentlicense,systeminfo,auxilliaryparams,productinfo,licensefiles,ela}

class RestDescriptionApi
{
    public static $methodArray = [
        "login" => ["uri" => "/security/token", "method" => "post", "fieldlist" => [
            "Username" => "",
            "Password" => ""
        ]],
        "logout" => ["uri" => "/security/token/{token}", "method" => "delete", "fieldlist" => [
            "token" => ""
        ]],

        "create_license" => ["uri" => "/licenses?sections=productinfo", "method" => "post", "fieldlist" => [
            "Customer" => ["Name" => "", "Company" => "", "Email" => "", "CountryId" => ""],
            "Price" => ["DistributorPrice" => ""],
            "Quantity" => "",
            "PurchaseType" => "",
            "Product" => "",
            "ExpirationDate" => "", //3/4/2015
            "Note" => "",
            "PublicResellerId" => "",
            "Sector" => "",
            "LicenseType" => "subscription",
            "ProxylocationId" => "1",
            "ChannelId" => "7",
            "SuppressEmail" => false,
            "PartnerId" => ""
        ]],

        "renew_license" => ["uri" => "/licenses/{epli}/renewals?resetup={resetup}&resetlicensekeys={resetlicensekeys}&sections=productinfo,systeminfo,parentlicense",
            "method" => "post", "fieldlist" => [
                "Customer" => ["Name" => "", "Company" => "", "Email" => "", "CountryId" => ""],
                "Price" => ["DistributorPrice" => ""],
                "ExpirationDate" => "",
                "Note" => "",
                "PublicResellerId" => "",
                "AdditionalParam" => "",
                "ChannelId" => "",
                "SuppressEmail" => false,
                "PartnerId" => ""
            ]],
        "enlarge_license" => ["uri" => "/licenses/{epli}/enlargements?resetup={resetup}&resetlicensekeys={resetlicensekeys}&sections=productinfo,systeminfo,parentlicense", "method" => "post", "fieldlist" => [
            "Customer" => ["Name" => "", "Company" => "", "Email" => "", "CountryId" => ""],
            "Price" => ["DistributorPrice" => ""],
            "Quantity" => "",
            "ExpirationDate" => "",
            "Note" => "",
            "PublicResellerId" => "",
            "AdditionalParam" => "",
            "ChannelId" => "",
            "SuppressEmail" => false,
            "PartnerId" => ""
        ]],
        "upgrade_license" => ["uri" => "/licenses/{epli}/upgrades?resetup={resetup}&resetlicensekeys={resetlicensekeys}&sections=productinfo,systeminfo,parentlicense", "method" => "post", "fieldlist" => [
            "Customer" => ["Name" => "", "Company" => "", "Email" => "", "CountryId" => ""],
            "Price" => ["DistributorPrice" => ""],
            "Quantity" => "",
            "ExpirationDate" => "",
            "Note" => "",
            "PublicResellerId" => "",
            "ChannelId" => "",
            "SuppressEmail" => false,
            "PartnerId" => ""
        ]],
        "downgrade_license" => ["uri" => "/licenses/{epli}/downgrades?resetup={resetup}&resetlicensekeys={resetlicensekeys}&sections=productinfo,systeminfo,parentlicense", "method" => "post", "fieldlist" => [
            "Customer" => ["Name" => "", "Company" => "", "Email" => "", "CountryId" => ""],
            "Price" => ["DistributorPrice" => ""],
            "Quantity" => "",
            "ExpirationDate" => "",
            "Note" => "",
            "PublicResellerId" => "",
            "ChannelId" => "",
            "SuppressEmail" => false,
            "PartnerId" => ""
        ]],

        "search_license" => ["uri" => "/licenses?sections=productinfo,systeminfo,parentlicense", "method" => "get", "fieldlist" => [
            'username' => "",
            'password' => "",
            'licensekey' => "",
            'customeremail' => "",
            'publiclicensekey' => "",
            'sections' => "",
            'commontag' => ""
        ]],
        "get_license_details_by_epli" => ["uri" => "/licenses/{epli}?sections=productinfo,systeminfo,parentlicense", "method" => "get", "fieldlist" => [
            'epli' => "",
            'sections' => ""
        ]],
        "get_license_credentials" => ["uri" => "/licenses/{epli}/credentials", "method" => "get", "fieldlist" => [
            'epli' => "",
        ]],
        "get_child_license" => ["uri" => "/licenses/{epli}/child", "method" => "get", "fieldlist" => [
            'epli' => "{epli}",
            'sections' => ""
        ]],
        "get_expiring_licenses" => ["uri" => "/licenses/expiring", "method" => "get", "fieldlist" => [
            'expirationdatefrom' => "",
            'expirationdateto' => "",
            'countryid' => "",
            'purchasetype' => "",
            'product' => "",
            'offset' => "",
            'limit' => "",
            'sections' => ""
        ]],
        "get_license_files" => ["uri" => "/licenses/{epli}/licensefiles?code={code}", "method" => "get", "model" => Price::class,
            "fieldlist" => ['epli' => "", "code" => ""]
        ],
        "generate_license_files" => ["uri" => "/licenses/{epli}/licensefiles", "method" => "put", "fieldlist" => ['epli' => ""]],
        "send_distributor_email" => ["uri" => "/licenses/{epli}/notifications/distributoremail", "method" => "put", "fieldlist" => ['epli' => ""]],
        "send_customer_email"   => ["uri" => "/licenses/{epli}/notifications/customeremail", "method" => "put", "fieldlist" => ['epli' => ""]],
        "reset_license_user"    => ["uri" => "/licenses/{epli}/credentials/up", "method" => "put", "fieldlist" => ['epli' => ""]],
        "reset_license_password" => ["uri" => "/licenses/{epli}/credentials/password", "method" => "put", "fieldlist" => [
            'epli' => "",
            'suppressemail' => false,
        ]],
        "reset_license_keys"    => ["uri" => "/licenses/{epli}/credentials/licensekeys", "method" => "put", "fieldlist" => [
            'epli' => "",
//            'disconnectseats' => 0,
            'suppressemail' => false,
        ]],
        "reset_all_credentials" => ["uri" => "/licenses/{epli}/credentials/", "method" => "put", "fieldlist" => ['epli' => ""]],
        "change_license_state" => ["uri" => "/licenses/{epli}/state", "method" => "put", "fieldlist" => [
            'epli' => "",
            'state' => "",
        ]],
        "get_licenses_status" => ["uri" => "/licenses/{epli}/state", "method" => "get", "fieldlist" => ['epli' => ""]],
        "purchase_license" => ["uri" => "/licenses/{epli}/purchases", "method" => "post", "fieldlist" => [
            'epli' => "",
            "Quantity" => "",
            "Product" => "",
            "Price" => [
                "DistributorPrice" => ""
            ]
        ]],
        "get_license_purchase" => ["uri" => "/licenses/{epli}/purchases", "method" => "get", "fieldlist" => [""]],
        "get_product_price" => ["uri" => "/licenses/price", "method" => "get", "fieldlist" => [
            'epli' => "",
            'product' => "",
            'expirationdate' => "",
            'quantity' => "",
            'country' => "",
            'partnerid' => "",
            'sector' => "",
            'licensetype' => ""
        ]],
        "get_product_price_for_renewals" => ["uri" => "/licenses/{epli}/renewals/price", "method" => "get", "fieldlist" => [
            'epli' => "",
            'expirationdate' => "",
            'country' => "",
            'partnerid' => "",
            'sector' => "",
            'licensetype' => ""
        ]],
        "get_product_price_for_enlarges" => ["uri" => "/licenses/{epli}/enlargements/price", "method" => "get", "fieldlist" => [
            'epli' => "",
            'expirationdate' => "",
            'country' => "",
            'quantity' => "",
            'partnerid' => "",
            'sector' => "",
            'licensetype' => ""
        ]],
        "get_product_price_for_upgrade" => ["uri" => "/licenses/{epli}/upgrades/price", "method" => "get", "fieldlist" => [
            'epli' => "",
            'expirationdate' => "",
            'country' => "",
            'quantity' => "",
            'product' => "",
            'partnerid' => "",
            'sector' => "",
            'licensetype' => ""
        ]],

        "get_register_key_header" => ["uri" => "/registerkeys/{batchCode}", "method" => "get", "fieldlist" => [
            "batchCode" => ""
        ]],
        "get_register_key_stack" => ["uri" => "/registerkeys/{batchcode}/keys", "method" => "get", "fieldlist" => [
            "batchcode" => "",
            'offset' => 1,
            'limit' => 10
        ]],
        "change_register_key_state" => ["uri" => "/registerkeys/{registerkey}/state", "method" => "put", "fieldlist" => [
            'registerkey' => "",
            'State' => ""
        ]],
        "get_register_key_state" => ["uri" => "/registerkeys/{registerkey}/state", "method" => "get", "fieldlist" => [
            'registerkey' => "",
        ]],
        "get_products" => ["uri" => "/products", "method" => "get", "fieldlist" => []],
        "get_discounts" => ["uri" => "/discounts", "method" => "get", "fieldlist" => []],
        "get_purchse_types" => ["uri" => "/purchasetypes", "method" => "get", "fieldlist" => []],
        "get_countries" => ["uri" => "/countries", "method" => "get", "fieldlist" => []],
        "get_partners" => ["uri" => "/partners", "method" => "get", "fieldlist" => []],
        "get_resellers" => ["uri" => "/resellers", "method" => "get", "fieldlist" => []],
        "get_errors" => ["uri" => "/errors", "method" => "get", "fieldlist" => []],

        "update_license" => [
            "uri" => "/licenses/{epli}",
            "method" => "put",
            "fieldlist" => [
                "epli" => '',
                "Customer" => [
                    "Name" => null,
                    "Company" => null,
                    "Email" => null,
                    "CountryId" => null
                ],
                "Price" => [
                    "DistributorPrice" => null,
                    "DiscountCode" => null,
                    "Discount" => null
                ],
                "Quantity" => null,
                "ExpirationDate" => null,
                "Note" => null,
                "PublicResellerId" => null,
                "ChannelId" => null,
                "DealCode" => null,
                "PartnerId" => null,
                "BundleProduct" => [
                    "Price" => [
                        "DistributorPrice" => null,
                        "DiscountCode" => null,
                        "Discount" => null
                    ],
                    "Quantity" => null
                ],
                "LicenseAdministrator" => [
                    "Email" => "orders-ee@eset.ee"
                ],
                "Sector" => null,
                "LicenseType" => null
            ]
        ],

    ];

}