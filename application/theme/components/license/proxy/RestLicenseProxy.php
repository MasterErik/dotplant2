<?php

namespace app\theme\components\license\proxy;


use app\theme\components\restClient\entity\CreateLicense;
use app\theme\models\Country;
use app\theme\models\Product;
use app\theme\models\User;

/**
 * @property CreateLicense $owner
 *
 * @property integer $id
 * @property integer $userId
 * @property Product $product
 * @property integer $productCode
 * @property Country $country
 * @property integer $countryCode
 * @property string $sector
 * @property integer $quantity
 * @property integer $period
 * @property string $purchaseType
 * @property integer $discountCode
 * @property float $discount
 * @property string $discountDocument
 * @property string $note
 * @property float $price
 * @property string $beginDate
 * @property string $endDate
 * @property string $state
 * @property string $username
 * @property string $password
 * @property string $epli
 * @property string $licenseKey
 * @property CreateLicense $parentLicense
 * @property string $parentLicenseEpli
 *
 * @property string $commonTag
 * @property string $publicLicenseKey
 *
 *
 * Class RestLicenseProxy
 * @package app\theme\components\license\proxy
 */
class RestLicenseProxy extends ProxyAbstract
{
	private $_product = false;
	private $_user = false;
	private $_country = false;
    private $_additionalParams = false;


	public function getFields()
    {
        return [
            'user_id',
            'product_code',
            'country_code',
            'quantity',
            'purchase_type',
            'register_state',
            'state',
            //'license_type_global' => ,
            //'license_type' => ,
            'sector',
            'bdate',
            'edate',
            'period',
            'discount_code',
            'username',
            'password',
            'epli',
            'license_key',
            'note',
            'common_tag',
            'public_license_key',
        ];
    }
    public function getProduct()
	{
		if ($this->_product === false) {
			$this->_product = Product::findOne(['eset_product_code'=>$this->productCode]);
		}
		return $this->_product;
	}

    public function getProductCode()
    {
        return $this->owner->LicenseInfo->Product;
    }

	public function getSector()
	{
		return null;
	}
    public function getRegisterState()
    {
        return \app\theme\models\License::REGISTER_STATE_REGISTERED;
    }
	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = User::findOne(['email'=>$this->owner->Customer->Email])->userProfile;
		}
		return $this->_user;
	}
    public function setUser($value)
    {
        $this->_user = $value;
    }
    public function getUserId()
    {
        return $this->user ? $this->user->id : null;
    }

	public function getCountry()
	{
		if ($this->_country === false) {
			$this->_country = Country::findOne(['code'=>$this->countryCode]);
		}
		return $this->_country;
	}

    public function getCountryCode()
    {
        return $this->owner->Customer->CountryId;
    }

	public function getQuantity()
	{
		return $this->owner->LicenseInfo->Quantity;
	}

	public function getPeriod()
	{
        $bdate = \DateTime::createFromFormat('m/d/Y H:i:s.u', $this->owner->SystemInfo->CreatedDate);
        $edate = \DateTime::createFromFormat('m/d/Y', $this->owner->LicenseInfo->ExpirationDate);
        $interval = $edate->diff($bdate);
        $years = $interval->format('%Y');
        $months = $interval->format('%m') + $years * 12;
        $days = $interval->format('%d');
        if( $days > 10 ) $months++;
		return $months;
	}

	public function getPurchaseType()
	{
		return $this->owner->LicenseInfo->PurchaseType;
	}

	public function getDiscountCode()
	{
		return $this->owner->Price->DiscountCode;
	}

	public function getDiscount()
	{
        return $this->owner->Price->SystemPrice - $this->owner->Price->DistributorPrice;
    }

    public function getDiscountDocument()
    {
        return null;
    }

    public function getNote()
    {
        return $this->owner->LicenseInfo->Note;
    }

	public function getPrice()
	{
        return $this->owner->Price->DistributorPrice;
	}

    public function getBeginDate()
    {
        $date = \DateTime::createFromFormat('m/d/Y H:i:s.u', $this->owner->SystemInfo->CreatedDate);
        return $date->format('Y-m-d 00:00:00');
    }
    public function getBdate()
    {
        return $this->getBeginDate();
    }
    public function getEndDate()
    {
        $date = \DateTime::createFromFormat('m/d/Y', $this->owner->LicenseInfo->ExpirationDate);
        return $date->format('Y-m-d 00:00:00');
    }
    public function getEdate()
    {
        return $this->getEndDate();
    }
    public function getState()
    {
        return $this->owner->LicenseInfo->State;
    }

    public function getUsername()
    {
        return $this->owner->Credentials->Username;
    }

    public function getPassword()
    {
        return $this->owner->Credentials->Password;
    }

    public function getEpli()
    {
        return $this->owner->Credentials->EPLI;
    }

    public function getLicenseKey()
    {
        return $this->owner->Credentials->LicenseKey;
    }

    public function getPublicLicenseKey()
    {
        return $this->owner->Credentials->PublicLicenseKey;
    }


    public function getCommonTag()
    {
        return $this->owner->Credentials->CommonTag;
    }

    public function getParentLicense()
    {
        if ($this->_parent_license === false) {
            if ($this->owner->ParentLicense) {
                $this->_parent_license = CreateLicense::findOne(['epli'=>$this->owner->ParentLicense->epli]);
            } else {
                $this->_parent_license = null;
            }
        }

        return $this->_parent_license;
    }

    public function getParentLicenseEpli()
    {
        return $this->owner->ParentLicense ? $this->owner->ParentLicense->Epli : null;
    }
}