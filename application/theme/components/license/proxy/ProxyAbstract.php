<?php

namespace app\theme\components\license\proxy;

use app\theme\models\UserProfile;
use app\theme\module\helpers\StyleCase;
use yii\base\Object;

/**
 *
 * @property UserProfile $reseller
 * @property UserProfile $partner
 * @property UserProfile $user
 *
 */
abstract class ProxyAbstract extends Object implements ProxyInterface
{
    public $owner;
    private $_errors = [];
    private $_partner = false;
    private $_reseller = false;

    /*
        public function init()
        {
            if( is_null($this->owner) ) {
                throw new Exception('Owner not found');
            }
        }
    */

    public function addError($error)
    {
        $this->_errors[] = $error;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function setErrors($errors)
    {
        return $this->_errors = $errors;
    }

    public function hasErrors()
    {
        return $this->_errors ? true : false;
    }

    public function getPartner()
    {
        if ($this->_partner === false) {
            $this->_partner = UserProfile::getParentPartner($this->getReseller()->parent_id);
        }
        return $this->_partner;
    }

    public function getReseller()
    {
        if ($this->_reseller === false) {
            $this->_reseller = UserProfile::getParentReseller($this->getUser()->parent_id);
        }
        return $this->_reseller;
    }

    public function getId()
    {
        return $this->owner->id;
    }

    public function __get($name)
    {

        $method = 'get' . StyleCase::toCamelCase($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        } else {
            $name = StyleCase::toSnakeCase($name);
            $values = $this->owner->getAttributes([$name]);
            if (!empty($values)) {
                return $values[$name];
            }
            Parent::__get($name);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . StyleCase::toCamelCase($name);
        if (method_exists($this, $method)) {
            return $this->$method($value);
        } else {
            $name = StyleCase::toSnakeCase($name);
            $values = $this->owner->getAttributes([$name]);
            if (!empty($values)) {
                $this->owner->setAttribute($name, $value);
            } else {
                Parent::__set($name, $value);
            }
        }
    }

    public function getAttributes($fields = null)
    {
        if (empty($fields)) {
            $fields = $this->getFields();
        };
        $result = [];
        foreach ($fields as $field) {
            $result[$field] = $this->__get($field);
        }
        return $result;
    }

    public abstract function getFields();
}