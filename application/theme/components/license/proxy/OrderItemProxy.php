<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 30.03.15
 * Time: 13:38
 */

namespace app\theme\components\license\proxy;

use app\theme\models\DiscountCodeList;
use app\theme\models\License;
use app\theme\models\Order;
use app\theme\models\OrderItem;
use app\theme\models\Product;
use app\theme\models\UserProfile;
use app\theme\module\helpers\DateUtils;


/**
 * @property OrderItem $owner
 *
 * @property integer $userId
 * @property integer $resellerId
 * @property integer $OrderDetailId
 * @property string $productCode
 * @property string $countryCode
 * @property integer $quantity
 * @property string $purchaseType
 * @property string $registerState
 * @property string $sector
 * @property string $bdate
 * @property string $edate
 * @property integer $period
 * @property integer $packet
 * @property string $note
 * @property integer $discountCode
 * @property integer $customDiscount
 * @property string $discountDocument
 * @property License $parentLicense

 *
 * Class CartProxy
 * @package app\theme\components\license\proxy
 */
class OrderItemProxy extends ProxyAbstract
{
    /**
     * @var Order
     */
    public $owner_order;
    private $_product = false;
    private $_reseller = false;
    private $_user = false;
    private $_country = false;
    private $_parent_license = false;

    public function getFields()
    {
        return [
            'user_id',
            'reseller_id',
            'order_detail_id',
            'product_code',
            'country_code',
            'quantity',
            'packet',
            'purchase_type',
            'register_state',
            'sector',
            'bdate',
            'edate',
            'period',
            'note',
            'discount_code',
            'custom_discount',
            'discount_document',
        ];
    }

    public function getOrderDetailId()
    {
        return $this->owner->id;
    }
    public function getProduct()
    {
        if ($this->_product === false) {
            $this->_product = Product::findOne($this->owner->product_id);
        }

        return $this->_product;
    }

    public function getProductCode()
    {
        return $this->product->eset_product_code;
    }

    public function getSector()
    {
        return $this->owner_order->eset_sector;
    }

    public function getUserId()
    {
        return $this->owner_order->user_id;
    }

    public function getReseller()
    {
        if ($this->_reseller === false) {
            $this->_reseller = UserProfile::getParentReseller($this->owner_order->manager_id);
        }

        return $this->_reseller;
    }
    public function getResellerId()
    {
        return  $this->owner_order->manager_id;
    }
    public function getRegisterState()
    {
        return License::REGISTER_STATE_UNREGISTERED;
    }
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserProfile::findOne($this->getUserId());
        }

        return $this->_user;
    }

    public function getCountry()
    {
        if ($this->_country === false) {
            $this->_country = $this->getPartner()->country;
        }

        return $this->_country;
    }

    public function getCountryCode()
    {
        return $this->country->code;
    }

    public function getQuantity()
    {
        return $this->owner->quantity;
    }

    public function getPeriod()
    {
        return $this->owner->eset_period;
    }

    public function getPurchaseType()
    {
        return $this->owner->eset_purchase_type;
    }

    public function getDiscountCode()
    {
        return $this->owner_order->eset_discount_code;
    }

    public function getCustomDiscount()
    {
        return $this->owner_order->eset_custom_discount;
    }

    public function getDiscountDocument()
    {
        return $this->discountCode == DiscountCodeList::CODE_FOR_DOCUMENT ? $this->owner_order->eset_discount_document : null;
    }

    public function getNote()
    {
        return $this->owner->eset_note;
    }

    public function getPrice()
    {
        return $this->owner->total_price;
    }

    public function getBdate()
    {
        return DateUtils::BegDate();
    }

    public function getEdate()
    {
        return DateUtils::EndDate($this->period);
    }

    public function getParentLicense()
    {
        if ($this->_parent_license === false) {
            if ($this->owner->eset_parent_license_id) {
                $this->_parent_license = License::findOne($this->owner->eset_parent_license_id);
            } else {
                $this->_parent_license = null;
            }
        }

        return $this->_parent_license;
    }

    public function getParentLicenseEpli()
    {
        return $this->parentLicense ? $this->parentLicense->epli : null;
    }

    public function setUserId($user_id)
    {
        $this->owner_order->user_id = $user_id;
    }

    public function setQuantity($quantity)
    {
        $this->owner->quantity = $quantity;
    }

    public function setPeriod($period)
    {
        $this->owner->eset_period = $period;
    }


    public function setPurchaseType($purchase_type)
    {
        $this->owner->eset_purchase_type = $purchase_type;
    }

    public function setPrice($price)
    {
        $this->owner->total_price = $price * $this->owner->packet;
    }

    public function setDiscountCode($discount_code)
    {
        return $this->owner_order->eset_discount_code = $discount_code;
    }

    public function save()
    {
        return $this->owner->save();
    }

}