<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 30.03.15
 * Time: 13:38
 */

namespace app\theme\components\license\proxy;

interface ProxyInterface
{
    public function getId();

	public function getReseller();

	public function getPartner();
}