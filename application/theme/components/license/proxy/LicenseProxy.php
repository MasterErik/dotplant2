<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 30.03.15
 * Time: 13:38
 */

namespace app\theme\components\license\proxy;

use app\theme\models\DiscountCodeList;
use app\theme\models\License;
use app\theme\models\Product;
use app\theme\models\UserProfile;
use Yii;

/**
 * @property string $username
 * @property string $password
 * @property string $epli
 * @property string $state
 * @property string $licenseKey
 * @property string $publicLicenseKey
 * @property string $commonTag

 * @property string $productCode
 * @property string $purchaseType
 * @property string $quantity
 * @property string $sector
 * @property License $parentLicense
 * @property string $countryCode
 *
 * @property License $owner
 *
 * Class LicenseProxy
 * @package app\theme\components\license\proxy
 */
class LicenseProxy extends ProxyAbstract
{
	private $_product = false;
	private $_user = false;
	private $_country = false;

    public function getFields()
    {
        return [
            'username', 'password', 'epli', 'state', 'licenseKey', 'publicLicenseKey', 'commonTag',
            'productCode', 'purchaseType', 'quantity', 'sector',
        ];
    }

    public function getProduct()
	{
		if ($this->_product === false) {
			$this->_product = Product::findOne($this->owner->orderDetail->product_id);
		}
		return $this->_product;
	}

	public function getUser()
	{
		if ($this->_user === false) {
			$this->_user = UserProfile::findOne($this->owner->user_id);
		}
		return $this->_user;
	}

	public function getCountry()
	{
		if ($this->_country === false) {
			$this->_country = $this->getPartner()->country;
		}
		return $this->_country;
	}

	public function getDiscount()
	{
        return $this->discountCode == DiscountCodeList::CODE_CUSTOM ? $this->owner->custom_discount : null;
	}

    public function getDiscountDocument()
    {
        return $this->discountCode == DiscountCodeList::CODE_FOR_DOCUMENT ? $this->owner->discount_document : null;
    }

    public function getNote()
    {
        return $this->owner->note;
    }

	public function getPrice()
	{
		return $this->owner->orderDetail ? $this->owner->orderDetail->total_price : 0;
	}
    public function getParentLicenseEpli()
    {
        return $this->parentLicense ? $this->parentLicense->epli : null;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->owner->register_state = License::REGISTER_STATE_REGISTERED;
        $res = $this->owner->save($runValidation, $attributeNames);
        if( !$res ) {
            $this->setErrors($this->owner->getErrors());
        }
        return $res;
    }

    public function getSuppressemail()
    {
        if (isset($_SESSION)) {
            return 1 === Yii::$app->session->get('dexterSuppressEmail') ? true : false;
        } else {
            return false;
        }
    }
    /**
     * @param $value \app\theme\components\restClient\entity\CreateLicense
     */
    public function setCredentials($value)
    {
        $this->username = $value->Credentials->Username;
        $this->password = $value->Credentials->Password;
        $this->epli = $value->Credentials->EPLI;
        $this->state = $value->LicenseInfo->State;
        $this->licenseKey = $value->Credentials->LicenseKey;
        $this->publicLicenseKey = $value->Credentials->PublicLicenseKey;
        $this->commonTag = $value->Credentials->CommonTag;
        //$this->owner->setAttributes($value->getAttributes($this->getFields()));
    }

}