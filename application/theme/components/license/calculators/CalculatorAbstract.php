<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 27.03.15
 * Time: 18:23
 */

namespace app\theme\components\license\calculators;

use app\theme\components\license\Collection;
use yii\base\Object;

abstract class CalculatorAbstract extends Object
{
	public $licenseCollection;

	public function __construct($config = [])
	{
		$this->licenseCollection = new Collection();
		$this->licenseCollection->setLicenses($config['licenseCollection']);
		unset($config['licenseCollection']);

		parent::__construct($config);
	}
}