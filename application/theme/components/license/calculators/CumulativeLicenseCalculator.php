<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 27.03.15
 * Time: 18:23
 */

namespace app\theme\components\license\calculators;

use app\theme\components\license\proxy\OrderItemProxy;
use app\theme\components\restClient\entity\EnlargePrice;
use app\theme\components\restClient\entity\Price;
use app\theme\components\restClient\entity\RenewPrice;
use app\theme\components\restClient\entity\UpgradePrice;
use app\theme\models\DiscountCodeList;
use app\theme\models\PurchaseType;
use app\theme\module\helpers\DateUtils;

class CumulativeLicenseCalculator extends CalculatorAbstract
{
    /**
     * Выполняет набор вычислений над коллекцией
     * Пример: ['price', 'period', function($license, $calculator) { ... }]
     * строка - это метод у CumulativeLicenseCalculator префиксом calc
     * @param array $commands
     */
    public function run($commands = [])
    {
        foreach ($this->licenseCollection as $license) {
            foreach ($commands as $command) {
                if (is_string($command)) {
                    $method = 'command' . ucfirst($command);
                    $this->$method($license);
                } elseif (is_callable($command)) {
                    call_user_func_array($command, [&$license, &$this]);
                }
            }
        }
    }

    public function calculate()
    {
        foreach ($this->licenseCollection as $license) {
            $this->commandPrice($license);
        }
    }
    /**
     * @param OrderItemProxy $proxy
     * @return bool
     */
    public function commandPrice($proxy)
    {
//        \Yii::info('start get_product_price', 'eset');

        $params = [
            'product' => $proxy->productCode,
            'quantity' => $proxy->quantity,
            'expirationdate' =>  DateUtils::formatRestDate($proxy->Edate),
            'countryid' => $proxy->countryCode,
            'sector' => $proxy->sector,
            'discountcode' => $proxy->discountCode,
        ];
        if ($proxy->discountCode == DiscountCodeList::CODE_CUSTOM) {
            $params['discount'] = $proxy->customDiscount;
        }
        if (!empty($proxy->parentLicense) && !empty($proxy->parentLicense->epli)) {
            $params['epli'] = $proxy->parentLicense->epli;
        }
        $user_id = $proxy->user ? $proxy->user->user_id : null;
        $price = null;

        switch ($proxy->purchaseType) {
            case PurchaseType::PURCHASE_TYPE_NEW:       $price = Price::one($params, $user_id);         break;
            case PurchaseType::PURCHASE_TYPE_RENEW:     $price = RenewPrice::one($params, $user_id);    break;
            case PurchaseType::PURCHASE_TYPE_NFR:       break;
            case PurchaseType::PURCHASE_TYPE_UPGRADE:   $price = UpgradePrice::one($params, $user_id);  break;
            case PurchaseType::PURCHASE_TYPE_ENLARGE:   $price = EnlargePrice::one($params, $user_id);  break;
            case PurchaseType::PURCHASE_TYPE_DOWNGRADE:  break;
            default: break;
        }

//        \Yii::info('end get_product_price, params: ' . print_r($params, 1) . ', price: ' . print_r($price, 1), 'eset');

        if (is_null($price)) {
            $proxy->setPrice(0);
            return true;
        }

        if ( is_string($price) ) {
            $proxy->setPrice(0);
            $proxy->addError($price);

            return false;
        }

        if ($price->isError()) {
            $proxy->setPrice(0);
            $proxy->addError($price->Message);

            return false;
        }

        if (!isset($price->Currency) && $price->Currency != 'EUR') {
            $proxy->setPrice(0);
            $proxy->addError('Not known currency: ' . $price->Currency);

            return false;
        }

        $proxy->setPrice($price->SystemPrice);

        return true;
    }
}