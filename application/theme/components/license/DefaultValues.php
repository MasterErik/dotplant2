<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 23.04.15
 * Time: 14:48
 */

namespace app\theme\components\license;

use app\theme\components\license\proxy\OrderItemProxy;
use app\theme\models\DiscountCodeList;
use app\theme\models\License;
use app\theme\models\Product;
use app\theme\models\PurchaseType;
use app\theme\models\UserProfile;
use yii\db\Exception;

/**
 *
 * Class DefaultValues
 * @package app\theme\components\license
 */
class DefaultValues
{
    /**
     * @param $model OrderItemProxy
     * @throws Exception
     */
    public static function initOrderItem($model)
    {
        //$user = UserProfile::getLeader();

        $model->quantity = Product::getDefaultValue($model->product->id, 'quantity');
        $model->period = Product::getDefaultValue($model->product->id, 'eset_period');
        $model->price = $model->product->price;

        if (!empty($model->userId)) {
            //$model->userId = $user->user_id;
            $renew_model = License::findForRenew($model->userId, $model->productCode);
            if ($renew_model) {
                $model->quantity = $renew_model->quantity;
                $model->period = $renew_model->period;
                $model->purchaseType = PurchaseType::PURCHASE_TYPE_RENEW;
            } else {
                $model->purchaseType = PurchaseType::PURCHASE_TYPE_NEW;
            }
        } else {
            $model->purchaseType = PurchaseType::PURCHASE_TYPE_NEW;
        }

        if( $model->discountCode === null ) {
            $model->discountCode = DiscountCodeList::CODE_NO_DISCOUNT;
        }
    }
}