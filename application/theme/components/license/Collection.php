<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 30.03.15
 * Time: 13:18
 */

namespace app\theme\components\license;


use yii\base\Object;

class Collection extends Object implements \Iterator, \ArrayAccess, \Countable
{

	private $_licenses = [];
	private $_position;

	public function setLicenses(array $licenses)
	{
		$this->_licenses = $licenses;
	}

	private function createLicense($config)
	{
		return \Yii::createObject($config);
	}

	public function current()
	{
		return $this->offsetGet($this->_position);
	}

	public function key()
	{
		return $this->_position;
	}

	public function next()
	{
		++$this->_position;
	}

	public function rewind()
	{
		$this->_position = 0;
	}

	public function valid()
	{
		return $this->_position < count($this->_licenses);
	}


	public function offsetExists($offset)
	{
		return isset($this->_licenses[$offset]);
	}

	public function offsetGet($offset)
	{
		if (!is_object($this->_licenses[$offset])) {
			$this->_licenses[$offset] = $this->createLicense($this->_licenses[$offset]);
		}

		return $this->_licenses[$offset];
	}

	public function offsetSet($offset, $value)
	{
		$this->_licenses[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->_licenses[$offset]);
	}

	public function count()
	{
		return count($this->_licenses);
	}

}