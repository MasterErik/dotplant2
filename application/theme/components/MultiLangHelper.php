<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 10.03.15
 * Time: 19:40
 */

namespace app\theme\components;

use Yii;

class MultiLangHelper
{
    const defaultLang = 'en';

    public static function enabled()
    {
        return isset(Yii::$app->params['languages']) && count(\Yii::$app->params['languages']) >= 1;
    }

    public static function getCurrentLanguage()
    {
        if (self::enabled()) {
            return Yii::$app->language;
        } else {
            return self::defaultLang;
        }
    }

    static function setCurrentLanguage($url = null)
    {
        $language = self::getLangByUrl($url);
        Yii::$app->language = ($language === null) ? self::defaultLang : $language;
    }

    public static function getLanguages()
    {
        if (self::enabled()) {
            return Yii::$app->params['languages'];
        } else {
            return [self::defaultLang];
        }
    }

    //Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        return $url;
    }
    public static function processLangInUrl($url)
    {
        if (self::enabled()) {
            $url_list = explode('/', $url);
            $lang_url = isset($url_list[1]) ? $url_list[1] : null;


            // проверим задан ли у нас такой язык
            if ($lang_url !== null && isset(\Yii::$app->params['languages'][$lang_url])) {

                self::setCurrentLanguage($lang_url);

                $url = substr($url, strlen($lang_url) + 1);
                if (empty($url)) {
                    $url = '/';
                }
            }
        }
        return $url;
    }

    public static function addLangInUrl($url, $language)
    {
        if (self::enabled()) {

            //check language exists in the URL
            $url_list = explode('/', $url);
            $lang_in_url = isset($url_list[1]) ? $url_list[1] : null;
            if ($lang_in_url !== null && isset(\Yii::$app->params['languages'][$lang_in_url])) {
                // уже есть
                return $url;
            }

            //Добавляем к URL префикс - буквенный идентификатор языка
            if ($url == '/') {
                return '/' . $language;
            } else {
                return '/' . $language . $url;
            }
        } else {
            return $url;
        }
    }

}