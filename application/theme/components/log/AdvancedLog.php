<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 16.09.15
 * Time: 18:10
 */

namespace app\theme\components\log;


trait AdvancedLog
{
    public $log_category = 'log';
    public $silence = true;
    public $debug = false;

    public function debug($msg)
    {
        if( $this->debug )  {
            echo date('Y-m-d H:i:s')."\t".$msg."\n";
        }
    }

    public function info($msg, $category = null)
    {
        if( !$this->silence )  {
            echo date('Y-m-d H:i:s')."\t".$msg."\n";
        }
        \Yii::info($msg, $category ? $category : $this->log_category);
    }

    public function error($msg, $category = null)
    {
        if( !$this->silence )  {
            echo date('Y-m-d H:i:s')."\t".$msg."\n";
        }
        \Yii::error($msg, $category ? $category : $this->log_category);
    }
}