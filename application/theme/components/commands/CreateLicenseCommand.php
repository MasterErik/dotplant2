<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 06.04.15
 * Time: 15:50
 */

namespace app\theme\components\commands;


use app\theme\commands\RestController;
use app\theme\components\license\calculators\CumulativeLicenseCalculator;
use app\theme\components\license\proxy\LicenseProxy;
use app\theme\components\restClient\entity\DowngradeLicense;
use app\theme\components\restClient\entity\EnlargeLicense;
use app\theme\components\restClient\entity\CreateLicense;
use app\theme\components\restClient\entity\RenewLicense;
use app\theme\components\restClient\entity\UpgradeLicense;
use app\theme\models\PurchaseType;

use app\theme\models\SystemLog;
use app\theme\module\helpers\DateUtils;
use Yii;

class CreateLicenseCommand extends CommandAbstract
{
    /**
     * @param $id
     * @return \app\theme\components\restClient\entity\CreateLicense
     */
    const online = 7;

    public function run($id)
    {
        //Yii::info('start create_license, id: ' . $id, 'eset');

        $license = \app\theme\models\License::findOne($id);
        if( !$license ) {
            $this->addError('\app\theme\models\License model not found, #id='.$id);
            return null;
        }

        if( $license->register_state == \app\theme\models\License::REGISTER_STATE_REGISTERED ) {
            $this->addError('The license has already been registered, #id='.$id);
            return null;
        }

        if (isset($_SERVER["REQUEST_URI"])) {
            $SuppressEmail = 1 === Yii::$app->session->get('dexterSuppressEmail') ? true : false;
        } else {
            $SuppressEmail = false;
        }

        $model = new LicenseProxy(['owner' => $license]);
        
        $params = [
            'Price' => [
                'DistributorPrice' => $model->price,
                'DiscountCode' => $model->discountCode,
                'Discount' => $model->customDiscount,
            ],
            'Customer' => [
                'Name' => $model->user->client_name,
                'Company' => $model->user->client_name,
                'Email' => $model->user->user->email,
                'CountryId' => $model->countryCode,
            ],
            'ExpirationDate' => DateUtils::formatDate1($model->Edate, '%m-%d-%Y'),
            'Note' => $model->note,
            //'PublicResellerId' => '',
            'ChannelId' => static::online,
            //'DealCode' => '',
            //'PartnerId' => $model->partner->user_id,
            'SuppressEmail' => $SuppressEmail,
        ];

        /** @var CreateLicense $restLicense */
        $restLicense = null;

        switch ($model->purchaseType) {
            case PurchaseType::PURCHASE_TYPE_NEW:
            case PurchaseType::PURCHASE_TYPE_NFR:
                $params['Product'] = $model->productCode;
                $params['PurchaseType'] = $model->purchaseType;
                $params['Quantity'] = $model->quantity;
                $params['Sector'] = $model->sector;
                //$params['LicenseType'] = '';
                //$params['ProxyLocationId'] = '';
                //$params['SetPartnerByCountry'] = true; //only for Deslock trial licenses, optional!!!
                $params['WithActivationToken'] = false;
                //$params['ActivationToken'] = '';
                $restLicense = CreateLicense::one($params, $model->user->user_id);
                break;

            case PurchaseType::PURCHASE_TYPE_RENEW:
                $params['epli'] = $model->parentLicense ? $model->parentLicense->epli : null;
                $params['resetup'] = 0;
                $params['resetlicensekeys'] = 0;
                $restLicense = RenewLicense::one($params, $model->user->user_id);
                break;

            case PurchaseType::PURCHASE_TYPE_ENLARGE:
                $params['Quantity'] = $model->quantity;
                //$params['AdditionalParam'] = ;
                $params['epli'] = $model->parentLicense ? $model->parentLicense->epli : null;
                $params['resetup'] = 0;
                $params['resetlicensekeys'] = 0;
                $restLicense = EnlargeLicense::one($params, $model->user->user_id);
                break;

            case PurchaseType::PURCHASE_TYPE_UPGRADE:
                $params['Quantity'] = $model->quantity;
                $params['Product'] = $model->productCode;
                $params['epli'] = $model->parentLicense->epli;
                $params['resetup'] = 0;
                $params['resetlicensekeys'] = 0;
                $restLicense = UpgradeLicense::one($params, $model->user->user_id);
                break;

            case PurchaseType::PURCHASE_TYPE_DOWNGRADE:
                $params['Quantity'] = $model->quantity;
                $params['Product'] = $model->productCode;
                $params['epli'] = $model->parentLicense->epli;
                $params['resetup'] = 0;
                $params['resetlicensekeys'] = 0;
                $restLicense = DowngradeLicense::one($params, $model->user->user_id);
                break;

            default:
                break;
        }

        //Yii::info('stop create_license, id: ' . $id . ', params: ' . print_r($params, 1) . ', license: ' . print_r($license, 1), 'eset');

        if (is_null($restLicense)) {
            return null;
        }

        if ($restLicense->isError()) {
            $this->addError($restLicense->toString());
            Yii::error($this->addError(), SystemLog::CATEGORY_DEXTER);
            return Processor::RESULT_ERROR;
        }

        $model->setCredentials($restLicense);

        if (!$model->save()) {
            Yii::error('license save to db error: ' . print_r($model->getErrors(), 1) . "\nlicense: " . print_r($restLicense, 1) . "\nmodel: " . print_r($model->owner->attributes, 1), SystemLog::CATEGORY_DEXTER);

            return Processor::RESULT_ERROR;
        }

        return Processor::RESULT_OK;
    }
}