<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 06.04.15
 * Time: 15:50
 */

namespace app\theme\components\commands;


abstract class CommandAbstract
{
    private $_errors = [];

    public function addError($error)
    {
        $this->_errors[] = $error;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function hasErrors()
    {
        return $this->_errors ? true : false;
    }

    abstract public function run($id);
}