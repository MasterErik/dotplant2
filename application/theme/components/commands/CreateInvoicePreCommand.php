<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 06.04.15
 * Time: 15:50
 */

namespace app\theme\components\commands;


use app\theme\components\invoice\Generator;
use app\theme\models\Invoice;
use app\theme\models\Order;


class CreateInvoicePreCommand extends CommandAbstract
{
    /**
     * @param $id
     * @return \app\theme\models\Invoice|bool
     */
    public function run($id)
    {
        /**
         * @var Order $order
         */
        $order = Order::find()
            ->where([Order::tableName() . '.id' => $id])
            ->noInvoice()
            ->finish()
            ->one();

        if (!empty($order)) {
            $generator = new Generator();
            $generator->makeInvoice($order, Invoice::TYPE_PREPAYMENT);
        }
        return Processor::RESULT_OK;
    }
}