<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 06.04.15
 * Time: 15:49
 */

namespace app\theme\components\commands;

use app\theme\models\EventPlanning;
use yii\base\Component;
use yii\db\Expression;

class Processor extends Component
{
    const RESULT_OK = 1;
    const RESULT_ERROR = 2;
    const RESULT_LOCK = 3;

    private $_errors = [];

    public function addError($error)
    {
        $this->_errors[] = $error;
    }

    public function setErrors($errors)
    {
        $this->_errors = $errors;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function hasErrors()
    {
        return $this->_errors ? true : false;
    }


    /**
     * @param EventPlanning $command
     * @return int
     */
    public function run($command)
    {
        $this->setErrors([]);

        if ($command->getLock()) {

            $className = __NAMESPACE__ . '\\' . str_replace(' ', '',
                    ucwords(implode(' ', explode('_', $command->name)))) . 'Command';
            if (!class_exists($className)) {
                $this->addError($className . ' not found');
                return self::RESULT_ERROR;
            }

            try {
                /** @var $class  CommandAbstract */
                $class = new $className;
                $result = $class->run($command->table_id);

                $command->releaseLock();

                if (isset($result)) {
                    $command->completeAsSuccess();

                    return self::RESULT_OK;
                } else {
                    $this->setErrors($class->getErrors());
                    $command->completeAsError();

                    return self::RESULT_ERROR;
                }
            } catch (\Exception $e) {

                $this->addError($e->getMessage());

                $command->completeAsError();
                return self::RESULT_ERROR;
            }

        } else {
            return self::RESULT_LOCK;
        }
    }
}