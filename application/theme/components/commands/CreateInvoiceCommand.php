<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 27.4.2016
 * Time: 14:04
 */

namespace app\theme\components\commands;


use app\theme\components\invoice\Generator;
use app\theme\models\Invoice;
use app\theme\models\Order;


class CreateInvoiceCommand extends CommandAbstract
{
    /**
     * @param $id
     * @return \app\theme\models\Invoice|bool
     */
    public function run($id)
    {
        /**
         * @var Order $order
         */
        $order = Order::find()
            ->where([Order::tableName() . '.id' => $id])
//            ->noInvoice()
            ->finish()
            ->one();
//            ->period(DateUtils::CurEndDay())

        if (!empty($order)) {
            $generator = new Generator();
            $generator->makeInvoice($order, Invoice::TYPE_PAYMENT);
        }
        /*
                $generator = new Generator();
                $generator->addOrder($order);
                foreach($orders as $order) {
                    $generator->addOrder($order);
                }
        */
        return Processor::RESULT_OK;
    }
}