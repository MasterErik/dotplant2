<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 19.05.2015
 * Time: 11:33
 */

namespace theme\components\commands;

use WebToPay;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class PayseraPaymentsType {

    public static $DefaultCountry = 'ee';

    private static function getPaymentTypes($total, $lang, $params)
    {
        $params = Json::decode($params);
        $profile = UserProfile::myProfile();
        $code = mb_strtolower(!empty($profile) ? $profile->country->iso_code : static::DefaultCountry);

        return WebToPay::getPaymentMethodList($params['projectid'], $params['currency'])
            ->filterForAmount($total, $params['currency'])
            ->setDefaultLanguage($lang)
            ->getCountry($code);
    }

}