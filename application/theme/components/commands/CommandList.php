<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 23.3.2016
 * Time: 12:54
 */

namespace app\theme\components\commands ;


use app\modules\shop\models\Currency;
use app\theme\components\license\calculators\CumulativeLicenseCalculator;
use app\theme\components\license\proxy\LicenseProxy;
use app\theme\components\license\proxy\OrderItemProxy;
use app\theme\components\restClient\entity\ChangeState;
use app\theme\components\restClient\entity\ResetKey;
use app\theme\components\restClient\entity\ResetPassword;
use app\theme\models\DiscountCodeList;
use app\theme\models\License;
use app\theme\models\Order;
use app\theme\models\OrderItem;
use app\theme\models\PurchaseType;
use app\theme\module\helpers\DateUtils;
use app\theme\module\helpers\MailHelper;
use app\theme\services\Synchronization;
use Yii;
use yii\helpers\Url;

class CommandList
{
    const ACTIVATE = 'active';
    const CANCEL = 'canceled';
    const SUSPEND = 'suspended';
    const SYNCHRONIZE = 'synchronize';
    const RESET_PWD = 'reset_pwd';
    const RESET_KEY = 'reset_key';
    const RENEW = PurchaseType::PURCHASE_TYPE_RENEW;
    const ENLARGE = PurchaseType::PURCHASE_TYPE_ENLARGE;
    const UPGRADE = PurchaseType::PURCHASE_TYPE_UPGRADE;
    const SEND = 'send';

    public static $actions = [self::ACTIVATE, self::CANCEL, self::SUSPEND, self::SYNCHRONIZE, self::RESET_PWD, self::RESET_KEY,
        self::RENEW, self::ENLARGE, self::UPGRADE, self::SEND];

    /**
     * @param integer $id
     * @param string $action
     * @param null $email
     * @return mixed
     */
    public static function exec($id, $action, $email = null)
    {
        if (!empty($id) && !empty($action)) {
            switch ($action) {
                case self::ACTIVATE: case self::CANCEL: case self::SUSPEND:
                    $license = License::findOne($id);
                    $model = new LicenseProxy(['owner' => $license]);
                    $params = $model->getAttributes(ChangeState::getParams());
                    $params['state'] = $action;
                    /** @var ChangeState $restStatus */
                    $restStatus = ChangeState::one($params, $model->user->user_id);
                    if ($restStatus->isError()) {
                        Yii::$app->session->setFlash('error', $restStatus->toString() );
                        return;
                    } else {
                        $license->state = $action;
                        $license->save(false, ['state']);
                    }
                    break;
                case self::RESET_PWD:
                    $license = License::findOne($id);
                    $model = new LicenseProxy(['owner' => $license]);
                    $params = $model->getAttributes(ResetPassword::getParams());
                    /** @var ResetPassword $restStatus */
                    $restStatus = ResetPassword::one($params, $model->user->user_id);
                    if ($restStatus->isError()) {
                        Yii::$app->session->setFlash('error', $restStatus->toString() );
                        return;
                    } else {
                        $license->password = $restStatus->Password;
                        $license->save(false, ['password']);
                    }
                    break;
                case self::RESET_KEY:
                    $license = License::findOne($id);
                    $model = new LicenseProxy(['owner' => $license]);
                    $params = $model->getAttributes(ResetKey::getParams());
                    /** @var ResetKey $restStatus */
                    $restStatus = ResetKey::one($params, $model->user->user_id);
                    if ($restStatus->isError()) {
                        Yii::$app->session->setFlash('error', $restStatus->toString() );
                        return;
                    } else {
                        $license->license_key = $restStatus->LicenseKey;
                        $license->save(false, ['license_key']);
                    }
                    break;
                case self::SYNCHRONIZE:
                    Synchronization::syncLicenseById($id);
                    break;
                case self::RENEW: case self::ENLARGE: case self::UPGRADE:
                    /** @var License $license */
                    $license_old = License::findOne($id);

                    $order = Order::getOrder(true);
                    $order->setCartFormScenario();
                    $order->user_id = $license_old->user_id;
                    $order->eset_discount_code = DiscountCodeList::CODE_NO_DISCOUNT;

                    $orderItem = new OrderItem;
                    //$orderItem->setAttributes($license_old->getAttributes(), false);
                    if (!empty($license_old->orderDetail)) {
                        $orderItem->quantity = $license_old->quantity;
                        $orderItem->eset_period = DateUtils::PeriodOfMonths($license_old->bdate, $license_old->edate);
                        $orderItem->product_id = $license_old->product->id;
                    } else {
                        $orderItem->quantity = $license_old->quantity;
                        $orderItem->eset_period = DateUtils::PeriodOfMonths($license_old->bdate, $license_old->edate);
                        $orderItem->product_id = $license_old->product->id;
                        //$orderItem->price_per_pcs = $license_old->product->price;
                        //$orderItem->total_price = 0;

                    }
                    $orderItem->order_id = $order->id;
                    $orderItem->eset_purchase_type = $action;
                    $orderItem->eset_parent_license_id = $license_old->id;

                    $collection = [new OrderItemProxy([
                        'owner' => $orderItem,
                        'owner_order' => $order,
                    ])];
                    $mainCurrency = Currency::getMainCurrency();
                    if ($collection) {
                        $calculator = new CumulativeLicenseCalculator(['licenseCollection' => $collection]);
                        $calculator->run(['price']);
                        foreach ($calculator->licenseCollection as $proxy) {
                            $prices[$proxy->owner->id] = $mainCurrency->format($proxy->getPrice());
                            if ($proxy->hasErrors()) {
                                Yii::$app->session->setFlash('error', implode(', ', $proxy->getErrors()));
                                return;
                            }
                            $proxy->save();
                        }
                    }


                if (!$order->save() || !$orderItem->save()) {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Cannot save order item.'));
                        return;
                    } else {
                        if ($license_old->orderDetail && $license_old->orderDetail->order->eset_discount_code != DiscountCodeList::CODE_NO_DISCOUNT ) {
                            Yii::$app->session->setFlash('error', Yii::t('eset', 'Users have a discount.'));
                        }
                        $order->calculate(true);
                        $eventData['__redirect'] = Url::toRoute(['/eset/cart']);
                        return $eventData;
                    }
                    break;
                case self::SEND:
                    /** @var License $license */
                    $license = License::findOne($id);
                    $template = '@app/theme/mail/license';
                    MailHelper::Send($email, $template, ['model' => $license], Yii::t('eset', $license->productName));
                    break;
            }
            Yii::$app->session->setFlash('info', Yii::t('eset', 'Operation complete') );
        }

    }
}