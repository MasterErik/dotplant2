<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 14.05.15
 * Time: 17:34
 */
namespace app\theme\components\validators;

use yii\base\InvalidConfigException;
use yii\validators\Validator;

class ConfirmEmailCodeValidator extends Validator
{
    public $emailAttribute;

    /**
     * @var callable the filter. This can be a global function name, anonymous function, etc.
     * The function signature must be as follows,
     *
     * ~~~
     * function foo($email, $code) {...return true|false; }
     * ~~~
     */
    public $validator;

    public function init()
    {
        parent::init();

        if ($this->emailAttribute === null) {
            throw new InvalidConfigException('ConfirmEmailCodeValidator::emailAttribute must be set.');
        }
        if ($this->validator === null) {
            throw new InvalidConfigException('ConfirmEmailCodeValidator::validator must be set.');
        }
        if ($this->message === null) {
            $this->message = \Yii::t('eset', '{attribute} is invalid.');
        }

    }

    public function validateAttribute($model, $attribute)
    {
        if (!call_user_func($this->validator, $model->{$this->emailAttribute}, $model->$attribute)) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}