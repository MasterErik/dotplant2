<?php

namespace app\theme\components\validators;

use Yii;
use yii\validators\Validator;

/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 5.10.2015
 * Time: 12:33
 */
class ReferenceValidator extends Validator
{
    public $targetClass;
    public $targetAttribute;
    public $message;

    private $modelClass;

    public function init()
    {
        parent::init();

        if ($this->targetClass === null) {
            throw new InvalidConfigException(Yii::t('eset','ReferenceValidator::targetClass must be set.'));
        }
        if ($this->message === null) {
            $this->message = Yii::t('eset', $this->targetAttribute . ' {attribute} not found.');
        }
        $this->targetAttribute = $this->targetAttribute === null ? $this->attributes[0] : $this->targetAttribute;
    }

    public function validateAttribute($model, $attribute)
    {
        $targetAttribute = $this->targetAttribute === null ? $attribute : $this->targetAttribute;

        $params = [$targetAttribute => $model->$attribute];
        $this->modelClass = new $this->targetClass;

        if ($this->modelClass->getAttributes([$targetAttribute])) {

            $this->modelClass->setAttributes($params);
            if (!$this->validateValue($targetAttribute)) {
                $model->addErrors($this->modelClass->getErrors($targetAttribute) );
            }
        } else
            $this->addError($model, $attribute, $this->message);
    }

    protected function validateValue($value)
    {
        if (!is_string($this->targetAttribute)) {
            throw new InvalidConfigException(Yii::t('eset', 'The {targetAttribute} property must be configured as a string.'));
        }

        return $this->modelClass->validate([$value]);
    }
}