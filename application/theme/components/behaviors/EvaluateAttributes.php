<?php
/**
 * Created by PhpStorm.
 * User: INITEC-5
 * Date: 11.02.2015
 * Time: 15:16
 */
namespace app\theme\components\behaviors;


trait EvaluateAttributes
{

    public function evaluateAttributes($event)
    {
        if (!empty($this->attributes[$event->name])) {
            $attributes = (array)$this->attributes[$event->name];
            $value = $this->getValue($event);
            foreach ($attributes as $attribute) {
                // ignore attribute names which are not string (e.g. when set by TimestampBehavior::updatedAtAttribute)
                if (is_string($attribute) && $this->owner->hasAttribute($attribute)) {
                    $this->owner->$attribute = $value;
                }
            }
        }
    }

}