<?php
/**
 * Created by PhpStorm.
 * User: INITEC-5
 * Date: 10.02.2015
 * Time: 15:56
 */

namespace app\theme\components\behaviors;

use Yii;
use yii\behaviors\BlameableBehavior;

class ShopBlameableBehavior extends BlameableBehavior
{
    use EvaluateAttributes;
}