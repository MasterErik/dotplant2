<?php
/**
 * Created by PhpStorm.
 * User: INITEC-5
 * Date: 11.02.2015
 * Time: 14:32
 */

namespace app\theme\components\behaviors;

use Yii;
use yii\behaviors\TimestampBehavior;

class ShopTimestampBehavior extends TimestampBehavior
{
    use EvaluateAttributes;
}