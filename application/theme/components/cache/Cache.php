<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 7.10.2015
 * Time: 16:12
 */

namespace app\components\cache;

use devgroup\TagDependencyHelper\ActiveRecordHelper;
use Yii;
use yii\caching\TagDependency;

class Cache
{
    public function getParams($model, $duration = 86400, $dependency = null)
    {
        $id = $model->id;
        $cacheKey = 'reseller:' . $id;
        $data = Yii::$app->cache->get($cacheKey);
        if (is_array($data) === false) {

            $data = self::getDb()
                ->createCommand('SELECT class_name, params FROM {{%wysiwyg}} WHERE id = :id', [':id' => $id])
                ->queryOne();
            $data['params'] = empty($data['params']) ? [] : Json::decode($data['params']);

            Yii::$app->cache->set(
                $cacheKey,
                $data,
                $duration,
                new TagDependency([
                    'tags' => [
                        ActiveRecordHelper::getObjectTag(self::className(), $id),
                    ]
                ])
            );
        }

        $tags = ActiveRecordHelper::getObjectTag($model::className(), $model->id);
        TagDependency::invalidate(Yii::$app->cache, $tags);
    }
}