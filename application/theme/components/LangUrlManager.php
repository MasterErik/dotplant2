<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 10.03.15
 * Time: 17:55
 */

namespace app\theme\components;

use yii\web\UrlManager;

/**
 * @inheritdoc
 */
class LangUrlManager extends UrlManager
{
    /**
     * @inheritdoc
     */
    public function createUrl($params)
    {

        if ($language = MultiLangHelper::getCurrentLanguage()) {
            unset($params[$language]);
        }

        $url = parent::createUrl($params); //Original URL (without prefix language)

        if (!$language) {
            return $url;
        }

        return MultiLangHelper::addLangInUrl($url, $language);
    }
}
