<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 15.09.15
 * Time: 21:08
 */

namespace app\theme\components\payment\offline;


use yii\base\Model;

class EmailMessage extends Model
{
    public $header;
    public $subject;
    public $from;
    public $to;
    public $cc;
    public $created_at;

    public $charset;
    public $encoding;
    public $multipart;
    public $body;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['header', 'subject', 'from', 'to', 'cc', 'created_at', 'charset', 'encoding', 'multipart', 'body'], 'safe'],
        ];
    }

}