<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 15.09.15
 * Time: 19:17
 */

namespace app\theme\components\payment\offline\parsers;


/**
 * Class SebParser
 *
 * Parse text:
 * Konto: EE981010220047491015 INITEC OÜ
 * Summa: +71.37 EUR
 * EE761010000003667016 INITEC OÜ
 * Viitenumber:
 * Selgitus: Pangalingi maksed 16/09/2015
 *
 *
 * @package app\theme\components\payment\offline\parsers
 */
class SebParser extends AbstractEmailParser
{
    public function parse()
    {
        $re = '/^Konto: (?P<iban>[A-Z0-9]+).*^Summa: (?P<summa>[\-+0-9.]+).*^(?P<payer_iban>[A-Z0-9]+) (?P<payer>.*)^Viitenumber:(.*)^Selgitus:(?P<description>.*)^.*/msS';
        if (!preg_match($re, $this->message->body, $result)) {
            return false;
        }
        $this->setAttributes($result);

        return true;
    }
}