<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 16.09.15
 * Time: 15:23
 */

namespace app\theme\components\payment\offline\parsers;


use app\theme\components\payment\offline\EmailMessage;
use yii\base\Model;

/**
 * Class AbstractEmailParser
 * @package app\theme\components\payment\offline\parsers
 *
 * @property array $from
 * @property EmailMessage $message
 */
abstract class AbstractEmailParser extends Model
{
    public $ptype = 'in';
    public $iban = null;
    public $summa = null;
    public $payer = null;
    public $payer_iban = null;
    public $viitenumber = null;
    public $description = null;

    private $_from;
    private $_message;

    abstract public function parse();

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['iban', 'payer_iban', 'summa', 'viitenumber', 'payer', 'description'], 'trim'],
            [['iban', 'payer_iban'], 'match', 'pattern' => '/^[A-Z]{2}[0-9]{10,20}$/'],
            ['summa', 'double'],
            ['viitenumber', 'match', 'pattern' => '/^[0-9]{8,10}$/'],
            [['payer', 'description'], 'safe'],
        ];
    }


    /**
     * @param $message EmailMessage
     * @return array|bool
     */
    public function parseBankTransaction($message)
    {
        $this->_message = $message;

        if( !$this->checkFrom() ) {
            return false;
        }

        if( !$this->parse() ) {
            return false;
        }

        if (!$this->validate()) {
            //todo
            //print_r($this->getErrors()); exit;
            return false;
        }

        return true;
    }

    public function setFrom($from)
    {
        $this->_from = $from;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * @return bool
     */
    public function checkFrom()
    {
        return in_array($this->_message->from, $this->_from);
    }
}