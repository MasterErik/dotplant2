<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 15.09.15
 * Time: 19:17
 */

namespace app\theme\components\payment\offline\parsers;

/**
 * Class SwedbankParser
 *
 * Parse text:
 * Konto EE192200221023414054 saldo suurenes +15.95 EUR Maksja: KASPAR ADMANN Selgitus: Initec OU tell. nr 11191461 - ESET NOD32 Antivirus
 *
 * @package app\theme\components\payment\offline\parsers
 */
class SwedbankParser extends AbstractEmailParser
{
    /**
     * @return bool
     */
    public function parse()
    {
        $result = [];

        $re = '/Konto (?P<iban>[A-Z0-9]+) saldo suurenes (?P<summa>[\-+0-9.]+) .* Maksja: (?P<payer>.*) Selgitus: (?P<description>.*)/S';
        if (!preg_match($re, $this->message->body, $result)) {
            return false;
        }

        // viitenumber -> nr 11191461
        if (isset($result['description'])) {
            if (preg_match('/nr (?P<viitenumber>[0-9]{8,10}) /', $result['description'], $m)) {
                $result['viitenumber'] = $m['viitenumber'];
            }
        }

        $this->setAttributes($result);

        return true;
    }
}