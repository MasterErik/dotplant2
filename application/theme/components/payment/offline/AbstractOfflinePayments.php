<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 15.09.15
 * Time: 19:10
 */

namespace app\theme\components\payment\offline;


use app\theme\commands\ConsoleController;
use yii\base\Model;

abstract class AbstractOfflinePayments extends Model
{
    /**
     * @var ConsoleController
     */
    public $owner;

    abstract public function process($owner);

}