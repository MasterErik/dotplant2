<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 15.09.15
 * Time: 18:07
 */

namespace app\theme\components\payment\offline;


use app\theme\components\payment\offline\parsers\AbstractEmailParser;
use app\theme\models\EsetBankTransaction;
use app\theme\models\EsetPaymentType;
use yii\console\Exception;

class EmailNotifications extends AbstractOfflinePayments
{
    public $partner_id;
    public $mailbox;
    public $username;
    public $password;

    /**
     * @var $parsers AbstractEmailParser[]
     */
    public $parsers;

    private $_mbox;

    public function init()
    {
        //todo check params

        foreach ($this->parsers as $key => $parser_config) {
            $this->parsers[$key] = \Yii::createObject($parser_config);
        }

    }

    /**
     * @param $owner
     * @throws Exception
     */
    public function process($owner)
    {
        $this->owner = $owner;

        $this->open_mbox();

        $num_messages = imap_num_msg($this->_mbox);
        if (!$num_messages) {
            return;
        }

        $this->owner->debug('The number of messages in the mailbox ' . $this->mailbox . ': ' . $num_messages);

        for ($message_id = 1; $message_id <= $num_messages; $message_id++) {

            $message = $this->retrieve_message($message_id);

            $f_result = false;
            foreach ($this->parsers as $parser) {
                if ($f_result = $parser->parseBankTransaction($message)) {
                    break;
                }
            }

            if ($f_result == false) {
                $this->owner->error('Could not parse mail message from mailbox ' . $this->mailbox . ': ' . $message->body);
                continue; // next message
            }

            if( !$this->save($message, $parser) ) {
                //todo errors
                $this->owner->error('Could not store mail message to DB ' . $this->mailbox . ': ' . $data);
                continue; // next message
            }

            imap_delete($this->_mbox, $message_id);

        }

        $this->close_mbox();

    }

    /**
     * @param $message EmailMessage
     * @param $parser AbstractEmailParser
     */
    private function save($message, $parser)
    {

        if( EsetBankTransaction::find()
            ->duplicate(
                $parser->iban,
                $message->created_at,
                $parser->summa,
                $parser->payer,
                $parser->description
            )->exists() )
        {
            return true;
        }


        $bank_transaction = new EsetBankTransaction();
        $bank_transaction->setAttributes($parser->getAttributes());
        $bank_transaction->partner_id = $this->partner_id;


        $bank = EsetPaymentType::find()->iban($parser->iban)->one();
        if( !$bank ) {

            $this->owner->info('Are unable to determine the Bank by IBAN: ' . $parser->iban);

            $bank = new EsetPaymentType();
            $bank->type = 'bank';
            $bank->iban = $parser->iban;
            $bank->partner_id = $this->partner_id;
            $bank->name = 'new';
            $bank->code = '?';
            $bank->swift = '?';
            $bank->email = $message->from;
            if( !$bank->save() ) {
                throw new Exception('Are unable to save the Bank details in the database: ' . print_r($bank->getErrors(),1));
            }
        }
        /*
        $bank = EsetPaymentType::find()->email($message->from)->partner($this->partner_id)->one();
        if( $bank ) {
            //eset_payment_id -> сохраняем payment
        } else {
            $this->owner->error('Are unable to determine the Bank by email: ' . $message->from . ', partner_id #' . $this->partner_id);
        }
        */
        $bank_transaction->eset_payment_type_id = $bank->id;

        //print_r($bank_transaction); exit;

        if(!$bank_transaction->save()) {
            throw new Exception('Are unable to save the transaction details in the database: ' . print_r($bank->getErrors()));
        }

        return true;
    }

    private function open_mbox()
    {
        @$this->_mbox = imap_open($this->mailbox, $this->username, $this->password);

        if (!$this->_mbox) {
            $errors = imap_errors();
            if (is_array($errors)) {
                $error = implode('; ', $errors);
            } else {
                $error = $errors;
            }
            throw new Exception('imap_open error: ' . $error);
        }
    }

    private function close_mbox()
    {
        imap_expunge($this->_mbox);
        imap_close($this->_mbox);
    }


    /**
     * @param $message_id
     * @return EmailMessage
     */
    private function retrieve_message($message_id)
    {
        $message = new EmailMessage();

        $header = imap_header($this->_mbox, $message_id);
        $message->setAttributes((array) $header);
        $message->from = $this->composeMailAddress($message->from);
        $message->to = $this->composeMailAddress($message->to);
        $message->cc = $this->composeMailAddress($message->cc);

        $message->created_at = date("Y-m-d H:i:s", strtotime($header->date));

        $header = imap_fetchheader($this->_mbox, $message_id);
        //$message->header = $header;

        $regs = explode("\n", $header);
        foreach ($regs as $row) {
            //Content-Transfer-Encoding: quoted-printable
            if (strpos($row, 'Content-Transfer-Encoding:') === 0) {
                $message->encoding = strtolower(trim(str_replace('Content-Transfer-Encoding:', '', $row)));
            }
            //Content-Type: text/plain; charset="ISO-8859-1"
            if (strpos($row, 'Content-Type:') === 0) {
                if (preg_match('/charset="(.+)"/', $row, $m)) {
                    $message->charset = $m[1];
                }
            }
        }

        $structure = imap_fetchstructure($this->_mbox, $message_id);
        if ($this->isMultipart($structure)) {
            $part_structure = imap_bodystruct($this->_mbox, $message_id, "1");

//            echo imap_body($this->_mbox, $message_id);
//            print_r($part_structure);

            $message->multipart = 1;
            $message->charset = $this->getCharsetPart($part_structure);
            $message->encoding = $this->getEncodingPart($part_structure);
            $message->body = imap_fetchbody($this->_mbox, $message_id, "1"); ## GET THE BODY OF MULTI-PART MESSAGE
        } else {
            $message->body = imap_body($this->_mbox, $message_id);
            $message->multipart = 0;
        }

        if ($message->encoding == 'base64') {
            $message->body = base64_decode($message->body);
        } elseif ($message->encoding == 'quoted-printable') {
            $message->body = trim(quoted_printable_decode($message->body));
        }

        //echo $message->charset;
        if ($message->charset) {
            //$message->body = mb_convert_encoding($message->body, 'utf8', $message->charset);
            $message->body = iconv($message->charset, 'utf8', $message->body);
        }

//        if (!$message->body) {
//            $message->body = '[NO TEXT ENTERED INTO THE MESSAGE]\n\n';
//        }


        return $message;
    }

    private function isMultipart($structure)
    {
        if ($structure->type == 1) {
            return true; ## MULTI-PART MESSAGE
        } else {
            return false; ## NOT A MULTI-PART MESSAGE
        }
    }

    /**
     * @param $structure
     * @return charset string
     */
    private function getCharsetPart($structure)
    {
        $charset = null;
        foreach ($structure->parameters as $param) {
            if (strtolower($param->attribute) == "charset") {
                $charset = $param->value;
                break;
            }
        }

        return $charset;
    }

    /**
     * @link http://php.net/manual/ru/function.imap-fetchstructure.php
     *
     * 0 7bit             ENC7BIT
     * 1 8bit             ENC8BIT
     * 2 Binary           ENCBINARY
     * 3 Base64           ENCBASE64
     * 4 Quoted-Printable ENCQUOTEDPRINTABLE
     * 5 other            ENCOTHER
     *
     * @param $structure
     * @return encoding string
     */
    private function getEncodingPart($structure)
    {
        switch ($structure->encoding) {
            case 0:
                return '7bit';
            case 1:
                return '8bit';
            case 2:
                return 'binary';
            case 3:
                return 'base65';
            case 4:
                return 'quoted-printable';
            case 5:
                return 'other';
        }

        return 'other';
    }

    /**
     * @param $addresses
     */
    private function composeMailAddress($addresses) {
        for( $i = 0; $i < count($addresses); $i++ ) {
            if( isset($addresses[$i]->mailbox) ) {
                return $addresses[$i]->mailbox . (isset($addresses[$i]->host) ? '@' . $addresses[$i]->host : '');
            }
        }

        return null;
    }

}