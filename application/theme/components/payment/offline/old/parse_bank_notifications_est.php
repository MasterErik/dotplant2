<?
// http://www.eset.ee/minute_cron.php
// http://www.eset.ee/z_bank_notifications_cron_test.php
require ('robot/parse_functions.inc.php');

// load IMAP extension
if(!extension_loaded('imap')){
	dl('php_imap.' . PHP_SHLIB_SUFFIX);
}

echo "<p style=\"font-size: 10pt; color: blue;\"><b>Parse bank notifications cron, est</b></p>\n";
echo "<p>Connecting to INBOX...</p>\n";
flush();
$mboxu = imap_open('{mail.zone.ee:110/pop3}INBOX', 'pank_makstud@eset.ee', '3D3Gvc');

if ($mboxu) {
  echo "<p>Connected. Number of messages in the INBOX is ".imap_num_msg($mboxu)."</p>\n";
}
else {
  echo "<p>Can't connect.</p>\n";
}

echo "<ul>\n";
flush();
for ($i=1; $i<=imap_num_msg($mboxu); $i++) 
{
  //if ($i > 30) {
  //  echo "</ul>\n<p>BREAKED</p>\n";
  //  break;
  //}
  $insert_id = '';
  $message = retrieve_message($mboxu, $i);
  $rec_acc = $amount = $snd_acc = $snd_name = $reference = $msg = '';
  $message[body] = trim(quoted_printable_decode($message[body]));
  $message['date'] = date("Y-m-d H:i:s", strtotime($message['date']));
  if (($message[fromaddress] == 'alerts@swedbank.ee' || $message[fromaddress] == 'alerts@hansa.ee' || $message[fromaddress] == 'test-alerts@hansa.ee') && $message[toaddress] == 'pank_teavitus@initec.ee') 
  {
    echo "<li>$i: $message[date], From: $message[fromaddress], ";
    if (preg_match("/Konto (\d+) saldo suurenes \+(\d+\.\d\d) .* Maksja: (.*) Selgitus:(.*)/", $message[body], $p)) 
    {
      $rec_acc = trim($p[1]);
      $amount = trim($p[2]);
      $snd_name = trim($p[3]);
      $msg = trim($p[4]);
      if (is_numeric($amount) && $amount > 0) {
        $insert_id = save_bank_mess_data('HP', $message['date'], $rec_acc, $amount, '', '', $snd_name, $msg, $message['header'], $message[body]);
      }
      else {
        echo "<span style=\"color: red;\">incorrect data</span>, rec_acc=$rec_acc, amount=$amount, string=$message[body].";
      }
    }
    else {
      echo "<span style=\"color: red;\">can't parse the body string</span> - $message[body].";
    }
  }
  else if (($message[fromaddress] == 'info@seb.ee' || $message[fromaddress] == 'automailer@seb.ee') && $message[toaddress] == 'pank_teavitus@initec.ee') 
  {
    echo "<li>$i: $message[date], From: <span style=\"color: green\"><b>$message[fromaddress]</b></span>, ";
    $regs = explode("\n", $message[body]);
    $ptype = 'in';
    while (is_array($regs) && sizeof($regs)) 
    {
      $str = array_shift($regs);
      if (preg_match("/Summa\s*:\s*-(\d*,?\d+\.\d\d)/", $str, $p)) 
      {
        $amount = -1 * str_replace(',', '', trim($p[1]));
        $ptype = 'out';
      }
      else if (preg_match("/Summa\s*:\s*\+(\d*,?\d+\.\d\d)/", $str, $p)) {
        $amount = str_replace(',', '', trim($p[1]));
      }
      else if (preg_match("/Konto\s*:\s*(\d+)/", $str, $p)) {
        $rec_acc = trim($p[1]);
      }
      else if (preg_match("/^(\d+)\s+(.+)/", $str, $p)) 
      {
        $snd_acc = trim($p[1]);
        $snd_name = trim($p[2]);
      }
      else if (preg_match("/Viitenumber\s*:\s*(\d+)/", $str, $p)) {
        $reference = trim($p[1]);
      }
      else if (preg_match("/Selgitus\s*:\s*(.*)/", $str, $p)) {
        $msg = trim($p[1]);
      }
    }
    $insert_id = save_bank_mess_data('EYP', $message['date'], $rec_acc, $amount, $reference, $snd_acc, $snd_name, $msg, $message['header'], $message[body], $ptype);
  }
  else
  {
    echo "<li>$i: $message[date], From: <b>$message[fromaddress]</b>, <span style=\"color: red;\">can't recognize address</span>.";
    //imap_delete($mboxu,$i);
    mail($system_conf[debug_email], "Parse bank notifications cron - can't recognize address", "Can't recognize address $message[fromaddress], $message[date]");
  }
  echo "</li>\n";
  flush();
  if ($insert_id) {
    imap_delete($mboxu,$i);
  }
}
echo "</ul>\n";
imap_expunge($mboxu);
imap_close($mboxu);

function save_bank_mess_data($bank_id, $mess_date, $rec_acc, $amount, $reference, $snd_acc, $snd_name, $msg, $header, $body, $ptype='in')
{
  $status = 'new';
  $mess_orig = $msg;
  if (!$bank_id || !is_numeric($amount) || (!$reference && !$msg))
  {
    echo "<span style=\"color: red;\">incorrect data</span>, bank_id=$bank_id, amount=$amount, rec_acc=$rec_acc, snd_acc=$snd_acc, snd_name=$snd_name, reference=$reference, [<span style=\"color: brown\">$msg</span>]";
    $status = 'incorrect';
  }
  if ($ptype == 'out') {
    echo "$bank_id, <b>$amount</b>, <span style=\"color: blue\">outgoing payment</span>, rec=$rec_acc, snd=$snd_acc, name=$snd_name, [<span style=\"color: brown\">$msg</span>]";
  }
  else if (strpos($msg, 'Pangalingi maksed') !== false) {
    $ptype = 'collect';
    echo "$bank_id, <b>$amount</b>, <span style=\"color: green\"><b>collect payment</b></span>, rec=$rec_acc, snd=$snd_acc, name=$snd_name, [<span style=\"color: brown\">$msg</span>]";
  }
  else
  {
    $msg = str_replace('NOD32', '', $msg);
    $msg = str_replace('NOD 32', '', $msg);
    $msg = str_replace('Nod32', '', $msg);
    $msg = str_replace('Nod 32', '', $msg);
    $msg = str_replace('nod32', '', $msg);
    $msg = str_replace('nod 32', '', $msg);
    $msg = preg_replace("/\d+\.\d+\.\d{2,4}/", '', $msg);
    $msg = preg_replace("/\d+\/\d+\/\d{2,4}/", '', $msg);
    $msg = preg_replace("/\d+,\d+,\d{2,4}/", '', $msg);
    if (preg_match("/(\d+)\D*(\d*)\D*(\d*)/", $msg, $p))
    {
      if (strlen($p[1]) >= 8) {
        $reference = $p[1];
      }
      else if ($p[1] > 3) {
        $invoice_nr = $p[1];
      }
      else if ($p[1] <= 3) {
        $p[1] = '';
      }
      if ($p[2] && strlen($p[2]) < 8 && $p[2] > 3) {
        $invoice_nr = $p[2];
      }
      else if ($p[2] && $invoice_nr && strlen($p[2]) >= 8) {
        $reference = $p[2];
      }
      else if ($p[2] <= 3) {
        $p[2] = '';
      }
      if ((strlen($p[1]) && strlen($p[1]) < 8 && strlen($p[2]) && strlen($p[2]) < 8 && $p[1] != $p[2]) 
           || $p[3] || strlen($p[1]) > 11 || strlen($p[2]) > 11 || (!$reference && !$invoice_nr)) 
      {
        if (preg_match("/arve:? *(nr)*\.? *(\d+)(.*)?/i", $msg, $p) && trim($p[2]) < 100000 && substr(trim($p[3]), 0, 1) != ',') 
        {
          $invoice_nr = trim($p[2]);
          $reference = '';
          echo " (<b><span style=\"color: red;\">*** $invoice_nr</b></span>) ";
        }
        else
        {
          echo "<span style=\"color: orange;\"><b>unrecognized message</b></span>, ";
          $status = 'unrecognized';
          $reference = $invoice_nr = '';
        }
      }
    }
    else {
      $status = 'unrecognized';
    }
    echo "$bank_id, <b>$amount</b>, [$p[1] / $p[2]] ref=<span style=\"color: magenta\"><b>$reference</b></span>, inv=<b>$invoice_nr</b> rec=$rec_acc, snd=$snd_acc, name=$snd_name, [<span style=\"color: brown\">$mess_orig</span>]";
  }
  return set_new_bank_notification($bank_id, $mess_date, $rec_acc, $amount, $reference, $invoice_nr, $snd_acc, $snd_name, $mess_orig, $header, $body, $ptype, $status);
}

function set_new_bank_notification($bank_id, $mess_date, $rec_acc, $amount, $reference, $invoice_nr, $snd_acc, $snd_name, $msg, $header, $body, $ptype, $status)
{
  if (check_for_duplicated_bank_notification($bank_id, $mess_date, $amount, $snd_name, $msg)) 
  {
    echo " <span style=\"color: red;\">duplicated</span>";
    return;
  }
  global $DB;
  $rec_acc = $rec_acc ? "'$rec_acc'" : 'NULL';
  $reference = $reference ? "'$reference'" : 'NULL';
  $invoice_nr = $invoice_nr ? "'$invoice_nr'" : 'NULL';
  $snd_acc = $snd_acc ? "'$snd_acc'" : 'NULL';
  $snd_name = $snd_name ? "'".mysql_escape_string($snd_name)."'" : 'NULL';
  $msg = $msg ? "'".mysql_escape_string($msg)."'" : 'NULL';
  $header = mysql_escape_string($header);
  $body = mysql_escape_string($body);
  $result = $DB->query("INSERT INTO cms_bank_notification (bank_id, created, message_date, rec_account, amount, reference, invoice_nr, snd_account, snd_name, message, header, body, ptype, status) 
                        VALUES ('$bank_id', '".date("Y-m-d H:i:s")."', '$mess_date', $rec_acc, '$amount', $reference, $invoice_nr, $snd_acc, $snd_name, $msg, '$header', '$body', '$ptype', '$status')");
  if ($result) {
    return mysql_insert_id();
  }
}

function check_for_duplicated_bank_notification($bank_id, $mess_date, $amount, $snd_name, $msg)
{
  global $DB;
  $result = $DB->query("SELECT * FROM cms_bank_notification WHERE bank_id = '$bank_id' AND message_date = '$mess_date' AND amount = '$amount' AND snd_name = '$snd_name' AND message = '$msg'");
  if ($result && mysql_num_rows($result)) {
    return true;
  }
}

// UPDATE cms_bank_notification SET status = 'completed' WHERE status = 'new'
// UPDATE cms_bank_notification SET reference = NULL, invoice_nr = NULL WHERE status = 'unrecognized'
?>
