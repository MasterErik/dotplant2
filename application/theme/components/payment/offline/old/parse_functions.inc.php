<?
	function retrieve_message($mbox, $messageid)
	{
		$message = array();
   
		$header = imap_header($mbox, $messageid);

		$structure = imap_fetchstructure($mbox, $messageid);

		$message['header'] = $hdr = imap_fetchheader($mbox, $messageid);

		$message['subject'] = $header->subject;
		$message['fromaddress'] =  $header->fromaddress;
		$message['toaddress'] =  $header->toaddress;
		$message['ccaddress'] =  $header->ccaddress;
		$message['date'] =  $header->date;
		
		$regs = explode("\n",$hdr);
		foreach ((array)$regs as $row) {
			if (substr($row,0,26) == 'content-transfer-encoding:') {
				$message[encoding] = trim(substr($row,27,strlen($row)));
			}
		}
				
		if (check_type($structure))  {
			$message['body'] = imap_fetchbody($mbox,$messageid,"1"); ## GET THE BODY OF MULTI-PART MESSAGE
			$message["multipart"] = 1;
			if(!$message['body']) {
			   $message['body'] = '[NO TEXT ENTERED INTO THE MESSAGE]\n\n';
			}
		}
		else {
			$message['body'] = imap_body($mbox, $messageid);
			$message["multipart"] = 0;
			if(!$message['body']) {
				$message['body'] = '[NO TEXT ENTERED INTO THE MESSAGE]\n\n';
			}
		}

		return $message;
	}

	function check_type($structure) {
		if($structure->type == 1) {
			return(true); ## MULTI-PART MESSAGE
		}
		else {
			return(false); ## NOT A MULTI-PART MESSAGE
		}
	} 


$type = array("text", "multipart", "message", "application", "audio",
"image", "video", "other");
// message encodings
$encoding = array("7bit", "8bit", "binary", "base64", "quoted-printable",
"other");

// parse message body
function parse($structure)
{
	global $type;
	global $encoding;

	// create an array to hold message sections
	$ret = array();

	// split structure into parts
	$parts = $structure->parts;

	for($x=0; $x<sizeof($parts); $x++)
	{
		$ret[$x]["pid"] = ($x+1);
		$thiss = $parts[$x];
	  // default to text
	  if ($thiss->type == "") { $thiss->type = 0; }
	  $ret[$x]["type"] = $type[$thiss->type] . "/" . strtolower($thiss->subtype);

	  // default to 7bit
	  if ($thiss->encoding == "") { $thiss->encoding = 0; }
	  $ret[$x]["encoding"] = $encoding[$thiss->encoding];

	  $ret[$x]["size"] = strtolower($thiss->bytes);

	  $ret[$x]["disposition"] = strtolower($thiss->disposition);

  	$params = $thiss->parameters;
  	foreach ($params as $p)
  	{
    if($p->attribute == "NAME")
    {
    $ret[$x]["name"] = $p->value;
    break;
    }
  }
}

return $ret;
}

function get_attachments($arr)
{
	for($x=0; $x<sizeof($arr); $x++)
	{
  if($arr[$x]["name"] == "nod32.lic")
  {
  $ret[] = $arr[$x];
  }
	}
	return $ret;
}
?>