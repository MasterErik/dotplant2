<?php

// start debugging
echo "<p style=\"font-size: 10pt; color: blue;\"><b>Process bank notifications cron</b></p>\n";
flush();

// fetch processed bank notifications
$result = $DB->query("SELECT * FROM cms_bank_notification WHERE processed IS NULL AND ptype = 'in' ORDER BY payment_id DESC");
if($result){ $num_found = mysql_num_rows($result); }
if(!$result or !$num_found){
	echo "<p>No new payments found for completing.</p>\n";
}else{
	// start output buffering
  ob_start();
  echo "<p>Found payments for completing: <b>$num_found</b>.</p>\n<ul>\n";
	
	// loop through processed payments
  while($payment = mysql_fetch_array($result)){
  	// debug payment informations
  	echo "<li><b>$payment[payment_id]</b>, $payment[amount], ref=<b>$payment[reference]</b>, inv=<b>$payment[invoice_nr]</b>, $payment[snd_name]: ";
    flush();
		
		// try to find order/invoice/license by payment
    $order = $license = $invoice = $alternative_invoice = '';
    if(!$payment[reference] && !$payment[invoice_nr]){
      // could not find refernce and/or invoice_nr
      echo "<span style=\"color: red;\"><b>missing reference and invoice_nr</b></span>, ";
      inform_about_error('missing_ref_inv', $payment);
    }else if($order = find_order_by_payment($payment[reference], $payment[invoice_nr])){
    	// order found
      echo "found <span style=\"color: #33CC33;\"><b>order</b></span> $order[order_id] - ";
			
			// throw and error if order and payment amouts do not match
      if($order[price] != $payment[amount]){
        echo "<span style=\"color: red;\"><b>payment amount does not match order amount</b></span>, ";
        inform_about_error('amount_not_match_order', $payment, $order);
      }else{
      	// try to find license by payment
        if(!$order[license_id] && ($license = find_license_by_payment($payment[invoice_nr]))){
          echo "<span style=\"color: red;\"><b>license_id is missing but we have found the license $license[license_id] for this payment invoice_nr</b></span>, ";
          inform_about_error('found_not_marked_license', $payment, $order, $license);
          $DB->query("UPDATE cms_order_request SET license_id = '$license[license_id]' WHERE order_id = '$order_id'");
          $order[license_id] = $license[license_id];
        }
				
				// if order is already paid for
        if($order[status] == 'paid' || $order[status] == 'completed'){
          echo "<span style=\"color: orange;\"><b>order is already set as $order[status]</b></span>, ";
        }else{
          set_order_paid($order[order_id], $payment);
          echo "<span style=\"color: #33CC33;\"><b>set order paid</b></span>, ";
        }
				
				// find order's invoice
        if($order[invoice_nr]){
          $invoice = find_invoice_by_payment($payment[reference], $order[invoice_nr]);
        }else{
          if($invoice = find_invoice_by_payment($payment[reference], $payment[invoice_nr])){
            $alternative_invoice = true;
          }
        }
				
        if($invoice){
          echo "also found the invoice <b>$invoice[invoice_nr]</b> for this payment, ";
          if($invoice[total_sum] != $payment[amount]){
            echo "<span style=\"color: red;\"><b>payment amount does not match invoice total_sum</b></span>, ";
            inform_about_error('amount_not_match_invoice', $payment, $order, '', $invoice);
          }
          else if ($invoice[client_id])
          {
            echo "<span style=\"color: red;\"><b>incorrect invoice type (reseller $invoice[client_id])  for this payment</b></span>, ";
            inform_about_error('incorrect_invoice_reseller', $payment, $order, '', $invoice);
          }
					else if ($invoice['paid'] == '1' and $invoice['check_status'] == 'mail')
					{
						echo "<span style=\"color: orange;\"><b>invoice has already been paid for</b></span>, ";
						inform_about_error('invoice_already_payed', $payment, '', '', $invoice);
					}
          else
          {
            echo "<span style=\"color: #33CC33;\"><b>set invoice paid</b></span>, ";
            set_invoice_paid($invoice, $payment);
            set_payment_completed($payment[payment_id], $order);
            if ($alternative_invoice) 
            {
              echo "set found <b>invoice_nr</b>, ";
              $DB->query("UPDATE cms_order_request SET invoice_nr = '$invoice[invoice_nr]' WHERE order_id = '$order_id'");
              $order[invoice_nr] = $invoice[invoice_nr];
            }
          }
        }
        else {
          set_payment_completed($payment[payment_id], $order);
        }
      }
    }
    else if ($license = find_license_by_payment($payment[invoice_nr])) {
      // LICENSE FOUND
      echo "found <span style=\"color: blue;\"><b>license</b></span> $license[license_id] - ";
      if (!($invoice = find_invoice_by_payment($payment[reference], $payment[invoice_nr])))
      {
        echo "<span style=\"color: red;\"><b>invoice for this invoice_nr is not found in the DB</b></span>, ";
        inform_about_error('invoice_not_found', $payment, '', $license);
      }
      else if ($invoice[total_sum] != $payment[amount])
      {
        echo "<span style=\"color: red;\"><b>payment amount does not match invoice total_sum</b></span>, ";
        inform_about_error('amount_not_match_invoice', $payment, '', '', $invoice);
      }
			else if ($invoice['paid'] == '1' and $invoice['check_status'] == 'mail')
			{
				echo "<span style=\"color: orange;\"><b>invoice has already been paid for</b></span>, ";
				inform_about_error('invoice_already_payed', $payment, '', '', $invoice);
			}
      else
      {
        echo "<span style=\"color: #33CC33;\"><b>set invoice paid</b></span>, ";
        set_invoice_paid($invoice, $payment);
        set_payment_completed($payment[payment_id]);
      }
    }
    else if ($invoice = find_invoice_by_payment($payment[reference], $payment[invoice_nr])) {
      // INVOICE FOUND
      echo "found <span style=\"color: magenta;\"><b>invoice</b></span> $invoice[invoice_id] - ";
      if ($invoice[total_sum] != $payment[amount])
      {
        echo "<span style=\"color: red;\"><b>payment amount does not match invoice total_sum</b></span>, ";
        inform_about_error('amount_not_match_invoice', $payment, '', '', $invoice);
      }
      else if (!$invoice[client_id])
      {
        echo "<span style=\"color: red;\"><b>license not found for this payment</b></span>, ";
        inform_about_error('license_not_found', $payment, '', '', $invoice);
      }
			else if ($invoice['paid'] == '1' and $invoice['check_status'] == 'mail')
			{
				echo "<span style=\"color: orange;\"><b>invoice has already been paid for</b></span>, ";
				inform_about_error('invoice_already_payed', $payment, '', '', $invoice);
			}
      else
      {
        echo "<span style=\"color: #33CC33;\"><b>set invoice paid</b></span>, ";
        set_invoice_paid($invoice, $payment);
        set_payment_completed($payment[payment_id]);
      }
    }
    else
    {
      // NOTHING FOUND FOR PROVIDED REFERENCE AND INVOICE_NR
      echo "<span style=\"color: red;\"><b>nothing found</b></span>, ";
      inform_about_error('nothing_found', $payment);
    }
    echo "</li>";
    $DB->query("UPDATE cms_bank_notification SET processed = '".date("Y-m-d H:i:s")."' WHERE payment_id = '$payment[payment_id]'");
  }
  echo "</ul>\n";
  $pagecontent = ob_get_contents(); 
  ob_end_clean();
  echo $pagecontent;
  send_mail_report($pagecontent);
}

function find_order_by_payment($reference='', $invoice_nr='')
{
  $and_created = " AND created > '".date("Y-m-d H:i:s", time() - 86400 * 120)."'";
  switch (true)
  {
    case $reference && $invoice_nr : $where = "reference = '$reference' AND invoice_nr = '$invoice_nr' $and_created"; break;
    case $reference                : $where = "reference = '$reference'"; break;
    case $invoice_nr               : $where = "invoice_nr = '$invoice_nr' $and_created"; break;
    default                        : return;
  }
  global $DB;
  $result = $DB->query("SELECT * FROM cms_order_request WHERE $where");
  if ($result && mysql_num_rows($result) == 1) {
    return mysql_fetch_array($result);
  }
}

function find_license_by_payment($invoice_nr)
{
  if (!$invoice_nr) {
    return;
  }
  global $DB;
  $result = $DB->query("SELECT * FROM dexter_licenses WHERE invoice_nr = '$invoice_nr' AND date > '".date("Y-m-d", time() - 86400 * 180)."' AND license_status != 'canceled'");
  if ($result && mysql_num_rows($result) == 1) {
    return mysql_fetch_array($result);
  }
}

function find_invoice_by_payment($reference='', $invoice_nr='')
{
  $and_out_date = " AND out_date > '".date("Y-m-d", time() - 86400 * 120)."'";
  switch (true)
  {
    case $reference && $invoice_nr : $where = "reference = '$reference' AND invoice_nr = '$invoice_nr' $and_out_date"; break;
    case $reference                : $where = "reference = '$reference'"; break;
    case $invoice_nr               : $where = "invoice_nr = '$invoice_nr' $and_out_date"; break;
    default                        : return;
  }
  global $invoice_conn, $system_conf;
  $company_id = $system_conf[invoice_company_id];
  $result = mysql_query("SELECT * FROM invoice_invoices WHERE $where AND company_id = '$company_id' AND kreedit = '0'", $invoice_conn);
  if ($result && mysql_num_rows($result) == 1) {
    return mysql_fetch_array($result);
  }
}

function set_invoice_paid($invoice, $payment)
{
  global $invoice_conn;
  if ($invoice && $payment) 
  {
    if (!$invoice[paid]) 
    {
      $and_set[] = "paid = '1', check_status = 'mail'";
    }
    if (!$invoice[paid_dt]) {
      $and_set[] = "paid_dt = '".date("Y-m-d H:i:s")."'";
    }
    if ($payment[bank_id] && !$invoice[bank_id]) {
      $and_set[] = "bank_id = '$payment[bank_id]'";
    }
    if ($payment[snd_account] && !$invoice[snd_account]) {
      $and_set[] = "snd_account = '$payment[snd_account]'";
    }
    if ($payment[snd_name] && !$invoice[snd_name]) {
      $and_set[] = "snd_name = '".mysql_escape_string(utf8_encode($payment[snd_name]))."'";
    }
    if ($payment[rec_account] && !$invoice[rec_account]) {
      $and_set[] = "rec_account = '$payment[rec_account]'";
    }
    if (is_array($and_set) && sizeof($and_set)) 
    {
      $set = join(', ', $and_set);
      return mysql_query("UPDATE invoice_invoices SET $set WHERE invoice_id = '$invoice[invoice_id]'", $invoice_conn);
    }
  }
}

function set_order_paid($order_id, $payment)
{
  global $DB;
  if ($order_id && $payment) 
  {
    if ($payment[bank_id]) {
      $and_set .= ", bank_id = '$payment[bank_id]'";
    }
    if ($payment[snd_account]) {
      $and_set .= ", snd_account = '$payment[snd_account]'";
    }
    if ($payment[snd_name]) {
      $and_set .= ", snd_name = '".mysql_escape_string($payment[snd_name])."'";
    }
    if ($payment[rec_account]) {
      $and_set .= ", rec_account = '$payment[rec_account]'";
    }
		
		$paidAt = date('Y-m-d H:i:s');
    return $DB->query("UPDATE cms_order_request SET status = 'paid', paid_dt='$paidAt', check_status = 'mail'$and_set WHERE order_id = '$order_id'");
  }
}

function set_payment_completed($payment_id, $order='')
{
  global $DB;
  if ($order[reference]) {
    $and_set .= ", reference = '$order[reference]'";
  }
  if (is_numeric($order[license_id])) {
    $and_set .= ", license_id = '$order[license_id]'";
  }
  if ($order[invoice_nr]) {
    $and_set .= ", invoice_nr = '$order[invoice_nr]'";
  }
  if ($payment_id) {
    return $DB->query("UPDATE cms_bank_notification SET status = 'completed'$and_set WHERE payment_id = '$payment_id'");
  }
}

function inform_about_error($error_name, $payment, $order='', $license='', $invoice='')
{
  if (!$error_name || !$payment) {
    return;
  }
  global $DB, $system_conf;
  switch ($error_name)
  {
    case 'missing_ref_inv'            : $status = 'unrecognized'; $unable = true;  $str = "Makse selgituses puuduvad viite- ja arve number. "; break;
    case 'amount_not_match_invoice'   : $status = 'incorrect';    $unable = true;  $str = "Makse summa $payment[amount] ei vasta arve $invoice[invoice_nr] summale $invoice[total_sum]. "; break;
    case 'license_not_found'          : $status = 'unrecognized'; $unable = true;  $str = "Antud maksel m�rgitud arve nr. j�rgi ei leitud litsentsi, kusjuures arve ei kuulu edasim��jale. "; break;
    case 'nothing_found'              : $status = 'unrecognized'; $unable = true;  $str = "Makse andmete alusel ei leitud ei tellimust, litsentsi ega arvet. "; break;
    case 'amount_not_match_order'     : $status = 'incorrect';    $unable = true;  $str = "Makse summa $payment[amount] ei vasta tellimuse $order[reference] summale $order[price]. "; break;
    case 'found_not_marked_license'   : $status = 'incorrect';    $unable = false; $str = "Tellimuses puudub litsentsi nr. kuid on leitud litsents $license[license_id]. "; break;
    case 'incorrect_invoice_reseller' : $status = 'unrecognized'; $unable = true;  $str = "Vale arve t��p: tellimus $order[reference] kuulub INITEC`le, kuid arve koostatud edasim��jale $invoice[client_id]. "; break;
    case 'invoice_not_found'          : $status = 'unrecognized'; $unable = true;  $str = "Maksel m�rgitud arve nr. puudub andmebaasis. "; break;
		case 'already_payed'							: $status = 'incorrect'; 		$unable = false; $str = "Tellimuse nr. $order[reference] (arve nr. $invoice[invoice_nr]) eest on juba makstud."; break;
		case 'invoice_already_payed'			: $status = 'incorrect'; 		$unable = false; $str = "Arve nr. $invoice[invoice_nr] on juba m�rgitud makstuks."; break;
    default : return;
  }
  echo " (<span style=\"color: blue\">$str</span>) ";
  if ($str && $status) 
  {
    $s .= "\n\nPank:        $payment[bank_id]\n";
    $s .= "Maksja:      $payment[snd_name]\n";
    if ($payment[invoice_nr]) {
      $s .= "Arve nr.:    $payment[invoice_nr]\n";
    }
    if ($payment[reference]) {
      $s .= "Viitenumber: $payment[reference]\n";
    }
    $s .= "Summa:       $payment[amount] EUR\n";
    $s .= "Selgitus:    $payment[message]\n\n";
    if ($unable) {
      $s .= "Programm ei leia tellimust v�i arvet, et m�rkida nad makstuks. Kahjuks peab seda t��d tegema k�sitsi.\n\n";
    }
    mail(order_email, 'Viga pangateate andmetel, eset.ee', $str.$s);
    //mail($system_conf[debug_email], 'Viga panga teade andmetel, eset.ee', $str.$s);
    return $DB->query("UPDATE cms_bank_notification SET status = '$status', error_str = '".mysql_escape_string($str)."' WHERE payment_id = '$payment[payment_id]'");
  }
}

function send_mail_report($s)
{
  return;
  global $system_conf;
  $hdr = "<HTML>
<HEAD>
<title>Hourly cron</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">
<style type=\"text/css\">
<!--
BODY, A, P, TD, INPUT, TEXTAREA, SELECT, LI  {
  font-family: verdana, arial, helvetica, sans-serif; color: black; font-size: 9pt;
}
-->
</style>
<body>
<p style=\"font-size: 10pt; color: blue;\"><b>Process bank notifications cron</b></p>\n";
  $s = $hdr.$s."</body>\n</html>\n";
  mail($system_conf[debug_email], 'Process bank notifications cron report, eset.ee', $s, "Content-type: text/html;");
}
