<?php
/**
 *
 * @link https://github.com/paypal/PayPal-PHP-SDK
 * @link http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
 */

namespace app\theme\components\payment;

use app\modules\shop\models\OrderTransaction;
use Yii;
use yii\helpers\Json;


abstract class AbstractPayment extends \app\components\payment\AbstractPayment
{

    /**
     * @param $result_data
     * @return void
     *
     */
    public function setTransactionSuccess($result_data)
    {
        $this->transaction->status = OrderTransaction::TRANSACTION_SUCCESS;
        $this->transaction->result_data = Json::encode($result_data);
        return $this->transaction->save(['status', 'result_data']);
    }

    public function setTransactionError($result_data)
    {
        $this->transaction->status = OrderTransaction::TRANSACTION_ERROR;
        $this->transaction->result_data = $result_data;
        return $this->transaction->save(['status', 'result_data']);
    }

    public static abstract function redirectToPayment();
}
