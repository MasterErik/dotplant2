<?php
/**
 *
 * @link https://github.com/paypal/PayPal-PHP-SDK
 * @link http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
 */

namespace app\theme\components\payment;

//use app\components\payment\AbstractPayment;
use app\modules\shop\models\Order;
use app\modules\shop\models\OrderItem;
use app\modules\shop\models\OrderTransaction;
use PayPal\Exception\PayPalConnectionException;
use yii\base\Exception;
use yii\helpers\Json;


use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use yii\web\BadRequestHttpException;


/**
 * @link https://developer.paypal.com/webapps/developer/applications/myapps
 */
class PaypalPayment extends AbstractPayment
{
    public $clientId;
    public $clientSecret;
    public $currency;
    public $mode;
    public $color;

    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';

    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_WARN = 'WARN';
    const LOG_LEVEL_ERROR = 'ERROR';
    const LOG_LEVEL_DEBUG = 'DEBUG';

    public static function redirectToPayment()
    {
        $class = new static;

    }

    public function content()
    {
        $url = $this->prepareUrl($this->order, $this->transaction);

        return $this->render(
            'paypal',
            [
                'order' => $this->order,
                'url' => $url,
            ]
        );
    }

    public function checkResult($hash = '')
    {
        $this->transaction = $this->loadTransaction(\Yii::$app->request->get('transactionId'));
        if (!$this->transaction || !$this->transaction->checkHash($hash)) {
            throw new BadRequestHttpException();
        }

        $paymentId = \Yii::$app->request->get('paymentId');
        $payerId = \Yii::$app->request->get('PayerID');

        $apiContext = $this->getApiContext($this->clientId, $this->clientSecret, $this->mode);
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            $result = $payment->execute($execution, $apiContext);
            $this->setTransactionSuccess(Json::encode($result));
            return $this->redirect($this->createSuccessUrl(['id' => $this->transaction->id]));
        } catch (Exception $ex) {
            $this->setTransactionError($ex->getMessage());
            return $this->redirect($this->createErrorUrl(['id' => $this->transaction->id]));
        }

    }

    /**
     * @param $order Order
     * @param $transaction OrderTransaction
     * @return bool|null|string
     */
    private function prepareUrl($order, $transaction)
    {
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $itemList = new ItemList();
        foreach ($order->items as $order_item) {
            /**
             * @var $order_item OrderItem
             */
            $item = new Item();
            $item->setName($order_item->product->name)
                ->setCurrency($this->currency)
                ->setQuantity(1)
                ->setPrice($order_item->total_price);
            $itemList->addItem($item);
        }

        //$amountDetails->setTax('0.06');
        $amountDetails = new Details();
        $amountDetails->setSubtotal($order->total_price - $order->eset_tax);
        $amountDetails->setTax($order->eset_tax);

        $amount = new Amount();
        $amount->setCurrency($this->currency)
            ->setTotal($order->total_price)
            ->setDetails($amountDetails);

        $paypal_transaction = new Transaction();
        $paypal_transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription(\Yii::t('app', 'Payment of order #{orderId}', ['orderId' => $order->id]))
            ->setInvoiceNumber($order->id);

        $redirectUrls = new RedirectUrls();


        // TODO с урлами в платежах какой то бардак, не все обрабутчики присутсвуют
        // в контролере app\modules\shop\controllers\PaymentController
        // так к примеру: CancelUrl
        $redirectUrls
            ->setReturnUrl($this->createResultUrl([
                'id' => $transaction->payment_type_id,
                'transactionId' => $this->transaction->id,
            ]))
            ->setCancelUrl($this->createFailUrl([
                'id' => $this->transaction->id
            ]));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($paypal_transaction));

        $request = clone $payment;

        $apiContext = $this->getApiContext($this->clientId, $this->clientSecret, $this->mode);

        try {
            $payment->create($apiContext);
        } catch (PayPalConnectionException $pce) {
            //echo '<pre>';print_r(json_decode($pce->getData()));exit;
            return false;
        } catch (Exception $ex) {
            return false;
        }

        $payment->
        $approvalUrl = $payment->getApprovalLink();

        return $approvalUrl;
    }


    /**
     * Helper method for getting an APIContext for all calls
     * @param string $clientId Client ID
     * @param string $clientSecret Client Secret
     * @param string $mode sandbox|live
     * @return \PayPal\Rest\ApiContext
     */
    private function getApiContext($clientId, $clientSecret, $mode)
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => $mode,
                'log.LogEnabled' => true,
                'log.FileName' => \Yii::getAlias('@runtime/logs/paypal.log'),
                'log.LogLevel' => YII_DEBUG ? self::LOG_LEVEL_DEBUG : self::LOG_LEVEL_FINE,
                'validation.level' => 'log',
                'cache.enabled' => true,
                'cache.FileName' => \Yii::getAlias('@runtime/cache/paypal.cache'),
            )
        );

        return $apiContext;
    }


}
