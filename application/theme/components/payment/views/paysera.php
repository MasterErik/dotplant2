<?php
/**
 * @var string $url
 */
?>
<div class="row">
    <?= \yii\helpers\Html::a(
        Yii::t('app', 'Confirm payment'),
        $url,
        [
            'class' => 'btn btn-success'
        ]
    ); ?>
</div>