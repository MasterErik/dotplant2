<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 14.05.2015
 * Time: 15:47
 */

namespace app\theme\components\payment;


use app\theme\models\UserProfile;


use WebToPay_Factory;
use WebToPay;

use yii\base\Exception;
use yii\helpers\Json;

use yii\web\BadRequestHttpException;
use Yii;
class PayseraPayment extends AbstractPayment {

    public $projectid;
    public $password;
    public $currency;
    public $mode;
    public $color;

    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';

    public static function redirectToPayment()
    {
        $class = new static;
        $request = WebToPay::redirectToPayment($class->buildData());
    }
    public function content()
    {
        $url = $this->prepareUrl();

        return $this->render(
            'paysera',
            [
                'order' => $this->order,
                'url' => $url,
            ]
        );
    }

    public function checkResult($hash = '')
    {
        //Yii::info('paysera', Yii::$app->request->get('transactionId'));
        $this->transaction = $this->loadTransaction(\Yii::$app->request->get('transactionId'));
        if (!$this->transaction || !$this->transaction->checkHash($hash)) {
            throw new BadRequestHttpException();
        }

        try {
            $response = WebToPay::checkResponse($_GET, [
                'projectid'     => $this->projectid,
                'sign_password' => $this->password,
            ]);
/*
            if ($response['test'] !== $this->mode) {
                throw new Exception('Not the same as mode of payment');
            }
*/
            if ($response['type'] !== 'macro') {
                throw new Exception('Only macro payment callbacks are accepted');
            }

            //$orderId = $response['orderid'];
            //$amount = $response['amount'];
            //$currency = $response['currency'];
            //@todo: patikrinti, ar užsakymas su $orderId dar nepatvirtintas (callback gali būti pakartotas kelis kartus)
            //@todo: patikrinti, ar užsakymo suma ir valiuta atitinka $amount ir $currency
            //@todo: patvirtinti užsakymą


            if ($this->setTransactionSuccess(Json::encode($response))) {
                echo 'OK';
            } else {
                throw new ServerErrorHttpException();
            }
        } catch (Exception $ex) {
            $this->setTransactionError($ex->getMessage());
        }
    }

    private function buildData()
    {
        $data = [
            'projectId'     => $this->projectid,
            'sign_password' => $this->password,
            'orderid'       => $this->order->id,
            'amount'        => (int) ($this->order->total_price * 100),
            'currency'      => $this->currency,
            'country'       => UserProfile::getUserCountry(),
            'payment'       => $this->order->paymentType->key,
            'accepturl'     => $this->createSuccessUrl([
                                'id' => $this->transaction->id]
                                ),
            'cancelurl'     => $this->createFailUrl([
                                'id' => $this->transaction->id
                            ]),
            'callbackurl'   => $this->createResultUrl([
                                'id' => $this->transaction->payment_type_id,
                                'transactionId' => $this->transaction->id,
                                ]),
            'lang'          => self::getLang(),
            'test'          => $this->mode == self::MODE_SANDBOX ? 1 : 0,
        ];
        return $data;
    }

    private static function getLang()
    {
        switch (Yii::$app->language) {
            case 'ru': return 'RUS';
            case 'en': return 'ENG';
            case 'lt': return 'LIT';
            case 'et': return 'EST';
            default:
                return 'ENG';
        }
    }

    /**
     * @return bool|string
     */
    private function prepareUrl()
    {

        try {
            $data = $this->buildData();
            $factory = new WebToPay_Factory(array('projectId' => $this->projectid, 'password' => $this->password));
            $url = $factory->getRequestBuilder()->buildRequestUrlFromData($data);

        } catch (WebToPayException $e) {
            // handle exception
            echo $e->getMessage();
            return false;
        }

        //print_r($url); exit;
        return $url;
    }
}