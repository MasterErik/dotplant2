<?php
namespace app\theme\components;

use Yii;
use yii\web\Request;

/**
 * @inheritdoc
 */
class LangRequest extends Request
{
    private $_lang_url;

    /**
     * @inheritdoc
     */
    public function getUrl()
    {
        if ($this->_lang_url === null) {
            $this->_lang_url = MultiLangHelper::processLangInUrl(parent::getUrl());
        }
        return $this->_lang_url;
    }

    public function getServerName()
    {
        return isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'localhost';
    }

}