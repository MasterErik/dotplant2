<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 30.10.2015
 * Time: 16:53
 */

namespace app\theme\module\events;

use app\modules\core\events\SpecialEvent;

class CustomStageEvent extends SpecialEvent
{
    protected $eventData = [];
    protected $status = false;

    /**
     * @param bool|false $status
     */
    public function setStatus($status = false)
    {
        $this->status = boolval($status);
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array Array of event data that will be passed though application or through js
     */
    public function eventData()
    {
        return $this->eventData;
    }

    public function setEventData(array $eventData)
    {
        $this->eventData = $eventData;
    }

    public function addEventData(array $eventData)
    {
        $this->eventData = array_merge($this->eventData, $eventData);
    }
}
?>