<?php

namespace app\theme\module;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\theme\module\controllers';

    public $controllerMap = [
        'widget' => [
            'class' => 'app\theme\widgets\controllers\WidgetController',
        ],
    ];

    public function init()
    {
        parent::init();

        $this->modules = [
            'user' => ['class' => 'app\theme\module\user\User'],
            'cabinet' => ['class' => 'app\theme\module\user\User'],
            'backend' => [
                'class' => 'app\theme\module\backend\Backend',
                'layout' => '@app/backend/views/layouts/main',
            ],
        ];
        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerMap = [];
        }

        /*
                $this->controllerMap = [
                    'widget' => [
                        'class' => 'app\theme\widgets\controllers\WidgetController',
                    ],
                ];
        */
    }
}
