<?php
return [
    'bootstrap' => ['log'],
    'modules' => [
        'eset' => [
            'class' => 'app\theme\module\Module',
            'controllerNamespace' => 'app\theme\commands'
        ],
        'financial' => [
            'class' => 'app\theme\module\financial\FinancialModule'
        ],
    ],
    'components' => [
        'old_database_ee' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=d6381.mysql.zone.ee;dbname=d6381sd4126',
            'username' => 'd6381sa85824',
            'password' => '6606703439739Qq',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
        'old_database_lv' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=d8692.mysql.zone.ee;dbname=d8692sd34841',
            'username' => 'd8692sa90105',
            'password' => '6606703439739Qq',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],

    ]
];