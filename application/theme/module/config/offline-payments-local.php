<?php
return [
    [
        'class' => 'app\theme\components\payment\offline\EmailNotifications',
        'partner_id' => 1,

        'mailbox' => '{mail.zone.ee:110/pop3}INBOX',
        'username' => 'bank_notifications@nod32.ee',
        'password' => 'Q84Fba',

//        'mailbox' => '{safertechnology.eu:110/pop3/novalidate-cert}INBOX',
//        'username' => 'noreply@safertechnology.eu',
//        'password' => 'noreply123456',
        'parsers' => [
            [
                'class' => 'app\theme\components\payment\offline\parsers\SwedbankParser',
                'from' => [
                    'alerts@swedbank.ee',
                    'alerts@hansa.ee',
                    'test-alerts@hansa.ee',
                    'pank_teavitus@initec.ee',
                    'prikotov@gmail.com'
                ],
            ],
            [
                'class' => 'app\theme\components\payment\offline\parsers\SebParser',
                'from' => [
                    'info@seb.ee',
                    'automailer@seb.ee',
                    'pank_teavitus@initec.ee'
                ],
            ],
        ]
    ],
//    [
//        'class' => 'app\theme\components\payment\offline\EmailNotifications',
//        'partner_id' => 1,
//        'mailbox' => '{mail.zone.ee:110/pop3}INBOX',
//        'username' => 'bank_notifications@nod32.ee',
//        'password' => 'Q84Fba',
//        'parsers' => [
//            [
//                'class' => 'app\theme\components\payment\offline\parsers\SwedbankParser',
//                'from' => [
//                    'alerts@swedbank.ee',
//                    'alerts@hansa.ee',
//                    'test-alerts@hansa.ee',
//                    'pank_teavitus@initec.ee'
//                ],
//            ],
//            [
//                'class' => 'app\theme\components\payment\offline\parsers\SebParser',
//                'from' => [
//                    'info@seb.ee',
//                    'automailer@seb.ee',
//                    'pank_teavitus@initec.ee'
//                ],
//            ],
//        ]
//    ],
];