<?php
use yii\helpers\ArrayHelper;

$config = [
    'timeZone' => 'Europe/Tallinn',
    'language' => 'en',
    'sourceLanguage' => 'en',
    'components' => [
        'i18n' => [
            'translations' => [
                'eset' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true,
                    'basePath' => '@app/theme/messages',
                    'fileMap' => [
                        'eset' => 'eset.php',
                    ],
                ],
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'yii' => 'yii.php',
                    ],
                ],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'log' => [
            'targets' => [

                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'mailer' => 'mail',
                    'levels' => ['error'],
                    'categories' => ['yii\db\Exception*'],
                    'message' => [
                        'to' => ['erik@eset.ee'],
                        'subject' => 'Log message: Error db connection',
                    ],
                ],

                'db' => [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['yii\web\HttpException:*', 'yii\db\Exception*', 'eset'], //'yii\i18n\I18N\*',
                    'prefix' =>  function () {
                        return isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : Yii::$app->requestedRoute;
                    },
                    'logVars' => [],
                    'logTable' => '{{%system_log}}'
                ],

                'all' => [
                    'class' => 'yii\log\FileTarget',
                    'logVars' => [],
                    'levels' => ['error', 'warning'],
                ],
/*
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['eset'],
                    'levels' => ['info'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/eset_info.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['eset'],
                    'levels' => ['error', 'warning'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/eset_error.log',
                ],
*/
            ]
        ],

        'restClient' => [
            'class' => 'app\theme\components\restClient\RestClient',
            'error' => 222,
            'configuration' => [
                'server' => YII_ENV == 'dev' ? 'dev' : 'work',
            ]
        ],

        'cache' => [
            'class' => YII_ENV ? 'yii\caching\DummyCache' : 'yii\caching\MemCache',
        ],
    ],
];

return $config;