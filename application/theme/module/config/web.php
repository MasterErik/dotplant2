<?php
use yii\helpers\ArrayHelper;

$config = [
    'bootstrap' => ['log', 'debug'],
    'modules' => [
        'eset' => [
            'class' => 'app\theme\module\Module',
        ],
        'financial' => [
            'class' => 'app\theme\module\financial\FinancialModule'
        ],

        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1'],
            'panels' => [
            ],
        ],
        //'gii' => 'yii\gii\Module',
    ],

    //'defaultRoute' => '/catalog/personal',
    //'defaultRoute' => '/shop/product/list',
    'components' => [
        'assetManager' => [
            'forceCopy' => false, //YII_DEBUG,
        ],

        'request' => [
            'class' => 'app\theme\components\LangRequest',
        ],
        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'enableStrictParsing' => true,
            'class'=>'app\theme\components\LangUrlManager',
            'rules' => [
                'license' => 'eset/license',

                'shop/cabinet/profile' => 'eset/user/cabinet/profile',
//                'user/user/profile' => 'eset/user/cabinet/profile',
                'signup' => 'eset/user/user/signup',
                'confirm' => 'eset/user/user/confirm',
                //'login' => 'eset/user/user/login',

                'user/user/login' => 'eset/user/user/login',
                'user/user/signup' => 'eset/user/user/signup',
                'user/user/complete-registration' => 'eset/user/user/complete-registration',

                'shop/cart/add' => 'eset/cart/add',
                'shop/cart/stage' => 'eset/cart/stage',
                'shop/cart' => 'eset/cart',

                'shop/payment/error' => 'eset/payment/error',
                'shop/payment/success' => 'eset/payment/success',
                'shop/payment' => 'eset/payment',
            ],
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/theme/views/modules/basic',
                    '@app/modules/page/views' => '@app/theme/views/modules/page',
                    '@app/modules/payment/views' => 'app/theme/views/modules/payment',
                    '@app/modules/shop/views' => '@app/theme/views/modules/shop',
//                    '@app/modules/user/views' => '@app/theme/views/modules/user',
                    '@app/widgets' => '@app/theme/views/widgets',
                    //'@app/extensions/DefaultTheme/widgets' => '@app/theme/views/widgets',
                ],
                //'baseUrl' => '@webroot/theme/views/controllers',
            ],
        ],

        'user' => [
            'class' => '\yii\web\User',
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/eset/user/user/login'],
            'on '. \yii\web\User::EVENT_AFTER_LOGIN => ['\app\theme\models\User', 'AfterLogin'],
        ],

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOpenId'
                ],
                'facebook' => [
//                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1002992039750948',
                    'clientSecret' => '9a05dc81a0c20b5c7ec36298dcfdf45f',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => 'twitter_consumer_key',
                    'consumerSecret' => 'twitter_consumer_secret',
                ],
                'linkedin' => [
                    'class' => 'yii\authclient\clients\LinkedIn',
                    'clientId' => 'linkedin_client_id',
                    'clientSecret' => 'linkedin_client_secret',
                ],

// посмотреть https://github.com/kotchuprik/yii2-odnoklassniki-authclient
//                'odnoklassniki' => [
//                    // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
//                    // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
//                    'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
//                    //'clientId' => '...',
//                    //'clientSecret' => '...',
//                    //'clientPublic' => '...',
//                    //'title' => 'Odnoklas.',
//                ],
            ],
        ],
    ],
    'on '. \yii\web\Application::EVENT_BEFORE_REQUEST => ['app\theme\components\restClient\config\StaticConfig', 'setReseller'],
];

$allConfig = ArrayHelper::merge(
    file_exists(__DIR__ . '/kartik.php') ? require(__DIR__ . '/kartik.php') : [],
    $config);

return $allConfig;