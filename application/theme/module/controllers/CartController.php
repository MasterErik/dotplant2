<?php

namespace app\theme\module\controllers;

use app\modules\shop\models\Currency;
use app\modules\shop\models\OrderCode;
use app\modules\shop\models\Product;

use app\theme\components\license\calculators\CumulativeLicenseCalculator;
use app\theme\components\license\DefaultValues;
use app\theme\components\license\proxy\OrderItemProxy;
use app\theme\models\EmailConfirmCode;
use app\theme\models\License;
use app\theme\models\Order;
use app\theme\models\OrderItem;
use app\theme\models\OrderStage;
use app\theme\models\RegistrationForm;
use app\theme\models\search\Select2Search;

use app\theme\services\Synchronization;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class CartController
 * @package app\theme\module\controllers
 */
class CartController extends \app\modules\shop\controllers\CartController
{

    const FormId = 'orderAttributesFormId';

    public function getViewPath()
    {
        return '@app/theme/views/modules/shop/' . $this->id;
        //return '@app/modules/shop/views/' . $this->id;
    }

    /**
     * @param bool|false $create
     * @param bool|true $throwException
     * @param null $id
     * @return Order
     * @throws NotFoundHttpException
     */
    public function loadOrder($create = false, $throwException = true, $id = null)
    {
        $model = Order::getOrder($create, $id);
        if (is_null($model) && ($throwException || !is_null($id))) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    /**
     * @inheritdoc
     * @return OrderItem
     */
    protected function loadOrderItem($id, $checkOrderAttachment = true)
    {
        /** @var OrderItem $orderItemModel */
        $orderModel = $checkOrderAttachment ? $this->loadOrder() : null;
        $orderItemModel = OrderItem::findOne($id);
        if (is_null($orderItemModel)
            || ($checkOrderAttachment && (is_null($orderModel) || $orderItemModel->order_id != $orderModel->id))
        ) {
            throw new NotFoundHttpException;
        }
        return $orderItemModel;
    }

    protected function saveOrder(Order $model)
    {
        if ($model->load(Yii::$app->request->post('Order'))) {
            $model->save();
        }
    }

    /**
     * Delete OrderItem action.
     * @param int $id
     * @throws NotFoundHttpException
     * @throws \Exception
     * @return array
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $order = $this->loadOrder();
        if (is_null($order->stage) || $order->stage->immutable_by_user == 1) {
            throw new BadRequestHttpException;
        }

        if (License::cleanTrash($id) && $this->loadOrderItem($id)->delete() && $order->calculate(true)) {
            $mainCurrency = Currency::getMainCurrency();
            return [
                'success' => true,
                'itemsCount' => $order->items_count,
                'totalPrice' => $mainCurrency->format($order->total_price),
                'itemModalPreview' => $this->renderPartial("item-modal-preview",
                    [
                        "order" => $order,
                        "orderItem" => null,
                        "product" => null
                    ]
                )
            ];
        } else {
            return [
                'success' => false,
                'message' => Yii::t('app', 'Cannot change additional params'),
            ];
        }
    }

    protected function saveOrderCode(Order $model)
    {
        $orderCode = null;
        if (Yii::$app->request->post('ordercode-code')) {
            /** @var OrderCode $orderCode */
            $orderCode = OrderCode::find()->where(['order_id' => $model->id, 'status' => 1])->one();
            if ($orderCode === null) {
                $orderCode = new OrderCode();


                $orderCode->load(Yii::$app->request->post());
                $orderCode->order_id = $model->id;
                if ($orderCode->save()) {
                    $model->calculate();
                    $this->refresh();
                }

            }
        }
        return $orderCode;
    }

    /**
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $order = $this->loadOrder(false, false);

        if (empty($order)) {
            throw new BadRequestHttpException(Yii::t('eset', 'Order not found'));
        }

        //$orderCode = $this->saveOrderCode($order);

        return $this->redirect($this->toStage());
    }

    public function actionAddFast($id = null)
    {
        $order = $this->loadOrder(true);
        $error = $order->addProduct($id);
        if (empty($error)) {
            return $this->redirect(Url::toRoute(['stage']));
        } else {
            Yii::$app->session->setFlash('error', $error);
        }
    }

    public function actionAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!is_array(Yii::$app->request->post('products', null))) {
            throw new BadRequestHttpException;
        }

        $order = $this->loadOrder(true);
        $stage = $order->stage;
        if (!(0 === ($stage->immutable_by_manager & $stage->immutable_by_assigned))) {
            return Yii::t('eset', 'Error acquired. Adding new items to cart is restricted until current order is complete. Please contact customer support.');
        }
        $initStage = OrderStage::getInitialStage();
        $order->order_stage_id = $initStage->id;

        return $this->addProductsToOrder(Yii::$app->request->post('products'));
    }

    protected function addProductsToOrder($products, $parentId = 0)
    {

        if (!is_array($products)) {
            throw new BadRequestHttpException;
        }
        $order = $this->loadOrder(true);
        $result = [
            'errors' => [],
            'itemModalPreview' => '',
        ];

        foreach ($products as $product) {
            if (!isset($product['id']) || is_null($productModel = Product::findById($product['id']))) {
                $result['errors'][] = Yii::t('app', 'Product not found.');
                continue;
            }
            /** @var Product $productModel */
//            $quantity = isset($product['quantity']) && $product['quantity'] > 0 ? $product['quantity'] : 1;

            $orderItem = new OrderItem;
            $orderItem->attributes = [
                'parent_id' => $parentId,
                'order_id' => $order->id,
                'product_id' => $productModel->id,
//                'quantity' => $quantity,
//                'price_per_pcs' => PriceHelper::getProductPrice(
//                    $productModel,
//                    $order,
//                    1,
//                    SpecialPriceList::TYPE_CORE
//                ),
            ];

            $orderItem->initAttributes();
            $orderItemProxy = new OrderItemProxy([
                'owner' => &$orderItem,
                'owner_order' => &$order,
            ]);

            DefaultValues::initOrderItem($orderItemProxy);

            $order->items_count += 1;

            if (!$order->save() || !$orderItem->save()) {
                $result['errors'][] = Yii::t('app', 'Cannot save order item.');
            }
            if (isset($product['children'])) {
                $result = ArrayHelper::merge(
                    $result,
                    $this->addProductsToOrder($product['children'], $orderItem->id)
                );
            }
        }

        $mainCurrency = Currency::getMainCurrency();
        return ArrayHelper::merge(
            $result,
            [
                'itemsCount' => $order->items_count,
                'success' => empty($result['errors']),
                'totalTax' => $mainCurrency->format($order->eset_tax),
                'totalPrice' => $mainCurrency->format($order->total_price),
            ]
        );
    }

    public function actionSendCode($email)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (EmailConfirmCode::sendCode($email)) {
            return [
                'success' => true,
                'message' => Yii::t('eset', 'Wait for the confirmation code in the email.'),
            ];
        } else {
            return [
                'success' => false,
                'message' => Yii::t('eset', 'Error send code.'),
            ];

        }
    }

    public function actionRegistration()
    {
        $model = new RegistrationForm(['scenario' => 'cart']);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->username = $model->email;
            $model->locale = Yii::$app->language;
            if ($model->signup()) {
                return $this->redirect('/eset/cart');
            }
        }

        $order = $this->loadOrder();

        return $this->render(
            'registration',
            [
                'model' => $model,
                'model_order' => $order,
            ]
        );

    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetPrice()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $collection = null;

        $order = $this->loadOrder();
        $order->setCartFormScenario();
        $old_user_id = $order->user_id;

        $item_id = null;
        $item_id = Yii::$app->request->post('item_id');
        if (isset($item_id) && !empty($item_id) && !($item_id == 'undefined')) {
            $orderItem = $this->loadOrderItem($item_id);
            if (isset($orderItem)) {
                $orderItem->initAttributes();
                $orderItem->load(Yii::$app->request->post()['OrderItem'][$item_id], '');
                $orderItem->normalizationType();
                if (!$orderItem->save()) {
                    return [
                        'success' => false,
                        'message' => Yii::t('eset', 'Cannot change cart item: ') . $this->glueErrors($orderItem->getErrors()),
                    ];
                }
                $collection = [new OrderItemProxy([
                    'owner' => $orderItem,
                    'owner_order' => $order,
                ])];
            }
        } elseif ($order->load(Yii::$app->request->post())) {
            if (!$order->save()) {
                return [
                    'success' => false,
                    'message' => Yii::t('eset', 'Cannot change cart: ') . $this->glueErrors($order->getErrors()),
                ];
            } else {
                if (!empty($order->user_id) && ($order->user_id != 0) && ($old_user_id != $order->user_id)) {
                    Synchronization::syncLicenseByUser($order->user_id);
                }
            }
            $collection = $order->getCollection();
        }


        $mainCurrency = Currency::getMainCurrency();
        $prices = $errors = [];
        if ($collection) {
            $calculator = new CumulativeLicenseCalculator(['licenseCollection' => $collection]);
            $calculator->run(['price']);
            foreach ($calculator->licenseCollection as $proxy) {
                $prices[$proxy->owner->id] = $mainCurrency->format($proxy->getPrice());
                if ($proxy->hasErrors()) {
                    $errors[$proxy->owner->id] = implode(', ', $proxy->getErrors());
                }
                $proxy->save();
            }
        }
        // $order->checkFill()
        $order->calculate();
        return $order->calculate(true) ? [
            'success' => true,
            'totalTax' => $mainCurrency->format($order->eset_tax),
            'totalPrice' => $mainCurrency->format($order->total_price),
            'prices' => $prices,
            'errors' => $errors,
        ] : [
            'success' => false,
            'message' => Yii::t('eset', 'Cannot change cart:') . array_reduce($order->getErrors(),
                    function ($result, $item) {
                        $result .= $item[0];
                        return $result;
                    },
                    ''
                ),
        ];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionGetParentLicense()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;


        $search = Yii::$app->request->post('search');
        $page = (int)Yii::$app->request->post('page');

        $order = $this->loadOrder();
        $order->setCartFormScenario();

        $item_id = Yii::$app->request->post('item_id');

        //$item = Yii::$app->request->post('OrderItem['.$item_id.']');
        //$item = Yii::$app->request->post('OrderItem');
        $orderItem = $this->loadOrderItem($item_id);
        $orderItem->initAttributes();

        if ($order->validate() && $orderItem->validate()) {

            $proxy = new OrderItemProxy([
                'owner' => $orderItem,
                'owner_order' => $order,
            ]);

            //Already called
            //License::syncByRest($proxy->userId);

            /** @var \app\theme\models\query\LicenseQuery $query */
            $query = License::find()->select2($proxy, $search);

            /** @var \app\theme\models\License $parents */
            $parents = $query
                ->limit(Select2Search::PAGE_SIZE)
                ->offset(($page - 1) * Select2Search::PAGE_SIZE)
                ->all();

            $results = [];
            foreach ($parents as $parent) {
                $results[] = [
                    'id' => $parent->id,
                    'text' => $parent->toString()
                ];
            }

            return [
                'items' => $results,
                'total_count' => $query->count()
            ];
        } else {

            return [
                'items' => [],
                'total_count' => 0
            ];

        }
    }

    public function actionChangeQuantity()
    {
        throw new NotSupportedException('Change quantity is not implemented.');
    }

    /**
     * @param array $errors is string
     * @return string
     */
    private function glueErrors($errors)
    {
        $e = [];
        foreach ($errors as $error) {
            $e[] = $error[0];
        }
        return implode('; ', $e);
    }


	/**
     * @param null $leaf_id
     * @return string|Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionStage($leaf_id = null)
    {
        $order = self::loadOrder();
        if (!isset($order)) {
            throw new BadRequestHttpException(Yii::t('eset', 'Order not found'));
        }

        $order_id = $order->id;

        if (empty($order_id)) {
            throw new BadRequestHttpException(Yii::t('eset', 'Order incorrect'));
        }

        $orderCode = $this->saveOrderCode($order);
        $eventData = ['order' => $order, 'orderCode' => $orderCode, 'formId' => self::FormId];

        $orderStage = null;
        if (!empty($leaf_id)) {
            //Execute event leaf
            $orderStage = $order->goLeaf($leaf_id, $eventData, Yii::$app->request->get('previous'));
        } else if (!empty($order->stage)) {
            //Current order stage
            $orderStage = $order->stage;
        } else {
            //Initialization order
            $orderStage = $order->goInitStage();
        }

        $compete = true;
        if ($orderStage) {
            $compete = $orderStage->executeEventStage($eventData);
        }

        if (!empty($eventData['__redirect'])) {
            return $this->redirect($eventData['__redirect']);
        }

        return $this->renderStage($eventData, $orderStage);

    }
    /**
     * @param $eventData
     * @param $orderStage
     * @return string
     */
    protected function renderStage($eventData, $orderStage)
    {
        return $this->render(
            'stage',
            [
                'order' => $eventData['order'],
                'stage' => $orderStage,
                'eventData' => $eventData,
            ]
        );
    }

	/**
     * @param null $id
     * @return string
     */
    protected function toStage($id = null)
    {
        return Url::toRoute(['stage', 'leaf_id' => $id]);
    }

}
