<?php

namespace app\theme\module\controllers;

use app\theme\components\invoice\Export\Export;
use app\theme\models\InvoiceDetail;
use app\theme\models\InvoiceDownloadForm;
use app\theme\models\search\InvoiceDetailSearch;
use app\theme\models\UserProfile;
use kartik\mpdf\Pdf;
use Yii;
use app\theme\models\Invoice;
use app\theme\models\search\InvoiceSearch;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    public $layout = 'main-cabinet';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['base'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoiceSearch();
        $dataInvoicesProvider = $searchModel->search(Yii::$app->request->queryParams);

        if( Yii::$app->request->isAjax && Yii::$app->request->get('_pjax') == '#invoice-grid' ) {
            return $this->renderPartial('_grid', [
                'dataProvider' => $dataInvoicesProvider,
                'searchModel' => $searchModel,
            ]);
        }

        if( UserProfile::iPartner() || UserProfile::iReseller() ) {
            $dataPaymentsProvider = $searchModel->searchPayments(Yii::$app->request->queryParams);
            if( Yii::$app->request->isAjax && Yii::$app->request->get('_pjax') == '#payments-grid' ) {
                return $this->renderPartial('_grid_payments', [
                    'dataProvider' => $dataPaymentsProvider,
                    'searchModel' => $searchModel,
                ]);
            }

            $downloadModel = new InvoiceDownloadForm();

            return $this->render('index', [
                'dataInvoicesProvider' => $dataInvoicesProvider,
                'dataPaymentsProvider' => $dataPaymentsProvider,
                'searchModel' => $searchModel,
                'downloadModel' => $downloadModel
            ]);

        }

        return $this->render('index_normal_user', [
            'dataProvider' => $dataInvoicesProvider,
            'searchModel' => $searchModel,
        ]);

    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new InvoiceDetailSearch();
        $searchModel->invoice_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['type' => SORT_ASC];

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoice();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        $pdf = new Pdf([
            //'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'mode' => Pdf::MODE_UTF8,
            //'defaultFontSize' => 8,
            'content' => $this->renderPartial('pdf/main', [
                'model' => $this->findModel($id),
                'items' => InvoiceDetail::find()->where(['invoice_id' => $id])->all(),
            ]),
            //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => 'body{font-size:9pt}',
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'showImageErrors' => true,
            ],
            'marginBottom' => 40,
            'methods' => [
                //'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
                'SetFooter' => [
                    [
                        'odd' => [
                            'L' => [
                                'content' => $this->renderPartial('pdf/footer_left'),
                                //'font-size' => 9,
                                //'font-style' => 'B',
                                //'font-family' => 'serif',
                                'color' => '#000000'
                            ],
                            'C' => [
                                'content' => $this->renderPartial('pdf/footer_center'),
                                //'font-size' => 9,
                                //'font-style' => 'B',
                                //'font-family' => 'serif',
                                'color' => '#000000'
                            ],
                            'R' => [
                                'content' => $this->renderPartial('pdf/footer_right'),
                                //'font-size' => 9,
                                //'font-style' => 'B',
                                //'font-family' => 'serif',
                                'color' => '#000000'
                            ],
                            'line' => 1,
                        ],
                        [
                            'even' => [],
                        ]
                    ]
                ],
            ]
        ]);


        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render();
    }

    public function actionDownload()
    {
        $model = new InvoiceDownloadForm();
        $model->load(Yii::$app->request->post());

        $export = new Export([
            'format' => $model->format,
            'invoices' => $model->getInvoiceQuery()
        ]);

        if( !$export->validate() ) {
            throw new BadRequestHttpException(print_r($export->getErrors(),1));
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xls');
        $headers->add('Content-Disposition', 'attachment; filename="'.$model->genFilename().'"');
        $a = $export->data;
        return $export->data;
    }

}
