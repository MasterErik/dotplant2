<?php

namespace app\theme\module\controllers;

use app\theme\components\commands\CommandList;
use kartik\grid\EditableColumnAction;
use Yii;
use app\theme\models\License;
use app\theme\models\search\LicenseSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LicenseController implements the CRUD actions for License model.
 */
class LicenseController extends Controller
{
    public $layout = 'main-cabinet';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['base'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Lists all License models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LicenseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if( Yii::$app->request->isAjax ) {
            return $this->renderPartial('_grid', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editnote' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => LicenseSearch::className(),       // the model for the record being edited
                'showModelErrors' => true,                        // show model validation errors after save
                //'errorOptions' => ['header' => ''],                // error summary HTML options
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }
    /**
     * Displays a single License model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $action = Yii::$app->request->post('action');
        $email = Yii::$app->request->post('email');
        $eventData = CommandList::exec($id, $action, $email);
        if (!empty($eventData['__redirect'])) {
            return $this->redirect($eventData['__redirect']);
        }
        if (Yii::$app->request->isPost ) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the License model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return License the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = License::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
