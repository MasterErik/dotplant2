<?php

namespace app\theme\module\controllers;

use app\modules\image\widgets\RemoveAction;
use app\modules\image\widgets\UploadAction;
use app\modules\image\widgets\views\AddImageAction;
use app\theme\models\UserProfile;
use Yii;
use app\theme\models\ResellerStatus;
use app\theme\models\search\ResellerStatusSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ResellerStatusController implements the CRUD actions for ResellerStatus model.
 */
class ResellerStatusController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['base'], // Only authorized users //'@'
                        'verbs' => ['POST', 'GET']
                    ],
                    [
                        'allow' => false, // For all other prohibited
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
//        return [
//            'addImage' => [
//                'class' => AddImageAction::className(),
//            ],
//            'upload' => [
//                'class' => UploadAction::className(),
//                'upload' => 'theme/resources/reseller-status-images',
//            ],
//            'remove' => [
//                'class' => RemoveAction::className(),
//                'uploadDir' => 'theme/resources/product-images',
//            ],
//        ];
    }

    /**
     * Lists all ResellerStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResellerStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ResellerStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ResellerStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ResellerStatus();
        $res = $model->load(Yii::$app->request->post());
        $model->partner_id = UserProfile::myProfile()->myParentPartner()->user_id;
        if ($res && $model->save() && !$model->hasErrors()) {
            Yii::$app->session->setFlash('success', Yii::t('eset', 'Reseller status created'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ResellerStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->hasErrors()) {
            Yii::$app->session->setFlash('success', Yii::t('eset', 'Reseller status updated'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ResellerStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ResellerStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ResellerStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ResellerStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
