<?php

namespace app\theme\module\controllers;

use app\models\User;
use app\models\UserService;
use app\theme\models\RegistrationForm;
use app\theme\models\UserProfile;
use Yii;
use yii\base\Security;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DefaultController extends Controller
{
	public function actions()
	{
		return [
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'successCallback'],
			],
		];
	}

	public function actionRestDebug()
	{
		if (Yii::$app->request->isAjax) {
			$method = Yii::$app->request->get('method');
			$params = json_decode(Yii::$app->request->get('params'), true);
			return json_encode(Yii::$app->restClient->execute($method, $params));
		} else {
			return $this->render('rest-debug');
		}

	}


	public function actionIndex()
	{
		//$this->layout = '@app/backend/views/layouts/main';
		return $this->render('index');
	}

//	public function actionSignup()
//	{
//		$model = new RegistrationForm();
//
//		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
//			Yii::$app->response->format = Response::FORMAT_JSON;
//			return ActiveForm::validate($model);
//		}
//
//		if ($model->load($_POST) && $model->signup()) {
//			if (Yii::$app->user->isGuest) {
//				\Yii::$app->session->setFlash('success', Yii::t('eset', 'You will be sent an email confirming registration.'));
//				return $this->goHome();
//			}
//			\Yii::$app->session->setFlash('success', Yii::t('eset', 'The user is sent a letter confirming registration.'));
//			$model = new RegistrationForm();
//		}
//
//		return $this->render(
//				'signup',
//				[
//					'model' => $model,
//				]
//		);
//	}

//	public function actionConfirm($code)
//	{
//		$user_profile = UserProfile::findOne(['confirm_registration_key' => $code]);
//		if ($user_profile) {
//			$transaction = Yii::$app->db->beginTransaction();
//			User::updateAll(['status' => User::STATUS_ACTIVE], ['id' => $user_profile->user_id]);
//			UserProfile::updateAll(['confirm_registration_key' => ''], ['user_id' => $user_profile->user_id]);
//			$transaction->commit();
//
//			$user = User::findOne($user_profile->user_id);
//			if ($user && Yii::$app->user->login($user, 0)) {
//				\Yii::$app->session->setFlash('success', Yii::t('eset', 'Registration completed'));
//			} else {
//				\Yii::$app->session->setFlash('error', Yii::t('eset', 'Registration failed'));
//			}
//		} else {
//			\Yii::$app->session->setFlash('error', Yii::t('eset', 'The confirmation code is not valid'));
//		}
//
//		return $this->goHome();
//
//		//TODO здесь можно сделать вьюшку говорящую поздравляющую с регистрацией и предлагающей следующие действия
////        return $this->render(
////                'registration_success',
////                [
////                    'model' => $UserProfile,
////                ]
////        );
//	}

//	public function successCallback($client)
//	{
//		$userAttributes = $client->getUserAttributes();
//		$serviceType = $client->className();
//		$serviceId = $userAttributes['id'];
//		$userService = UserService::findOne(
//			[
//				'service_type' => $serviceType,
//				'service_id' => $serviceId,
//			]
//		);
//
//		if (is_null($userService)) {
//			if (Yii::$app->user->isGuest) {
//				preg_match('#^(.+?)([^\\\\]+)$#', $serviceType, $service);
//				switch ($service[2]) {
//					case 'GoogleOpenId':
//					case 'VK':
//					case 'Facebook':
//						$firstName = $userAttributes['first_name'];
//						$lastName = $userAttributes['last_name'];
//						$email = isset($userAttributes['email']) ? $userAttributes['email'] : '';
//						break;
//					case 'YandexOpenId':
//						$firstName = $userAttributes['name'];
//						$lastName = '';
//						$email = $userAttributes['email'];
//						break;
//					default:
//						$firstName = '';
//						$lastName = '';
//						$email = '';
//				}
//
//                //$user = new User(['scenario' => 'registerService']);
//                $user = new \app\theme\models\User(['scenario' => 'registerService']);
//                $security = new Security();
//				$user->attributes = [
//					'email' => $email,
//                    'username' => $user->getUniqueUsername($firstName),
//                    'password' => $security->generateRandomString(16),
//					'first_name' => $firstName,
//					'last_name' => $lastName,
//				];
//				if(!$user->save()) {
//                    print_r($user->getErrors()); exit;
//                    throw new Exception(print_r($user->getErrors(),1));
//                }
//				$userService = new UserService;
//				$userService->setAttributes(
//					[
//						'user_id' => $user->id,
//						'service_type' => $serviceType,
//						'service_id' => (string)$serviceId,
//					]
//				);
//                if(!$userService->save()) {
//                    print_r($userService->getErrors()); exit;
//                    throw new Exception(print_r($userService->getErrors(),1));
//                }
//			} else {
//				$userService = new UserService;
//				$userService->setAttributes(
//					[
//						'user_id' => Yii::$app->user->id,
//						'service_type' => $serviceType,
//						'service_id' => (string)$serviceId,
//					]
//				);
//                if(!$userService->save()) {
//                    throw new Exception(print_r($userService->getErrors(),1));
//                }
//				Yii::$app->user->setReturnUrl(['cabinet/profile']);
//			}
//		} elseif (!Yii::$app->user->isGuest) {
//			$userService->delete();
//			Yii::$app->user->setReturnUrl(['cabinet/profile']);
//		}
//
//		if (!is_null($userService->user)) {
//			if (Yii::$app->user->login($userService->user, 0)) {
//				if (!UserProfile::find()->where(['user_id' => $userService->user_id])->exists()) {
//					// если не заполнен UserProfile то просим заполнить
//					Yii::$app->user->setReturnUrl(['cabinet/profile']);
//				}
//			}
//		}
//
//	}

}
