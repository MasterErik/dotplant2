<?php

/** @var \app\modules\config\models\Configurable $configurable */
/** @var \app\backend\components\ActiveForm $form */
/** @var \app\theme\module\financial\models\ConfigConfigurationModel $model */

use app\backend\widgets\BackendWidget;

?>



<div class="row">
    <div class="col-md-6 col-sm-12">
        <?php BackendWidget::begin(['title' => Yii::t('app', 'Main settings'), 'options' => ['class' => 'visible-header']]); ?>
        <?= $form->field($model, 'iban') ?>
        <?php BackendWidget::end() ?>
    </div>
</div>