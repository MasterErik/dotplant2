<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 22.4.2016
 * Time: 16:38
 */

namespace app\theme\module\financial;


use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Module;

class FinancialModule extends Module
{
    public $iban = 'EE981010220047491015';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'configurableModule' => [
                'class' => 'app\modules\config\behaviors\ConfigurableModuleBehavior',
                'configurationView' => '@app/theme/modules/financial/views/configurable/_config',
                'configurableModel' => 'app\theme\module\financial\models\ConfigConfigurationModel',
            ]
        ];
    }
}