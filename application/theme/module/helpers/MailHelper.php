<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 28.3.2016
 * Time: 13:51
 */

namespace app\theme\module\helpers;


use Yii;

class MailHelper
{
    /**
     * @param string $email
     * @param string $template
     * @param array|null $params
     * @param string|null $subject
     * @param string|null $lang
     * @param string|null $file
     * @return bool
     */
    public static function Send($email, $template, $params = null, $subject = null, $lang = null, $file = null) {
        if (empty($lang)) {
            $lang = Yii::$app->language;
        }
        $view = $template . '_' . $lang;
        if (!file_exists($view)) {
            $view = $template . '_' . 'en';
        }

        $mailComponent = Yii::$app->getMailer();
        $message = $mailComponent->compose($view, $params);
        if (!empty($file)) {
            $message->attach($file);
        }
        $status = $message
            ->setFrom(Yii::$app->getModule('core')->emailConfig['mailFrom'])
            ->setTo($email)
            ->setSubject($subject)
            ->send();
        return $status;
    }
}