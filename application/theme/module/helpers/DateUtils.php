<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 29.3.2016
 * Time: 15:20
 */

namespace app\theme\module\helpers;


use DateTime;

class DateUtils
{
    public static $TIME_FROM = [0, 0, 0];
    public static $TIME_TO = [23,59,59];
    public static $DATE_RANGE_SEPARATOR = ' - ';
    const FMT_TIMESTAMP = 1;
    const FMT_INTEGER = 2;

    /**
     * @return string
     */
    public static function BegDate() {
        return strftime('%Y-%m-%d', strtotime('now'));
    }

    /**
     * @return string|mixed
     */
    public static function CurEndDay()
    {
        return date('Y-m-d H:i:s', strtotime('midnight first day of'));
    }
    /**
     * @param integer  $period
     * @return string
     */
    public static function EndDate($period)
    {
        return strftime('%Y-%m-%d', strtotime('now +' . (int)$period . ' month'));
    }

    /**
     * @param string $date
     * @return string format date MM-DD-YYYY
     */
    public static function formatRestDate($date)
    {
        return strftime('%m/%d/%Y', strtotime($date));
    }
    /**
     * @param string $date
     * @param string $format
     * @return string format date MM-DD-YYYY
     */
    public static function formatDate1($date, $format = '%Y-%m-%d')
    {
        return strftime($format, strtotime($date));
    }

    /**
     * @param $value string
     * @param int $format
     * @param $time array
     * @return DateTime|string|integer
     */
    public static function formatDate($value, $format, $time = [0, 0, 0])
    {
        /** @var DateTime $date */
        $date = new DateTime(trim($value));
        $date->setTime($time[0], $time[1], $time[2]);
        if (static::FMT_TIMESTAMP == $format) {
            $value = $date->format('Y-m-d H:i:s');
        } else if (static::FMT_INTEGER == $format) {
            $value = $date->getTimestamp();
        }
        return $value;
    }

    /**
     * @param $value string
     * @param int $format
     * @return DateTime|string|integer
     */
    public static function formatDateTime($value, $format)
    {
        $date = new DateTime(trim($value));
        if (static::FMT_TIMESTAMP == $format) {
            $value = $date->format('Y-m-d H:i:s');
        } else if (static::FMT_INTEGER == $format) {
            $value = $date->getTimestamp();
        }
        return $value;
    }
    /**
     * @param $value string
     * @param $time array
     * @return DateTime
     */
    public static function convertDate($value, $time)
    {
        /** @var DateTime $date */
        $date = new DateTime(trim($value));
        $date->setTime($time[0], $time[1], $time[2]);
        return $date;
    }

    public static function PeriodOfMonths($date_from, $date_to)
    {
        $date_from = self::convertDate($date_from, static::$TIME_FROM);
        $date_to = self::convertDate($date_to, static::$TIME_TO);
        $interval = $date_from->diff($date_to, true);
        return $interval->format('%y%')*12 + $interval->format('%m%');
    }
}