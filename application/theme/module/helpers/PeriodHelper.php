<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 16.10.15
 * Time: 13:33
 */

namespace app\theme\module\helpers;


class PeriodHelper
{
    private $_period;

    public $years;
    public $months;

    public function __construct($period)
    {
        $this->_period = $period;
        $this->years = floor($this->_period / 12);
        $this->months = $this->_period - $this->years * 12;
    }

    public static function format($period)
    {
        $period = new self($period);

        return \Yii::t('eset',
            //'{years, plural, =0{} =1{one year} one{# year} few{# years} many{# years} other{# years}} {months, plural, =0{} =1{one month} one{# month} few{# months} many{# months} other{# months}}',
            '{years, plural, =0{} one{# year} few{# years} many{# years} other{# years}} {months, plural, =0{} one{# month} few{# months} many{# months} other{# months}}',
            [
                'years' => $period->years,
                'months' => $period->months
            ]
        );
    }

}