<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 14.09.2015
 * Time: 16:48
 */

namespace app\theme\module\helpers;


class StyleCase
{

    /**
     * @param $value string
     * @return string
     */
    public static function toCamelCase($value) {
        return str_replace(' ', '', ucwords(implode(' ', explode('_', $value))));
    }

    /**
     * @param $value string
     * @return string
     */
    public  static function toSnakeCase($value) {
/*
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $value, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
*/
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $value));
    }
}