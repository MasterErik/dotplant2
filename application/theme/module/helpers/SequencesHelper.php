<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 27.4.2016
 * Time: 10:35
 */

namespace app\theme\module\helpers;

use Yii;

class SequencesHelper
{
    const COUNTER_VIITENUMBER = 'viitenumber';

    public static function getNext($counterName)
    {
        return Yii::$app->db->createCommand("SELECT nextval(:counterName)", [':counterName' => $counterName])->queryScalar();
    }
}