<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 22.06.15
 * Time: 21:40
 */

namespace app\theme\module\helpers;


use app\modules\shop\models\OrderTransaction;
use app\theme\components\license\calculators\CumulativeLicenseCalculator;
use app\theme\components\payment\AbstractPayment;
use app\theme\models\EventPlanning;
use app\theme\models\InvoiceMode;
use app\theme\models\Order;
use app\theme\models\PaymentType;
use app\theme\models\RegistrationForm;
use app\theme\models\User;
use app\theme\models\UserProfile;
use app\theme\module\events\CustomStageEvent;
use app\theme\services\Synchronization;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Class EsetOrderStageHandlers
 * @package app\theme\module\helpers
 */
class EsetOrderStageHandlers
{

    /**
     * @param Order $order
     * @throws \Exception
     */
    public static function setIdentity($order)
    {
        if (empty($order->user_id)) {
            $registration = new RegistrationForm();
            if ($registration->load(Yii::$app->request->post())) {
                /** @var User $user */
                $user = $registration->AutoRegistration();
                if (!empty($user)) {
                    $order->user_id = $user->id;
                    $manager = Yii::$app->authManager;
                    if(Yii::$app->user->isGuest && (!$manager->checkAccess($user->id, 'admin') && !$manager->checkAccess($user->id, 'manager')) ) {
                        Yii::$app->user->login($user, 60*5);
                        UserProfile::revokeRole('base');
                    }
                } else {
                    Yii::$app->session->setFlash('error', $registration->getErrors());
                    return;
                }
            } else if (!Yii::$app->user->isGuest) {
                $order->user_id = Yii::$app->user->id;
            }
        }

        if (empty($order->manager_id)) {
            $reseller = UserProfile::getParentReseller($order->user_id);
            $order->manager_id = $reseller->user_id;
        }
    }
    public static function handleStageCustomer(CustomStageEvent $event)
    {
        $event->setStatus(false);

        if (\Yii::$app->request->isPost) {
            /** @var Order $order */
            $order = $event->eventData()['order'];
            if (empty($order)) {
                return;
            }

            $order->setCartFormScenario();
            $order->load(\Yii::$app->request->post());

            self::setIdentity($order);

            if (empty($order->user_id) || Yii::$app->user->isGuest) {
                $event->setStatus(false);
                return;
            }
            

            $collection = $order->getProxyCollection();
            $errors = [];
            $calculator = new CumulativeLicenseCalculator(['licenseCollection' => $collection]);
            $calculator->run(['price']);
            foreach ($calculator->licenseCollection as $proxy) {
                if ($proxy->hasErrors()) {
                    $errors[$proxy->owner->id] = implode(', ', $proxy->getErrors());
                    break;
                }
                $proxy->save();
            }
            if (!empty($errors)) {
                Yii::$app->session->setFlash('error', $errors);
            }


            if ($order->save() && $order->checkFill()) {
                $event->setStatus(true);
            }
        }

    }

    public static function handleCustomer(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];

        $calculator = new CumulativeLicenseCalculator(['licenseCollection' => $order->getProxyCollection()]);
        $calculator->calculate();
        $order->calculate(true);

        $registration = new RegistrationForm();

        $event->addEventData([
            'user_id' => $order->user_id,
            'registration' => $registration
        ]);

    }

    public static function handleStageRegistration(CustomStageEvent $event)
    {
        //if stage registration and user login back to begin
        if (!Yii::$app->user->isGuest) {
            self::goToInitStage($event);
            return;
        }

        $model = new RegistrationForm(['scenario' => 'cart']);

//        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return ActiveForm::validate($model);
//        }

        if ($model->load(Yii::$app->request->post())) {
            $model->username = $model->email;
            $model->locale = Yii::$app->language;
            if ($model->signup()) {
                Synchronization::syncByRest(Yii::$app->user->id);
                $event->setStatus(true);
            }
        }

        if (!$event->getStatus()) {
            $event->addEventData([
                'model' => $model
            ]);
        }

    }
    public static function handleRegistration(CustomStageEvent $event)
    {
        //if stage registration and user login back to begin
        if (!Yii::$app->user->isGuest) {
            self::goToInitStage($event);
            return null;
        }

        if (!isset($event->eventData()['model'])) {
            $event->addEventData([
                'model' => new RegistrationForm(['scenario' => 'cart'])
            ]);
        }

    }

    public static function handleStagePayment(CustomStageEvent $event)
    {
        $event->setStatus(false);

        if (\Yii::$app->request->isPost) {
            /** @var Order $order */
            $order = $event->eventData()['order'];
            if (empty($order)) {
                return null;
            }

            /** @var PaymentType $paymentType */
            $paymentType = PaymentType::findOne(['id' => \Yii::$app->request->post('PaymentType'), 'active' => 1]);
            if (empty($paymentType)) {
                return null;
            }


            $order->payment_type_id = $paymentType->id;
            if ($order->save()) {
                $event->setStatus(true);
                //self::redirectToPayment($order);
            }
        }

    }

    private static function redirectToPayment($order)
    {
        $transaction = !empty(OrderTransaction::findLastByOrder($order))
            ? OrderTransaction::findLastByOrder($order)
            : OrderTransaction::createForOrder($order);
        /** @var $payment AbstractPayment **/
        $payment = $order->paymentType->getPayment($order, $transaction);
        $payment::redirectToPayment();

    }
    public static function handlePayment(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];
        /** @var PaymentType[] $paymentTypes */
        $paymentTypes = PaymentType::getPaymentTypes(1, $order->total_price);
        $paymentType = !empty($order->paymentType) ? $order->paymentType->id : null;

        $event->addEventData([
            'paymentTypes' => $paymentTypes,
            'paymentType' => $paymentType,
            'totalPayment' => $order->total_price,
        ]);

    }


    public static function handleStagePaymentPay(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];

        $hasSuccess = OrderTransaction::find()
            ->where(['order_id' => $order->id])
            ->andWhere(['status' => OrderTransaction::TRANSACTION_SUCCESS])
            ->one();

        if (is_null($hasSuccess)) {
            Yii::$app->session->setFlash('error', Yii::t('eset', 'Payment is not made'));
        }
        $event->setStatus(!is_null($hasSuccess));
    }

    public static function handlePaymentPay(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];

        $paymentType = !empty($order->paymentType) ? $order->paymentType : null;
        /** @var OrderTransaction $orderTransaction */
        $transaction = !empty(OrderTransaction::findLastByOrder($order))
            ? OrderTransaction::findLastByOrder($order)
            : OrderTransaction::createForOrder($order);
        $transaction->refresh();

        $event->addEventData([
            'transaction' => $transaction,
            'paymentType' => $paymentType,
        ]);

    }

    public static function handleStageLicense(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];
        if ($order->checkFill()) {
            $order->createLicense(true);

            if (EventPlanning::checkExecuteLicense($order->id)) {

                $event->setStatus(true);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('eset', 'Thanks so much for purchasing ESET security product! 
                    We are as excited as you are and we look forward to providing you with great products and service. 
                    E-mail with registration information will be sent in 2 hours. 
                    If you don`t receive it within the next couple hours please contact ESET support.'));
            }
        } else {
            $event->setStatus(false);
        }

    }

    public static function handleStageFinal(CustomStageEvent $event)
    {

    }

    public static function handleFinal(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];
        /** @var UserProfile $user_profile */
        $user_profile = UserProfile::myProfile();
        if ($user_profile && InvoiceMode::INVOICE_IMMEDIATELY == $user_profile->invoice_mode) {
            $transaction = OrderTransaction::findOne(['order_id' => $order->id, 'status' => OrderTransaction::TRANSACTION_SUCCESS]);
            $type_invoice = !empty($transaction) ? EventPlanning::EVENT_TYPE_INVOICE : EventPlanning::EVENT_TYPE_INVOICE_PRE;
            EventPlanning::addImmediatelyCmd($order->id, $type_invoice);
        }
    }

    /**
     * @param CustomStageEvent $event
     * @throws BadRequestHttpException
     */
    private static function goToInitStage(CustomStageEvent $event)
    {
        /** @var Order $order */
        $order = $event->eventData()['order'];
        $save_status = false;
        $order->goInitStage($save_status);
        if ($save_status) {
            $event->addEventData([
                '__redirect' => Url::toRoute(['stage'])
            ]);
        } else {
            throw new BadRequestHttpException(Yii::t('eset', 'Cannot go to init stage'));
        }
        return;
    }

}