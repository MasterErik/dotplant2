<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 31.08.15
 * Time: 20:47
 */

namespace app\theme\module\helpers;


use app\theme\models\Order;
use app\theme\models\UserProfile;

class PriceHelper extends \app\modules\shop\helpers\PriceHelper
{
        public static function getOrderEsetTax(Order $order, $type = null)
    {
        // определяем партнёра
        $partner = UserProfile::getParentPartner($order->user_id);
        // определяем страну
        return $partner->country ? (float) $partner->country->tax / 100 : 0;
    }

}