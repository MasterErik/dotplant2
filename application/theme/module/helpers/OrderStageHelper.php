<?php

namespace app\theme\module\helpers;

use app\theme\models\Order;
use app\theme\models\OrderStage;
use app\theme\models\OrderStageLeaf;
use yii\helpers\Url;
use Yii;

class OrderStageHelper
{
    /**
     * @param OrderStage $stage
     * @param Order $order
     * @param bool|true $absolute
     * @return mixed
     */
    public static function getNextButtons(OrderStage $stage, Order $order, $absolute = true)
    {
        return array_reduce($stage->nextLeafs,
            function ($result, $item) use ($stage, $order, $absolute)
            {
                /** @var OrderStageLeaf $item */
                $result[] = [
                    'label' => $item->button_label,
                    'css' => $item->button_css_class . ($stage->isModify() ? '' : ' disabled'),
                    'url' => Url::toRoute(['cart/stage', 'leaf_id' => $item->id], $absolute)
                ];
                return $result;
            }, []);
    }

    /**
     * @param OrderStage $stage
     * @param Order $order
     * @param bool|true $absolute
     * @return mixed
     */
    public static function getPreviousButtons(OrderStage $stage, Order $order, $absolute = true)
    {
        return array_reduce($stage->prevLeafs,
            function ($result, $item) use ($stage, $order, $absolute)
            {
                /** @var OrderStageLeaf $item */
                if (0 === intval($item->stageFrom->immutable_by_user)) {
                    $result[] = [
                        'label' => \Yii::t('app', 'Back'),
                        'css' => 'btn btn-primary' . ($stage->isModify() ? '' : ' disabled'),
                        'url' => Url::toRoute(['cart/stage', 'leaf_id' => $item->id, 'previous' => 1], $absolute)
                    ];
                }
                return $result;
            }, []);
    }

}
?>