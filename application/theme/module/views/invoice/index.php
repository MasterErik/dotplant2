<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('eset', 'Invoices & Payments');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-10">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>

</div>

<div class="row">
    <div class="invoice-index col-lg-6">

        <h1><?= Yii::t('eset', 'Invoices') ?></h1>

        <?= $this->render('_grid', ['dataProvider' => $dataInvoicesProvider]); ?>
        <div class="well">
            <?php /*= Html::a(Yii::t('eset', 'Create Invoice'), ['create'], ['class' => 'btn btn-success'])*/ ?>
            <?= $this->render('_download', ['model' => $downloadModel]); ?>
        </div>

    </div>

    <div class="invoice-index col-lg-6">
        <h1><?= Yii::t('eset', 'Payments') ?></h1>
        <?= $this->render('_grid_payments', ['dataProvider' => $dataPaymentsProvider]); ?>
    </div>

</div>
