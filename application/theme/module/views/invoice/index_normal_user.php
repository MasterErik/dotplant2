<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('eset', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Yii::t('eset', 'Invoices') ?></h1>

<?= $this->render('_grid', ['dataProvider' => $dataProvider]); ?>
