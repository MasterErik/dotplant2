<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use app\theme\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\theme\models\search\InvoiceSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="invoice-search">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'method' => 'get',
        'formConfig' => [
            'deviceSize' => ActiveForm::SIZE_LARGE,
            'showLabels' => true,
        ],
    ]);
    ?>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'date_range', [
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                    ]
                ],
                'options' => [
                    'class' => 'drp-container form-group'
                ]
            ])->widget(\kartik\daterange\DateRangePicker::className(), [
                'useWithAddon' => true,
                'presetDropdown' => true,
            ]);
            ?>

            <?= $form->field($model, 'partner_id')->dropDownList(\yii\helpers\ArrayHelper::merge(array('' => ''), $model->partners)) ?>
        </div>

        <div class="col-lg-6">
            <?= $form->field($model, 'user_id')->widget(Select2::className(), [
                'pluginOptions' => ['allowClear' => true],
                'selectModel' => \app\theme\models\User::className()]
            )?>

            <?= $form->field($model, 'type')->dropDownList(\yii\helpers\ArrayHelper::merge(array('' => ''), $model->types)) ?>
        </div>
    </div>


    <div class="form-group">
        <div class="well">
            <?= Html::submitButton(Yii::t('eset', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?php // echo Html::resetButton(Yii::t('eset', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php \kartik\form\ActiveForm::end(); ?>

</div>
