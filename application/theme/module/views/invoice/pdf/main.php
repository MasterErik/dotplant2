<?php

use app\theme\models\Invoice;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\theme\models\Invoice */

$this->title = $model->id;
?>

<div class="row">
    <table class="table">
        <tr>
            <td width="60%">
                <?= Html::img(Yii::$app->request->baseUrl . '/theme/images/invoice/logo.png', ['style'=>'width: 80mm']) ?>
            </td>
            <td class="text-right">
                <b>INITEC OÜ</b>
                <br>Reg. nr.: 10979083
                <br>KMKR EE100909438
                <br>SEB bic: EEUHEE2X
                <br>Teaduspargi 8, 12618 Tallinn
                <br>SEB IBAN: EE981010220047491015
                <br>SEB pank a/a: 10220047491015
                <br><?= Yii::$app->formatter->asDate($model->created_at, 'long') ?>
            </td>
        </tr>

        <tr>
            <td></td>
            <td>
                <table class="table">
                    <tr>
                        <td colspan="2" style="border: 1px solid #000000">
                            <b><?= $model->type == Invoice::TYPE_PREPAYMENT ? 'Ettemaksuarve nr.' : 'Arve nr.'?> <?= $model->number ?></b>
                        </td>
                    </tr>

                    <tr>
                        <td>Viitenumber:</td>
                        <td><?= $model->viitenumber ?></td>
                    </tr>

                    <tr>
                        <td>Maksja:</td>
                        <td><?= $model->userProfile->client_name ?></td>
                    </tr>

                    <tr>
                        <td>KMKR:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>Aadress:</td>
                        <td><?= $model->userProfile->address ?></td>
                    </tr>

                    <tr>
                        <td>Tel:</td>
                        <td><?= $model->userProfile->telefon ?></td>
                    </tr>

                    <tr>
                        <td>E­post:</td>
                        <td><?= $model->user->email ?></td>
                    </tr>

                    <tr>
                        <td>Maksetähtaeg:</td>
                        <td><?= Yii::$app->formatter->asDate($model->due_date, 'long') ?></td>
                    </tr>


                </table>
            </td>
        </tr>
    </table>

</div>


<?php
    $i = 0;
?>
<table class="table table-bordered table-hover" id="cart-table">
    <thead>
    <tr>
        <th>NR</th>
        <th>Order nr.</th>
        <th>Tyyp</th>
        <th>Summa</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($items as $item): ?>
        <?php $i++; ?>
        <tr>
            <td class="table-counter"><?= $i ?> </td>
            <td><?= $item->order_id ?></td>
            <td><?= $item->type ?></td>
            <td><?= Yii::$app->formatter->asDecimal($item->sum - $item->tax, 2) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="row">
    <div class="col-xs-offset-8 col-xs-4">
        <table class="table">
            <tr>
                <th>Kokku:</th>
                <td class="text-right"><?= Yii::$app->formatter->asDecimal($model->sum-$model->tax, 2)?> EUR</td>
            </tr>
            <tr>
                <th>Käibemaks 20%:</th>
                <td class="text-right"><?= Yii::$app->formatter->asDecimal($model->tax,2)?> EUR</td>
            </tr>
            <tr>
                <th>Summa kokku:</th>
                <td class="text-right"><?= Yii::$app->formatter->asDecimal($model->sum,2)?> EUR</td>
            </tr>
            <tr>
                <th>Ümardamine:</th>
                <td class="text-right"><?= Yii::$app->formatter->asDecimal(0,2)?> EUR</td>
            </tr>
            <tr>
                <th>Tasumiseks:</th>
                <td class="text-right"><?= Yii::$app->formatter->asDecimal($model->sum,2)?> EUR</td>
            </tr>
        </table>

    </div>
</div>

<b>Summa sõnadega: <?= $model->sumAsSpellOut($model->sum)?></b>
<br>Tasumisel palume kindlasti märkida viitenumber: <?= $model->viitenumber ?>

<br>
<br>
<br>
<br>
<br>
<br>
<table class="table">
    <tr>
        <td width="50%">Väljastas: </td>
        <td>Veso Võttis vastu:</td>
    </tr>
</table>

