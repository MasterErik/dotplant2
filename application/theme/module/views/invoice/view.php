<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\theme\models\Invoice */

$this->title = 'Invoice  No ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'created_at',
            'due_date',
            [
                'label' => $model->getAttributeLabel('partner_id'),
                'value' => $model->partnerProfile->client_name
            ],
            [
                'label' => $model->getAttributeLabel('user_id'),
                'value' => $model->userProfile->client_name
            ],
            'type',
            'tax',
            'sum',
//            'iban',
//            'swift',
            'viitenumber',
//            'bill_to',
        ],
    ]) ?>

</div>

<div class="invoice-detail-grid">

    <h2>Invoice Detail</h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'showFooter' => true,
        'showPageSummary' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
                'pageSummary' => false,
            ],
            [
                //'attribute' => 'order.userProfile.user_id',
                'header' => Yii::t('eset', 'Client'),
                //'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'format'=>'raw',
                'value' => function($model){
                    /**
                     * @var $model \app\theme\models\InvoiceDetail
                     */
                    return $model->order ? Html::a($model->order->clientUserProfile->client_name, ["/shop/orders/show", "hash" => $model->order->hash]) : null;
                },
                'pageSummary'=>'Total Summary',
            ],
            //'discount_id',
            [
                'attribute' => 'type',
                'group' => true,
                'groupedRow' => true,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \app\theme\models\Invoice::getInvoiceTypes(),
                'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns'=>[[0, 1]], // columns to merge in summary
                        'content'=>[              // content to show in each summary cell
                            0=>'Summary (' . $model->type . ')',
                            3=>GridView::F_SUM,
                        ],
                        'contentFormats'=>[      // content reformatting for each summary cell
                            3=>['format'=>'number', 'decimals'=>2],
                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            //3=>['style'=>'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options'=>['class'=>'success','style'=>'font-weight:bold;']
                    ];
                },
            ],
            //'tax',
            [
                'attribute' => 'sum',
                //'format'=>['decimal', 2],
                'pageSummary' => true,
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

