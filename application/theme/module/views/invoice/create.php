<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\theme\models\Invoice */

$this->title = Yii::t('eset', 'Create Invoice');
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
