<?php
/**
 * @var $model \app\theme\models\InvoiceDownloadForm
 * @var $this yii\web\View
 */
use yii\helpers\Html;
use kartik\form\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'action' => ['download'],
        'method' => 'post',
        'type' => 'inline',
]); ?>

<?= $form->field($model, 'period')->widget(\kartik\widgets\DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    //'convertFormat' => true,
    'pluginOptions' => [
        'format' => \app\theme\models\InvoiceDownloadForm::FORMAT_PERIOD,
        'viewMode' => "months",
        'minViewMode' => "months",
        'autoclose'=>true,
    ]
]); ?>

<?= $form->field($model, 'format')->dropDownList($model->formats) ?>

<button type="submit" class="btn btn-default"><?= Yii::t('eset', 'Download'); ?></button>

<?php ActiveForm::end(); ?>
