<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\theme\models\Invoice */

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'invoice-grid',
]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],

        //'id',
        [
            'attribute' => 'created_at',
            'value' => function($model) {
                return Yii::$app->formatter->asDate($model->created_at);
            }
        ],
        'type',
        'number',
        [
            'attribute' => 'partner_id',
            'value' => function($model) {
                return $model->partnerProfile ? $model->partnerProfile->client_name : null;
            },
            'visible' => \app\theme\models\UserProfile::iPartner() || \app\theme\models\UserProfile::iReseller()
        ],
        [
            'attribute' => 'user_id',
            'value' => function($model) {
                return $model->userProfile ? $model->userProfile->client_name : null;
            },
            'visible' => \app\theme\models\UserProfile::iPartner() || \app\theme\models\UserProfile::iReseller()
        ],
        //'bill_to',
        'due_date',
        //'tax',
        'sum',
        // 'iban',
        // 'swift',
        // 'viitenumber',


        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{print} {view}',
            'buttons'=>[
                'print'=>function ($url, $model) { return Html::a('<span class="glyphicon glyphicon-print"></span>', ['/eset/invoice/print', 'id'=>$model->id], ['title'=>Yii::t('eset', 'Print'), 'target'=>'_blank', 'data-pjax'=>0]); }
            ],
            'contentOptions' => [
                'class' => 'text-nowrap'
            ]
        ],
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>