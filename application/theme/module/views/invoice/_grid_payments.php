<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\theme\models\Invoice */

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'payments-grid',
]); ?>

<?= GridView::widget([

    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],

        //'id',
        [
            'attribute' => 'created_at',
            'value' => function($model) {
                return Yii::$app->formatter->asDate($model->created_at);
            }
        ],
        'type',
        [
            'attribute' => 'partner_id',
            'value' => function($model) {
                return $model->partnerProfile ? $model->partnerProfile->client_name : null;
            }
        ],
        [
            'attribute' => 'user_id',
            'value' => function($model) {
                return $model->userProfile ? $model->userProfile->client_name : null;
            }
        ],
        'eset_payment_type_id',
        'summa',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'contentOptions' => [
                'class' => 'text-nowrap'
            ]
        ],
    ],
]); ?>

<?php \yii\widgets\Pjax::end(); ?>