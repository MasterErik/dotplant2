<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\theme\models\ResellerStatus */

$this->title = Yii::t('eset', 'Create Reseller Status');
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Reseller Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reseller-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
