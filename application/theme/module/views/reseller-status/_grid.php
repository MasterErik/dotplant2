<?php
/**
 * Created by PhpStorm.
 * User: dp
 * Date: 25.09.15
 * Time: 14:47
 */
use app\theme\models\ResellerStatusList;
use kartik\widgets\DatePicker;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\ResellerStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //'id',
        //'partner_id',
        [
            'attribute' => 'reseller_status',
            'headerOptions' => ['width' => '200'],
            'filter' => ResellerStatusList::getDropDownList(),

        ],
        'discount',
        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'headerOptions' => ['width' => '250'],
            'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'options' => ['placeholder' => 'Start'],
                    'options2' => ['placeholder' => 'End'],
                    'type' => DatePicker::TYPE_RANGE,
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                       // 'autoclose' => true,
                    ]
            ]),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]); ?>

