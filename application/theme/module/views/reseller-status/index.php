<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\ResellerStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('eset', 'Reseller Status');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reseller-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('eset', 'Create Reseller Status'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
