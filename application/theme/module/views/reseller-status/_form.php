<?php

use app\theme\models\ResellerStatusList;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\theme\models\ResellerStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reseller-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reseller_status')->dropDownList(ResellerStatusList::getDropDownList())  ?>

    <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

    <?php /*
    <?=
    \yii\helpers\Html::tag(
        'span',
        \kartik\icons\Icon::show('plus') . Yii::t('app', 'Add files..'),
        [
            'class' => 'btn btn-success fileinput-button'
        ]
    ) ?>
    <?= \app\modules\image\widgets\ImageDropzone::widget([
        'name' => 'file',
        'url' => ['/eset/reseller-status/upload'],
        'removeUrl' => ['remove'],
        'uploadDir' => 'theme/resources/reseller-status-images',
        'sortable' => true,
        'sortableOptions' => [
            'items' => '.dz-image-preview',
        ],
        'objectId' => 1, //$object->id,
        'modelId' => $model->id,
        'htmlOptions' => [
            'class' => 'table table-striped files',
            'id' => 'previews',
        ],
        'options' => [
            'clickable' => ".fileinput-button",
        ],
    ]); */?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('eset', 'Create') : Yii::t('eset', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
