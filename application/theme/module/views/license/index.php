<?php

use app\theme\models\UserProfile;
use kartik\form\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\LicenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('eset', 'Licenses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="license-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        //'enableClientValidation' => true,
        //'enableAjaxValidation' => true,
        //'layout' => 'horizontal',
        'method' => 'get',
        'formConfig' => [
            'deviceSize' => ActiveForm::SIZE_LARGE,
            'showLabels' => true,
        ],
    ]);
    ?>

    <?php if (!UserProfile::iNormalUser()) {
        echo $this->render('_search', ['model' => $searchModel, 'form' => $form]);
    }?>
    <?= $this->render('_grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'form' => $form]); ?>

    <?php ActiveForm::end(); ?>

</div>
