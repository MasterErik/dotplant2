<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 18.3.2016
 * Time: 16:40
 */
use app\theme\components\commands\CommandList;
use app\theme\models\License;
use app\theme\models\UserProfile;
use kartik\icons\Icon;
use kartik\widgets\Select2;
use yii\helpers\Html;


/* @var License CommandList */
?>

<div class="license-search">

    <?php if (UserProfile::iPartner()): ?>
    <div class="row">
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('glyphicon glyphicon-ok-sign', ['class'=>'fa-lg'], Icon::BSG) . Yii::t('app', 'Activate'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => CommandList::ACTIVATE,
                ]
            )
            ?>
        </div>
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('glyphicon glyphicon-remove-sign', ['class'=>'fa-lg'], Icon::BSG) . Yii::t('app', 'Cancel'),
                [
                    'class' => 'btn btn-danger',
                    'name' => 'action',
                    'value' => CommandList::CANCEL,
                ]
            )
            ?>
        </div>
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('save') . Yii::t('app', 'Suspend'),
                [
                    'class' => 'btn btn-warning',
                    'name' => 'action',
                    'value' => CommandList::SUSPEND,
                ]
            )
            ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton(
                Icon::show('fa fa-refresh') . Yii::t('app', 'Synchronize'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => CommandList::SYNCHRONIZE,
                ]
            )
            ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton(
                Icon::show('times') . Yii::t('app', 'Reset password'),
                [
                    'class' => 'btn btn-danger',
                    'name' => 'action',
                    'value' => CommandList::RESET_PWD,
                ]
            )
            ?>
        </div>
        <div class="col-md-2">
            <?= Html::submitButton(
                Icon::show('times') . Yii::t('app', 'Reset license key'),
                [
                    'class' => 'btn btn-danger',
                    'name' => 'action',
                    'value' => CommandList::RESET_KEY,
                ]
            )
            ?>
        </div>
    </div>
    <BR>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('save') . Yii::t('app', 'Renew'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => CommandList::RENEW,
                ]
            )
            ?>
        </div>
        <?php if (UserProfile::iPartner() || UserProfile::iReseller() || UserProfile::iEmployee() ): ?>
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('save') . Yii::t('app', 'Enlarge'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => CommandList::ENLARGE,
                ]
            )
            ?>
        </div>
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('save') . Yii::t('app', 'Upgrade'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => CommandList::UPGRADE,
                ]
            )
            ?>
        </div>
        <div class="col-md-2">
            <?= /** @var License $model */
            Select2::widget(
                [
                    'name' => 'email',
                    'data' =>  $model->getEmails(),
                    'options' => [
                        'multiple' => true,
                        'placeholder' => Yii::t('eset', 'Select email'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                    ],
//                    'value' => $model->client->email . ' , ' . $model->reseller->email,
                ]
            )
            //'class' => 'form-control select-list',
            ?>
        </div>
        <div class="col-md-1">
            <?= Html::submitButton(
                Icon::show('save') . Yii::t('app', 'Send license'),
                [
                    'class' => 'btn btn-success',
                    'name' => 'action',
                    'value' => 'send',
                ]
            )
            ?>
        </div>
        <?php endif; ?>
    </div>
</div>
