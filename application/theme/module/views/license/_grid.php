<?php

use app\theme\models\DiscountCodeList;
use app\theme\models\LicenseStatus;
use app\theme\models\PurchaseType;
use app\theme\models\UserProfile;
use app\theme\module\helpers\PeriodHelper;
use kartik\editable\Editable;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\LicenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php \yii\widgets\Pjax::begin(['id' => 'license-grid']); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['style' => 'overflow: auto'],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
//    'responsive' => true,
//    'toggleDataContainer' => ['class' => 'btn-group-sm'],
    'pjax' => true,
    'toolbar' => [],
    'panel'=>[
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<i class="glyphicon glyphicon-book"></i>   ' . Yii::t('eset', 'Licenses'),
    ],
    'columns' => [
        [
            'attribute' => 'client_name',
            'value' => function($model) {
                return $model->client ? Html::a($model->client->client_name,
                    Url::toRoute(['user/cabinet/view-my-user', 'id' => $model->user_id])) : null;
            },
            'format' => 'raw',
            //'visible' => !\app\theme\models\UserProfile::iNormalUser(),
        ],
        [
            'attribute' => 'reseller_name',
            'value' => function($model) {
                return $model->reseller && !UserProfile::iNormalUser()? Html::a($model->reseller->client_name,
                    Url::toRoute(['user/cabinet/view-my-user', 'id' => $model->reseller_id])) : null;
            },
            'format' => 'raw',
        ],

        [
            'attribute' => 'product_name',
            'value' => 'product.name',
            'filter' => \app\theme\models\Product::getListCode(),

            //'attribute' => 'product_code',
//            'headerOptions' => ['width' => '300'],
//            'filter' => \app\theme\widgets\Select2::widget([
//                'model' => $searchModel,
//                'attribute' => 'product_code',
//                'key' => 'eset_product_code',
//                'initValueText' => $searchModel->product ? $searchModel->product->name : null,
//                'pluginOptions' => ['allowClear' => true],
//                'selectModel' => \app\theme\models\Product::className()
//            ]),
        ],
//        [
//            'attribute' => 'country_code',
//            'value' => 'country.name',
//            'headerOptions' => ['width' => '90'],
//        ],
        [
            'attribute' => 'purchase_type',
            'filter' => PurchaseType::getDropDownList(),
            'headerOptions' => ['width' => '60'],
        ],
        [
            'attribute' => 'period',
            'value' => function($model) {
                return PeriodHelper::format($model->period);
            },
            'headerOptions' => ['width' => '80'],
        ],
        [
            'attribute' => 'quantity',
            'headerOptions' => ['width' => '20'],
        ],
        [
            'attribute' => 'discount_code',
            'value' => 'discountType',
            'visible' => \app\theme\models\UserProfile::iPartner(),
            'filter' => DiscountCodeList::getDropDownList(),
            'headerOptions' => ['width' => '20'],
        ],
        [
            'attribute' => 'custom_discount',
            'visible' => \app\theme\models\UserProfile::iPartner(),
            'headerOptions' => ['width' => '20'],
        ],
        [
            'attribute' => 'total_price',
            'value' => 'orderDetail.total_price',
            'headerOptions' => ['width' => '30'],
        ],
        [
            'attribute' => 'state',
            'value' => function ($model) {
                return '<span class="' . LicenseStatus::getIcon($model->state) . '"></span>';
            },
            'format' => 'raw',
            'filter' => LicenseStatus::getDropDownList(),
            'headerOptions' => ['width' => '50'],
        ],
        [
            'attribute' => 'created_at',
            'format' => 'datetime',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->created_at);
            },
            'filter' => '',
            'headerOptions' => ['width' => '90'],
        ],
/*        [
            'attribute' => 'updated_at',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->updated_at);
            },
        ],
*/
        //'sector',
        [
            'attribute' => 'bdate',
            'format' => 'date',
//            'filter' => '',
            'headerOptions' => ['width' => '90'],
        ],
        [
            'attribute' => 'edate',
            'format' => 'date',
            'visible' => \app\theme\models\UserProfile::iPartner(),
            'headerOptions' => ['width' => '90'],
        ],
        [
            'attribute' => 'username',
            'headerOptions' => ['width' => '140'],
        ],
//        'password',
        [   'attribute' => 'license_key',
            'headerOptions' => ['width' => '225'],
        ],
/*
        [
            'attribute' => 'public_license_key',
            'visible' => \app\theme\models\UserProfile::iPartner(),
        ],
        [
            'attribute' => 'epli',
            'visible' => \app\theme\models\UserProfile::iPartner(),
        ],
*/
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'note_own',
            'headerOptions' => ['width' => '100'],
            'visible' => \app\theme\models\UserProfile::iPartner(),

            'editableOptions'=> [
                'formOptions' => ['action' => ['editnote']],
                'size' => 'lg',
                'placement' => PopoverX::ALIGN_LEFT,
                'format' => Editable::INPUT_TEXTAREA,
            ]
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'contentOptions' => ['class' => 'text-nowrap'],
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open fa-lg"></span>', ['license/view', 'id' => $model->id], [
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax' => 0
                    ]);
                },
            ],
        ],
    ],
]); ?>


<?php \yii\widgets\Pjax::end(); ?>