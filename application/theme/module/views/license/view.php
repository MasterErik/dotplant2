<?php

use app\theme\models\UserProfile;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use app\theme\widgets\LicenseWidget;
/* @var $this yii\web\View */
/* @var $model app\theme\models\License */

    $this->title = $model->productName;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Licenses'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;

    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'method' => 'post',
        'formConfig' => [
            'deviceSize' => ActiveForm::SIZE_LARGE,
            'showLabels' => true,
        ],
    ]);
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= LicenseWidget::widget(['model' => $model]) ?>
<?= $this->render('actions', ['model' => $model, 'form' => $form]) ?>

<?php ActiveForm::end(); ?>