<?php

use app\theme\models\UserProfile;
use app\theme\models\YesNo;
use yii\helpers\Html;
use app\theme\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\theme\models\search\LicenseSearch */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="license-search">

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'search_bdate_range', [
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                    ]
                ],
                'options' => [
                    'class' => 'drp-container form-group'
                ]
            ])->widget(\kartik\daterange\DateRangePicker::className(), [
                'useWithAddon' => true,
                'presetDropdown' => true,
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'Y-m-d']
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'search_edate_range', [
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                        ]
                    ],
                    'options' => [
                        'class' => 'drp-container form-group'
                    ]
                ])->widget(\kartik\daterange\DateRangePicker::className(), [
                'useWithAddon' => true,
                'presetDropdown' => true,
                'convertFormat' => true,

                ]);
            ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'search_user_id')->widget(Select2::className(), [
                    'initValueText' => $model->search_user_id ? UserProfile::findOne($model->search_user_id)->client_name : '',
                    'options' => [
                        'placeholder' => Yii::t('eset', 'Select user'),
                    ],
                    'pluginOptions' => ['allowClear' => true],
                    'selectModel' => \app\theme\models\User::className()]
            )?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'search_reseller_id')->widget(Select2::className(), [
                    'initValueText' => $model->search_reseller_id ? UserProfile::findOne($model->search_reseller_id)->client_name : '',
                    'options' => [
                        'placeholder' => Yii::t('eset', 'Select reseller'),
                    ],
                    'pluginOptions' => ['allowClear' => true],
                    'selectModel' => \app\theme\models\User::className()]
            )?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'search_child')->dropDownList(YesNo::getDropDownList(true)) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'public_license_key') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'epli') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'created_at', [
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-calendar"></i>'
                    ]
                ],
                'options' => [
                    'class' => 'drp-container form-group'
                ],
                ])->widget(\kartik\daterange\DateRangePicker::className(), [
                    'useWithAddon' => true,
                    'presetDropdown' => true,
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'Y-m-d h:i']
                    ],
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="well">
            <?= Html::submitButton(Yii::t('eset', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?php // echo Html::resetButton(Yii::t('eset', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>

<!--    --><?php //ActiveForm::end(); ?>

</div>
