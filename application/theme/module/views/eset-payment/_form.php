<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\theme\models\EsetPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eset-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'partner_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'eset_payment_type_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'web' => 'Web', 'invoice' => 'Invoice', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'viitenumber')->textInput() ?>

    <?= $form->field($model, 'summa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('eset', 'Create') : Yii::t('eset', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
