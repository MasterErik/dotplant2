<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\theme\models\EsetPayment */

$this->title = Yii::t('eset', 'Update {modelClass}: ', [
    'modelClass' => 'Eset Payment',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Eset Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('eset', 'Update');
?>
<div class="eset-payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
