<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\theme\models\search\EsetPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eset-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'partner_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'eset_payment_type_id') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'viitenumber') ?>

    <?php // echo $form->field($model, 'summa') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('eset', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('eset', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
