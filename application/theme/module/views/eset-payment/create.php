<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\theme\models\EsetPayment */

$this->title = Yii::t('eset', 'Create Eset Payment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Eset Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eset-payment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
