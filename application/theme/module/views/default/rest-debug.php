<?php

$items = [];
foreach(\app\theme\components\restClient\RestDescriptionApi::$methodArray as $method => $params) {
    $items[$method] = $method;
}
?>


<div class="hello-default-index">
    <h1>Debug REST API</h1>

    <form>
        <div class="form-group">
            <label>Method</label>
            <?= \yii\helpers\Html::dropDownList('method', 'get_product_price', $items, ['class'=>'form-control', 'id'=>'RequestMethod']) ?>
        </div>
        <div class="form-group">
            <label>Request</label>
            <textarea  class="form-control" id="RequestParams" placeholder="Enter request Json" rows="10"></textarea>
        </div>
        <button type="button" class="btn btn-default" id="sendRest">Send</button>
        <div class="form-group">
            <label>Response</label>
            <textarea  class="form-control" id="ResponseResult" placeholder="Response Json" rows="10"></textarea>
        </div>
    </form>
</div>

<?php
$this->registerJs("
    $(function() {
        $('#sendRest').click(function(){
            $.ajax({
                url: '/eset/default/rest-debug',
                data: {method: $('#RequestMethod').val(), params: $('#RequestParams').val()},
                success: function(data) {
                    $('#ResponseResult').val(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#ResponseResult').val(jqXHR.responseText);
                },
                type: 'GET',
                dataType: 'text'
            });
        });
    });", \yii\web\View::POS_END);
