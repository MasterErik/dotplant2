<?php
/**
 * Author: Eugine Terentev <eugine@terentev.net>
 */

namespace app\theme\module\backend\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;


class InformationController extends Controller
{
    public function actionIndex()
    {

            return $this->render('index');

    }
}