<?php

namespace app\theme\module\backend\controllers;

use Yii;
use app\theme\models\Translate;
use app\theme\models\TranslateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TranslateController implements the CRUD actions for Translate model.
 */
class TranslateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['setting manage'],
                    ],
                ],
            ],
        ];
    }

	//TODO accessControl

    /**
     * Lists all Translate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TranslateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Translate model.
     * @param string $table
     * @param integer $model_id
     * @param string $attribute
     * @param string $language
     * @return mixed
     */
    public function actionView($table, $model_id, $attribute, $language)
    {
        return $this->render('view', [
            'model' => $this->findModel($table, $model_id, $attribute, $language),
        ]);
    }

    /**
     * Creates a new Translate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Translate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'table' => $model->table, 'model_id' => $model->model_id, 'attribute' => $model->attribute, 'language' => $model->language]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Translate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $table
     * @param integer $model_id
     * @param string $attribute
     * @param string $language
     * @return mixed
     */
    public function actionUpdate($table, $model_id, $attribute, $language)
    {
        $model = $this->findModel($table, $model_id, $attribute, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'table' => $model->table, 'model_id' => $model->model_id, 'attribute' => $model->attribute, 'language' => $model->language]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Translate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $table
     * @param integer $model_id
     * @param string $attribute
     * @param string $language
     * @return mixed
     */
    public function actionDelete($table, $model_id, $attribute, $language)
    {
        $this->findModel($table, $model_id, $attribute, $language)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Translate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $table
     * @param integer $model_id
     * @param string $attribute
     * @param string $language
     * @return Translate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($table, $model_id, $attribute, $language)
    {
        if (($model = Translate::findOne(['table' => $table, 'model_id' => $model_id, 'attribute' => $attribute, 'language' => $language])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
