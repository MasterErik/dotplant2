<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\theme\models\TranslateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'table') ?>

    <?= $form->field($model, 'model_id') ?>

    <?= $form->field($model, 'attribute') ?>

    <?= $form->field($model, 'language') ?>

    <?= $form->field($model, 'translate') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('eset', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('eset', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
