<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\theme\models\Translate */

$this->title = $model->table;
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('eset', 'Update'), ['update', 'table' => $model->table, 'model_id' => $model->model_id, 'attribute' => $model->attribute, 'language' => $model->language], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('eset', 'Delete'), ['delete', 'table' => $model->table, 'model_id' => $model->model_id, 'attribute' => $model->attribute, 'language' => $model->language], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('eset', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'table',
            'model_id',
            'attribute',
            'language',
            'translate:ntext',
        ],
    ]) ?>

</div>
