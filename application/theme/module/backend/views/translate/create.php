<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\theme\models\Translate */

$this->title = Yii::t('eset', 'Create Translate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
