<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\theme\models\Translate */

$this->title = Yii::t('eset', 'Update {modelClass}: ', [
    'modelClass' => 'Translate',
]) . ' ' . $model->table;
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->table, 'url' => ['view', 'table' => $model->table, 'model_id' => $model->model_id, 'attribute' => $model->attribute, 'language' => $model->language]];
$this->params['breadcrumbs'][] = Yii::t('eset', 'Update');
?>
<div class="translate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
