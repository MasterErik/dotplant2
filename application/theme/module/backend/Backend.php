<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 21.04.2015
 * Time: 16:55
 */

namespace app\theme\module\backend;

use app\backend\BackendModule;

class Backend extends BackendModule
{
    public $defaultRoute = 'dashboard/index';
    public $administratePermission = 'administrate';

    public function init()
    {
        parent::init();
    }
}