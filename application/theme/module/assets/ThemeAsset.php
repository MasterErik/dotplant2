<?php

namespace app\theme\module\assets;
use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    public $basePath = '@webroot/theme';
    public $baseUrl = '@web/theme';
    public $css = [
        'css/eset.css',
    ];

    public $js = [
        //'js/cart.js',
     ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
