<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 11.09.2015
 * Time: 20:32
 */

namespace app\theme\module\assets;

use yii\web\AssetBundle;


class ProtocolAsset extends AssetBundle
{
    public $sourcePath = '@app/theme/module/assets';
    public $css = [
    ];

    public $js = [
        'js\ajaxTransfer.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

//    public $depends = [
//        'app\assets\AppAsset',
//    ];

}