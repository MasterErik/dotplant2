/**
 * Created by Erik on 11.09.2015.
 */


var Select2_processor = {

    getAjaxData: function(params) {
        return {search: params.term, page: params.page};
    },

    processResults: function (data, params) {
        params.page = params.page || 1;
        return {
            results: data.items,
            pagination: {
                more: (params.page * 30) < data.total_count
            }
        };
    },
}
