<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 8:16 PM
 */

namespace app\theme\assets;

use yii\web\AssetBundle;

class FlotAsset extends AssetBundle{
    public $sourcePath = '@bower/flot';
    public $js = [
        'jquery.flot.js'
    ];

    public $depends = [
        '\yii\web\JqueryAsset'
    ];
} 