<?php

namespace app\theme\module\user\controllers;

use app\components\AuthClientHelper;
use app\modules\user\actions\AuthAction;
use app\modules\user\models\UserService;
use app\theme\models\RegistrationForm;
use app\theme\models\User;
use app\theme\models\UserProfile;
use Yii;
use yii\caching\TagDependency;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\base\ErrorException;
use yii\helpers\Url;

class UserController extends \app\modules\user\controllers\UserController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['signup'],
                'allow' => true,
                'roles' => ['base', '?'],
            ],
            [
                'actions' => ['logout', 'profile'],
                'allow' => true,
                'roles' => ['@'],
            ],
            [
                'actions' => ['profile'],
                'allow' => true,
                'roles' => ['base'],
            ],

        ];
        return $behaviors;
    }
/*
    public function getViewPath()
    {
        return '@app/modules/user/views/' . $this->id;
    }
*/
    /**
     * Action for loggin in users
     * @param null $returnUrl
     * @return string|Response
     */
    public function actionLogin($returnUrl = null)
    {
        parent::actionLogin(Url::to('/catalog') );
    }
	public function actionSignup()
	{
		$model = new RegistrationForm();
        $model->position = current(UserProfile::getAvailablePositions());

		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if ($model->load(Yii::$app->request->post()) && $model->signup()) {
			if (Yii::$app->user->isGuest) {
				\Yii::$app->session->setFlash('success', Yii::t('eset', 'Verification e-mail sent to registered user'));
				return $this->goHome();
			}
            if ($model->email) {
                \Yii::$app->session->setFlash('success', Yii::t('eset', 'Verification e-mail sent to registered user'));
            } else {
                \Yii::$app->session->setFlash('success', Yii::t('eset', 'The user created.'));
            }
			$model = new RegistrationForm();
		}

		return $this->render('signup',	['model' => $model]);
	}

    public function actionConfirm($code)
    {

        $user_profile = UserProfile::findOne(['confirm_registration_key' => $code]);
        if ($user_profile) {
            $transaction = Yii::$app->db->beginTransaction();
            User::updateAll(['status' => User::STATUS_ACTIVE], ['id' => $user_profile->user_id]);
            UserProfile::updateAll(['confirm_registration_key' => ''], ['user_id' => $user_profile->user_id]);
            $transaction->commit();

            $user = User::findOne($user_profile->user_id);
            $userRole = Yii::$app->authManager->getRole('base');
            Yii::$app->authManager->assign($userRole, $user->id);

            if ($user && Yii::$app->user->login($user, 0)) {
                \Yii::$app->session->setFlash('success', Yii::t('eset', 'Registration completed'));
            } else {
                \Yii::$app->session->setFlash('error', Yii::t('eset', 'Registration failed'));
            }
        } else {
            \Yii::$app->session->setFlash('error', Yii::t('eset', 'The confirmation code is not valid'));
        }

        return $this->goHome();
    }

    /**
     * @inheritdoc
     */
    public function successCallback($client)
    {
        $model = AuthClientHelper::findUserByService($client);
        if (is_object($model) === false) {
            // user not found, retrieve additional data
            $client = AuthClientHelper::retrieveAdditionalData($client);

            $attributes = AuthClientHelper::mapUserAttributesWithService($client);

            // check if it is anonymous user
            if (Yii::$app->user->isGuest === true) {
                $model = new \app\modules\user\models\User(['scenario' => 'registerService']);
                $security = Yii::$app->security;



                $model->setAttributes($attributes['user']);
                $model->status = \app\modules\user\models\User::STATUS_ACTIVE;

                if (empty($model->username) === true) {
                    // if we doesn't have username - generate unique random temporary username
                    // it will be needed for saving purposes
                    $model->username = $security->generateRandomString(18);
                    $model->username_is_temporary = 1;
                }

                $model->setPassword($security->generateRandomString(16));

                $model->generateAuthKey();


                if ($model->save() === false) {

                    if (isset($model->errors['username'])) {
                        // regenerate username
                        $model->username = $security->generateRandomString(18);
                        $model->username_is_temporary = 1;
                        $model->save();
                    }

                    if (isset($model->errors['email'])) {
                        // empty email
                        $model->email = null;
                        $model->save();
                    }
                    if (count($model->errors) > 0) {
                        throw new ErrorException(Yii::t('app', "Temporary error signing up user"));
                    }
                }
            } else {
                // non anonymous - link to existing account
                /** @var \app\modules\user\models\User $model */
                $model = Yii::$app->user->identity;
            }

            $service = new UserService();
            $service->service_type = $client->className();
            $service->service_id = '' . $attributes['service']['service_id'];
            $service->user_id = $model->id;
            if ($service->save() === false) {
                throw new ErrorException(Yii::t('app', "Temporary error saving social service"));
            }

        } elseif (Yii::$app->user->isGuest === false) {
            // service exists and user logged in
            // check if this service is binded to current user
            if ($model->id != Yii::$app->user->id) {
                throw new ErrorException(Yii::t('app', "This service is already binded to another user"));
            } else {
                throw new ErrorException(Yii::t('app', 'This service is already binded.'));
            }
        }
        TagDependency::invalidate(Yii::$app->cache, ['Session:'.Yii::$app->session->id]);
        Yii::$app->user->login($model, 86400);

        if ($model->username_is_temporary == 1 || empty($model->email)) {
            // show post-registration form
            //$this->layout = $this->module->postRegistrationLayout;
            $this->layout = \Yii::$app->getModule('user')->postRegistrationLayout;
            $model->setScenario('completeRegistration');

            $model_profile = UserProfile::findOne($model->id);
            if (!$model_profile) {
                $model_profile = new UserProfile;
                $model_profile->user_id = $model->id;
            }

            echo $this->render('post-registration', [
                'model' => $model,
                'model_profile' => $model_profile,
            ]);
            Yii::$app->end();
            return;
        }
    }

    public function actionCompleteRegistration()
    {
        /** @var \app\modules\user\models\User $model */
        /** @var UserProfile $model_profile */
        $model = Yii::$app->user->identity;

        if (intval($model->username_is_temporary) === 1) {
            // reset username
            $model->username = '';
        }
        $model->setScenario('completeRegistration');
        $res_load_model = $model->load(Yii::$app->request->post());

        $model_profile = UserProfile::findOne(Yii::$app->user->id);
        if (!$model_profile) {
            $model_profile = new UserProfile;
            $model_profile->user_id = $model->id;
        }
        $res_load_model_profile = $model_profile->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax && $res_load_model && $res_load_model_profile) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validateMultiple([$model, $model_profile]);
        }


        if (Yii::$app->request->isPost && $model->validate() && $model_profile->validate()) {

            $model->username_is_temporary = 0;

            if ($model->save() && $model_profile->save()) {
                $auth_action = new AuthAction('post-registration', $this);
                return $auth_action->redirect('/');
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Internal error'));
            }
        }

        $this->layout = $this->module->postRegistrationLayout;
        return $this->render('post-registration', [
            'model' => $model,
        ]);
    }

/*
    public function actionGetUsers($search = null, $page = null)
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return Select2Search::getData(User::className(), $search, $page, 'id');
    }
*/
}
