<?php

namespace app\theme\module\user\controllers;

use app\models\Property;
use app\models\PropertyGroup;
use app\modules\user\models\User;
use app\properties\HasProperties;
use app\theme\models\search\UserProfileSearch;
use app\theme\models\UserProfile;
use kartik\form\ActiveForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['base'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionProfile()
    {
        /** @var User|HasProperties $model */
        $model = User::findOne(Yii::$app->user->id);
        $model->scenario = 'updateProfile';

        $model->getPropertyGroups(false, false, true);
        $model->abstractModel->setAttributesValues(Yii::$app->request->post());

        $model_profile = UserProfile::findOne(Yii::$app->user->id);
        if (!$model_profile) {
            $model_profile = new UserProfile;
            $model_profile->user_id = $model->id;
        }
        //$model_profile->scenario = 'updateProfile';

        $res_load = $model->load(Yii::$app->request->post()) && $model_profile->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax && $res_load) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validateMultiple([$model, $model_profile]);
        }

        if (
            $res_load
            && $model->validate()
            && $model_profile->validate()
            && $model->abstractModel->validate()
        ) {
            if ($model->save() && $model_profile->save()) {
                $model->getPropertyGroups(true);
                $model->saveProperties(Yii::$app->request->post());
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your profile has been updated'));
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Internal error'));
            }
        }
        $propertyGroups = PropertyGroup::getForModel($model->getObject()->id, $model->id);
        $properties = [];
        foreach ($propertyGroups as $propertyGroup) {
            $properties[$propertyGroup->id] = [
                'group' => $propertyGroup,
                'properties' => Property::getForGroupId($propertyGroup->id),
            ];
        }
        unset($propertyGroups);

        return $this->render(
            'profile',
            [
                'model' => $model,
                'model_profile' => $model_profile,
                'propertyGroups' => $properties,
                'services' => ArrayHelper::map($model->services, 'id', 'service_type'),
            ]
        );
    }


    public function actionIndex()
    {
        $searchModel = new UserProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if( Yii::$app->request->isAjax ) {
            return $this->renderPartial('_grid_my_users', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
    public function actionActivate($id)
    {
        UserProfile::userRestore($id);
        return $this->redirect(Url::toRoute(['index']));
    }

    public function actionDelete($id)
    {
        UserProfile::userDelete($id);
        return $this->redirect(Url::toRoute(['index']));
    }
    /**
     * Displays a single UserProfile model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewMyUser($id)
    {

        $model = $this->findMyUserModel($id);
        return $this->render('my_user_view', ['model' => $model]);
    }

    /**
     * Updates an existing UserProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateMyUser($id)
    {
        /** @var User|HasProperties $model */

        $model_profile = $this->findMyUserModel($id);

        $model = $model_profile->user;
        $model->scenario = 'updateProfile';
        //$model->abstractModel->setAttrubutesValues(Yii::$app->request->post());

        //$model_profile = $this->findMyUserModel($id);
        if (!$model_profile) {
            $model_profile = new UserProfile;
            $model_profile->user_id = $model->id;
        }
        //$model_profile->scenario = 'updateProfile';

        $res_load = $model->load(Yii::$app->request->post()) && $model_profile->load(Yii::$app->request->post());

        if (Yii::$app->request->isAjax && $res_load) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validateMultiple([$model, $model_profile]);
        }

        if (
            $res_load
            && $model->validate()
            && $model_profile->validate()
            && $model->abstractModel->validate()
        ) {
            if ($model->save() && $model_profile->save() && !$model->hasErrors() && !$model_profile->hasErrors()) {
                $model->getPropertyGroups(true);
                $model->saveProperties(Yii::$app->request->post());
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your profile has been updated'));
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Internal error'));
            }
        }
//        $propertyGroups = PropertyGroup::getForModel($model->getObject()->id, $model->id);
//        $properties = [];
//        foreach ($propertyGroups as $propertyGroup) {
//            $properties[$propertyGroup->id] = [
//                'group' => $propertyGroup,
//                'properties' => Property::getForGroupId($propertyGroup->id),
//            ];
//        }
//        unset($propertyGroups);

        return $this->render(
            'my_user_update',
            [
                'model' => $model,
                'model_profile' => $model_profile,
                //'propertyGroups' => $properties,
                //'services' => ArrayHelper::map($model->services, 'id', 'service_type'),
            ]
        );


    }


    /**
     * Finds the UserProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMyUserModel($id)
    {
       if (($model = UserProfile::find()->addByPk($id)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
