<?php

/**
 * @var $model app\models\User
 * @var $model_profile \app\theme\models\UserProfile
 * @var $propertyGroups \app\models\PropertyGroup[]
 * @var $services array
 * @var $this yii\web\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'My Users');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('app', 'Personal cabinet'),
        'url' => '/shop/cabinet'
    ],
    $this->title,
];

?>
<div class="cabinet-my-users">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= $this->render('_grid_my_users', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('eset', 'Create User'), [Url::toRoute(['/signup'])], ['class' => 'btn btn-success']) ?>
    </p>


</div>
