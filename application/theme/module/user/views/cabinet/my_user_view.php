<?php

use app\theme\models\Gender;
use app\theme\models\InvoiceMode;
use app\theme\models\UserProfile;
use app\theme\models\YesNo;
use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\theme\models\UserProfile */

$this->title = $model->client_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'My Users'), 'url' => ['my-users']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-user-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">

        <div class="col-sm-4">
            <table class="table table-no-bordered">
                <tr>
                    <td class="text-right"><?= $model->getAttributeLabel('client_name') ?>:</td>
                    <td><?= $model->client_name ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?= $model->getAttributeLabel('id') ?>:</td>
                    <td><?= $model->id ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?= $model->getAttributeLabel('resellerStatus.discount') ?>:</td>
                    <td><?= $model->resellerStatus ? $model->resellerStatus->discount : null ?></td>
                </tr>
            </table>
        </div>

        <div class="col-sm-4">
            <table class="table table-no-bordered">
                <tr>
                    <td class="text-right">Jooksva kuu tehinguid:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Uuendmit vajab:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Arvad:</td>
                    <td>?</td>
                </tr>
            </table>
        </div>


        <div class="col-sm-4">
            <table class="table table-no-bordered">
                <tr>
                    <td class="text-right"><?= $model->getAttributeLabel('reseller_status') ?></td>
                    <td><?= Html::img($model->resellerStatus ? $model->resellerStatus->getImageUrl() : '', ['height'=>'30']) ?></td>
                </tr>
                <tr>
                    <td class="text-right">Jooksva kuu saldo:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Jooksva aasto saldo:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Järgmine staatus:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Jooksva staatuse täituvus:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Boonuseid:</td>
                    <td>?</td>
                </tr>
                <tr>
                    <td class="text-right">Ettemaks:</td>
                    <td>?</td>
                </tr>
            </table>

        </div>
    </div>

    <hr>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user.username',
            'user.email',
            [
                'attribute' => 'user.status',
                'value' => \app\theme\models\User::getStatuses()[$model->user->status],
            ],
            [
                'attribute' => 'country_id',
                'value' => $model->country ? $model->country->name : null,
            ],
            'locale',
            'address',
            'comment',
            'page',
            'contact',
            'url',
            'telefon',
            [
                'attribute' => 'parent_id',
                'value' => $model->parent ? $model->parent->client_name : null,
            ],
            'position',
            [
                'attribute' => 'invoice_mode',
                'value' => InvoiceMode::getDesc($model->invoice_mode),
            ],
            [
                'attribute' => 'auto_license',
                'value' => YesNo::getDesc($model->auto_license),
            ],
            [
                'attribute' => 'gender',
                'value' => Gender::getDesc($model->gender),
            ],


            'birthday',
            'reseller_status',

            [
                'attribute' => 'created_at',
                'value' => Yii::$app->formatter->asDatetime($model->created_at),
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->formatter->asDatetime($model->updated_at),
            ],
        ]
    ]) ?>

    <?= Html::a(Yii::t('yii', 'Update'), ['/eset/cabinet/update-my-user', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>

</div>
