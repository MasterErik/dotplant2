<?php

/**
 * @var $model app\models\User
 * @var $model_profile \app\theme\models\UserProfile
 * @var $propertyGroups \app\models\PropertyGroup[]
 * @var $services array
 * @var $this yii\web\View
 */

use app\theme\models\Country;
use app\theme\models\Gender;
use app\theme\models\Page;
use app\theme\models\UserProfile;
use app\theme\models\Position;
use kartik\date\DatePicker;
use kartik\widgets\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Your profile');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('app', 'Personal cabinet'),
        'url' => '/shop/cabinet'
    ],
    $this->title,
];

?>
<h1><?= $this->title ?></h1>

<?php
$form = ActiveForm::begin([
    'id' => 'profile-form',
    //'type' => ActiveForm::TYPE_HORIZONTAL,
    'type' => ActiveForm::TYPE_VERTICAL,
    'enableAjaxValidation' => true,
    'formConfig' => [
//        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_MEDIUM,
        'deviceSize' => 'md'
    ],
    'fieldConfig' => [
        'inputOptions' => ['autocomplete' => 'off'],
        'labelOptions' => ['class' => 'control-label'],

    ],
]);
?>


    <div class="col-md-12">
        <?= $form->errorSummary($model); ?>
        <?= $form->errorSummary($model_profile); ?>

        <?= $form->field($model, 'email') ?>


        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model_profile, 'position')->dropDownList(UserProfile::getAvailablePositions()) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model_profile, 'telefon', [
                    //'autoPlaceholder' => true,
                    'feedbackIcon' => [
                        'default' => 'earphone',
                        'success' => 'ok',
                        'error' => 'exclamation-sign',
                        'defaultOptions' => ['class' => 'text-primary']
                    ],
                    'addon' => [
                        'prepend' => [ 'content' => '<i class="glyphicon glyphicon-earphone"></i>']
                    ]
                ])->textInput() ?>
            </div>
        </div>

        <div class="row not-legal-entity">
            <div class="col-md-6">
                <?= $form->field($model, 'first_name', [
                    'enableClientValidation' => false,
                    'autoPlaceholder' => true,
                    'feedbackIcon' => [
                        'default' => 'user',
                        'success' => 'ok',
                        'error' => 'exclamation-sign',
                        'defaultOptions' => ['class' => 'text-primary']
                    ],
                ])->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'last_name', ['autoPlaceholder' => true])->textInput() ?>
            </div>
        </div>

        <div class="row legal-entity">
            <div class="col-md-12">
                <?= $form->field($model_profile, 'client_name', [
                    'enableClientValidation' => false,
                    'autoPlaceholder' => true,
                    'feedbackIcon' => [
                        //'default' => 'user',
                        'success' => 'ok',
                        'error' => 'exclamation-sign',
                        'defaultOptions' => ['class' => 'text-primary']
                    ],
                ])->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model_profile, 'country_id', [
                    'autoPlaceholder' => true,
                    'feedbackIcon' => [
                        'default' => 'globe',
                    ],
                    'addon' => [
                        'prepend' => [ 'content' => '<i class="glyphicon glyphicon-globe"></i>']
                    ]
                ])->dropDownList(Country::getList()) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model_profile, 'url', [
                    'autoPlaceholder' => true,
                    'feedbackIcon' => [
                        'default' => 'globe',
                        'defaultOptions' => ['class' => 'text-primary']
                    ],
                    'addon' => [
                        'prepend' => [ 'content' => '<i class="glyphicon glyphicon-globe"></i>']
                    ]
                ])->textInput() ?>
            </div>
        </div>


        <?= $form->field($model_profile, 'locale')->dropDownList(\app\theme\components\MultiLangHelper::getLanguages()) ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model_profile, 'contact', [
                    'addon' => [
                        'prepend' => [ 'content' => '<i class="fa fa-file-text-o"></i>']
                    ]
                ])->textarea() ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model_profile, 'comment' , [
                    'addon' => [
                        'prepend' => [ 'content' => '<i class="fa fa-file-text-o"></i>']
                    ]
                ])->textarea() ?>
            </div>
        </div>



        <?= $form->field($model_profile, 'gender')->dropDownList(Gender::getDropDownList()) ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model_profile, 'birthday')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => Yii::t('eset', 'Enter birth date') . ' ....'],
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model_profile, 'page')->dropDownList(Page::getList()) ?>
            </div>

        </div>

        <?php foreach ($propertyGroups as $group): ?>
            <?php if ($group['group']->hidden_group_title == 0): ?>
                <h4><?= $group['group']->name; ?></h4>
            <?php endif; ?>
            <?php
            /** @var \app\models\Property[] $properties */
            $properties = $group['properties'];
            ?>
            <?php foreach ($properties as $property): ?>
                <?= $property->handler($form, $model->abstractModel, [], 'frontend_edit_view'); ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

        <div class="form-group">
            <div class="col-md-offset-10 col-md-12">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>


    <div class="col-md-12">
        <br>
        <div class="well">
            <?php
            $authChoice = AuthChoice::begin([
                'baseAuthUrl' => ['default/auth']
            ]);
            ?>
            <?php if (count($authChoice->clients) > count($services)): ?>
                <h3><?= Yii::t('app', 'Attach service') ?></h3>
                <ul class="auth-clients clear">
                    <?php foreach ($authChoice->clients as $client): ?>
                        <?php if (!in_array($client->className(), $services)): ?>
                            <li class="auth-client"><?= $authChoice->clientLink($client) ?></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <?php AuthChoice::end(); ?>
            <?php if (!empty($services)): ?>
                <h3><?= Yii::t('app', 'Detach service') ?></h3>
                <?php
                $authChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['default/auth']
                ]);
                ?>
                <ul class="auth-clients clear">
                    <?php foreach ($authChoice->clients as $client): ?>
                        <?php if (in_array($client->className(), $services)): ?>
                            <li class="auth-client"><?= $authChoice->clientLink($client) ?></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <?php AuthChoice::end(); ?>
            <?php endif; ?>
        </div>
    </div>



<?php
$this->registerJs("
var userprofile_position_change = function(){
    switch( jQuery('#userprofile-position').val() ) {
        case '". Position::POSITION_EMPLOYEE ."':
        case '". Position::POSITION_CLIENT ."':
            jQuery('.not-legal-entity').show();
            jQuery('.legal-entity').hide();
            break;
        default:
            jQuery('.not-legal-entity').hide();
            jQuery('.legal-entity').show();
            break;
    }
};
jQuery('#userprofile-position').change(userprofile_position_change);
userprofile_position_change();
", \yii\web\View::POS_END, 'userprofile-position-change');
?>
