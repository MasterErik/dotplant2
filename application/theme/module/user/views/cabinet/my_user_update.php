<?php

use app\theme\models\Gender;
use app\theme\models\ResellerStatusList;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use app\theme\models\UserProfile;
use app\theme\models\Position;
use app\theme\models\ResellerStatus;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\theme\models\User */
/* @var $model_profile app\theme\models\UserProfile */

$this->title = Yii::t('yii', 'Update') . ' ' . $model_profile->client_name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('eset', 'My Users'), 'url' => ['my-users']];
$this->params['breadcrumbs'][] = ['label' => $model_profile->client_name, 'url' => ['view-my-user', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('yii', 'Update');
?>
<div class="license-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $form = ActiveForm::begin([
        'id' => 'profile-form',
        //'type' => ActiveForm::TYPE_VERTICAL,
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'enableAjaxValidation' => true,
        'formConfig' => [
            'labelSpan' => 3,
            'deviceSize' => 'md'
        ],
    ]);
    ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->errorSummary($model_profile); ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model_profile, 'position')->dropDownList(UserProfile::getAvailablePositions()) ?>

    <?= $form->field($model, 'first_name', ['options' => ['class' => 'form-group not-legal-entity']]) ?>
    <?= $form->field($model, 'last_name', ['options' => ['class' => 'form-group not-legal-entity']]) ?>

    <?= $form->field($model_profile, 'client_name', ['options' => ['class' => 'form-group legal-entity']]) ?>
    <?= $form->field($model_profile, 'contact', ['options' => ['class' => 'form-group legal-entity']])->textarea() ?>

    <?php
    if( UserProfile::iPartner() ) {
        echo $form->field($model_profile, 'reseller_status', ['options' => ['class' => 'form-group reseller-entity']])
            ->dropDownList(ResellerStatusList::getDropDownList(true)); // ArrayHelper::merge([''=>''], ArrayHelper::map(ResellerStatus::find()->my()->orderBy('name')->all(), 'id', 'name')));
    }
    ?>

    <?= $form->field($model_profile, 'country_id')->dropDownList(\app\theme\models\Country::getList()) ?>
    <?= $form->field($model_profile, 'locale')->dropDownList(\app\theme\components\MultiLangHelper::getLanguages()) ?>
    <?= $form->field($model_profile, 'address')->textarea() ?>
    <?= $form->field($model_profile, 'comment')->textarea() ?>
    <?= $form->field($model_profile, 'page')->dropDownList(\app\theme\models\Page::getList()) ?>

    <?= $form->field($model_profile, 'url')->textInput() ?>
    <?= $form->field($model_profile, 'telefon')->textInput() ?>

    <?= $form->field($model_profile, 'gender')->dropDownList(Gender::getDropDownList()) ?>
    <?= $form->field($model_profile, 'birthday')->textInput(['placeholder' => 'YYYY-MM-DD']) ?>

    <?php /* foreach ($propertyGroups as $group): ?>
        <?php if ($group['group']->hidden_group_title == 0): ?>
            <h4><?= $group['group']->name; ?></h4>
        <?php endif; ?>
        <?php
        /** @var \app\models\Property[] $properties * /
        $properties = $group['properties'];
        ?>
        <?php foreach ($properties as $property): ?>
            <?= $property->handler($form, $model->abstractModel, [], 'frontend_edit_view'); ?>
        <?php endforeach; ?>
    <?php endforeach;*/ ?>

    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs("
var userprofile_position_change = function(){
    switch( jQuery('#userprofile-position').val() ) {
        case '". Position::POSITION_EMPLOYEE ."':
        case '". Position::POSITION_CLIENT ."':
            jQuery('.not-legal-entity').show();
            jQuery('.legal-entity').hide();
            jQuery('.reseller-entity').hide();
            break;
        case '". Position::POSITION_RESELLER ."':
            jQuery('.not-legal-entity').hide();
            jQuery('.legal-entity').show();
            jQuery('.reseller-entity').show();
            break;
        default:
            jQuery('.not-legal-entity').hide();
            jQuery('.legal-entity').show();
            jQuery('.reseller-entity').hide();
            break;
    }
};
jQuery('#userprofile-position').change(userprofile_position_change);
userprofile_position_change();
", \yii\web\View::POS_END, 'userprofile-position-change');
?>
