<?php

use app\theme\models\Position;
use app\theme\models\ResellerStatus;
use app\theme\models\ResellerStatusList;
use app\theme\models\UserProfile;
use app\theme\models\YesNo;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use app\theme\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\theme\models\search\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'columns' => [
            [
                'attribute' => 'username',
                'format' => 'text',
            ],
            'client_name',
            'telefon',
            [
                'attribute' => 'position',
                'filter' => Position::getDropDownList(),
            ],

            [
                'attribute' => 'reseller_status_id',
                'value' => function($model) {
                    return $model->resellerStatus ? Html::a($model->resellerStatus->reseller_status,
                        Url::toRoute(['/eset/reseller-status', 'id' => $model->reseller_status_id])) : null;
                },
                'format' => 'raw',
                'filter' => ResellerStatusList::getDropDownList(),
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    $statuses = User::getStatuses();
                    return $model->user ? isset($statuses[$model->user->status]) ? $statuses[$model->user->status] : $model->user->status : null;
                },
                'filter' => User::getStatuses(),
            ],
            [
                'attribute' => 'auto_license',
                'value' => function($model) {
                    $list =  YesNo::getDropDownList();
                    return $model->auto_license ? $list[$model->auto_license] : null;
                },
                'filter' => YesNo::getDropDownList(),
            ],
            'email',
            [
                'attribute' => 'create_time',
                'format' => 'datetime',
                'headerOptions' => ['width' => '200'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'options' => ['placeholder' => 'Start'],
                    'options2' => ['placeholder' => 'End'],
                    'type' => DatePicker::TYPE_RANGE,
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                    ]
                ]),
            ],
            'comment',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'headerOptions' => ['width' => '100'],
                'template' => '{view} {update} {delete} {create} {license}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/eset/user/cabinet/view-my-user', 'id' => $model->user_id], [
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax' => 0
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/eset/user/cabinet/update-my-user', 'id' => $model->user_id], [
                            'title' => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax' => 0
                        ]);
                    },
                    'create' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', ['/eset/user/cabinet/activate', 'id' => $model->user_id], [
                            'title' => Yii::t('yii', 'Activate'),
                            'aria-label' => Yii::t('yii', 'Activate'),
                            'data-pjax' => 0
                        ]);
                    },
                    'license' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon glyphicon-oil"></span>', ['/eset/license', 'user_id' => $model->user_id], [
                            'title' => Yii::t('eset', 'License'),
                            'aria-label' => Yii::t('eset', 'License'),
                            'data-pjax' => 0
                        ]);
                    },
                ],
                'contentOptions' => ['class' => 'text-nowrap'],
            ],
        ],
    ]);
//<a href="/en/eset/cabinet/view?id=1" title="View" aria-label="View" data-pjax="0"></a>

?>


