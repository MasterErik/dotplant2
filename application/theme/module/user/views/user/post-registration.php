<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\User */
/* @var $model_profile \app\theme\models\UserProfile */

$this->title = Yii::t('app', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
if ($model->username_is_temporary) $model->username = '';
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Please fill in required fields to complete registration') ?></p>
    <?php $form = ActiveForm::begin(['id' => 'form-signup', 'action' => ['/user/user/complete-registration'], 'enableAjaxValidation' => true]); ?>
    <?= \app\widgets\Alert::widget() ?>
    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>

            <?= $form->field($model_profile, 'position')->dropDownList(\app\theme\models\UserProfile::getAvailablePositions()) ?>

            <?= $form->field($model_profile, 'country_id')->dropDownList(\app\theme\models\Country::getList()) ?>
            <?= $form->field($model_profile, 'locale')->dropDownList(\app\theme\components\MultiLangHelper::getLanguages()) ?>
            <?= $form->field($model_profile, 'address')->textarea() ?>
            <?= $form->field($model_profile, 'comment')->textarea() ?>
            <?= $form->field($model_profile, 'page')->dropDownList(\app\theme\models\Page::getList()) ?>
            <?= $form->field($model_profile, 'contact')->textarea() ?>
            <?= $form->field($model_profile, 'url')->textInput() ?>
            <?= $form->field($model_profile, 'telefon')->textInput() ?>

            <?= $form->field($model_profile, 'gender')->dropDownList(\app\theme\models\Gender::getDropDownList()) ?>
            <?= $form->field($model_profile, 'birthday')->textInput(['placeholder' => 'YYYY-MM-DD']) ?>



        </div>
        <div class="col-md-6">

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Complete registration'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
