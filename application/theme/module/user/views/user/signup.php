<?php

/**
 * @var $model \app\theme\models\RegistrationForm
 * @var $this \yii\web\View
 */

use app\theme\models\Gender;
use app\theme\models\InvoiceMode;
use app\theme\models\Position;
use app\theme\models\UserProfile;
use app\theme\models\YesNo;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>


<?php
$form = ActiveForm::begin([
    'id' => 'form-signup',
    'type' => ActiveForm::TYPE_VERTICAL,
    'enableAjaxValidation' => true,

    'formConfig' => [
//        'labelSpan' => 3,
        'deviceSize' => ActiveForm::SIZE_MEDIUM,
        'showLabels' => true,
    ],
    'fieldConfig' => [
//        'template' => "<div class='col-sm-6'>{label}\n<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-user\"></i></span>{input}\n</div>{hint}\n{error}</div>",
//        'autoPlaceholder' => true,
        'inputOptions' => ['autocomplete' => 'off'],
        'labelOptions' => ['class' => 'control-label'],
    ],
]);
?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'position')->dropDownList(UserProfile::getAvailablePositions()) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'email', [
            'enableClientValidation' => false,
//            'autoPlaceholder' => true,
            'feedbackIcon' => [
                'default' => 'envelope',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
            'addon' => [
                'prepend' => [ 'content' => '<i class="glyphicon glyphicon-envelope"></i>']
            ]
        ])
        ?>
    </div>
</div>

<?php if (Yii::$app->user->isGuest) { ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'password', [
                'autoPlaceholder' => true,
                'feedbackIcon' => [
                    'default' => 'lock',
                    'success' => 'ok',
                    'error' => 'exclamation-sign',
                    'defaultOptions' => ['class' => 'text-primary']
                ],
                'addon' => [
                    'prepend' => [ 'content' => '<i class="glyphicon glyphicon-lock"></i>']
                ]
            ])->passwordInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'confirmPassword', [
                'autoPlaceholder' => true,
                'feedbackIcon' => [
                    'default' => 'lock',
                    'success' => 'ok',
                    'error' => 'exclamation-sign',
                    'defaultOptions' => ['class' => 'text-primary']
                ],
                'addon' => [
                    'prepend' => [ 'content' => '<i class="glyphicon glyphicon-lock"></i>']
                ]
            ])->passwordInput() ?>
        </div>
    </div>

<?php } ?>

<div class="row not-legal-entity">
    <div class="col-md-6">
        <?= $form->field($model, 'first_name', [
            'enableClientValidation' => false,
            'autoPlaceholder' => true,
            'feedbackIcon' => [
                'default' => 'user',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
        ])->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'last_name', ['autoPlaceholder' => true])->textInput() ?>
    </div>
</div>
<div class="row legal-entity">
    <div class="col-md-12">
        <?= $form->field($model, 'client_name', [
            'enableClientValidation' => false,
            'autoPlaceholder' => true,
            'feedbackIcon' => [
                //'default' => 'user',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
        ])->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'username', [
            'autoPlaceholder' => true,
            'feedbackIcon' => [
                'default' => 'user',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
            'addon' => [
                'prepend' => [ 'content' => '<i class="glyphicon glyphicon-user"></i>']
            ]
        ])
        ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'telefon', [
            'autoPlaceholder' => true,
            'feedbackIcon' => [
                'default' => 'earphone',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
            'addon' => [
                'prepend' => [ 'content' => '<i class="glyphicon glyphicon-earphone"></i>']
            ]
        ])->textInput() ?>
    </div>
</div>

<div class="row legal-entity">
    <div class="col-md-12">
        <?= $form->field($model, 'address', [
            'addon' => [
                'prepend' => [ 'content' => '<i class="fa fa-file-text-o"></i>']
            ]
        ])->textarea() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'locale')->dropDownList(\app\theme\components\MultiLangHelper::getLanguages()) ?>
    </div>

    <?php
    if (UserProfile::iPartner()) {
        echo '<div class="col-md-6">';
        $form->field($model, 'invoice_mode')->dropDownList(InvoiceMode::getDropDownList());
        echo '</div><div class="col-md-6">';
        $form->field($model, 'auto_license')->dropDownList(YesNo::getDropDownList());
        echo '</div>';
    }
    ?>
    <div class="col-md-6">
        <?= $form->field($model, 'gender')->dropDownList(Gender::getDropDownList()) ?>
    </div>
</div>

<div class="form-group">
    <?= $form->errorSummary($model, ['class'=>'alert alert-danger']); ?>
    <div class="col-md-offset-6 col-md-12">
        <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php
$this->registerJs("
dig = new Array('','0','1','2','3','4','5','6','7','8','9');
char = new Array('','a','b','c','d','e','f','g','h','i','j','k','l',
    'm','n','o','p','q','r','s','t','u','v','w','x','y','z');

function rnd(x,y,z) {
     var num;
     do {
        num = parseInt(Math.random()*z);
        if (num >= x && num <= y) break;
     } while (true);
    return(num);
};
function gen_pass() {
    var pswrd = '';
    var znak, s;
    var k = 0;
    var n = 15;
    var pass = new Array();
    var w = rnd(30,80,100);
    var low;
    var value = $('#registrationform-first_name');
    low = (value.val()) ? (value.val()) : '';
    var value = $('#registrationform-last_name');
    low = low + ((value.val()) ? (value.val()) : '');

    if (low === '') {
        low = char;
    }

    for (var r = 0; r < w; r++) {
        znak = rnd(1, low.length-1, 100); pass[k] = low[znak]; k++;
        znak = rnd(1, 10, 100); pass[k] = dig[znak]; k++;
    }
    for (var i = 0; i < Math.min(n, pass.length-1); i++) {
        s = rnd(1,k-1,100);
        pswrd += pass[s];
    }
    return pswrd;
}
var registrationform_gen_login = function(){
    if (this.value === '') {
        this.value = gen_pass();
    }
};
var registrationform_position_change = function(){
    switch( $('#registrationform-position').val() ) {
        case '" . Position::POSITION_CLIENT . "':
            $('.legal-entity').hide();
            break;
        default:
            $('.legal-entity').show();
            break;
    }
};
$('#registrationform-position').change(registrationform_position_change);
registrationform_position_change();
$('#registrationform-username').focus(registrationform_gen_login);
", \yii\web\View::POS_END, 'registrationform-position-change');
//  jQuery('.not-legal-entity').show();
//  jQuery('.not-legal-entity').hide();
?>

<?php ActiveForm::end(); ?>

