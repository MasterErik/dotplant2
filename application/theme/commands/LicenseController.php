<?php
//create database d6381sd4126 character set utf8;
//create user 'd6381sd4126'@'localhost' identified by 'd6381sd4126';
//grant all privileges on d6381sd4126.* to 'd6381sd4126'@'localhost';
//cat dexter.sql | mysql -u d6381sd4126 -pd6381sd4126 d6381sd4126

namespace app\theme\commands;

use app\theme\components\restClient\entity\SearchLicense;
use app\theme\components\restClient\entity\UpdateLicense;
use yii\db\Connection;
use Yii;
use yii\db\Query;

class LicenseController extends ConsoleController
{
    /** @var Connection $db */
    public $db;
    public $default_user_id = 1;

    public function init()
    {
        parent::init();
        $this->db = Yii::$app->old_database_ee;
//        $this->db = Yii::$app->old_database_lv;
    }

    public function actionSyncEmail()
    {
        Yii::$app->params['defaultResellerId'] = 1;
        Yii::$app->restClient->configuration['server'] = 'work';

//        $this->addColumn('dexter_licenses', 'epli', 'VARCHAR(50) NULL');
//        $this->addColumn('dexter_licenses', 'need_update', 'tinyint(1) default 0');
//        $this->addColumn('dexter_licenses', 'license_key', 'VARCHAR(50) NULL');
//        $this->addColumn('dexter_licenses', 'public_license_key', 'VARCHAR(50) NULL');

        $sql = 'SELECT license_id, epli, need_update, date, expirationdate, email, reseller_email, username, password
                FROM dexter_licenses
                WHERE license_status="active"
                    AND email IS NOT NULL AND username IS NOT NULL AND password IS NOT NULL AND (epli IS NULL OR need_update=1)
                    AND date > STR_TO_DATE("2016-01-01 00:00:00", "%Y-%m-%d %H:%i:%s")';
                    //expirationdate
                    //AND license_id = 82522';

        $reader = $this->db->createCommand($sql)->query();

/*
        $query = new Query();
        $rows = $query
            ->select('license_id, epli, need_update, date, expirationdate, firstname, lastname, company_name, email, reseller_email, username, password')
            ->from('dexter_licenses')
//            ->where('expirationdate > NOW() - INTERVAL 6 MONTH')
            ->where('state="active"')
            ->andWhere('email IS NOT NULL')
            ->andWhere('username IS NOT NULL')
            ->andWhere('password IS NOT NULL')
            ->andWhere('epli IS NULL OR need_update=1')
            ->andWhere('expirationdate > STR_TO_DATE("2013-01-01 00:00:00", "%Y-%m-%d %H:%i:%s")')
//            ->andFilterWhere(['license_id' => 83474])
            ->all($this->db);
        $this->debugMsg('Need update: ' . count($rows));
*/

        $i = 0;
        foreach ($reader as $row) {
            $i++;
            // запросим epli

            try {
                $rest_licenses = SearchLicense::all([
                    //'customeremail' => $row['reseller_email'],
                    'username' => $row['username'],
                    'password' => $row['password'],
                ], $this->default_user_id);
            } catch (\Exception $e) {
                continue;
            }

            if (!is_array($rest_licenses) || count($rest_licenses) == 0) {
                $this->error('License for ' . $row['license_id'] . '  ' . $row['username'] . ':' . $row['password'] . ' not found');
                continue;
            }

            $index = 0;

            if (count($rest_licenses) > 1) {
                $index = -1;
                foreach ($rest_licenses as $key => $rest_license) {
                    if ($row['expirationdate'] == date('Y-m-d', strtotime($rest_license->LicenseInfo->ExpirationDate))) {
                        $index = $key;
                        break;
                    }
                }

                if ($index == -1) {
                    $this->debugMsg('License for ' . $row['license_id'] . ' ' . $row['username'] . ':' . $row['password'] . ' not valid');
                    continue;
                }
            }

            $need_update = 0;
            if ($rest_licenses[$index]->Customer->Email != $row['email']) {
                $need_update = 1;
                $this->debugMsg('License for ' . $row['license_id'] . ' ' . $row['username'] . ':' . $row['password'] . ' need update Email');
            }

            //$transaction  = $this->db->beginTransaction();

            $sql = "UPDATE dexter_licenses
                SET epli = :epli,
                    license_key = :license_key,
                    public_license_key = :public_license_key,
                    need_update = :need_update
                WHERE license_id = :license_id";
            $this->db->createCommand($sql, [
                ':epli' => $rest_licenses[$index]->Credentials->EPLI,
                ':need_update' => $need_update,
                ':license_key' => $rest_licenses[$index]->Credentials->LicenseKey,
                ':public_license_key' => $rest_licenses[$index]->Credentials->PublicLicenseKey,
                ':license_id' => $row['license_id']
            ])->execute();

            $epli = $rest_licenses[$index]->Credentials->EPLI;

            $email = $row['email'];


            if ($need_update) {
                $license = UpdateLicense::one([
                    'epli' => $epli,
                    'Customer' => [
                        'Email' => $email,
                        'Name' => $rest_licenses[$index]->Customer->Name,
                        'CountryId' => 22,
                    ],
                    'Quantity' => $rest_licenses[$index]->LicenseInfo->Quantity,
                    'ExpirationDate' => $rest_licenses[$index]->LicenseInfo->ExpirationDate,
                    'LicenseAdministrator' => [
                        'Email' => 'orders-ee@eset.ee'
                    ],
                ], $this->default_user_id);

                // проверить результат
                if (isset($license->ErrorId)) {
                    $sql = "UPDATE dexter_licenses
                                SET need_update = $license->ErrorId
                                WHERE license_id = :license_id";
                    $this->db->createCommand($sql, [':license_id' => $row['license_id']])->execute();
                    $this->debugMsg('Error:' . $license->ErrorId . ' license for ' . $row['license_id']);
                } elseif ($license->Customer->Email == $email) {
                    $sql = "UPDATE dexter_licenses SET need_update = 0 WHERE license_id = :license_id";
                    $this->db->createCommand($sql, [':license_id' => $row['license_id']])->execute();
                    $this->debugMsg('Change email license for ' . $row['username'] . ':' . $row['password']);
                } else {
                    $this->error('Error change email license for ' . $row['username'] . ':' . $row['password']);
                }
            }
            //$transaction->commit();
            echo '.';
            if (($i % 100) == 0) {
                echo "\n";
            }
        }
        //exit;
        $this->debugMsg('Synchronization is complete');
    }

    private function addColumn($table, $column, $type)
    {
        // There is no difference between the privileges required for SHOW statements and those required to select information from INFORMATION_SCHEMA. In either case, you have to have some privilege on an object in order to see information about it.
        // @link https://dev.mysql.com/doc/refman/5.0/en/information-schema.html

        $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :table AND COLUMN_NAME = :column";
        $res = $this->db->createCommand($sql, [':table' => $table, ':column' => $column])->queryOne();

        if (!$res) {
            $this->db->createCommand()->addColumn($table, $column, $type)->execute();
        }


    }
}
