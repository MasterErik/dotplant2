<?
ini_set("memory_limit", "512M");
ini_set("max_execution_time", "600");

$rates_2010 = array('EEK' => 1, 'EUR' => 0.0639116485370624, 'USD' => 0.085391991938996, 'LVL' => 0.0453646409842313);#pre_eur, base = eek, EP kursid 31.12.2010, http://www.eestipank.ee/dynamic/erp/erp_et.jsp
$rates_2011 = array('EEK' => 15.6466, 'EUR' => 1, 'USD' => 1.4170, 'LVL' => 0.7092);#base = eur

#echo $_SERVER['DOCUMENT_ROOT'];
#--------------------------------
function replace_newline($string) {
    return (string)str_replace(array("\r", "\r\n", "\n"), '', $string);
}
#--------------------------------
if (!function_exists('fputcsv')) {

    function fputcsv(&$handle, $fields = array(), $delimiter = ',', $enclosure = '"') {
        $str = '';
        $escape_char = '\\';
        foreach ($fields as $value) {
            if (strpos($value, $delimiter) !== false ||
                strpos($value, $enclosure) !== false ||
                strpos($value, "\n") !== false ||
                strpos($value, "\r") !== false ||
                strpos($value, "\t") !== false ||
                strpos($value, ' ') !== false) {
                $str2 = $enclosure;
                $escaped = 0;
                $len = strlen($value);
                for ($i=0;$i<$len;$i++) {
                    if ($value[$i] == $escape_char) {
                        $escaped = 1;
                    } else if (!$escaped && $value[$i] == $enclosure) {
                        $str2 .= $enclosure;
                    } else {
                        $escaped = 0;
                    }
                    $str2 .= $value[$i];
                }
                $str2 .= $enclosure;
                $str .= $str2.$delimiter;
            } else {
                $str .= $value.$delimiter;
            }
        }
        $str = substr($str,0,-1);
        $str .= "\n";
        return fwrite($handle, $str);
    }
}
#----------------------------
#argo 22.07.2011, hansaworld sbe export, parsimine allpool, arvete nimekirja koostamise juures
function hansaExport($awinvoices, $awitems) {

    $filename = '/export/invoices_export_hw_sbe_'.date('Ymd_His').'.txt';
    $exportfile = $_SERVER['DOCUMENT_ROOT'].$filename;
    @unlink($exportfile);
    $fp = fopen($exportfile, "w");
    @fwrite($fp, '');

    $headers[] = array('format');
    $headers[] = array('1', '44', '1', '0', '0');
    $headers[] = array('');
    $headers[] = array('commentstring');
    $headers[] = array('');
    $headers[] = array('');
    $headers[] = array('IVVc');
    #write header rows
    foreach ($headers as $header) {
        fputcsv($fp, $header, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
    }
    #write invoice rows
    if (is_array($awinvoices)) {
        foreach ($awinvoices as $invoice_id => $invoice_data) {
            fputcsv($fp, $invoice_data, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
            if (is_array($awitems[$invoice_id])) {
                foreach ($awitems[$invoice_id] as $items_data) {
                    #utf8_decode($col);
                    fputcsv($fp, $items_data, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
                }
            }
            fputcsv($fp, array(), chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
        }
    }
    @fclose($fp);
    /*
    if (!headers_sent()) {
        header('Location:http://www.e-arve.com'.$filename);
        return;
    }
    */
    #readfile($exportfile);
    return '<a href="'.$filename.'">lae alla eksportfail Hansaworld SBE jaoks</a>';
}
#--------------------------------
// http://www.e-arve.com/index.php?menu=invoices&action=edit&invoice_nr=1005&year=2008
if ($invoice_nr > 0 && $year)
{
    $inv = mysql_fetch_array($DB->query("SELECT i.* FROM invoice_invoices i, invoice_users u WHERE i.invoice_nr = '$invoice_nr' AND u.uid = '".${$USER}[uid]."' AND i.company_id = u.company_id AND YEAR(i.out_date) = '$year'"));
    if ($inv[invoice_id])
    {
        header("Location: ?menu=invoices&action=edit&invoice_id=".$inv[invoice_id]);
        exit;
    }
}
else if ($invoice_id > 0)
{
    $inv = mysql_fetch_array($DB->query("SELECT i.* FROM invoice_invoices i, invoice_users u WHERE i.invoice_id = '$invoice_id' AND u.uid = '".${$USER}[uid]."' AND (i.company_id = u.company_id OR u.company_id = '0')"));
    if (!$inv[invoice_id])
    {
        header("Location: ?menu=invoices");
        exit;
    }
}

if (!${$USER}[uid] || $priv[invoices] == 0) {
    header("Location: /index.php");exit;
}

if ($priv[invoices] < 2 && strlen($action)) {
    header("Location: ?menu=invoices");exit;
}

include ("lib/number_parser.class.php");

function getNextInvoiceNR($company_id,$year,$month,$day,$invoice_id = 0)
{
    global $DB;

    list($number_format) = mysql_fetch_row($DB -> query("SELECT number_format FROM invoice_companies WHERE company_id = '$company_id'"));
    list($number_start) = mysql_fetch_row($DB -> query("SELECT number_start	FROM invoice_companies WHERE company_id = '$company_id'"));
    switch ($number_format) {
        case "number":
            list($max_nr) = mysql_fetch_row($DB -> query("SELECT MAX(CAST(invoice_nr AS UNSIGNED)) FROM invoice_invoices WHERE company_id = '$company_id'"));
            $max_nr = (int)$max_nr;
            if (!$max_nr) {
                $max_nr = $number_start;
            }
            else {
                $max_nr = (int)$max_nr + 1;
            }
            break;
        case "date":
            if ($invoice_id > 0) {
                list($invoice_nr) = mysql_fetch_row($DB -> query("SELECT invoice_nr FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));
                if (substr($invoice_nr,0,8) == "$day$month$year") {
                    return $invoice_nr;
                }
            }
            $result = $DB -> query("SELECT invoice_nr FROM invoice_invoices WHERE company_id = '$company_id' AND LEFT(invoice_nr,8) = '$day$month$year'".($invoice_id > 0 ? " AND invoice_id <> '$invoice_id'" : ""));
            if (!mysql_num_rows($result)) {
                $max_nr = "$day$month$year-1";
            }
            else {
                $numbers = "";
                while ($r = mysql_fetch_array($result)) {
                    $regs = explode("-",$r[invoice_nr]);
                    $nrs[] = $regs[1];
                }

                $max_nr = max($nrs) + 1;
                $max_nr = "$day$month$year-".$max_nr;
            }
            break;
    }


    return $max_nr;
} //end func

function reCalculate($invoice_id)
{
    global $DB,$LANG,${$LANG};

    list($wat) = mysql_fetch_row($DB -> query("SELECT C.wat FROM invoice_invoices AS I LEFT JOIN invoice_companies AS C USING(company_id) WHERE I.invoice_id = '$invoice_id'"));
    $wat = "1.".$wat;

    list($currency_id) = mysql_fetch_row($DB -> query("SELECT currency_id FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));

    //list($total[item_sum]) = mysql_fetch_row($DB -> query("SELECT SUM(item_price * item_count) FROM invoice_invoice_items WHERE invoice_id = '$invoice_id'"));
    list($kreedit) = mysql_fetch_row($DB -> query("SELECT kreedit FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));

    $result = $DB -> query("SELECT * FROM invoice_invoice_items WHERE invoice_id = '$invoice_id'");
    while ($r = mysql_fetch_array($result)) {
        $r[item_price] = ($r[discount] > 0 ? $r[item_price] - ($r[item_price] * ($r[discount] / 100)): $r[item_price]);
        $total[item_sum] += ($r[item_price] * $r[item_count]);
        if ($kreedit) {
            $total[item_sum] = 0 - abs($total[item_sum]);
        }
    }
    list($client_id) = mysql_fetch_row($DB -> query("SELECT client_id FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));
    list($viivis_summa) = mysql_fetch_row($DB -> query("SELECT viivis_summa FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));
    list($include_wat) = mysql_fetch_row($DB -> query("SELECT include_wat FROM invoice_clients WHERE client_id = '$client_id'"));

    if (!$client_id) {
        $include_wat = true;
    }

    if ($include_wat) {
        $total[tax_sum] = round(($total[item_sum] * $wat) - $total[item_sum],2);
        $total[sum_with_tax] = round($total[item_sum] + $total[tax_sum],2);
    }
    else {
        $total[sum_with_tax] = $total[item_sum];
    }


    if ($currency_id != 'EUR') {
        if (${$LANG}[lang_id] <> 9) {
            //&uuml;mardamine
            $tempSum = $total[sum_with_tax] * 100;
            $tempSum = round($tempSum);
            $tempSum = (int)$tempSum;
            if ($tempSum % 5 <= 2) {
                $total[rounding] = 0 - (($tempSum % 5) / 100);
            }
            else {
                $total[rounding] = 0.05 - (($tempSum % 5) / 100);
            }

        }
    }

    $total[total_sum] = $total[sum_with_tax]; // + $total[rounding];

    $np = false;
    if ((!mb_strlen($currency_id) || $currency_id == 'LVL') && ${$LANG}[lang_str] == 'lv') {
        $np = true;
    }
    elseif ((!mb_strlen($currency_id) || $currency_id == 'EEK' || $currency_id == 'EUR' || $currency_id == 'LVL') && (${$LANG}[lang_str] == 'est' || ${$LANG}[lang_str] == 'eng' || ${$LANG}[lang_str] == 'lv')) {
        $np = true;
    }
    $np_currency = $currency_id; //'EEK';

    if ($np) {
        $NParser = new NumberParser(number_format($total[total_sum] + $viivis_summa,2,".",""),$np_currency, ${$LANG}[lang_str]);
    }

    $DB -> query("UPDATE invoice_invoices SET item_sum = '$total[item_sum]', tax_sum = '$total[tax_sum]', sum_with_tax = '$total[sum_with_tax]', rounding = '$total[rounding]', total_sum = '$total[total_sum]', sum_in_words = '".($NParser->output)."' WHERE invoice_id = '$invoice_id'");
} //end func

switch ($action) {
    case "setpaid":
        $DB -> query("UPDATE invoice_invoices SET paid = '1' WHERE invoice_id = '$invoice_id'");
        $DB -> query("UPDATE invoice_invoices SET paid_dt = '".date("Y-m-d H:i:s")."' WHERE invoice_id = '$invoice_id' AND paid_dt IS NULL");
        if ($backurl_menu) {
            header("Location: ?menu=$backurl_menu&action=$backurl_action&client_id=$client_id");exit;
        }
        else {
            header("Location: ?menu=invoices&action=edit&invoice_id=$invoice_id");exit;
        }
        break;
    case "delete":
        if (${$USER}[uid] == 2) {
            $DB -> query("DELETE FROM invoice_invoices WHERE invoice_id = '$invoice_id'");
            $DB -> query("DELETE FROM invoice_invoice_items WHERE invoice_id = '$invoice_id'");
        }
        header("Location: ?menu=invoices");exit;
        break;
    case "delitem":
        $item_id = (int)$item_id;
        $invoice_id = (int)$invoice_id;
        if ($invoice_id > 0 && $item_id > 0) {
            $DB -> query("DELETE FROM invoice_invoice_items WHERE item_id = '$item_id' AND invoice_id = '$invoice_id'");
        }
        reCalculate($invoice_id);
        header("Location: ?menu=invoices&action=edit&invoice_id=$invoice_id");exit;
        break;
    case "save":
        $invoice[company_id] = (int)$invoice[company_id];
        if (!$invoice[company_id]) {
            $errors[company_id] = $LANG_STRINGS[invoices][err_puudub_firma];
        }
        if (mysql_num_rows($DB -> query("SELECT * FROM invoice_invoices WHERE company_id = '$invoice[company_id]' AND invoice_nr = '$invoice[invoice_nr]' AND YEAR(out_date) = '$invoice[v_year]' ".($invoice_id > 0 ? " AND invoice_id <> '$invoice_id'" : "")))) {
            $errors[invoice_nr] = $LANG_STRINGS[invoices][err_arve_number_kasutuses];
        }
        if (!checkdate($invoice[v_month],$invoice[v_day],$invoice[v_year])) {
            $errors[out_date] = $LANG_STRINGS[invoices][err_vigane_valjastamiskuupaev];
        }
        if (!checkdate($invoice[m_month],$invoice[m_day],$invoice[m_year])) {
            $errors[makse_date] = $LANG_STRINGS[invoices][err_viganemaksekuupaev];
        }
        if (mktime(0,0,0,$invoice[v_month],$invoice[v_day],$invoice[v_year]) > mktime(0,0,0,$invoice[m_month],$invoice[m_day],$invoice[m_year])) {
            $errors[out_date] = $LANG_STRINGS[invoices][err_maksekuupaev_suurem];
            $errors[makse_date] = $LANG_STRINGS[invoices][err_valjastamiskuupaev_suurem];
        }
        $invoice[client_id] = (int)$invoice[client_id];
        if (!$invoice[client_id] && !strlen($invoice[client_name])) {
            $errors[client_id] = $LANG_STRINGS[invoices][err_puudub_arve_saaja];
        }
        elseif ($invoice[client_id] && !strlen($invoice[client_name])) {
            $errors[client_name] = $LANG_STRINGS[invoices][err_puudub_arve_saaja];
        }

        if (!sizeof($errors)) {

            $invoice[item_sum] = str_replace(",",".",$invoice[item_sum]);
            $invoice[tax_sum] = str_replace(",",".",$invoice[tax_sum]);
            $invoice[total_sum] = str_replace(",",".",$invoice[total_sum]);

            $invoice['viivis_automaatne'] = (int)$invoice['viivis_automaatne'];
            $invoice['viivis_summa'] = str_replace(',', '.', $invoice['viivis_summa']);

            list($company["number_format"]) = mysql_fetch_row($DB -> query("SELECT number_format FROM invoice_companies WHERE company_id = '$invoice[company_id]'"));
            if ($company["number_format"] == "date") {
                $invoice[invoice_nr] = getNextInvoiceNR($invoice[company_id],$invoice[v_year],$invoice[v_month],$invoice[v_day],$invoice_id);
            }

            if (!mb_strlen($invoice['currency_id'])) {
                list($invoice['currency_id']) = mysql_fetch_row($DB -> query("SELECT currency_id FROM invoice_companies WHERE company_id = '$invoice[company_id]'"));
                list($invoice['alt_currency_id']) = mysql_fetch_row($DB -> query("SELECT alt_currency_id FROM invoice_companies WHERE company_id = '$invoice[company_id]'"));
            }
            if (!mb_strlen($invoice['currency_id'])) {
                $invoice['currency_id'] = 'EEK';
            }

            $DB -> query("START TRANSACTION");

            $DB -> query(($invoice_id > 0 ? "UPDATE " : "INSERT INTO ")." invoice_invoices SET invoice_nr = '$invoice[invoice_nr]', ettemaks_invoice_nr = '$invoice[ettemaks_invoice_nr]', company_id = '$invoice[company_id]', out_date = '$invoice[v_year]-$invoice[v_month]-$invoice[v_day]', makse_date = '$invoice[m_year]-$invoice[m_month]-$invoice[m_day]', client_id = '$invoice[client_id]', client_name = '$invoice[client_name]', client_kmkr = '$invoice[client_kmkr]', client_phone = '$invoice[client_phone]', client_email = '$invoice[client_email]', client_address = '$invoice[client_address]', transport_address = '" . mysql_real_escape_string($invoice['transport_address']) . "', item_sum = '$invoice[item_sum]', tax_sum = '$invoice[tax_sum]', rounding = '$invoice[rounding]', total_sum = '$invoice[total_sum]', sum_in_words = '$invoice[sum_in_words]', kreedit = '$invoice[kreedit]', include_wat = '$invoice[include_wat]', currency_id = '$invoice[currency_id]', alt_currency_id = '$invoice[alt_currency_id]', paid = '$invoice[paid]', credit_created = '$invoice[credit_created]', invoice_comments = '$invoice[invoice_comments]', viivis_automaatne = '$invoice[viivis_automaatne]', viivis_summa = '$invoice[viivis_summa]'".($invoice_id > 0 ? " WHERE invoice_id = '$invoice_id'" : ""));
            if (!$invoice_id) {
                $invoice_id = mysql_insert_id();
            }

            $curdate = date("Y-m-d H:i:s");

            if ($invoice[paid]) {
                $DB -> query("UPDATE invoice_invoices SET paid_dt = '$curdate' WHERE invoice_id = '$invoice_id' AND paid_dt IS NULL");
            }
            if ($invoice[kreedit]) {
                $DB -> query("UPDATE invoice_invoices SET kreedit_dt = '$curdate' WHERE invoice_id = '$invoice_id' AND kreedit_dt IS NULL");
            }
            if ($invoice[credit_created]) {
                $DB -> query("UPDATE invoice_invoices SET credit_created_dt = '$curdate' WHERE invoice_id = '$invoice_id' AND credit_created_dt IS NULL");
            }

            //teenuste salvestamine
            if (is_array($item) && sizeof($item)) {
                foreach ($item as $key => $val) {
                    if ($invoice[kreedit]) {
                        $item[$key][item_price] = 0 - abs($item[$key][item_price]);
                    }
                    $DB -> query("UPDATE invoice_invoice_items SET item_name = '".$item[$key][item_name]."', item_count = '".$item[$key][item_count]."', item_price = '".$item[$key][item_price]."', item_unit = '".$item[$key][item_unit]."', discount = '".$item[$key][discount]."' WHERE item_id = '$key'");
                }
            }

            $DB -> query("COMMIT");

            //$DB -> query("INSERT INTO invoice_invoices_track SET invoice_id = '$invoice_id', save_date = NOW(), uid = '".${$USER}[uid]."', invoice_nr = '$invoice[invoice_nr]', company_id = '$invoice[company_id]', out_date = '$invoice[v_year]-$invoice[v_month]-$invoice[v_day]', makse_date = '$invoice[m_year]-$invoice[m_month]-$invoice[m_day]', client_id = '$invoice[client_id]', client_name = '$invoice[client_name]', client_kmkr = '$invoice[client_kmkr]', client_phone = '$invoice[client_phone]', client_email = '$invoice[client_email]', client_address = '$invoice[client_address]', item_sum = '$invoice[item_sum]', tax_sum = '$invoice[tax_sum]', rounding = '$invoice[rounding]', total_sum = '$invoice[total_sum]', sum_in_words = '$invoice[sum_in_words]', kreedit = '$invoice[kreedit]', include_wat = '$invoice[include_wat]', paid = '$invoice[paid]', credit_created = '$invoice[credit_created]'");

            //teenuse lisamine
            $add_item[item_name] = trim($add_item[item_name]);
            if (strlen($add_item[item_name])) {
                $add_item[item_price] = str_replace(",",".",$add_item[item_price]);
                $add_item[item_count] = str_replace(",",".",$add_item[item_count]);
                $DB -> query("INSERT INTO invoice_invoice_items SET invoice_id = '$invoice_id', item_name = '$add_item[item_name]', item_count = '$add_item[item_count]',item_price = '$add_item[item_price]', item_unit = '$add_item[item_unit]', discount = '$add_item[discount]'");
            }

            reCalculate($invoice_id);

            if ($printing) {
                header("Location: /print_template.php?invoice_id=$invoice_id");exit;
            }
            header("Location: ?menu=invoices&action=edit&invoice_id=$invoice_id");exit;
        }
    case "edit":
        $invoice_id = (int)$invoice_id;

        if ($refr == 1) {
            $invoice[out_date] = "$invoice[v_year]-$invoice[v_month]-$invoice[v_day]";
            $invoice[makse_date] = "$invoice[m_year]-$invoice[m_month]-$invoice[m_day]";
        }
        elseif ($refr == 0 && $action == "edit" && !$invoice_id) {
            $invoice[out_date] = date("Y-m-d");
            $invoice[makse_date] = date("Y-m-d",mktime(0,0,0,date("m"),date("d")+5,date("Y")));
        }
        elseif ($invoice_id > 0) {
            $invoice = mysql_fetch_array($DB -> query("SELECT * FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));
        }

        if ($invoice_id > 0) {
            $invoice_totals = mysql_fetch_array($DB -> query("SELECT * FROM invoice_invoices WHERE invoice_id = '$invoice_id'"));
        }

        if (!$invoice_id && ${$USER}[company_id] && !$refr && $action == "edit") {
            $select_company = 1;
            $invoice[company_id] = ${$USER}[company_id];
            $invoice[v_year] = date("Y");
        }

        if (!$invoice_id && !$refr && $action == "edit") {
            $invoice[include_wat] = 1;
        }


        if ((!strlen($invoice[invoice_nr]) && $invoice_id == 0) || $select_company) {
            if ($invoice_id > 0 && $invoice_totals[company_id] == $invoice[company_id]) {
                $invoice[invoice_nr] = $invoice_totals[invoice_nr];
            }
            else {
                $invoice[invoice_nr] = getNextInvoiceNR($invoice[company_id],$invoice[v_year],$invoice[v_month],$invoice[v_day],$invoice_id);
            }

            list($invoice['currency_id']) = mysql_fetch_row($DB -> query("SELECT currency_id FROM invoice_companies WHERE company_id = '$invoice[company_id]'"));
        }

        list($company["number_format"]) = mysql_fetch_row($DB -> query("SELECT number_format FROM invoice_companies WHERE company_id = '$invoice[company_id]'"));
        if ($company["number_format"] == "date" && $refr) {
            $invoice[invoice_nr] = getNextInvoiceNR($invoice[company_id],$invoice[v_year],$invoice[v_month],$invoice[v_day],$invoice_id);
        }


        if ($select_client) {
            $invoice[client_id] = (int)$invoice[client_id];
            if ($invoice[client_id] > 0) {
                $client = mysql_fetch_array($DB -> query("SELECT * FROM invoice_clients WHERE client_id = '$invoice[client_id]'"));
                $invoice[client_name] = $client[client_name];
                $invoice[client_address] = $client[client_address];
                $invoice[client_phone] = $client[client_phone];
                $invoice[client_email] = $client[client_email];
                $invoice[client_kmkr] = $client[client_kmkr];
                $invoice[makse_date] = date("Y-m-d",mktime(0,0,0,date("m"),date("d")+$client[invoice_due],date("Y")));
            }
        }

        include ("includes/errors.inc.php");
        ?>
        <table cellpadding="3" cellspacing="2" border="0" width="100%">
            <form method="post" action="?menu=invoices" enctype="multipart/form-data" name="invoiceForm">
                <input type="hidden" name="invoice_id" value="<?=$invoice_id?>">
                <input type="hidden" name="invoice[invoice_nr]" value="<?=$invoice[invoice_nr]?>">
                <input type="hidden" name="action" value="edit">
                <input type="hidden" name="refr" value="1">
                <input type="hidden" name="select_client" value="0">
                <input type="hidden" name="select_company" value="0">
                <input type="hidden" name="printing" value="0">
                <?
                if (${$USER}[company_id]) {
                    ?>
                    <input type="hidden" name="invoice[company_id]" value="<?=${$USER}[company_id]?>">
                    <?
                }
                ?>
                <tr bgcolor="#64768E">
                    <td class="boxhead" colspan="2"><?=$LANG_STRINGS[invoices][str_arve_andmed]?></td>
                </tr>
                <tr>
                    <td width="150" class="<?=is_array($errors) && in_array("company_id",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_arve_valjastaja]?>:</td>
                    <td>
                        <?
                        if (${$USER}[company_id]) {
                            echo ${$USER}[company_name];
                        }
                        else {
                            ?>
                            <select name="invoice[company_id]" size="1" class="inputs" onChange="selectCompany()">
                                <option value="0">--<?=$LANG_STRINGS[invoices][str_vali_firma]?>--</option>
                                <?
                                $result = $DB -> query("SELECT * FROM invoice_companies ORDER BY company_name");
                                while ($r = mysql_fetch_array($result)) {
                                    ?>
                                    <option value="<?=$r[company_id]?>" <?=$r[company_id] == $invoice[company_id] ? "selected" : ""?>><?=htmlspecialchars(stripslashes($r[company_name]))?></option>
                                    <?
                                }
                                ?>
                            </select>
                            <?
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="150" class="<?=is_array($errors) && in_array("invoice_nr",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_arve_nr]?>:</td>
                    <td><?=$invoice[invoice_nr]?></td>
                </tr>
                <?if($invoice[reference]){?>
                    <tr>
                        <td width="150" class="<?=is_array($errors) && in_array("invoice_nr",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_reference_nr]?>:</td>
                        <td><?=$invoice[reference]?></td>
                    </tr>
                <?}?>
                <tr>
                    <td width="150" class="form_label" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_ettemaksu_nr]?>:</td>
                    <td><input type="text" name="invoice[ettemaks_invoice_nr]" value="<?=htmlspecialchars(stripslashes($invoice[ettemaks_invoice_nr]))?>" class="inputs" style="width:120px;"></td>
                </tr>
                <tr>
                    <td width="150" class="<?=is_array($errors) && in_array("makse_date",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_valjastamise_kuupaev]?>:</td>
                    <td><?=dates("invoice[v_day]","invoice[v_month]","invoice[v_year]",$invoice[out_date],true,2004,date("Y")+1,"class=\"inputs\"")?></td>
                </tr>
                <tr>
                    <td width="150" class="<?=is_array($errors) && in_array("makse_date",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7"><?=$LANG_STRINGS[invoices][str_maksetahtaeg]?>:</td>	<td><?=dates("invoice[m_day]","invoice[m_month]","invoice[m_year]",$invoice[makse_date],true,2004,date("Y")+1,"class=\"inputs\"")?></td>
                </tr>
                <tr>
                    <td width="150" class="<?=is_array($errors) && in_array("client_id",array_keys($errors)) ? "error" : "form_label"?>" bgcolor="F7F7F7" valign="top"><?=$LANG_STRINGS[invoices][str_saaja]?>:</td>
                    <td>
                        <table cellpadding="2" cellspacing="0" border="0" width="320">
                            <tr>
                                <td colspan="2">
                                    <select name="invoice[client_id]" size="1" class="inputs" style="width:320px;" onChange="selectClient();">
                                        <option value=0>--<?=$LANG_STRINGS[invoices][str_vali_klient]?>--</option>
                                        <?
                                        $result = $DB -> query("SELECT * FROM invoice_clients ".(${$USER}[company_id] > 0 ? " WHERE company_id = '".${$USER}[company_id]."'" : "")." ORDER BY client_name");
                                        while ($r = mysql_fetch_array($result)) {
                                            ?>
                                            <option value="<?=$r[client_id]?>" <?=$invoice[client_id] == $r[client_id] ? "selected" : ""?>><?=htmlspecialchars(stripslashes($r[client_name]))?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" class="<?=is_array($errors) && in_array("client_name",array_keys($errors)) ? "error" : "v10b"?>"><?=$LANG_STRINGS[invoices][str_klient]?>:</td>
                                <td><input type="text" name="invoice[client_name]" value="<?=htmlspecialchars(stripslashes($invoice[client_name]))?>" class="inputs" style="width:220px;"></td>
                            </tr>
                            <tr>
                                <td width="100" class="v10b"><?=$LANG_STRINGS[companies][str_kmkr]?>:</td>
                                <td><input type="text" name="invoice[client_kmkr]" value="<?=htmlspecialchars(stripslashes($invoice[client_kmkr]))?>" class="inputs" style="width:220px;"></td>
                            </tr>
                            <tr>
                                <td width="100" class="v10b" valign="top"><?=$LANG_STRINGS[invoices][str_aadress]?>:</td>
                                <td><textarea name="invoice[client_address]" rows="3" class="inputs" style="width:220px;"><?=htmlspecialchars(stripslashes($invoice[client_address]))?></textarea></td>
                            </tr>
                            <tr>
                                <td width="100" class="v10b" valign="top"><?=$LANG_STRINGS[invoices][str_tarne_aadress]?>:</td>
                                <td><textarea name="invoice[transport_address]" rows="3" class="inputs" style="width:220px;"><?=htmlspecialchars(stripslashes($invoice[transport_address]))?></textarea></td>
                            </tr>
                            <tr>
                                <td width="100" class="v10b" valign="top"><?=$LANG_STRINGS[invoices][str_telefon]?>:</td>
                                <td><input type="text" name="invoice[client_phone]" value="<?=htmlspecialchars(stripslashes($invoice[client_phone]))?>" class="inputs" style="width:220px;"></td>
                            </tr>
                            <tr>
                                <td width="100" class="v10b" valign="top"><?=$LANG_STRINGS[invoices][str_email]?>:</td>
                                <td><input type="text" name="invoice[client_email]" value="<?=htmlspecialchars(stripslashes($invoice[client_email]))?>" class="inputs" style="width:220px;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#64768E">
                    <td class="boxhead" colspan="2"><?=$LANG_STRINGS[invoices][str_teenused]?></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <table cellpadding="3" cellspacing="1" border="0" width="100%">
                            <tr bgcolor="#64768E">
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_nr]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_nimetus]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_yhik]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_hind]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_kogus]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_soodustus]?></td>
                                <td class="boxhead"><?=$LANG_STRINGS[invoices][str_summa]?></td>
                                <td class="boxhead">&nbsp;</td>
                            </tr>
                            <?
                            $result = $DB -> query("SELECT * FROM invoice_invoice_items WHERE invoice_id = '$invoice_id'");
                            $c = 1;
                            while ($r = mysql_fetch_array($result)) {
                                $r[total_sum] = ($r[discount] > 0 ? ($r[item_price] - ($r[item_price] * ($r[discount] / 100))) * $r[item_count] : $r[item_price] * $r[item_count]);
                                $items_total[sum] += $r[total_sum];
                                ?>
                                <tr>
                                    <td valign="top"><?=$c?>.</td>
                                    <td valign="top"><textarea name="item[<?=$r[item_id]?>][item_name]" class="inputs" rows="4" style="width:280px;"><?=htmlspecialchars(stripslashes($r[item_name]))?></textarea></td>
                                    <td valign="top">
                                        <select name="item[<?=$r[item_id]?>][item_unit]" class="inputs">
                                            <option value="<?=$LANG_STRINGS[invoices][str_tk]?>" <?=$r[item_unit] == $LANG_STRINGS[invoices][str_tk] ? "selected" : ""?>><?=$LANG_STRINGS[invoices][str_tk]?></option>
                                            <option value="<?=$LANG_STRINGS[invoices][str_tund]?>" <?=$r[item_unit] == $LANG_STRINGS[invoices][str_tund] ? "selected" : ""?>><?=$LANG_STRINGS[invoices][str_tund]?></option>
                                            <option value="<?=$LANG_STRINGS[invoices][str_mm]?>" <?=$r[item_unit] == $LANG_STRINGS[invoices][str_mm] ? "selected" : ""?>><?=$LANG_STRINGS[invoices][str_mm]?></option>
                                            <option value="<?=$LANG_STRINGS[invoices][str_cm]?>" <?=$r[item_unit] == $LANG_STRINGS[invoices][str_cm] ? "selected" : ""?>><?=$LANG_STRINGS[invoices][str_cm]?></option>
                                            <option value="<?=$LANG_STRINGS[invoices][str_toll]?>" <?=$r[item_unit] == $LANG_STRINGS[invoices][str_toll] ? "selected" : ""?>><?=$LANG_STRINGS[invoices][str_toll]?></option>
                                        </select>
                                    </td>
                                    <td valign="top"><input type="text" name="item[<?=$r[item_id]?>][item_price]" value="<?=number_format($r[item_price],2,".","")?>" class="inputs" style="width:70px;"></td>
                                    <td valign="top"><input type="text" name="item[<?=$r[item_id]?>][item_count]" value="<?=number_format($r[item_count],2,".","")?>" class="inputs" style="width:40px;"></td>
                                    <td valign="top"><input type="text" name="item[<?=$r[item_id]?>][discount]" value="<?=$r[discount]?>" class="inputs" style="width:40px;"></td>
                                    <td valign="top" class="v10b"><?=number_format($r[total_sum],2,".","")?></td>
                                    <td valign="top"><a href="?menu=invoices&action=delitem&item_id=<?=$r[item_id]?>&invoice_id=<?=$r[invoice_id]?>"><img src="/img/sys_images/buttons/kustuta.gif" border="0"></a></td>
                                </tr>
                                <?
                                $c++;
                            }
                            ?>
                            <tr>
                                <td colspan="5" class="form_label" align="right"><?=$LANG_STRINGS[yld][str_kokku]?>:&nbsp;</td>
                                <td class="form_label" nowrap><?=number_format($items_total[sum],2,"."," ")?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="7" class="form_label"><?=$LANG_STRINGS[invoices][str_teenuse_lisamine]?>:</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td valign="top"><textarea name="add_item[item_name]" class="inputs" rows="4" style="width:280px;"></textarea></td>
                                <td valign="top">
                                    <select name="add_item[item_unit]" class="inputs">
                                        <option value="tk"><?=$LANG_STRINGS[invoices][str_tk]?></option>
                                        <option value="tund"><?=$LANG_STRINGS[invoices][str_tund]?></option>
                                        <option value="mm"><?=$LANG_STRINGS[invoices][str_mm]?></option>
                                        <option value="cm"><?=$LANG_STRINGS[invoices][str_cm]?></option>
                                        <option value="toll"><?=$LANG_STRINGS[invoices][str_toll]?></option>
                                    </select>
                                </td>
                                <td valign="top"><input type="text" name="add_item[item_price]" class="inputs" style="width:70px;"></td>
                                <td valign="top"><input type="text" name="add_item[item_count]" class="inputs" style="width:40px;"></td>
                                <td valign="top"><input type="text" name="add_item[discount]" value="" class="inputs" style="width:40px;"></td>
                                <td valign="top" colspan="2"><input type="submit" value="<?=$LANG_STRINGS[yld][str_lisa]?>" class="buttons" onClick="document.invoiceForm.action.value='save'"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table cellpadding="3" cellspacing="1" border="0" width="100%">
                            <tr>
                                <td width="150" class="form_label"><?=$LANG_STRINGS[invoices][str_summa_kokku]?>:</td>
                                <td><?=$invoice_totals[item_sum].' '.$invoice['currency_id']?></td>
                            </tr>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_kaibemaks]?>:</td>
                                <td><?=$invoice_totals[tax_sum].' '.$invoice['currency_id']?></td>
                            </tr>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_summa_kaibemaksuga]?>:</td>
                                <td><?=$invoice_totals[sum_with_tax].' '.$invoice['currency_id']?></td>
                            </tr>
                            <?
                            if (${$LANG}[lang_id] <> 9) {
                                ?>
                                <tr>
                                    <td class="form_label"><?=$LANG_STRINGS[invoices][str_ymardamine]?>:</td>
                                    <td><?=$invoice_totals[rounding].' '.$invoice['currency_id']?></td>
                                </tr>
                                <?
                            }
                            if ($invoice['viivis_summa'] > 0) {
                                $invoice_totals['total_sum'] += $invoice['viivis_summa'];
                                ?>
                                <tr>
                                    <td class="form_label"><?=$LANG_STRINGS[invoices][str_viivis]?>:</td>
                                    <td><?=$invoice_totals[viivis_summa].' '.$invoice['currency_id']?></td>
                                </tr>
                                <?
                            }
                            ?>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_tasumiseks]?>:</td>
                                <td><?=$invoice_totals[total_sum].' '.$invoice['currency_id']?></td>
                            </tr>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_summa_sonadega]?>:</td>
                                <td><?=$invoice_totals[sum_in_words]?></td>
                            </tr>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_arve_kommentaarid]?>:</td>
                                <td><textarea name="invoice[invoice_comments]" rows="3" class="inputs" style="width:320px;"><?=htmlspecialchars(stripslashes($invoice[invoice_comments]))?></textarea></td>
                            </tr>
                            <tr>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_viivis_summa]?>:</td>
                                <td><input type="text" name="invoice[viivis_summa]" class="inputs" style="width:100px;" value="<?=$invoice['viivis_summa']?>"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="form_label" align="left">
                        <table cellpadding="2" cellspacing="0" border="0" width="400">
                            <tr>
                                <td class="form_label">Currency</td>
                                <td>
                                    <?
                                    $result = $DB -> query("SELECT * FROM invoice_currencies ORDER BY currency_id");
                                    ?>
                                    <select name="invoice[currency_id]" size="1" <?if ($priv[companies] <  2)echo "disabled";?> class="inputs">
                                        <option value=""></option>
                                        <?
                                        while ($r = mysql_fetch_assoc($result)) {
                                            ?>
                                            <option value="<?=$r[currency_id]?>" <?=($r[currency_id] == $invoice[currency_id] ? "selected"  : "")?>><?=$r['currency_id'].'-'.$r['currency_name']?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="20"><input type="checkbox" name="invoice[kreedit]" value="1" <?=$invoice[kreedit] ? "checked" : ""?>></td>
                                <td width="120" class="form_label"><?=$LANG_STRINGS[invoices][str_kreeditarve]?></td>
                                <td width="260"><? if($invoice[kreedit_dt]) echo date("d.m.Y H:i", strtotime($invoice[kreedit_dt]));?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="invoice[paid]" value="1" <?=$invoice[paid] ? "checked" : ""?>></td>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_makstud]?></td>
                                <td><? if($invoice[paid_dt]) echo date("d.m.Y H:i", strtotime($invoice[paid_dt]))?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="invoice[credit_created]" value="1" <?=$invoice[credit_created] ? "checked" : ""?>></td>
                                <td class="form_label"><?=$LANG_STRINGS[invoices][str_credit_created]?></td>
                                <td><? if($invoice[credit_created_dt] ) echo date("d.m.Y H:i", strtotime($invoice[credit_created_dt]))?>&nbsp;</td>
                            </tr>
                        </table>
                        <?if($invoice[snd_name]){?>
                            <table cellpadding="2" cellspacing="0" border="0" width="400" style="margin-top: 10px; margin-bottom: 10px;">
                                <?if($invoice[snd_name]){?>
                                    <tr>
                                        <td width="140" class="form_label"><?=$LANG_STRINGS[invoices][str_snd_name]?>:</td>
                                        <td width="260">&nbsp;<?=$invoice[snd_name]?></td>
                                    </tr>
                                <?}?>
                                <?if($invoice[snd_account]){?>
                                    <tr>
                                        <td class="form_label"><?=$LANG_STRINGS[invoices][str_snd_account]?>:</td>
                                        <td>&nbsp;<?=$invoice[snd_account]?></td>
                                    </tr>
                                <?}?>
                                <?if($invoice[rec_account]){?>
                                    <tr>
                                        <td class="form_label"><?=$LANG_STRINGS[invoices][str_rec_account]?>:</td>
                                        <td>&nbsp;<?=$invoice[rec_account]?></td>
                                    </tr>
                                <?}?>
                                <?if($invoice[paid] && $invoice[check_status] && $invoice[snd_name]){?>
                                    <tr>
                                        <td class="form_label"><?=$LANG_STRINGS[invoices][str_check_status]?>:</td>
                                        <?if($invoice[check_status] == 'signed'){?><td>&nbsp;<?=$LANG_STRINGS[invoices][str_check_status_signed]?>&nbsp;</td><?}?>
                                        <?if($invoice[check_status] == 'mail'){?><td>&nbsp;<?=$LANG_STRINGS[invoices][str_check_status_mail]?>&nbsp;</td><?}?>
                                        <?if($invoice[check_status] == 'manual'){?><td>&nbsp;<?=$LANG_STRINGS[invoices][str_check_status_manual]?></td><?}?>
                                    </tr>
                                <?}?>
                            </table>
                        <?}?>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="button" value="<?=$LANG_STRINGS[yld][str_tagasi]?>" class="buttons" onClick="document.location = '?menu=invoices'">&nbsp;<input type="submit" value="<?=$LANG_STRINGS[yld][str_salvesta]?>" class="buttons" onClick="document.invoiceForm.action.value='save'">&nbsp;<input type="submit" value="<?=$LANG_STRINGS[yld][str_prindi]?>" class="buttons" onClick="printInvoice();"></td>
                </tr>
            </form>
        </table>
        <script language="JavaScript">
            <!--
            function selectClient()
            {
                document.invoiceForm.select_client.value = '1';
                document.invoiceForm.submit();
            } //end func

            function selectCompany()
            {
                document.invoiceForm.select_company.value = '1';
                document.invoiceForm.submit();
            } //end func

            function printInvoice()
            {
                document.invoiceForm.printing.value = '1';
                document.invoiceForm.action.value = 'save';
            } //end func
            //-->
        </script>
        <?
        break;
    default:
        if ($submitSearch) {
            foreach ($search as $key => $val) {
                $search_string .= (strlen($search_string) ? "&" : "")."search[$key]=".rawurlencode($val);
            }

            header("Location: ?menu=invoices&$search_string");exit;
        }
        else {
            if (!sizeof($search)) {
                $search[year_start] = date("Y");
                $search[month_start] = date("m");
                $search[day_start] = date("1");
                $search[year_end] = date("Y");
                $search[month_end] = date("m");
                $search[day_end] = date("31");
            }

            if (${$USER}[company_id]) {
                $search[company_id] = ${$USER}[company_id];
            }
        }

        if (is_array($search) && sizeof($search)) {
            foreach ($search as $key => $val) {
                $search[$key] = rawurldecode($val);
                $url .= (strlen($url) ? "&" : "")."search[$key]=".rawurlencode($val);
            }

            if ($search[company_id] > 0) {
                $show_invoices = true;
            }
        }
        ?>
        <table cellpadding="2" cellspacing="0" border="0">
            <form method="post" action="?menu=invoices">
                <input type="hidden" name="submitSearch" value="1">
                <?
                if (${$USER}[company_id]) {
                    ?>
                    <input type="hidden" name="search[company_id]" value="<?=${$USER}[company_id]?>">
                    <?
                }
                ?>
                <tr>
                    <td class="form_label"><?=$LANG_STRINGS[invoices][str_valjastaja]?></td>
                    <td class="form_label"><?=$LANG_STRINGS[invoices][str_periood]?></td>
                    <td class="form_label"><?=$LANG_STRINGS[invoices][str_maksmata]?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <?
                        if (${$USER}[company_id]) {
                            echo ${$USER}[company_name];
                        }
                        else {
                            ?>
                            <select name="search[company_id]" size="1" class="inputs">
                                <option value="0">--<?=$LANG_STRINGS[invoices][str_vali_firma]?>--</option>
                                <?
                                $result = $DB -> query("SELECT * FROM invoice_companies ORDER BY company_name");
                                while ($r = mysql_fetch_array($result)) {
                                    ?>
                                    <option value="<?=$r[company_id]?>" <?=$r[company_id] == $search[company_id] ? "selected" : ""?>><?=htmlspecialchars(stripslashes($r[company_name]))?></option>
                                    <?
                                }
                                ?>
                            </select>
                            <?
                        }
                        ?>
                    </td>
                    <td class="form_label"><?=dates("search[day_start]","search[month_start]","search[year_start]","$search[year_start]-$search[month_start]-$search[day_start]",true,2004,date("Y")+1,"class='inputs'")?>&nbsp;-&nbsp;<?=dates("search[day_end]","search[month_end]","search[year_end]","$search[year_end]-$search[month_end]-$search[day_end]",true,2004,date("Y")+1,"class='inputs'")?></td>
                    <td align="center"><input type="checkbox" name="search[not_paid]" value="1" <?=($search[not_paid] == 1 ? "checked" : "")?>></td>
                    <td><input type="submit" value="Otsi" class="buttons"></td>
                </tr>
                <tr>
                    <td colspan="2" class="form_label"><?=$LANG_STRINGS[invoices][str_teenuse_nimetus]?>&nbsp;<input type="text" name="search[item_name]" value="<?=$search[item_name]?>" class="inputs" style="width:150px;"></td>
                    <td colspan="2" class="form_label">Eksport&nbsp;<select name="search[export]"><option value="">-</option><option value="hansaworld">Hansaworld</option></select></td>
                </tr>
            </form>
        </table>
        <br>
        <?
        if ($show_invoices) {
            ?>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#F2EAEA">
                <tr>
                    <td>
                        <table cellpadding="2" cellspacing="1" border="0" width="100%">
                            <tr bgcolor="#90ADC8">
                                <td class="boxhead" nowrap><a href="?menu=invoices&<?=$url?>&orderby=inr&sort=<?=($sort=="ASC" && $orderby == "inr" ? "DESC" : "ASC")?>" class="boxhead"><?=$LANG_STRINGS[invoices][str_arve_nr]?></a></td>
                                <td width="50%" class="boxhead"><?=$LANG_STRINGS[invoices][str_reference_nr]?></td>
                                <td nowrap class="boxhead"><a href="?menu=invoices&<?=$url?>&orderby=out_date&sort=<?=($sort=="ASC" && $orderby == "out_date" ? "DESC" : "ASC")?>" class="boxhead"><?=$LANG_STRINGS[invoices][str_kuupaev]?></a></td>
                                <td nowrap class="boxhead"><a href="?menu=invoices&<?=$url?>&orderby=makse_date&sort=<?=($sort=="ASC" && $orderby == "makse_date" ? "DESC" : "ASC")?>" class="boxhead"><?=$LANG_STRINGS[invoices][str_maksetahtaeg]?></a></td>
                                <td class="boxhead"><a href="?menu=invoices&<?=$url?>&orderby=client_area&sort=<?=($sort=="ASC" && $orderby == "client_area" ? "DESC" : "ASC")?>" class="boxhead"><?=$LANG_STRINGS[clients][str_piirkond]?></a></td>
                                <td width="50%" class="boxhead"><?=$LANG_STRINGS[invoices][str_saaja]?></td>
                                <td nowrap class="boxhead"><?=$LANG_STRINGS[invoices][str_summa_kmta]?></td>
                                <td nowrap class="boxhead"><?=$LANG_STRINGS[invoices][str_km]?></td>
                                <td nowrap class="boxhead"><?=$LANG_STRINGS[invoices][str_summa_kmga]?></td>
                                <td nowrap class="boxhead" align="center"><?=$LANG_STRINGS[invoices][str_m]?></td>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <?
                            $search[company_id] = (int)$search[company_id];
                            if ($search[company_id]) {
                                $add .= (strlen($add) ? " AND " : "")." I.company_id = '$search[company_id]'";
                            }
                            if ($search[day_start] && $search[month_start] && $search[year_start]) {
                                $add .= (strlen($add) ? " AND " : "")." I.out_date >= '$search[year_start]-$search[month_start]-$search[day_start]'";
                            }
                            if ($search[day_end] && $search[month_end] && $search[year_end]) {
                                $add .= (strlen($add) ? " AND " : "")." I.out_date <= '$search[year_end]-$search[month_end]-$search[day_end]'";
                            }
                            if ($search[not_paid] == 1) {
                                $add .= (strlen($add) ? " AND " : "")." I.paid = '0' AND I.kreedit = '0' AND I.credit_created = '0'";
                            }
                            if (strlen($add)) {
                                $add = " WHERE $add ";
                            }

                            if ($orderby != "inr" && $orderby != "out_date" && $orderby != 'client_area') {
                                $orderby = "inr";
                            }
                            if ($sort != "ASC" && $sort != "DESC") {
                                $sort = "DESC";
                            }

                            $c = 0;
                            if (strlen($search[item_name])) {
                                $result = $DB -> query("
					SELECT
						I.*,
						II.*,
						C.*,
						CL.client_area,
						IF(I.paid = 0 AND I.makse_date < CURDATE(), 1, IF(I.paid = 1, 2, 0)) AS paidStatus,
						DATE_FORMAT(I.out_date,'%d/%m/%Y') AS D,
						DATE_FORMAT(I.makse_date,'%d/%m/%Y') AS MD,
						CAST(I.invoice_nr AS UNSIGNED) AS inr
					FROM invoice_invoice_items AS II
					LEFT JOIN invoice_invoices AS I
						USING(invoice_id)
					LEFT JOIN invoice_clients CL
						ON (CL.client_id = I.client_id)
					LEFT JOIN invoice_companies AS C
						ON(C.company_id = I.company_id)
					$add
					".(strlen($add) ? " AND " : " WHERE ")."
						(
							II.item_name LIKE '%$search[item_name]%' OR
							I.client_name LIKE '%$search[item_name]%' OR
							I.client_email LIKE '%$search[item_name]%' OR
							I.invoice_nr LIKE '%$search[item_name]%' OR
							I.reference LIKE '%$search[item_name]%'
						)
					GROUP BY I.invoice_id
					ORDER BY $orderby $sort".($orderby == "out_date" ? ", inr $sort" : ""));
                            }
                            else {
                                $result = $DB -> query("
					SELECT
						I.*,
						C.*,
						IF(I.paid = 0 AND I.makse_date < CURDATE(), 1, IF(I.paid = 1, 2, 0)) AS paidStatus,
						CL.client_area,
						DATE_FORMAT(out_date,'%d/%m/%Y') AS D,
						DATE_FORMAT(makse_date,'%d/%m/%Y') AS MD,
						CAST(invoice_nr AS UNSIGNED) AS inr
					FROM invoice_invoices AS I
					LEFT JOIN invoice_companies AS C
						USING(company_id)
					LEFT JOIN invoice_clients CL
						ON (CL.client_id = I.client_id)
					$add
					ORDER BY $orderby $sort".($orderby == "out_date" ? ", inr $sort" : ""));
                            }

                            while ($r = mysql_fetch_array($result)) {
                                $total[item_sum] += $r[item_sum];
                                $total[tax_sum] += $r[tax_sum];
                                $total[total_sum] += $r[total_sum];
                                $bgcolor = $c%2 ? BGCOLOR1 : BGCOLOR2;
                                if ($r[kreedit] || $r[credit_created]) {
                                    $bgcolor = '#CFEBFE';
                                }
                                else if ($r[print_status] == 'online_print' || $r[print_status] == 'reseller_print') {
                                    $bgcolor = '#FECFCF';
                                }
                                else if ($r[print_status] == 'online') {
                                    $bgcolor = '#CFFEEB';
                                }

                                $r['dueDateColor'] = '#000000';
                                if ($r['paidStatus'] == 1) {
                                    $r['dueDateColor'] = '#C10005';
                                }
                                elseif ($r['paidStatus'] == 2) {
                                    $r['dueDateColor'] = '#008000';
                                }

                                $c++;
                                ?>
                                <!--export parse-->
                                <tr bgcolor="<?=$bgcolor?>" onMouseOver="this.bgColor='<?=MOUSEOVER_COLOR?>';" onMouseOut="this.bgColor='<?=$bgcolor?>';" style="cursor:hand;">
                                    <td nowrap <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><?=$r[invoice_nr]?></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><?=htmlspecialchars(stripslashes($r[reference]))?></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><?=htmlspecialchars(stripslashes($r[D]))?></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><span style="color:<?php echo $r['dueDateColor'] ?>"><?=htmlspecialchars(stripslashes($r[MD]))?></span></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><?=htmlspecialchars(stripslashes($r[client_area]))?></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?>><?=htmlspecialchars(stripslashes($r[client_name]))?></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?> align="right"><nobr><?=htmlspecialchars(stripslashes($r[item_sum]))?></nobr></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?> align="right"><nobr><?=htmlspecialchars(stripslashes($r[tax_sum]))?></nobr></td>
                                    <td <?if($priv[invoices]==2){?>onClick="document.location='?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>'"<?}?> align="right"><nobr><?=htmlspecialchars(stripslashes($r[total_sum]))?></nobr></td>
                                    <td bgcolor="#FFFFFF" align="center"><img src="/img/sys_images/buttons/<?=($r[paid] ? "" : "not_")?>checked.gif"></td>
                                    <td bgcolor="#FFFFFF" class="v10b"><?if($priv[invoices]==2){?><a href="?menu=invoices&action=edit&invoice_id=<?=$r[invoice_id]?>"><?=$LANG_STRINGS[yld][str_muuda]?></a>&nbsp;/&nbsp;<?}?><?if($priv[invoices]==2 && ${$USER}[uid] == 2){?><a href="javascript:if(confirm('Delete?')){document.location='?menu=invoices&action=delete&invoice_id=<?=$r[invoice_id]?>'}"><?=$LANG_STRINGS[yld][str_kustuta]?></a>&nbsp;/&nbsp;<?}?><a href="/print_template.php?invoice_id=<?=$r[invoice_id]?>"><?=$LANG_STRINGS[yld][str_prindi]?></a></td>
                                </tr>
                                <?php
                                #exportfile parsing
                                if ($_GET['search']['export'] == 'hansaworld') {

                                    $awiresult = $DB -> query("SELECT * FROM invoice_invoice_items WHERE invoice_id = '".mysql_real_escape_string(intval($r['invoice_id']))."'");
                                    while ($awirow = mysql_fetch_array($awiresult)) {
                                        $awirow[total_sum] = ($awirow[discount] > 0 ? ($awirow[item_price] - ($awirow[item_price] * ($awirow[discount] / 100))) * $awirow[item_count] : $awirow[item_price] * $awirow[item_count]);
                                        $awirow[hind_kokku] = number_format($awirow[total_sum],2,"."," ");
                                        #$awirow[nimetus] = htmlspecialchars(stripslashes($awirow[item_name]));

                                        $awitotal +=  $awirow[hind_kokku];
                                        $awitotal_quantity += $awirow[item_count];

                                        $awitems[$r['invoice_id']][$awirow['item_id']]['stp'] = '1';//sisu v2li, alati 1
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['ArtCode'] = '';//artiklikood
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Quant'] = intval(trim($awirow[item_count]));//kogus
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Price'] = str_replace('.', ',', trim($awirow[item_price]));//yhiku hind
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Sum'] = str_replace('.', ',', trim($awirow[hind_kokku]));//summa
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['vRebate'] = $awirow[discount];//ale %
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['SalesAcc'] = '3010';//myygikonto, Allan Kinsigo OTRS , October 05, 2011 4:09 Kui andmed imporditakse Books'i v?liss?steemist, tuleb selle s?steemi ?mber seadistada nii, et m??gikontoks oleks "3010" ning KM koodiks "5".
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Objects'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['OrdRow'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['BasePrice'] = '0,00';//artikli ostuhind
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['rowGP'] = '0,00';//artikli myygikate
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['FIFO'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Spec'] = replace_newline(trim($awirow[item_name]));//artikli nimetus
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['VATCode'] = '5';//KM kood
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Recepy'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['SerialNr'] = '';//seeria nr
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['PriceFactor'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['VARList'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['CUPNr'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['FIFORowVal'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['Coefficient'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['CuAccCode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['ExciseNr'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['PeriodCode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitCode'] = trim($awirow[item_unit]);//yhik - tk, kast jne
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitFactQuant'] = '0';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitFactPrice'] = '0,00';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitXval'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitYval'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['UnitZval'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['VECode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['CreditCard'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['AuthorizationCode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['PosCode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['CurncyCode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['FrRate'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['ToRateB1'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['ToRateB2'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['BaseRate1'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['BaseRate2'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['PayMode'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['GCNr'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['CustOrdNr'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['RepaExVAT'] = '';
                                        $awitems[$r['invoice_id']][$awirow['item_id']]['BasePriceB2'] = '';
                                    }
                                    $awinvoices[$r['invoice_id']]['SerNr'] = $r[invoice_nr];
                                    $awinvoices[$r['invoice_id']]['InvDate'] = date('d.m.Y', strtotime($r[out_date]));
                                    $awinvoices[$r['invoice_id']]['CustCode'] = $r[client_id];#'1003';
                                    $awinvoices[$r['invoice_id']]['PayDate'] = ($r[paid] == '1' ? date('d.m.Y', strtotime($r[paid_dt])) : '');
                                    $awinvoices[$r['invoice_id']]['Addr0'] = trim($r[client_name]);
                                    $awinvoices[$r['invoice_id']]['Addr1'] = trim($r[client_address]);
                                    $awinvoices[$r['invoice_id']]['Addr2'] = '';
                                    $awinvoices[$r['invoice_id']]['Addr3'] = '';
                                    $awinvoices[$r['invoice_id']]['OurContact'] = 'Allan Kinsigo';
                                    $awinvoices[$r['invoice_id']]['ClientContact'] = '';//kliendi esindaja
                                    $awinvoices[$r['invoice_id']]['ExportFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['PayDeal'] = round(abs(strtotime($r[makse_date]) - strtotime($r[out_date]))/60/60/24);//tasumistingimus
                                    $awinvoices[$r['invoice_id']]['OrderNr'] = '';
                                    $awinvoices[$r['invoice_id']]['Prntdf'] = '';
                                    $awinvoices[$r['invoice_id']]['OKFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['pdays'] = round(abs(strtotime($r[makse_date]) - strtotime($r[out_date]))/60/60/24);//tasumistingimus
                                    $awinvoices[$r['invoice_id']]['pdvrebt'] = '0';
                                    $awinvoices[$r['invoice_id']]['pdrdays'] = '0';
                                    $awinvoices[$r['invoice_id']]['CustCat'] = 'EKSP';//kliendiklass
                                    $awinvoices[$r['invoice_id']]['pdComment'] = '';
                                    $awinvoices[$r['invoice_id']]['x1'] = '';
                                    $awinvoices[$r['invoice_id']]['InvType'] = '1';//arvetyyp, 1=tavaline, 2=sularahaarve
                                    $awinvoices[$r['invoice_id']]['xStatFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['PriceList'] = 'EUR';//hinnakiri
                                    $awinvoices[$r['invoice_id']]['Objects'] = '';//objekt
                                    $awinvoices[$r['invoice_id']]['InclVAT'] = $r[include_wat];//sis.KM ??
                                    $awinvoices[$r['invoice_id']]['ARAcc'] = str_replace('.', ',', ($r[total_sum] - ($r[include_wat] == '1' ? $r[tax_sum] : 0)));//myygiv6lakonto, arve summa ilma KM-ta
                                    $awinvoices[$r['invoice_id']]['InvComment'] = '';
                                    $awinvoices[$r['invoice_id']]['CredInv'] = '';
                                    $awinvoices[$r['invoice_id']]['CredMark'] = '';
                                    $awinvoices[$r['invoice_id']]['SalesMan'] = 'A';//myygimees
                                    $awinvoices[$r['invoice_id']]['ToRateB1'] = '1';//kurss baasvaluutaga
                                    $awinvoices[$r['invoice_id']]['TransDate'] = $awinvoices[$r['invoice_id']]['InvDate'];//kande kuupv
                                    $awinvoices[$r['invoice_id']]['CurncyCode'] = (array_key_exists($r[currency_id], $rates_2010) ? $r[currency_id] : (intval(date('Y', strtotime($r[out_date]))) < 2011 ? 'EEK' : 'EUR'));//valuuta		date('d.m.Y', strtotime($r[out_date]));$rates_2010
                                    $awinvoices[$r['invoice_id']]['LangCode'] = 'EST';//keelekood
                                    $awinvoices[$r['invoice_id']]['UpdStockFlag'] = '1';//muuda ladu
                                    $awinvoices[$r['invoice_id']]['LastRemndr'] = '';
                                    $awinvoices[$r['invoice_id']]['LastRemDate'] = '';
                                    $awinvoices[$r['invoice_id']]['Sign'] = 'A';//myygimees
                                    $awinvoices[$r['invoice_id']]['FrPrice'] = '';
                                    $awinvoices[$r['invoice_id']]['FrBase'] = '';
                                    $awinvoices[$r['invoice_id']]['FrItem'] = '';
                                    $awinvoices[$r['invoice_id']]['FrVATCode'] = '';
                                    $awinvoices[$r['invoice_id']]['FrObjects'] = '';
                                    $awinvoices[$r['invoice_id']]['OrgCust'] = '';
                                    $awinvoices[$r['invoice_id']]['FrGP'] = '0,00';
                                    $awinvoices[$r['invoice_id']]['FrGPPercent'] = '';
                                    $awinvoices[$r['invoice_id']]['Sum0'] = '0,00';
                                    $awinvoices[$r['invoice_id']]['Sum1'] = str_replace('.', ',', trim($r[item_sum]));//summa KM-ta
                                    $awinvoices[$r['invoice_id']]['Sum2'] = '';
                                    $awinvoices[$r['invoice_id']]['Sum3'] = str_replace('.', ',', trim($r[tax_sum]));//KM
                                    $awinvoices[$r['invoice_id']]['Sum4'] = str_replace('.', ',', trim($r[total_sum]));//summa KM-ga
                                    $awinvoices[$r['invoice_id']]['VATNr'] = trim($r[client_kmkr]);//KMK nr
                                    $awinvoices[$r['invoice_id']]['ShipDeal'] = 'FOB';//lahetustingimus
                                    $awinvoices[$r['invoice_id']]['ShipAddr0'] = trim($r[client_name]);//lahetusaadress
                                    $awinvoices[$r['invoice_id']]['ShipAddr1'] = trim($r[client_address]);//lahetusaadress
                                    $awinvoices[$r['invoice_id']]['ShipAddr2'] = '';//lahetusaadress
                                    $awinvoices[$r['invoice_id']]['ShipAddr3'] = '';//lahetusaadress
                                    $awinvoices[$r['invoice_id']]['ShipMode'] = '';
                                    $awinvoices[$r['invoice_id']]['Location'] = '';//ladu
                                    $awinvoices[$r['invoice_id']]['PRCode'] = '';
                                    $awinvoices[$r['invoice_id']]['FrSalesAcc'] = '';
                                    $awinvoices[$r['invoice_id']]['Tax1Sum'] = '0';
                                    $awinvoices[$r['invoice_id']]['CustVATCode'] = '1';//1-eesti, 2-EU, 3-nonEU
                                    $awinvoices[$r['invoice_id']]['RebCode'] = '';
                                    $awinvoices[$r['invoice_id']]['CalcFinRef'] = '';
                                    $awinvoices[$r['invoice_id']]['Phone'] = '';//kliendi tel
                                    $awinvoices[$r['invoice_id']]['Fax'] = '';//kliendi fax
                                    $awinvoices[$r['invoice_id']]['IntCode'] = '0';
                                    $awinvoices[$r['invoice_id']]['ARonTR'] = '1';
                                    $awinvoices[$r['invoice_id']]['CustOrdNr'] = '';
                                    $awinvoices[$r['invoice_id']]['ExportedFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['BaseSum4'] = str_replace('.', ',', trim($r[total_sum]));//summa KM-ga
                                    $awinvoices[$r['invoice_id']]['FrRate'] = '1';
                                    $awinvoices[$r['invoice_id']]['ToRateB2'] = '';
                                    $awinvoices[$r['invoice_id']]['BaseRate1'] = (intval(date('Y', strtotime($r[out_date]))) < 2011 ? $rates_2010[$awinvoices[$r['invoice_id']]['CurncyCode']] : $rates_2011[$awinvoices[$r['invoice_id']]['CurncyCode']]);//baasvaluutade 1 ja 2 suhe, pre 2011
                                    $awinvoices[$r['invoice_id']]['BaseRate2'] = '1';#pre 2011 EEK, hilisemad EUR,
                                    $awinvoices[$r['invoice_id']]['InvoiceNr'] = '';
                                    $awinvoices[$r['invoice_id']]['DiscPerc'] = '';
                                    $awinvoices[$r['invoice_id']]['DiscSum'] = '';
                                    $awinvoices[$r['invoice_id']]['TotGP'] = '';//arve myygikate kokku
                                    $awinvoices[$r['invoice_id']]['LocOKNr'] = '';
                                    $awinvoices[$r['invoice_id']]['Invalid'] = '0';
                                    $awinvoices[$r['invoice_id']]['CreditCard'] = '';
                                    $awinvoices[$r['invoice_id']]['AuthorizationCode'] = '';
                                    $awinvoices[$r['invoice_id']]['RecValue'] = '';
                                    $awinvoices[$r['invoice_id']]['RetValue'] = '';
                                    $awinvoices[$r['invoice_id']]['FromBUQT'] = '0';
                                    $awinvoices[$r['invoice_id']]['Sorting'] = '';
                                    $awinvoices[$r['invoice_id']]['NoInterestFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['NoRemndrFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['SVONr'] = '';
                                    $awinvoices[$r['invoice_id']]['InstallmentInv'] = '0';
                                    $awinvoices[$r['invoice_id']]['OfficialSerNr'] = '';
                                    $awinvoices[$r['invoice_id']]['LegalInvNr'] = '';
                                    $awinvoices[$r['invoice_id']]['TotQty'] = trim($awitotal_quantity);//toodete arv, xlsis 3 ??
                                    $awinvoices[$r['invoice_id']]['TotWeight'] = '0';
                                    $awinvoices[$r['invoice_id']]['TotVolume'] = '0';
                                    $awinvoices[$r['invoice_id']]['Commision'] = '0';
                                    $awinvoices[$r['invoice_id']]['SumIncCom'] = str_replace('.', ',', trim($r[total_sum]));//summa KM-ga
                                    $awinvoices[$r['invoice_id']]['InvAddr3'] = '';//riik
                                    $awinvoices[$r['invoice_id']]['InvAddr4'] = '';
                                    $awinvoices[$r['invoice_id']]['DelAddr3'] = '';//riik
                                    $awinvoices[$r['invoice_id']]['DelAddr4'] = '';
                                    $awinvoices[$r['invoice_id']]['DelAddrCode'] = '';
                                    $awinvoices[$r['invoice_id']]['AutoGiro'] = '0';
                                    $awinvoices[$r['invoice_id']]['SalesGroup'] = 'TLN';//osakond
                                    $awinvoices[$r['invoice_id']]['DisputedFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['NoColectionFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['QTNr'] = '';
                                    $awinvoices[$r['invoice_id']]['FiscalFlag'] = '0';
                                    $awinvoices[$r['invoice_id']]['JobNr'] = '';
                                    $awinvoices[$r['invoice_id']]['RetnValue'] = '';
                                    $awinvoices[$r['invoice_id']]['MachineName'] = '1';
                                    $awinvoices[$r['invoice_id']]['TransTime'] = '00:00:00';//transp kellaaeg ??
                                    $awinvoices[$r['invoice_id']]['DrawerCode'] = '';
                                    $awinvoices[$r['invoice_id']]['Site'] = '';
                                    $awinvoices[$r['invoice_id']]['colnr'] = '0';
                                    $awinvoices[$r['invoice_id']]['StatVal'] = '';
                                    $awinvoices[$r['invoice_id']]['EInvFunc'] = '';
                                    $awinvoices[$r['invoice_id']]['EInvExpFlag'] = '';
                                    $awinvoices[$r['invoice_id']]['EInvExpDate'] = '';
                                    $awinvoices[$r['invoice_id']]['EInvExpQty'] = '';
                                    $awinvoices[$r['invoice_id']]['ServiceDelDate'] = '';
                                }
                            }
                            ?>
                            <tr>
                                <td colspan="6" align="right" class="v10b"><?=$LANG_STRINGS[yld][str_kokku]?>:&nbsp;</td>
                                <td nowrap class="v10b"><?=number_format($total[item_sum],2,"."," ")?></td>
                                <td nowrap class="v10b"><?=number_format($total[tax_sum],2,"."," ")?></td>
                                <td nowrap class="v10b"><?=number_format($total[total_sum],2,"."," ")?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                            if (is_array($awinvoices)) {
                                #echo '<pre>';
                                #print_r($awinvoices);
                                #echo '</pre>';
                                echo '<tr><td colspan="9" align="right" class="v10b" bgcolor="#FFFFFF">'.hansaExport($awinvoices, $awitems).'</td></tr>';
                            }
                            ?>
                        </table>
                    </td>
                </tr>
            </table>
            <?
        }
        else {
            ?>
            <div align="center" class="form_label"><?=$LANG_STRINGS[invoices][str_vali_arve_valjastaja]?></div>
            <?
        }
}
?>