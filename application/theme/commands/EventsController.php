<?php
namespace app\theme\commands;

use app\theme\components\commands\Processor;
use app\theme\models\EventPlanning;
use yii\db\Expression;

class EventsController extends ConsoleController
{

    public function actionRun()
    {

        $processor = new Processor();

        while ($command = EventPlanning::find()->nextTask()->one()) {

            switch($processor->run($command)) {
                case Processor::RESULT_OK:
                    $this->info(sprintf('Command %s [%d] - OK', $command->name, $command->id));
                    break;
                case Processor::RESULT_ERROR:
                    $this->error(sprintf('Command %s [%d] - ERROR: %s', $command->name, $command->id, implode(', ', $processor->getErrors())));
                    break;
                default:
                    $this->info(sprintf('Command %s [%d] - SKIP', $command->name, $command->id));
                    break;
            }

            sleep(1);
        }
    }
}
