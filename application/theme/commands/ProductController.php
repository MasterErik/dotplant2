<?php
namespace app\theme\commands;

use app\theme\models\Product;

class ProductController extends ConsoleController
{

    public function actionUpdateValidators()
    {
        $products = Product::find()->where('eset_product_code IS NOT NULL')->all();
        $this->debugMsg('The quantity of products for update validators: '. count($products));
        foreach( $products as $product ) {

            $info_product = ' #' .$product->id . ' '. $product->name;
            $this->debugMsg('Product: '. $info_product);

            Product::generateAttributes($product);
        }

        $this->debugMsg('OK');
    }
}
