<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 2.09.2015
 * Time: 11:45
 */

namespace app\theme\commands;

use Yii;

class ExportController extends ConsoleController
{
    private $filename;
    private $handle;

    public function actionRun()
    {
        $sql = "SELECT i.invoice_id, i.invoice_nr, i.out_date, i.client_id, i.paid, i.paid_dt, i.client_name, i.client_address, i.makse_date,
                  i.include_wat, i.total_sum, i.tax_sum, i.item_sum, i.client_kmkr,
                  ii.item_id, ii.discount, ii.item_price, ii.item_count, ii.item_unit, ii.item_name
                FROM invoice_invoice_items AS ii
                LEFT JOIN invoice_invoices AS i ON ii.invoice_id = i.invoice_id
                WHERE i.out_date BETWEEN :bdate AND :edate
                ORDER BY i.invoice_id, i.out_date";
        //STR_TO_DATE('2009-08-14 00:00:00', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('2010-08-23 23:59:59', '%Y-%m-%d %H:%i:%s')
        $reader = \Yii::$app->db->createCommand($sql, [':bdate' => '2015-06-01', ':edate' => '2015-06-30'])->query();

        $this->createFile();
        foreach ($reader as $row) {
            $invoice = $this->formatArray($row);
            $items = $invoice['items'];
            unset($invoice['items']);
            $this->hansaExport($invoice, $items);
        }
        $this->closeFile();
    }

    #exportfile parsing
    public function formatArray($invoice)
    {
        /* Structure table invoice_invoice_items
         * item_id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
         * invoice_id int(10) UNSIGNED NOT NULL DEFAULT 0,
         * item_name text DEFAULT NULL,
         * item_count double(14, 2) NOT NULL DEFAULT 0.00,
         * item_price double(14, 2) NOT NULL DEFAULT 0.00,
         * discount tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
         * item_unit varchar(255) DEFAULT NULL,
         */

        $reader = \Yii::$app->db->createCommand('SELECT * FROM invoice_invoice_items WHERE invoice_id = :id', [':id' => $invoice['invoice_id']])->query();
        $quantity = 0;
        $items = [];

        foreach ($reader as $item) {
            #($item[item_name]);
            $item['total_sum'] = ($item['discount'] > 0 ? ($item['item_price'] - ($item['item_price'] * ($item['discount'] / 100))) * $item['item_count'] : $item['item_price'] * $item['item_count']);
            $quantity += $item['item_count'];

            $items[$item['item_id']]['stp'] = '1';//sisu v2li, alati 1
            $items[$item['item_id']]['ArtCode'] = '';//artiklikood
            $items[$item['item_id']]['Quant'] = intval(trim($item['item_count']));//kogus
            $items[$item['item_id']]['Price'] = $this->nrFormat($item['item_price']); //yhiku hind
            $items[$item['item_id']]['Sum'] = $this->nrFormat($item['total_sum'], " ");//summa
            $items[$item['item_id']]['vRebate'] = $item['discount'];//ale %
            $items[$item['item_id']]['SalesAcc'] = '3010';//myygikonto, Allan Kinsigo OTRS , October 05, 2011 4:09 Kui andmed imporditakse Books'i v?liss?steemist, tuleb selle s?steemi ?mber seadistada nii, et m??gikontoks oleks "3010" ning KM koodiks "5".
            $items[$item['item_id']]['Objects'] = '';
            $items[$item['item_id']]['OrdRow'] = '';
            $items[$item['item_id']]['BasePrice'] = '0,00';//artikli ostuhind
            $items[$item['item_id']]['rowGP'] = '0,00';//artikli myygikate
            $items[$item['item_id']]['FIFO'] = '';
            $items[$item['item_id']]['Spec'] = $this->replace_newline(trim($item['item_name']));//artikli nimetus
            $items[$item['item_id']]['VATCode'] = '5';//KM kood
            $items[$item['item_id']]['Recepy'] = '';
            $items[$item['item_id']]['SerialNr'] = '';//seeria nr
            $items[$item['item_id']]['PriceFactor'] = '';
            $items[$item['item_id']]['VARList'] = '';
            $items[$item['item_id']]['CUPNr'] = '';
            $items[$item['item_id']]['FIFORowVal'] = '';
            $items[$item['item_id']]['Coefficient'] = '';
            $items[$item['item_id']]['CuAccCode'] = '';
            $items[$item['item_id']]['ExciseNr'] = '';
            $items[$item['item_id']]['PeriodCode'] = '';
            $items[$item['item_id']]['UnitCode'] = trim($item['item_unit']);//yhik - tk, kast jne
            $items[$item['item_id']]['UnitFactQuant'] = '0';
            $items[$item['item_id']]['UnitFactPrice'] = '0,00';
            $items[$item['item_id']]['UnitXval'] = '';
            $items[$item['item_id']]['UnitYval'] = '';
            $items[$item['item_id']]['UnitZval'] = '';
            $items[$item['item_id']]['VECode'] = '';
            $items[$item['item_id']]['CreditCard'] = '';
            $items[$item['item_id']]['AuthorizationCode'] = '';
            $items[$item['item_id']]['PosCode'] = '';
            $items[$item['item_id']]['CurncyCode'] = '';
            $items[$item['item_id']]['FrRate'] = '';
            $items[$item['item_id']]['ToRateB1'] = '';
            $items[$item['item_id']]['ToRateB2'] = '';
            $items[$item['item_id']]['BaseRate1'] = '';
            $items[$item['item_id']]['BaseRate2'] = '';
            $items[$item['item_id']]['PayMode'] = '';
            $items[$item['item_id']]['GCNr'] = '';
            $items[$item['item_id']]['CustOrdNr'] = '';
            $items[$item['item_id']]['RepaExVAT'] = '';
            $items[$item['item_id']]['BasePriceB2'] = '';
        }
        $header['items'] = $items;

        $header['SerNr'] = $invoice['invoice_nr'];
        $header['InvDate'] = date('d.m.Y', strtotime($invoice['out_date']));
        $header['CustCode'] = $invoice['client_id'];#'1003';
        $header['PayDate'] = ($invoice['paid'] == '1' ? date('d.m.Y', strtotime($invoice['paid_dt'])) : '');
        $header['Addr0'] = trim($invoice['client_name']);
        $header['Addr1'] = trim($invoice['client_address']);
        $header['Addr2'] = '';
        $header['Addr3'] = '';
        $header['OurContact'] = 'Allan Kinsigo';
        $header['ClientContact'] = '';//kliendi esindaja
        $header['ExportFlag'] = '0';
        $header['PayDeal'] = round(abs(strtotime($invoice['makse_date']) - strtotime($invoice['out_date'])) / 60 / 60 / 24);//tasumistingimus
        $header['OrderNr'] = '';
        $header['Prntdf'] = '';
        $header['OKFlag'] = '0';
        $header['pdays'] = round(abs(strtotime($invoice['makse_date']) - strtotime($invoice['out_date'])) / 60 / 60 / 24);//tasumistingimus
        $header['pdvrebt'] = '0';
        $header['pdrdays'] = '0';
        $header['CustCat'] = 'EKSP';//kliendiklass
        $header['pdComment'] = '';
        $header['x1'] = '';
        $header['InvType'] = '1';//arvetyyp, 1=tavaline, 2=sularahaarve
        $header['xStatFlag'] = '0';
        $header['PriceList'] = 'EUR';//hinnakiri
        $header['Objects'] = '';//objekt
        $header['InclVAT'] = $invoice['include_wat'];//sis.KM ??
        $header['ARAcc'] = $this->nrFormat($invoice['total_sum'] - ($invoice['include_wat'] == '1' ? $invoice['tax_sum'] : 0));//myygiv6lakonto, arve summa ilma KM-ta
        $header['InvComment'] = '';
        $header['CredInv'] = '';
        $header['CredMark'] = '';
        $header['SalesMan'] = 'A';//myygimees
        $header['ToRateB1'] = '1';//kurss baasvaluutaga
        $header['TransDate'] = date('d.m.Y', strtotime($invoice['out_date']));//kande kuupv
        $header['CurncyCode'] = 'EUR'; //(array_key_exists($invoice[currency_id], $rates_2010) ? $invoice[currency_id] : (intval(date('Y', strtotime($invoice[out_date]))) < 2011 ? 'EEK' : 'EUR'));//valuuta		date('d.m.Y', strtotime($invoice[out_date]));$rates_2010
        $header['LangCode'] = 'EST';//keelekood
        $header['UpdStockFlag'] = '1';//muuda ladu
        $header['LastRemndr'] = '';
        $header['LastRemDate'] = '';
        $header['Sign'] = 'A';//myygimees
        $header['FrPrice'] = '';
        $header['FrBase'] = '';
        $header['FrItem'] = '';
        $header['FrVATCode'] = '';
        $header['FrObjects'] = '';
        $header['OrgCust'] = '';
        $header['FrGP'] = '0,00';
        $header['FrGPPercent'] = '';
        $header['Sum0'] = '0,00';
        $header['Sum1'] = $this->nrFormat($invoice['item_sum']);//summa KM-ta
        $header['Sum2'] = '';
        $header['Sum3'] = $this->nrFormat($invoice['tax_sum']);//KM
        $header['Sum4'] = $this->nrFormat($invoice['total_sum']);//summa KM-ga
        $header['VATNr'] = trim($invoice['client_kmkr']);//KMK nr
        $header['ShipDeal'] = 'FOB';//lahetustingimus
        $header['ShipAddr0'] = trim($invoice['client_name']);//lahetusaadress
        $header['ShipAddr1'] = trim($invoice['client_address']);//lahetusaadress
        $header['ShipAddr2'] = '';//lahetusaadress
        $header['ShipAddr3'] = '';//lahetusaadress
        $header['ShipMode'] = '';
        $header['Location'] = '';//ladu
        $header['PRCode'] = '';
        $header['FrSalesAcc'] = '';
        $header['Tax1Sum'] = '0';
        $header['CustVATCode'] = '1';//1-eesti, 2-EU, 3-nonEU
        $header['RebCode'] = '';
        $header['CalcFinRef'] = '';
        $header['Phone'] = '';//kliendi tel
        $header['Fax'] = '';//kliendi fax
        $header['IntCode'] = '0';
        $header['ARonTR'] = '1';
        $header['CustOrdNr'] = '';
        $header['ExportedFlag'] = '0';
        $header['BaseSum4'] = $this->nrFormat($invoice['total_sum']);//summa KM-ga
        $header['FrRate'] = '1';
        $header['ToRateB2'] = '';
        $header['BaseRate1'] = '1'; //(intval(date('Y', strtotime($invoice[out_date]))) < 2011 ? $rates_2010[$header['CurncyCode']] : $rates_2011[$header['CurncyCode']]);//baasvaluutade 1 ja 2 suhe, pre 2011
        $header['BaseRate2'] = '1';#pre 2011 EEK, hilisemad EUR,
        $header['InvoiceNr'] = '';
        $header['DiscPerc'] = '';
        $header['DiscSum'] = '';
        $header['TotGP'] = '';//arve myygikate kokku
        $header['LocOKNr'] = '';
        $header['Invalid'] = '0';
        $header['CreditCard'] = '';
        $header['AuthorizationCode'] = '';
        $header['RecValue'] = '';
        $header['RetValue'] = '';
        $header['FromBUQT'] = '0';
        $header['Sorting'] = '';
        $header['NoInterestFlag'] = '0';
        $header['NoRemndrFlag'] = '0';
        $header['SVONr'] = '';
        $header['InstallmentInv'] = '0';
        $header['OfficialSerNr'] = '';
        $header['LegalInvNr'] = '';
        $header['TotQty'] = trim($quantity);//toodete arv, xlsis 3 ??
        $header['TotWeight'] = '0';
        $header['TotVolume'] = '0';
        $header['Commision'] = '0';
        $header['SumIncCom'] = $this->nrFormat($invoice['total_sum']);//summa KM-ga
        $header['InvAddr3'] = '';//riik
        $header['InvAddr4'] = '';
        $header['DelAddr3'] = '';//riik
        $header['DelAddr4'] = '';
        $header['DelAddrCode'] = '';
        $header['AutoGiro'] = '0';
        $header['SalesGroup'] = 'TLN';//osakond
        $header['DisputedFlag'] = '0';
        $header['NoColectionFlag'] = '0';
        $header['QTNr'] = '';
        $header['FiscalFlag'] = '0';
        $header['JobNr'] = '';
        $header['RetnValue'] = '';
        $header['MachineName'] = '1';
        $header['TransTime'] = '00:00:00';//transp kellaaeg ??
        $header['DrawerCode'] = '';
        $header['Site'] = '';
        $header['colnr'] = '0';
        $header['StatVal'] = '';
        $header['EInvFunc'] = '';
        $header['EInvExpFlag'] = '';
        $header['EInvExpDate'] = '';
        $header['EInvExpQty'] = '';
        $header['ServiceDelDate'] = '';
        return $header;
    }

    private function createFile()
    {
        $this->filename = 'invoices_export_hw_sbe_' . date('Ymd_His') . '.txt';
        @unlink($this->filename);
        $this->handle = fopen($this->filename, "w");
        @fwrite($this->handle, '');

        $headers[] = array('format');
        $headers[] = array('1', '44', '1', '0', '0');
        $headers[] = array('');
        $headers[] = array('commentstring');
        $headers[] = array('');
        $headers[] = array('');
        $headers[] = array('IVVc');
        #write header rows
        foreach ($headers as $header) {
            $this->fputcsv($this->handle, $header, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
        }
    }

    private function closeFile()
    {
        @fclose($this->handle);
    }

    #hansaworld sbe export, parsimine allpool, arvete nimekirja koostamise juures
    protected function hansaExport($invoice, $items)
    {
        #write invoice rows
        if (is_array($invoice)) {
            $this->fputcsv($this->handle, $invoice, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
            if (is_array($items)) {
                foreach ($items as $item) {
                    #utf8_decode($col);
                    $this->fputcsv($this->handle, $item, chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
                }
            }
            $this->fputcsv($this->handle, array(), chr(9)/*';'*/, chr(0));#chr(9) == <TAB>
        }
    }

    private function fputcsv(&$handle, $fields = array(), $delimiter = ',', $enclosure = '"')
    {
        $str = '';
        $escape_char = '\\';
        foreach ($fields as $value) {
            if (strpos($value, $delimiter) !== false ||
                strpos($value, $enclosure) !== false ||
                strpos($value, "\n") !== false ||
                strpos($value, "\r") !== false ||
                strpos($value, "\t") !== false ||
                strpos($value, ' ') !== false) {
                $str2 = $enclosure;
                $escaped = 0;
                $len = strlen($value);
                for ($i = 0; $i < $len; $i++) {
                    if ($value[$i] == $escape_char) {
                        $escaped = 1;
                    } else if (!$escaped && $value[$i] == $enclosure) {
                        $str2 .= $enclosure;
                    } else {
                        $escaped = 0;
                    }
                    $str2 .= $value[$i];
                }
                $str2 .= $enclosure;
                $str .= $str2 . $delimiter;
            } else {
                $str .= $value . $delimiter;
            }
        }
        $str = substr($str, 0, -1);
        $str .= "\n";
        return fwrite($handle, $str);
    }

    private function replace_newline($string)
    {
        return (string)str_replace(array("\r", "\r\n", "\n"), '', $string);
    }

    /**
     * @param $value integer
     * @param string $space
     * @return string
     */
    private function nrFormat($value, $space = '')
    {
        return number_format($value, 2, ',', $space);
    }
}