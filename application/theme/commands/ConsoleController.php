<?php

namespace app\theme\commands;

use app\theme\models\SystemLog;
use yii\console\Controller;
use Yii;

abstract class ConsoleController extends Controller
{

    public $silence = true;
    public $debug = false;

    private $microtime_start;

    public function options($actionId)
    {
        $option = parent::options($actionId);
        $option[] = 'silence';
        $option[] = 'debug';
        return $option;
    }
    public function init()
    {
        $this->microtime_start = microtime(1);
        //ini_set('memory_limit', '1024M');
        //Yii::$app->log->flushInterval = 0;
    }
    public function debugMsg($msg)
    {
        if ($this->debug) {
            echo date('Y-m-d H:i:s') . "\t" . $msg . "\n";
        }
    }
    private function echoMsg($msg)
    {
        if (!$this->silence) {
            echo date('Y-m-d H:i:s') . "\t" . $msg . "\n";
        }
    }
    public function info($msg, $category = null)
    {
        $this->echoMsg($msg);
        Yii::info($msg, $category ? $category : SystemLog::CATEGORY_CONSOLE);
    }

    public function error($msg, $category = null)
    {
        $this->echoMsg($msg);
        Yii::error($msg, $category ? $category : SystemLog::CATEGORY_CONSOLE);
    }

}