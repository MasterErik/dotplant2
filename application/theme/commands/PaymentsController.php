<?php
namespace app\theme\commands;

use app\theme\components\payment\offline\AbstractOfflinePayments;
use app\theme\models\EsetBankTransaction;
use app\theme\models\EsetPayment;
use app\theme\models\SystemLog;
use yii\console\Exception;


class PaymentsController extends ConsoleController
{
    public function actionSync()
    {
        $boxes = require(\Yii::getAlias('@app/theme/module/config/offline-payments.php'));

        $this->debugMsg('The quantity of partners for parse payments: ' . count($boxes));

        foreach ($boxes as $config) {
            $payments = $this->createPaymentProcessor($config);
            $payments->process($this);
        }
        $this->debugMsg('Parsing offline payment completed.');


        // проверить наличие записей в eset_bank_transaction и перенести в eset_payment
        $new_transactions = EsetBankTransaction::find()->fresh()->all();
        foreach($new_transactions as $transaction) {

            $payment = $transaction->create_payment();
            if( $payment->hasErrors() ) {
                //throw new Exception('Ошибка при записи платежа в базу данных' . print_r($payment->getErrors(),1));
                $this->error('Are unable to create the payment: ' . print_r($payment->getErrors(),1), SystemLog::CATEGORY_PAYMENT);
                continue;
            }

        }

        // проверить наличие новых записей в eset_payment и попытаться их связать с инвойсами
        $new_payments = EsetPayment::find()->fresh()->all();
        foreach($new_payments as $payment) {
            $payment->autoLink();
        }



    }

    /**
     * @param $config
     * @return AbstractOfflinePayments
     * @throws \yii\base\InvalidConfigException
     */
    private function createPaymentProcessor($config)
    {
        return \Yii::createObject($config);
    }


}
