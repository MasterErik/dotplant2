<?php
namespace app\theme\commands;

use app\theme\components\invoice\Generator;
use app\theme\models\Order;
use app\theme\models\SystemLog;
use app\theme\module\helpers\DateUtils;

class InvoiceController extends ConsoleController
{
    /**
     * @throws \yii\db\Exception
     */
    public function actionGenAutoInvoices()
    {
        $generator = new Generator();

        $orders = Order::find()
            ->noInvoice()
            ->userInvoiceAuto()
            ->period(DateUtils::CurEndDay())
            ->finish()
            ->all();

        //echo count($orders); exit;

        $this->debugMsg('The quantity of orders for generation invoices: '. count($orders));

        $beginTransaction = \Yii::$app->db->beginTransaction();
        foreach($orders as $order) {
            $invoice = $generator->addOrder($order);
            if( !$invoice ) {
                $this->error('order #'. $order->id .', errors:' . print_r($generator->getErrors(),1), SystemLog::CATEGORY_INVOICE);
                exit;
            }
            $this->debugMsg('The order #'. $order->id .' ' . $order->total_price . ' add to invoice: No '. $invoice->number . ' sum=' . $invoice->sum);
        }
        $beginTransaction->commit();
        $this->debugMsg('OK');
    }
}
