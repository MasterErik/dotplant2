<?php
namespace app\theme\commands;

use app\theme\components\restClient\entity\Price;
use app\theme\models\DiscountCodeList;
use app\theme\models\Product;
use app\theme\models\Sector;
use app\theme\models\SystemLog;
use app\theme\models\User;
use app\theme\models\UserProfile;
use app\theme\services\Synchronization;


class RestController extends ConsoleController
{
    public function actionSyncPrice($product_id = null)
    {
        $query = Product::find()->where('eset_product_code IS NOT NULL AND active=1');
        if( $product_id ) {
            $query->andWhere('id=:id', [':id' => $product_id]);
        }
        $products = $query->all();

        $this->debugMsg('The quantity of products for synchronization: '. count($products));

        foreach( $products as $product ) {

            $info_product = ' #' .$product->id . ' '. $product->name;
            $this->debugMsg('Product: '. $info_product);

            $period = Product::getDefaultValue($product->id, 'eset_period');
            $quantity = Product::getDefaultValue($product->id, 'quantity');
            if( !$period ) {
                $this->error('Error getting period' . $info_product, SystemLog::CATEGORY_PRODUCT);
                continue;
            }
            if( !$quantity ) {
                $this->error('Error getting quantity' . $info_product, SystemLog::CATEGORY_PRODUCT);
                continue;
            }

            $params = [
                'product' => $product->eset_product_code,
                'countryid' => UserProfile::getParentPartner()->country->code,
                'sector' => Sector::SECTOR_BUSINESS,
                'discountcode' => DiscountCodeList::CODE_NO_DISCOUNT,
                'licensetype' => 'subscription',
                'expirationdate' => strftime('%m/%d/%Y', strtotime('now +' . $period . ' month')),
                'quantity' => $quantity,
            ];

            $price = Price::one($params, \Yii::$app->params['defaultResellerId']);

            if (is_null($price)) {
                $this->error('Unknown error check price' . $info_product);
                continue;
            }

            if ( is_string($price) ) {
                $this->error('Error check price' . $price .', ' . $info_product);
                continue;
            }

            if ($price->isError()) {
                $this->error('Error check price: ' . $price->Message .', ' . $info_product);
                continue;
            }

            if (!isset($price->Currency) && $price->Currency != 'EUR') {
                $this->error('Error check price: Currency is not EUR - ' . $price->Currency . ', ' . $info_product);
                continue;
            }

            if( $product->price != $price->SystemPrice ) {
//                $product->old_price = $product->price;
                $product->price = $price->SystemPrice;
                if( !$product->save() ) {
                    $this->error('Error save price product: ' . $info_product .', price=' . $price->SystemPrice . ', ' . print_r($product->getErrors(),1));
                }
                $this->info('New price product: ' . $info_product .' ' . $price->SystemPrice);
            }

        }

        $this->debugMsg('Synchronization is complete price');
    }

    public function actionSyncLicense($user_id = null)
    {
        $synchronization = new Synchronization();

        if (!empty($user_id)) {
            $users = User::findOne(['id' => $user_id]);
        } else {
            $users = User::find()->addActive()->all();
        }

        if (!empty($user_id)) {
            $synchronization->run($users);
        } else {
            foreach ($users as $user) {
                $synchronization->run($user);
            }
        }
    }

}
