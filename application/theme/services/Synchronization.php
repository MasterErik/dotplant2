<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 12.2.2016
 * Time: 13:49
 */

namespace app\theme\services;

use app\theme\components\license\proxy\RestLicenseProxy;
use app\theme\components\restClient\entity\LicenseByEpli;
use app\theme\components\restClient\entity\SearchLicense;

use app\theme\models\License;
use app\theme\models\User;
use yii\base\Exception;
use Yii;
use yii\helpers\ArrayHelper;

class Synchronization
{
    const syncKey = 'epli';
    const cacheKey = 'RestLicenseByUser:';
    public $countNew, $countLink, $countUpd;


    public static function syncLicenseById($license_id) {
        try {

            /* @var License $db_licenses */
            $license = License::findOne(['id' => $license_id]);
            $db_licenses = License::find()->where(['id' => $license_id])->indexBy(self::syncKey)->asArray()->all();

            /* @var SearchLicense[] $rest_licenses */
            $rest_licenses = LicenseByEpli::all(['epli' => $license->epli], $license->user_id);

            $sync = new self();
            if (!$sync->isExistsLicenses($rest_licenses)) {
                License::deleteAll(['id' => $license_id]);
                return; //TODO redirect to License list
            }
            $rest_licenses = ArrayHelper::index($rest_licenses, function($element) {
                return $element->Credentials->EPLI;
            });

            $sync->countUpd = 0;
            $existsLicenses = array_intersect_key($rest_licenses, $db_licenses);
            $sync->updateLicenses($existsLicenses, $db_licenses, $license->user, $sync->countUpd);

            $sync->notification();
            return;
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error' , $e->getMessage());
        }
    }
    public static function syncLicenseByUser($user_id) {
        try {
            $synchronization = new self();
            $user = User::findOne($user_id);
            $synchronization->run($user);
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'dexter');
        }
    }
    private function isExistsLicenses($rest_licenses)
    {
        if (is_array($rest_licenses)) {
            return true;
        } elseif (is_object($rest_licenses) && $rest_licenses->isError() == true) {
            if ($rest_licenses->ErrorId !== 2) {
                throw new Exception('Error license sync: ' . $rest_licenses->toString());
            } else {
                return false;
            }
        } else {
            throw new Exception('Error license sync: ' . print_r($rest_licenses, 1));
        }
    }


    private function notification()
    {
        if (isset($_SERVER["REQUEST_URI"]) && ($this->countNew > 0 || $this->countLink > 0 || $this->countUpd > 0)) {
            Yii::$app->session->setFlash('info', Yii::t('eset', 'Synchronized licenses [new:{new}, update:{upd}, link:{link}]', [
                'new' => $this->countNew,
                'upd' => $this->countUpd,
                'link' => $this->countLink,
            ]));
        }
    }

    private static function getCacheKey($id)
    {
        return self::cacheKey . $id;
    }
    public static function isNeedQuery($id)
    {
        return !Yii::$app->cache->get(self::getCacheKey($id));
    }
    public function run($user)
    {
        if (self::isNeedQuery($user->id)) {

            /* @var SearchLicense[] $rest_licenses */
            $rest_licenses = SearchLicense::all(['customeremail' => $user->email], $user->id);
            $this->isExistsLicenses($rest_licenses);
            $rest_licenses = ArrayHelper::index($rest_licenses, function($element) {
                return $element->Credentials->EPLI;
            });
            /* @var License[] $db_licenses */
            $db_licenses = License::find()->where(['user_id' => $user->id])->indexBy(self::syncKey)->asArray()->all();

            $this->countNew = 0;
            $this->countLink = 0;
            $this->countUpd = 0;

            $newLicenses = array_diff_key($rest_licenses, $db_licenses);
            $licenses = $this->saveLicense($newLicenses, $user, $this->countNew);
            $this->saveLink($licenses, $this->countLink);
            $existsLicenses = array_intersect_key($rest_licenses, $db_licenses);
            $this->updateLicenses($existsLicenses, $db_licenses, $user, $this->countUpd);

            $this->notification();

            Yii::$app->cache->set(self::getCacheKey($user->id), true, 3600);
            return;
        }
    }

    /**
     * Save new licenses
     * @param $restLicenses
     * @param $user
     * @param $countLicenses
     * @return array
     */
    private function saveLicense($restLicenses, $user, &$countLicenses)
    {
        /** @var RestLicenseProxy $proxy */
        $proxy = new RestLicenseProxy();
        $proxy->user = $user;
        $result = [];
        foreach ($restLicenses as $rest_license) {
            $proxy->owner = $rest_license;
            /* @var License $license */
            $license = new License(['scenario' => 'rest']);
            $license->setAttributes($proxy->getAttributes());
            if (!$license->save()) {
                Yii::error('Error license insert: ' . print_r($license->getErrors(), 1), 'dexter');
            } else {
                $countLicenses++;
            }
            $result[$license->epli] = $license->toArray();
            $result[$license->epli]['parent_license_epli'] = $proxy->parentLicenseEpli;

        }
        return $result;
    }

    /**
     * Save Tree Licenses
     * @param License[] $licenses
     * @param $countLicenses
     * @throws \yii\db\Exception
     */
    private function saveLink($licenses, &$countLicenses)
    {
        $sql = 'UPDATE eset_license SET child_license_id = :child_license_id WHERE epli = :epli';
        foreach ($licenses as $key => $license) {
            if ($license['parent_license_epli']) {
                $child_license_id = $license['id'];
                $parent_epli = $license['parent_license_epli'];
                Yii::$app->db->createCommand($sql, [
                    ':child_license_id' => $child_license_id,
                    ':epli' => $parent_epli,
                ])->execute();
                $countLicenses++;
            }
        }
    }

    /**
     * @param $attributes
     * @param $db_license
     * @return bool
     */
    private function checkAttributes($attributes, $db_license)
    {
        $to_save = false;
        foreach ($attributes as $key => $value) {
            if (strtolower($db_license[$key]) != strtolower($value)) {
                $to_save = true;
                break;
            }
        }
        return $to_save;
    }

    /**
     * @param $attributes
     * @param $id
     */
    private function loadSaveLicense($attributes, $id)
    {
        /* @var License $save_license */
        $save_license = License::findOne(['id' => $id]);
        $save_license->setScenario('rest');
        $save_license->setAttributes($attributes);
        $save_license->save();

    }
    /**
     * @param $existsLicenses
     * @param $db_licenses
     * @param $user
     * @param $countLicenses
     */
    private function updateLicenses($existsLicenses, $db_licenses, $user, &$countLicenses) {
        $proxy = new RestLicenseProxy();
        $proxy->user = $user;
        foreach ($existsLicenses as $key => $rest_license) {
            $proxy->owner = $rest_license;
            $attributes = $proxy->getAttributes();
            $license = $db_licenses[$key];
            if ($this->checkAttributes($attributes, $license)) {
                $this->loadSaveLicense($attributes, $license['id']);
                $countLicenses++;
            }
        }

    }

}