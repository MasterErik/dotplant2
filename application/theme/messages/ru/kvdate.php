<?php
/**
 * Перевод сообщений.
 *
 * Данный файл автоматически сгенерирован командой 'yii message'.
 * В нем содержатся локализуемые сообщения, извлеченные из исходного кода.
 * Вы можете изменить данный файл, выполнив перевод извлеченных сообщений.
 *
 * Каждый элемент массива представляет собой перевод (значение) сообщения (ключ).
 * Если значение не заполнено, сообщение считается не переведенным.
 * Сообщения, которые уже переводить не нужно, в переведенном виде будут 
 * стоять между парой знаков '@@'.
 *
 * Строка сообщения может использоваться в формате множественного числа. См. раздел i18n
 * руководства.
 *
 * ПРИМЕЧАНИЕ. Данный файл необходимо сохранить в кодировке UTF-8.
 */
return [
    'Clear field' => 'Очистить поле',
    'Select date' => 'Выбрать дату',
];
