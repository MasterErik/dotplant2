<?php
/**
 * Перевод сообщений.
 *
 * Данный файл автоматически сгенерирован командой 'yii message'.
 * В нем содержатся локализуемые сообщения, извлеченные из исходного кода.
 * Вы можете изменить данный файл, выполнив перевод извлеченных сообщений.
 *
 * Каждый элемент массива представляет собой перевод (значение) сообщения (ключ).
 * Если значение не заполнено, сообщение считается не переведенным.
 * Сообщения, которые уже переводить не нужно, в переведенном виде будут 
 * стоять между парой знаков '@@'.
 *
 * Строка сообщения может использоваться в формате множественного числа. См. раздел i18n
 * руководства.
 *
 * ПРИМЕЧАНИЕ. Данный файл необходимо сохранить в кодировке UTF-8.
 */
return [
    'Actions' => 'Действия',
    'Active' => 'Активный',
    'Add Book' => 'Добавить книгу',
    'All' => 'Все',
    'Are you sure to delete this item?' => 'Вы уверены, что хотите удалить данный элемент?',
    'Book Listing' => 'Список книг',
    'CSV' => 'CSV',
    'Clear selection' => 'Очистить выделенное',
    'Collapse' => 'Свернуть',
    'Collapse All' => 'Свернуть все',
    'Comma Separated Values' => 'Значения, разделенные запятыми',
    'Delete' => 'Удалить',
    'Disable any popup blockers in your browser to ensure proper download.' => 'Отключите все блокировщики всплывающих окон в вашем браузере для обеспечения надлежащей загрузки.' ,
    'Download Selected' => 'Загрузить выбранное',
    'Excel' => 'Excel',
    'Expand' => 'Развернуть',
    'Expand All' => 'Развернуть все',
    'Export' => 'Экспортировать',
    'Export All Data' => 'Экспортировать все данные',
    'Export Page Data' => 'Экспортировать данные страницы',
    'ExportWorksheet' => 'Экспортировать рабочую таблицы',
    'Generated' => 'Сгенерировано',
    'Generating the export file. Please wait...' => 'Генерирование экспортного файла. Пожалуйста, подождите...',
    'Grid Export' => 'Экспорт сетки',
    'HTML' => 'HTML',
    'Hyper Text Markup Language' => 'Язык гипертекстовой разметки',
    'Inactive' => 'Неактивный',
    'Invalid editable index or model form name' => 'Недопустимое имя редактируемого индекса или формы модели',
    'Invalid or bad editable data' => 'Недопустимые или неверные редактируемые данные',
    'JSON' => 'JSON',
    'JavaScript Object Notation' => 'Нотация объекта JavaScript',
    'Library' => 'Библиотека',
    'Microsoft Excel 95+' => 'Microsoft Excel 95+',
    'No data found' => 'Данные не найдены',
    'No valid editable model found' => 'Допустимая редактируемая модель не найдена',
    'Ok to proceed?' => 'Продолжить?',
    'PDF' => 'PDF',
    'PDF export generated by kartik-v/yii2-grid extension' => 'Экспорт PDF, сгенерированный расширением kartik-v/yii2-grid',
    'Page' => 'Страница',
    'Portable Document Format' => 'Формат переносимых документов',
    'Request submitted! You may safely close this dialog after saving your downloaded file.' => 'Запрос отправлен! После сохранения загруженного файла вы можете безопасно закрыть данное диалоговое окно.' ,
    'Reset Grid' => 'Сбросить сетку',
    'Resize table columns just like a spreadsheet by dragging the column edges.' => 'Чтобы изменить размер столбцов таблицы в соответствии с электронной таблицей, перетащите края столбцов.',
    'Show all data' => 'Показать все данные',
    'Show first page data' => 'Показать данные первой страницы',
    'Tab Delimited Text' => 'Текст с разделителем-табуляцией',
    'Text' => 'Текст',
    'The CSV export file will be generated for download.' => 'Будет сгенерирован экспортный файл CSV  для загрузки.',
    'The EXCEL export file will be generated for download.' => 'Будет сгенерирован экспортный файл EXCEL для загрузки.',
    'The HTML export file will be generated for download.' => 'Будет сгенерирован экспортный файл HTML для загрузки.',
    'The JSON export file will be generated for download.' => 'Будет сгенерирован экспортный файл JSON для загрузки.',
    'The PDF export file will be generated for download.' => 'Будет сгенерирован экспортный файл PDF для загрузки.',
    'The TEXT export file will be generated for download.' => 'Будет сгенерирован экспортный ТЕКСТОВЫЙ файл для загрузки.',
    'The page summary displays SUM for first 3 amount columns and AVG for the last.' => 'На сводной странице отображается СУММА числовых значений первых трех столбцов и СРЕДНЕЕ АРИФМЕТИЧЕСКОЕ последнего столбца.',
    'The table header sticks to the top in this demo as you scroll' => 'Во время прокрутки содержимого в данной демоверсии заголовок таблицы отображается вверху',
    'There are {totalCount} records. Are you sure you want to display them all?' => 'Имеются записи {totalCount}. Вы уверены, что хотите отобразить все записи?',
    'Update' => 'Обновить',
    'View' => 'Просмотреть',
    'Yii2 Grid Export (PDF)' => 'Экспорт сетки Yii2 (PDF)',
    'export' => 'экспорт',
    'grid-export' => 'экспорт сетки',
    'krajee, grid, export, yii2-grid, pdf' => 'krajee, сетка, экспорт, yii2-grid, pdf',
    'В© Krajee Yii2 Extensions' => 'Расширения В© Krajee Yii2',
];
