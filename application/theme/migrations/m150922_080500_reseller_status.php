<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\ResellerStatusList;
use yii\db\Schema;

class m150922_080500_reseller_status extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_reseller_status}}', [
            'id' => Schema::TYPE_PK,
            'partner_id' => Schema::TYPE_INTEGER ." UNSIGNED  NOT NULL",
            'reseller_status' => "enum('" . implode("','", ResellerStatusList::getKey()) . "') NOT NULL DEFAULT '" . ResellerStatusList::RESELLER_STATUS_REG . "'",
            'discount' => Schema::TYPE_DECIMAL . "(8) NOT NULL",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
        ]);

        $this->addForeignKey('fk_eset_user_profile__reseller_status_id', '{{%eset_reseller_status}}', 'partner_id', '{{%user}}', 'id', 'restrict', 'cascade');
        $time = time();
        $this->batchInsert('{{%eset_reseller_status}}',
            ['id', 'partner_id', 'reseller_status', 'discount', 'created_at'],
            [
                [1, 1, ResellerStatusList::RESELLER_STATUS_REG, 20, $time],
                [2, 1, ResellerStatusList::RESELLER_STATUS_GOLD, 35, $time],
                [3, 1, ResellerStatusList::RESELLER_STATUS_SILVER, 30, $time],
                [4, 1, ResellerStatusList::RESELLER_STATUS_BRONZE, 25, $time],
            ]
        );

        //$this->insert('{{%eset_reseller_status}}', ['name' => 'GOLD', 'discount' => 20]);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_eset_user_profile__reseller_status_id', '{{%eset_reseller_status}}');
        $this->dropTable('{{%eset_reseller_status}}');
    }
}
