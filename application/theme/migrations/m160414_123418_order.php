<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m160414_123418_order extends CustomMigration
{

    public function safeUp()
    {
        $this->alterColumn(\app\theme\models\Order::tableName(), 'user_id', 'INT(10) UNSIGNED DEFAULT NULL');
    }

    public function safeDown()
    {
    }

}
