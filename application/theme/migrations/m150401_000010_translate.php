<?php

use app\theme\migrations\CustomMigration;

class m150401_000010_translate extends CustomMigration
{
    public function up()
    {
        $this->createTable(
            '{{%eset_translate}}',
            [
                'table' => 'VARCHAR(255) CHARACTER SET latin1 NOT NULL',
                'model_id' => ' INT NOT NULL',
                'attribute' => 'VARCHAR(256) CHARACTER SET latin1 NOT NULL',
                'language' => 'CHAR(2) CHARACTER SET latin1 NOT NULL',
                'translate' => 'MEDIUMTEXT NOT NULL',
            ]
        );
        $this->addPrimaryKey('pk_eset_translate', '{{%eset_translate}}', ['table', 'model_id', 'attribute', 'language']);

        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 2, 'attribute' => 'name', 'language' => 'ru', 'translate' => 'Новости']);
        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 2, 'attribute' => 'name', 'language' => 'ee', 'translate' => 'Uudised']);
        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 2, 'attribute' => 'name', 'language' => 'lv', 'translate' => 'Naujienos']);

        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 3, 'attribute' => 'name', 'language' => 'ru', 'translate' => 'Специальное предложение']);
        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 3, 'attribute' => 'name', 'language' => 'ee', 'translate' => 'Eripakkumine']);
        $this->insert('{{%eset_translate}}', ['table' => '{{%navigation}}', 'model_id' => 3, 'attribute' => 'name', 'language' => 'lv', 'translate' => 'Specialus pasiūlymas']);
    }

    public function down()
    {
        $this->dropTable('{{%eset_translate}}');
    }
}
