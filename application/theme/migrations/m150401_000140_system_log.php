<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000140_system_log extends CustomMigration
{
    public function up()
    {
        $this->dropTable('{{%system_log}}');
        $this->createTable('{{%system_log}}', [
            'id' => Schema::TYPE_PK,
            'level' => Schema::TYPE_INTEGER,
            'category' => Schema::TYPE_STRING . '(255)',
            'log_time' => Schema::TYPE_INTEGER . ' NOT NULL',
            'prefix' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'message' => Schema::TYPE_TEXT
        ]);

        $this->createIndex('idx_log_level', '{{%system_log}}', 'level');
        $this->createIndex('idx_log_category', '{{%system_log}}', 'category');
        $this->insert('{{%backend_menu}}', [
            'parent_id' => 15,
            'name' => 'Log',
            'route' => 'eset/backend/log/index',
            'icon' => 'flash',
            'added_by_ext' => 'core',
            'rbac_check' => 'setting manage',
            'translation_category' => 'eset'
        ]);
    }

    public function down()
    {
        $this->delete('{{%backend_menu}}', ['route' => 'eset/backend/log/index']);
        $this->dropTable('{{%system_log}}');
    }
}
