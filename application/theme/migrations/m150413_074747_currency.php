<?php

use app\theme\migrations\CustomMigration;

class m150413_074747_currency extends CustomMigration
{
    public function up()
    {
        $this->execute("CREATE
            TRIGGER trig_currency_D
        	BEFORE DELETE
	        ON currency
	        FOR EACH ROW
            BEGIN
                IF OLD.id = 1 THEN
                    SIGNAL SQLSTATE '45000'
                    SET MESSAGE_TEXT = 'Can not delete this record';
                END IF;
            END");

        $this->execute("UPDATE {{%currency}} SET is_main=0");
        $this->execute("UPDATE {{%currency}} SET is_main=1 WHERE iso_code='EUR'");
    }


    public function down()
    {
        $this->execute("DROP TRIGGER trig_currency_D");
    }
}
