<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000060_discount extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_discount}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => "INT UNSIGNED NULL",
            'product_id' => "INT UNSIGNED DEFAULT NULL",
            'sign' => Schema::TYPE_STRING . "(1) NOT NULL",
            'cost' => Schema::TYPE_DECIMAL . "(8) NOT NULL",
            'bdate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'edate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '2037-01-01 00:00:00'",

            // мини-история
            // время
            'created_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            // пользователь
            'creator_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
            'updater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
        ]);

        // indexes
        $this->createIndex('idx_eset_discount_date_range', '{{%eset_discount}}', ['bdate', 'edate']);
        // ForeignKey
        $this->addForeignKey('fk_eset_discount_product_id', '{{%eset_discount}}', 'product_id', '{{%product}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_discount_user_id', '{{%eset_discount}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_discount}}');
    }
}
