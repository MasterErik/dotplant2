<?php

use yii\db\Migration;

class m151015_111039_order extends Migration
{
    public function safeUp()
    {
        $this->execute("DELETE FROM `order_item`  WHERE NOT EXISTS
          (SELECT o.id FROM `order` o WHERE o.id = order_item.order_id)"
        );

        $this->addForeignKey('fk_order_item_order_id', '{{%order_item}}', 'order_id', '{{%order}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_item_order_id', '{{%order_item}}');
    }

}
