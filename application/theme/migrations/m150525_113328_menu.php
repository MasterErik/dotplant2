<?php

use yii\db\Schema;
use app\theme\migrations\CustomMigration;
use app\widgets\navigation\models\Navigation;

class m150525_113328_menu extends CustomMigration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $navigation = Navigation::findOne(['parent_id' => 0]);
        $this->batchInsert(
            Navigation::tableName(),
            ['parent_id', 'name', 'url', 'route_params'],
            [
                [$navigation->id, 'Contact', '/contacts', '{}'],
                [$navigation->id, 'Order', 'cabinet/orders', '{}'],
                [$navigation->id, 'License', '/license', '{}'],
                [$navigation->id, 'About', '/about', '{}'],
            ]
        );
    }
    
    public function safeDown()
    {
        $this->delete(Navigation::tableName(), ['name' => 'Contact']);
        $this->delete(Navigation::tableName(), ['name' => 'Order']);
        $this->delete(Navigation::tableName(), ['name' => 'License']);
        $this->delete(Navigation::tableName(), ['name' => 'About']);
    }
}
