<?php

use app\theme\migrations\CustomMigration;
use app\models\OrderStatus;
use app\theme\models\Translate;

class m150430_000000_translate_order_status extends CustomMigration
{
    public function safeUp()
    {
//MariaDB [dotplant2]> select id,title,short_title,internal_comment from  order_status;
//+----+--------------------------------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------+
//| id | title                                                  | short_title                                        | internal_comment                                                                                                                       |
//+----+--------------------------------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------+
//|  1 | Формирование заказа клиентом                           | не сформирован                                     | Клиент определяется с заказом.                                                                                                         |
//|  2 | Ожидается оплата                                       | оплата                                             | Ожидается подтверждение оплаты от платежной системы или администратора.                                                                |
//|  3 | Оплачен/ожидается обработка                            | оплачен / требует обработки                        | Оплачен или оплата наличными                                                                                                           |
//|  4 | В обработке                                            | в обработке                                        | Заказ в процессе обработки сотрудниками магазина.                                                                                      |
//|  5 | Доставка                                               | доставка                                           | Доставка в процессе или доставлено на склад для самовывоза                                                                             |
//|  6 | Доставлен                                              | закрыт                                             | Доставлен и/или оплачен наличными. Заказ закрыт.                                                                                       |
//|  7 | Отменен                                                | отменен                                            | Заказ отменен                                                                                                                          |
//+----+--------------------------------------------------------+----------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------+

        $translates = [
            ['model_id' => 1, 'attribute' => 'title', 'language' => 'ru', 'translate' => 'Формирование заказа клиентом'],
            ['model_id' => 1, 'attribute' => 'title', 'language' => 'en', 'translate' => 'Formation of Customer Orders'],
            ['model_id' => 1, 'attribute' => 'short_title', 'language' => 'ru', 'translate' => 'не сформирован'],
            ['model_id' => 1, 'attribute' => 'short_title', 'language' => 'en', 'translate' => 'not formed'],
//            ['model_id' => 1, 'attribute' => 'internal_comment', 'language' => 'en', 'translate' => 'Клиент определяется с заказом.'],
//            ['model_id' => 1, 'attribute' => 'internal_comment', 'language' => 'en', 'translate' => ''],


            ['model_id' => 2, 'attribute' => 'title', 'language' => 'ru', 'translate' => 'Ожидается оплата'],
            ['model_id' => 2, 'attribute' => 'title', 'language' => 'en', 'translate' => 'Expected payment'],
            ['model_id' => 2, 'attribute' => 'short_title', 'language' => 'ru', 'translate' => 'оплата'],
            ['model_id' => 2, 'attribute' => 'short_title', 'language' => 'en', 'translate' => 'payment'],

            ['model_id' => 3, 'attribute' => 'title', 'language' => 'ru', 'translate' => 'Оплачен/ожидается обработка'],
            ['model_id' => 3, 'attribute' => 'title', 'language' => 'en', 'translate' => 'Paid / expected treatment'],
            ['model_id' => 3, 'attribute' => 'short_title', 'language' => 'ru', 'translate' => 'оплачен / требует обработки'],
            ['model_id' => 3, 'attribute' => 'short_title', 'language' => 'en', 'translate' => 'paid / requires treatment'],

            ['model_id' => 4, 'attribute' => 'title', 'language' => 'ru', 'translate' => 'В обработке'],
            ['model_id' => 4, 'attribute' => 'title', 'language' => 'en', 'translate' => 'In progress'],
            ['model_id' => 4, 'attribute' => 'short_title', 'language' => 'ru', 'translate' => 'в обработке'],
            ['model_id' => 4, 'attribute' => 'short_title', 'language' => 'en', 'translate' => 'in Progress'],
        ];

        foreach( $translates as $translate ) {
            $this->insert(Translate::tableName(), [
                'table' => OrderStatus::tableName(),
                'model_id' => $translate['model_id'],
                'attribute' => $translate['attribute'],
                'language' => $translate['language'],
                'translate' => $translate['translate']
            ]);
        }
    }

    public function safeDown()
    {
        $this->delete(Translate::tableName(), '`table`=:table AND model_id IN (1,2,3,4)', [':table'=>OrderStatus::tableName()]);
    }
}

