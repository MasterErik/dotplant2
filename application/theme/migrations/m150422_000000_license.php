<?php

use app\theme\models\LicenseStatus;
use app\theme\models\PurchaseType;
use app\theme\migrations\CustomMigration;
use app\theme\models\search\LicenseSearch;
use yii\db\Schema;

class m150422_000000_license extends CustomMigration
{
    public function safeUp()
    {
        $this->dropTable('{{%eset_license}}');

        $this->createTable('{{%eset_license}}', [
            'id' => Schema::TYPE_PK,
            'order_detail_id' => "INT UNSIGNED",
            'child_license_id' => Schema::TYPE_INTEGER,
            'user_id' => "INT UNSIGNED NOT NULL",
            'reseller_id' => "INT UNSIGNED DEFAULT NULL",
            'product_code' => Schema::TYPE_INTEGER . " NOT NULL",
            'country_code' => Schema::TYPE_INTEGER . " NOT NULL",
            'quantity' => Schema::TYPE_INTEGER . " NOT NULL",
            'purchase_type' => PurchaseType::getEnum() . "NOT NULL DEFAULT '" . PurchaseType::PURCHASE_TYPE_NEW . "'",
            'register_state' => "enum('registered','unregistered') NOT NULL DEFAULT 'unregistered'",
            'state' => LicenseStatus::getEnum() . "NOT NULL DEFAULT 'inactive'",
            'license_type_global' => "enum('normal','deslock') NOT NULL DEFAULT 'normal'",
            'license_type' => "enum('subscription','perpetual') NOT NULL DEFAULT 'subscription'",
            'sector' => "enum('Business', 'Government', 'Education')" . " DEFAULT NULL",
            'bdate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'edate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00'",
            'period' => Schema::TYPE_INTEGER . " NOT NULL",

            'discount_code' => Schema::TYPE_INTEGER . " NOT NULL",
            'custom_discount' => Schema::TYPE_DECIMAL . "(5,2) DEFAULT 0",
            'discount_document' => Schema::TYPE_STRING . "(50) DEFAULT NULL",

            'username' => Schema::TYPE_STRING . "(50) NULL",
            'password' => Schema::TYPE_STRING . "(50) NULL",
            'epli' => Schema::TYPE_STRING . "(50) NULL",
            'license_key' => Schema::TYPE_STRING . "(50) NULL",
            'public_license_key' => Schema::TYPE_STRING . "(50) NULL",
            'common_tag' => Schema::TYPE_STRING . "(50) NULL",
            'note' => Schema::TYPE_STRING . "(255) NULL",
            'note_own' => Schema::TYPE_STRING . "(255) NULL",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
        ]);

        // indexes
        $this->createIndex('idx_eset_license_date_range', '{{%eset_license}}', ['bdate', 'edate']);
        $this->createIndex('idx_eset_license_order_detail_id_unique', '{{%eset_license}}', ['order_detail_id'], true);
        //ForeignKey
        $this->addForeignKey('fk_eset_license_order_detail_id', '{{%eset_license}}', 'order_detail_id', '{{%order_item}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_license_user_id', '{{%eset_license}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_license_reseller_id', '{{%eset_license}}', 'reseller_id', '{{%user}}', 'id', 'restrict', 'cascade');
        // unique indexes
        $this->createIndex('idx_eset_license_epli', '{{%eset_license}}', ['epli'], true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_license}}');
    }
}
