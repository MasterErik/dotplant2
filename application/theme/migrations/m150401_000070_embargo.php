<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

/**
 * Эмбарго
 */
class m150401_000070_embargo extends CustomMigration
{
    public function safeUp()
    {
        // product table
        $this->createTable('{{%eset_embargo}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => "INT UNSIGNED NOT NULL",
            'product_id' => "INT UNSIGNED NOT NULL",

            // период активности
            'bdate' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP',
            'edate' => Schema::TYPE_TIMESTAMP . " DEFAULT '0000-00-00 00:00:00'",
        ]);

        // indexes
        $this->createIndex('idx_eset_embargo_date_range', '{{%eset_embargo}}', ['bdate', 'edate']);

        // ForeignKey
        $this->addForeignKey('fk_eset_embargo_user_id', '{{%eset_embargo}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_embargo_product_id', '{{%eset_embargo}}', 'product_id', '{{%product}}', 'id', 'cascade', 'cascade');

    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_embargo}}');
    }
}
