<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 25.09.2015
 * Time: 18:42
 */

namespace app\theme\migrations;

use yii\db\Migration;

class CustomMigration extends Migration
{
    public function dropTable($table) {
        parent::dropTable('IF EXISTS ' . $table);
    }

    public function addColumn($table, $column, $type)
    {
        if (!$this->existsField($table, $column)) {
            parent::addColumn($table, $column, $type);
        }
    }

    public function existsField($table, $column)
    {
        $sql = 'SHOW COLUMNS FROM ' .$table . ' LIKE :field';
        $result = $this->getDb()->createCommand($sql, [':field' => $column])->queryColumn();
        return count($result) != 0;
    }

}