<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\UserProfile;


class m160111_174846_fix_eset_user_profile extends CustomMigration
{
    public function safeUp()
    {
        if (!$this->existsField(UserProfile::tableName(), 'client_name')) {
            $this->addColumn(UserProfile::tableName(), 'client_name', Schema::TYPE_STRING . "(255)");
            $this->execute('UPDATE '. UserProfile::tableName() .' SET client_name=(SELECT IFNULL(CONCAT(first_name, " ", last_name), username) FROM user WHERE id=user_id ) WHERE client_name IS NULL');
        }
        if (!$this->existsField(UserProfile::tableName(), 'reseller_status_id')) {
            $this->addColumn(UserProfile::tableName(), 'reseller_status_id', Schema::TYPE_INTEGER . " UNSIGNED  NOT NULL");
            $this->addForeignKey('fk_eset_user_profile__reseller_status_id', UserProfile::tableName(), 'reseller_status_id', '{{%eset_reseller_status}}', 'id', 'restrict', 'cascade');
        }
        $this->addForeignKey('fk_eset_reseller_status__partner_id', UserProfile::tableName(), 'reseller_status_id', '{{%eset_reseller_status}}', 'id', 'restrict', 'cascade');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_eset_reseller_status__partner_id', UserProfile::tableName());
    }

}
