<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m150901_000000_invoices extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_invoice}}', [
            'id' => Schema::TYPE_PK,

            'partner_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',

            'number' => Schema::TYPE_STRING . "(30) NOT NULL",
            'number_invoice' => Schema::TYPE_STRING . "(30)",

            'user_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'type' => "enum('payment', 'prepayment', 'invoice', 'credit') NOT NULL DEFAULT 'prepayment'",
            'builder' => "enum('system', 'user') NOT NULL DEFAULT 'system'",

            'tax' => Schema::TYPE_DECIMAL . "(8,2) NOT NULL",
            'sum' => Schema::TYPE_DECIMAL . "(10,2) NOT NULL",

            'iban' => Schema::TYPE_STRING . "(34) NOT NULL",
            'swift' => Schema::TYPE_STRING . "(30) NOT NULL",
            'viitenumber' => Schema::TYPE_INTEGER,

            'bill_to' => Schema::TYPE_STRING . "(1024) NOT NULL",
            'due_date' => Schema::TYPE_DATE . " NOT NULL",

            // пользователь
            'creator_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
            // мини-история
            'created_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',

        ]);
        $this->addForeignKey('fk_eset_invoice__partner_id', '{{%eset_invoice}}', 'partner_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_invoice__user_id', '{{%eset_invoice}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx_eset_invoice__number', '{{%eset_invoice}}', 'number', true);

        $this->createTable('{{%eset_invoice_detail}}', [
            'id' => Schema::TYPE_PK,
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'order_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT NULL',
            'discount_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'type' => "enum('order', 'bonus', 'partner', 'info', 'alarm') NOT NULL DEFAULT 'order'",
            'tax' => Schema::TYPE_DECIMAL . "(8,2) NOT NULL",
            'sum' => Schema::TYPE_DECIMAL . "(10,2) NOT NULL",
        ]);
        $this->addForeignKey('fk_eset_invoice_detail__invoice_id', '{{%eset_invoice_detail}}', 'invoice_id', '{{%eset_invoice}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_invoice_detail__order_id', '{{%eset_invoice_detail}}', 'order_id', '{{%order}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_invoice_detail__discount_id', '{{%eset_invoice_detail}}', 'discount_id', '{{%eset_discount}}', 'id', 'restrict', 'cascade');

        $this->createTable('{{%eset_invoice_online_pay}}', [
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'order_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'key' => "CHAR(32) NOT NULL",
            'created_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->addForeignKey('fk_eset_invoice_detail_pay__invoice_id', '{{%eset_invoice_online_pay}}', 'invoice_id', '{{%eset_invoice}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_invoice_detail_pay__order_id', '{{%eset_invoice_online_pay}}', 'order_id', '{{%order}}', 'id', 'cascade', 'cascade');

    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_invoice_online_pay}}');
        $this->dropTable('{{%eset_invoice_detail}}');
        $this->dropTable('{{%eset_invoice}}');
    }
}
