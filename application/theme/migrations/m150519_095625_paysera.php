<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;
use yii\helpers\Json;
use yii\url;

use app\theme\models\PaymentType;
use app\theme\components\payment\PayseraPayment;
use app\theme\components\payment\PaypalPayment;

class m150519_095625_paysera extends CustomMigration
{
    private $payConf;
    public function safeUp()
    {
        $this->addColumn(PaymentType::tableName(), 'key', 'VARCHAR(50) DEFAULT NULL');
        $this->addColumn(PaymentType::tableName(), 'country_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
        $this->addColumn(PaymentType::tableName(), 'min', Schema::TYPE_DECIMAL . '(4,2) DEFAULT 0');

        $logoImg = '/theme/resources/payments/';

        $this->payConf = [
            "projectid" => 66101,
            "password" => "e4dc89291eccb511cb39bd3099d44b58",
            "currency" => "EUR",
            "mode" => "sandbox",
            "color" => "",
        ];

        $this->delete(PaymentType::tableName());

        $this->batchInsert(PaymentType::tableName(),
            ['id', 'name', 'key', 'class', 'country_id', 'active', 'payment_available', 'sort', 'params', 'commission', 'min', 'logo'],
            [
                [20, 'Paysera', NULL, PayseraPayment::className(), NULL, 1, 0, 0, $this->getConf('#161616'), 0, 0, ''],

                [21, 'Swedbank', 'hanzaee', PayseraPayment::className(), 3, 1, 1, 1, $this->getConf('#ff6803'), 0, 0.01, $logoImg . 'swedbank.png'],
                [22, 'SEB', 'sebee', PayseraPayment::className(), 3, 1, 1, 2, $this->getConf('#161616'), 0, 0.90, $logoImg . 'seb.png'],
                [23, 'Nordea', 'nordeaee', PayseraPayment::className(), 3, 1, 1, 3, $this->getConf('#036aa0'), 0, 0.28, $logoImg . 'nordea.png'],
                [24, 'Danske', 'sampoee', PayseraPayment::className(), 3, 1, 1, 4, $this->getConf('#034362'), 0, 0.28, $logoImg . 'danske.png'],
                [25, 'Eesti Krediidipank', 'krediidi_ee', PayseraPayment::className(), 3, 1, 1, 5, $this->getConf('#9b0332'), 0, 0.28, $logoImg . 'krediidipank.png'],
                [26, 'LHV', 'lhv_ee', PayseraPayment::className(), 3, 1, 1, 6, $this->getConf('#161616'), 0, 0.28, $logoImg . 'lhv.jpg'],

                [27, 'Paysera', 'wallet', PayseraPayment::className(), NULL, 1, 1, 7, $this->getConf(), 0, 0.01, $logoImg . 'paysera.png'],
                [28, 'International payment in Euros', 'worldhand', PayseraPayment::className(), NULL, 1, 1, 8, $this->getConf(), 0, 0.28, $logoImg . 'worldhand.png'],
                [29, 'WebMoney', 'webmoney', PayseraPayment::className(), NULL, 1, 1, 9, $this->getConf(), 0, 0.28, $logoImg . 'webmoney.png'],
                [30, 'Credit cards', 'lv_lpb', PayseraPayment::className(), NULL, 1, 1, 10, $this->getConf(), 0, 0.02, $logoImg . 'visa-mastercard-maestro.png'],
                [31, '1stPayments', '1stpay', PayseraPayment::className(), NULL, 1, 1, 11, $this->getConf(), 0, 0.28, $logoImg . '1stpay.png'],

                [33, 'Swedbank', 'hanzalv', PayseraPayment::className(), 4, 1, 1, 1, $this->getConf('#ff6803'), 0, 0.15, $logoImg . 'swedbank.png'],
                [34, 'SEB', 'seblv', PayseraPayment::className(), 4, 1, 1, 2, $this->getConf('#161616'), 0, 0.15, $logoImg . 'seb.png'],
                [35, 'Nordea', 'nordealv', PayseraPayment::className(), 4, 1, 1, 3, $this->getConf('#036aa0'), 0, 0.15, $logoImg . 'nordea.png'],
                [36, 'Citadele', 'parexlv', PayseraPayment::className(), 4, 1, 1, 4, $this->getConf(), 0, 0.15, $logoImg . 'citadele.png'],
                [37, 'MAXIMA LV', 'maximalv', PayseraPayment::className(), 4, 1, 1, 4, $this->getConf(), 0, 0.28, $logoImg . 'maxima.png'],

                [40, 'Paypal', 'Paypal', PaypalPayment::className(), NULL, 1, 1, 0, Json::encode(
                    [
                        'clientId' => 'AbfURip-2vcpEYxHUuWDeKMP3cjEXRcbwUnE4QxKKZ90cWQBAn4M03dcna51vs0e1W4eiv1ml-PxRj-7',
                        'clientSecret' => 'EL7JcWX3VD4r2YnjhVVi9SybFpmtnN9dyEvAgWW14Fm_ABlaBH51l4ll6N191NrIZ8U5h3CT2_VVXj03',
                        'currency' => 'EUR',
                        'mode' => 'sandbox',
                    ]),
                    0, 0, $logoImg . 'paypal.png'
                ]
            ]
        );
    }

    protected function getConf($color = null)
    {
        if (isset($color)) {
            $this->payConf["color"] = $color;
        };
        return Json::encode($this->payConf);
    }

    public function safeDown()
    {
        $this->dropColumn(PaymentType::tableName(), 'key');
        $this->dropColumn(PaymentType::tableName(), 'country_id');
        $this->dropColumn(PaymentType::tableName(), 'min');
    }
}
