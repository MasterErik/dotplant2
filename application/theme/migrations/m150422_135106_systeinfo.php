<?php

use app\theme\migrations\CustomMigration;

class m150422_135106_systeinfo extends CustomMigration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->update('{{%backend_menu}}', ['route' => 'eset/backend/information/index'], ['id' => 2]);
    }
    
    public function safeDown()
    {
        $this->update('{{%backend_menu}}', ['route' => 'backend/dashboard/index'], ['id' => 2]);
    }
}
