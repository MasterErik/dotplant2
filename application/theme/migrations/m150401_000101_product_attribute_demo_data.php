<?php

use app\theme\migrations\CustomMigration;

class m150401_000101_product_attribute_demo_data extends CustomMigration
{
    public function up()
    {
        if($this->db->createCommand('SELECT id FROM product WHERE id = 1')->queryScalar()) {
            // данные для разработки (связь с demo-данными
            $this->insert('{{%eset_product_attribute}}', ['product_id' => 1, 'attribute' => 'quantity', 'value' => json_encode([
                'rules' => [
                    ['quantity', 'default', 'value' => 1],
                    ['quantity', 'required'],
                    ['quantity', 'integer', 'min' => 1, 'max' => 5]
                ],
                'type' => 'text',
                'hint' => 'Enter a value in the range from 1 to 5'
            ])]);

            $this->insert('{{%eset_product_attribute}}', ['product_id' => 1, 'attribute' => 'period', 'value' => json_encode([
                'rules' => [
                    ['period', 'default', 'value' => 12],
                    ['period', 'required'],
                    ['period', 'in', 'range' => [12, 24, 36]],
                ],
                'type' => 'select',
                'data' => [12 => '1 year', 24 => '2 years', 36 => '3 years']
            ])]);
        };

        if($this->db->createCommand('SELECT id FROM product WHERE id = 1')->queryScalar()) {
            $this->insert('{{%eset_product_attribute}}', ['product_id' => 2, 'attribute' => 'quantity', 'value' => json_encode([
                'rules' => [
                    ['quantity', 'default', 'value' => 5],
                    ['quantity', 'required'],
                    ['quantity', 'integer', 'min' => 5, 'max' => 25]
                ],
                'type' => 'text',
                'hint' => 'Enter a value in the range from 5 to 25'
            ])]);


            $this->insert('{{%eset_product_attribute}}', ['product_id' => 2, 'attribute' => 'period', 'value' => json_encode([
                'rules' => [
                    ['period', 'default', 'value' => 12],
                    ['period', 'required'],
                    ['period', 'in', 'range' => [12, 24, 36]],
                ],
                'type' => 'select',
                'data' => [12 => '1 year', 24 => '2 years', 36 => '3 years']
            ])]);
        };
    }

    public function down()
    {
        $this->delete('{{%eset_product_attribute}}', ['product_id' => 1]);
        $this->delete('{{%eset_product_attribute}}', ['product_id' => 2]);
    }
}
