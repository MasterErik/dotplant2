<?php

use app\theme\migrations\CustomMigration;

class m150401_000000_country extends CustomMigration
{
    public function up()
    {

        $this->execute("ALTER TABLE {{%country}} ADD COLUMN code INTEGER(11) DEFAULT NULL;");
        $this->execute("ALTER TABLE {{%country}} ADD COLUMN tax decimal(8, 0) DEFAULT 20;");


        $this->batchInsert(
            '{{%country}}',
            ['id', 'name', 'iso_code', 'slug', 'code', 'tax'],
            [
                [3, 'Estonia', 'EST', 'estonia', 22, 20],
                [4, 'Latvia', 'LV', 'latvia', 47, 21],
            ]
        );

    }

    public function down()
    {
        $this->execute("ALTER TABLE {{%country}} DROP COLUMN tax;");
        $this->execute("ALTER TABLE {{%country}} DROP COLUMN code;");
        $this->delete('{{%country}}', 'id in (3, 4)');
    }
}
