<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m150831_000000_order_eset_tax extends CustomMigration
{
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'eset_tax', Schema::TYPE_DECIMAL . '(8,2) NOT NULL DEFAULT 0.00');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'eset_tax');
    }
}
