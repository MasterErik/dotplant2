<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000110_discount_code extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_discount_code}}', [
            'id' => Schema::TYPE_PK,
            'code' => "INT UNSIGNED NOT NULL",
            'name' => Schema::TYPE_STRING . "(255)  NOT NULL",
            'cost' => Schema::TYPE_DECIMAL . "(5,2) NOT NULL",
            'note' => Schema::TYPE_TEXT,
            'state' => Schema::TYPE_STRING . "(20)  NOT NULL",

            // мини-история
            // время
            'created_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            // пользователь
            'creator_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
            'updater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
        ]);

        foreach (json_decode('[
            {"Code":1,"Name":"Educational","DiscountPercentage":50.0,"Note":"Primary schools, secondary schools and instatutions involved in higher education","State":"Active"},
            {"Code":2,"Name":"Government","DiscountPercentage":50.0,"Note":"Entities subject to public finance, all governmental institutions","State":"Active"},
            {"Code":3,"Name":"Student","DiscountPercentage":50.0,"Note":"ISIC card holder/Limited to 1 workstation license","State":"Active"},
            {"Code":4,"Name":"Non-profit","DiscountPercentage":50.0,"Note":"Non-profit organizations","State":"Active"},
            {"Code":5,"Name":"Disabled people","DiscountPercentage":50.0,"Note":"Applicants must be holders of a valid ID proving their handicapped status","State":"Active"},
            {"Code":6,"Name":"Competitive/Migration discount","DiscountPercentage":30.0,"Note":"Migration from competitive solutions","State":"Active"},
            {"Code":0,"Name":"Custom discount ","DiscountPercentage":0.0,"Note":"Discount will be entered manualy","State":"Active"},
            {"Code":7,"Name":"No discount","DiscountPercentage":999.0,"Note":"Price without discount","State":"Active"}
            ]') as $row) {
            $this->insert('{{%eset_discount_code}}', [
                'code' => $row->Code,
                'name' => $row->Name,
                'cost' => $row->DiscountPercentage,
                'note' => $row->Note,
                'state' => $row->State,
            ]);
        }
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_discount_code}}');
    }
}
