<?php

use yii\db\Schema;
use app\theme\migrations\CustomMigration;
use app\widgets\navigation\models\Navigation;

class m150626_181818_menu_reinstall extends CustomMigration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->execute("DELETE FROM {{%navigation}} WHERE parent_id <> 0");

        $navigation = Navigation::findOne(['parent_id' => 0]);
        $this->batchInsert(
            Navigation::tableName(),
            ['parent_id', 'name', 'url', 'route_params'],
            [
                [$navigation->id, 'Contacts', '/contacts', '{}'],
//                [$navigation->id, 'Orders', '/cabinet/orders', '{}'],
//                [$navigation->id, 'Licenses', '/license', '{}'],
                [$navigation->id, 'About', '/about', '{}'],
            ]
        );
    }
    
    public function safeDown()
    {
        $this->delete(Navigation::tableName(), ['name' => 'Contact']);
        $this->delete(Navigation::tableName(), ['name' => 'Order']);
        $this->delete(Navigation::tableName(), ['name' => 'License']);
        $this->delete(Navigation::tableName(), ['name' => 'About']);
    }
}
