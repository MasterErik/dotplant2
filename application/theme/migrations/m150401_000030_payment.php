<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000030_payment extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_payment_type}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . "(50) NOT NULL",
            'type' => "enum('bank','e-money') NOT NULL DEFAULT 'bank'",
            'code' => Schema::TYPE_STRING . "(30) NOT NULL",
            'IBAN' => Schema::TYPE_STRING . "(34) NOT NULL",
            'SWIFT' => Schema::TYPE_STRING . "(30) NOT NULL",
            'bdate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'edate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '2037-01-01 00:00:00'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'changer_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'creater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
        ]);

        $this->createTable('{{%eset_payment}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => "INT UNSIGNED NOT NULL",
            'eset_paymept_type_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'type' => "enum('web','invoice') NOT NULL DEFAULT 'web'",
            'vittenumber' => Schema::TYPE_INTEGER . " NULL",
            'summa' => Schema::TYPE_DECIMAL . "(8,2) NOT NULL",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
        ]);

        $this->addForeignKey('fk_eset_payment_paymept_type_id', '{{%eset_payment}}', 'eset_paymept_type_id', '{{%eset_payment_type}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_user_id', '{{%eset_payment}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');


        $this->createTable('{{%eset_payment_link}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => "INT UNSIGNED NOT NULL",
            'eset_payment_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'order_id' => "INT UNSIGNED NOT NULL",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
        ]);

        $this->addForeignKey('fk_eset_payment_link_order_id', '{{%eset_payment_link}}', 'order_id', '{{%order}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_link_eset_payment_id', '{{%eset_payment_link}}', 'eset_payment_id', '{{%eset_payment}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_link_user_id', '{{%eset_payment_link}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_payment_link}}');
        $this->dropTable('{{%eset_payment}}');
        $this->dropTable('{{%eset_payment_type}}');

    }
}
