<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000120_cart_eset_fields extends CustomMigration
{
    public function safeUp()
    {
//        $this->addColumn('{{%cart}}', 'to_user_id', 'INT UNSIGNED DEFAULT NULL');
//        $this->addColumn('{{%cart}}', 'sector', "enum('Business', 'Government', 'Education') DEFAULT NULL");
//        $this->addColumn('{{%cart}}', 'discount_code', 'INT DEFAULT NULL');
//        $this->addColumn('{{%cart}}', 'custom_discount', 'DECIMAL(5,2) DEFAULT NULL');
//        $this->addColumn('{{%cart}}', 'discount_document', 'VARCHAR(50) DEFAULT NULL');
    }

    public function safeDown()
    {
//        $this->dropColumn('{{%cart}}', 'to_user_id');
//        $this->dropColumn('{{%cart}}', 'sector');
//        $this->dropColumn('{{%cart}}', 'discount_code');
//        $this->dropColumn('{{%cart}}', 'custom_discount');
//        $this->dropColumn('{{%cart}}', 'discount_document');
    }
}
