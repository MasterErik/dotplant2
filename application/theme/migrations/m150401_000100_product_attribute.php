<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000100_product_attribute extends CustomMigration
{
    public function up()
    {
        // связующая таблица
        $this->createTable('{{%eset_product_attribute}}', [
            'id' => Schema::TYPE_PK,
            //'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'product_id' => 'INT UNSIGNED NOT NULL',
            'attribute' => Schema::TYPE_STRING . ' NOT NULL',
            'value' => Schema::TYPE_TEXT . ' NOT NULL',
            'show' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT '1'", // пусть остаётся
            // мини-история
            // время
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
            // пользователь
            'creator_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
            'updater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",

        ]);

        $this->createIndex('idx_eset_product_attribute_product_attribute', '{{%eset_product_attribute}}', ['product_id', 'attribute'], true);
        $this->addForeignKey('fk_eset_product_attribute__product_id', '{{%eset_product_attribute}}', 'product_id', '{{%product}}', 'id', 'cascade', 'cascade');

    }

    public function down()
    {
        $this->dropTable('{{%eset_product_attribute}}');
    }
}
