<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m151015_164300_eset_invoice_fix extends CustomMigration
{
    public function safeUp()
    {
        $this->alterColumn(\app\theme\models\Invoice::tableName(), 'creator_id', Schema::TYPE_INTEGER . ' NULL DEFAULT NULL');
    }

    public function safeDown()
    {
    }
}
