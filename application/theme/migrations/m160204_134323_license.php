<?php

use yii\db\Migration;

class m160204_134323_license extends Migration
{
    public function up()
    {
        $this->createIndex('idx_eset_license_order_detail_id_unique', '{{%eset_license}}', ['order_detail_id'], true);
        echo "createIndex idx_eset_license_order_detail_id.\n";
    }

    public function down()
    {
//        $this->dropIndex('idx_eset_license_order_detail_id_unique', '{{%eset_license}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
