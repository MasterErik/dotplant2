<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\OrderStage;
use yii\db\mysql\Schema;

class m160121_163059_event_handlers extends CustomMigration
{

    // Use safeUp/safeDown to run migration code within a transaction

    public function safeUp()
    {
        $this->createTable('{{%eset_event_handlers}}', [
            'id' => Schema::TYPE_PK,
            'class_name' => Schema::TYPE_STRING . ' NOT NULL',
            'function_name' => Schema::TYPE_STRING,
            'function_stage_name' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at_ts' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->createIndex('i_eset_event_handlers__function_stage_name', '{{%eset_event_handlers}}', ['function_stage_name'], true);

        $this->batchInsert(
            '{{%eset_event_handlers}}',
            ['id', 'class_name', 'function_name', 'function_stage_name'],
            [
                [1, 'app\theme\module\helpers\EsetOrderStageHandlers', 'handleCustomer', 'handleStageCustomer'],
                [2, 'app\theme\module\helpers\EsetOrderStageHandlers', 'handleRegistration', 'handleStageRegistration'],
                [3, 'app\theme\module\helpers\EsetOrderStageHandlers', 'handlePayment', 'handleStagePayment'],
                [4, 'app\theme\module\helpers\EsetOrderStageHandlers', 'handlePaymentPay', 'handleStagePaymentPay'],
                [5, 'app\theme\module\helpers\EsetOrderStageHandlers', '', 'handleStageLicense'],
                [6, 'app\theme\module\helpers\EsetOrderStageHandlers', 'handleFinal', 'handleStageFinal'],
            ]
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_event_handlers}}');

    }

}
