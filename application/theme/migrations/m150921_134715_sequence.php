<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150921_134715_sequence extends CustomMigration
{
    public function up()
    {
        $this->createTable('{{%sequences}}', [
            'name' => ' CHAR(20) PRIMARY KEY',
            'value' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL'
        ]);

        $this->execute("CREATE FUNCTION nextval(seqname CHAR(20))
            RETURNS int(11) UNSIGNED
            MODIFIES SQL DATA
            BEGIN
                INSERT INTO sequences VALUES (seqname, LAST_INSERT_ID(1))
                    ON DUPLICATE KEY UPDATE value = LAST_INSERT_ID(value+1);
                RETURN LAST_INSERT_ID();
            END");

    }

    public function down()
    {
        $this->execute("DROP FUNCTION nextval");
        $this->dropTable('{{%sequences}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
