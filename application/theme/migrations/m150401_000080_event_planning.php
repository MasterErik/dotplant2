<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m150401_000080_event_planning extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_event_planning}}', [
            'id' => Schema::TYPE_PK,
            'table_id' => "INT UNSIGNED NOT NULL",
            'name' => Schema::TYPE_STRING . "(20) NOT NULL",
            'bdate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'edate' => Schema::TYPE_TIMESTAMP . " NULL DEFAULT NULL",

            'errors' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'last_run_ts' => Schema::TYPE_TIMESTAMP . " NULL DEFAULT NULL",

            // мини-история
            // время
            'created_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            // пользователь
            'creator_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
            'updater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '1'",
        ]);

        // indexes
        $this->createIndex('idx_eset_event_planning_date_range', '{{%eset_event_planning}}', ['bdate', 'edate']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_event_planning}}');
    }
}
