<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\InvoiceMode;
use app\theme\models\UserProfile;
use app\theme\models\Position;

use app\theme\models\YesNo;
use yii\db\Schema;
class m150401_000020_user_profile extends CustomMigration
{
    public function up()
    {

        $time = time();
        $this->update('{{%user}}', ['first_name' => 'Allan'], ['id' => 1]);

        if(!$this->db->createCommand('SELECT id FROM user WHERE id = 2')->queryScalar()) {
            $this->insert('{{%user}}', [
                'id' => 2,
                'username' => 'esetlv',
                'email' => 'info@eset.lv',
                'first_name' => 'Sven',
                'password_hash' => \Yii::$app->getSecurity()->generatePasswordHash('esetlv'),
                'auth_key' => \Yii::$app->getSecurity()->generateRandomString(),
                'create_time' => $time,
                'update_time' => $time,
            ]);
        }

        $this->createTable('{{%eset_user_profile}}', [
            'user_id' => 'INT UNSIGNED NOT NULL PRIMARY KEY',
            'country_id' => Schema::TYPE_INTEGER . " NULL",
            'locale' => Schema::TYPE_STRING . "(32) NOT NULL DEFAULT 'en'",
            'address' => Schema::TYPE_STRING . "(1024) NULL",
            'comment' => Schema::TYPE_STRING . "(2048) NULL",
            'page' => Schema::TYPE_STRING . "(255) NULL COMMENT 'start page'",
            'contact' => Schema::TYPE_STRING . "(255) NULL",
            'url' => Schema::TYPE_STRING . "(255) NULL",
            'telefon' => Schema::TYPE_STRING . "(30) NULL",
            'oldpass' => Schema::TYPE_STRING . " NULL COMMENT 'password old system'",
            'parent_id' => Schema::TYPE_INTEGER . " NULL DEFAULT '1' COMMENT 'partner id'",
            'position' => "enum('partner','reseller','business','client','employee') NOT NULL",

            'invoice_mode' => "enum('manual','auto','immediately') NOT NULL DEFAULT '" . InvoiceMode::INVOICE_IMMEDIATELY . "'",
            'auto_license' => Schema::TYPE_BOOLEAN . " NOT NULL DEFAULT " . YesNo::NO,

            'gender' => Schema::TYPE_BOOLEAN . " NULL",
            'birthday' => Schema::TYPE_DATE . " NULL",

			'confirm_registration_key' => Schema::TYPE_STRING . "(32) DEFAULT NULL",

            // мини-история
            'created_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
        ]);

        $this->addForeignKey('fk_eset_user_profile_user_id', '{{%eset_user_profile}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_eset_user_profile_country_id', '{{%eset_user_profile}}', 'country_id', '{{%country}}', 'id', 'cascade', 'cascade');
		$this->createIndex('idx_eset_user_profile__confirm_key', '{{%eset_user_profile}}', 'confirm_registration_key');

        // indexes
        $this->createIndex('idx_eset_user_telefon', '{{%eset_user_profile}}', 'telefon', true);
        $this->createIndex('idx_eset_user_parent_id', '{{%eset_user_profile}}', 'parent_id');


        $this->batchInsert(
            '{{%eset_user_profile}}',
            ['user_id', 'country_id', 'locale', 'parent_id', 'telefon', 'invoice_mode', 'auto_license', 'position', 'created_at', 'updated_at'],
            [
                [1, 3, 'en', 1, '3726617952', InvoiceMode::INVOICE_MANUAL, UserProfile::YES, Position::POSITION_PARTNER, $time, $time],
                [2, 4, 'en', 2, '37167276520', InvoiceMode::INVOICE_MANUAL, UserProfile::YES, Position::POSITION_PARTNER, $time, $time],
            ]
        );


    }

    public function down()
    {
        $this->dropTable('{{%eset_user_profile}}');
    }
}
