<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\Country;
use app\modules\core\models\EventHandlers;
class m150622_222222_beta extends CustomMigration
{

    public function safeUp()
    {
        // вешаем свой обработчик на события обработки заказа
          $this->update(EventHandlers::tableName(),
            ['handler_class_name' => 'app\theme\module\helpers\EsetOrderStageHandlers'],
            ['handler_class_name' => 'app\modules\shop\helpers\BaseOrderStageHandlers']
        );
/*
        $sql = 'UPDATE {{%event_handlers}} SET handler_class_name = :to WHERE handler_class_name = :from';
        $this->execute($sql,[
            ':from' => 'app\modules\shop\helpers\BaseOrderStageHandlers',
            ':to' => 'app\theme\module\helpers\EsetOrderStageHandlers'
        ]);
*/
        $this->addColumn(Country::tableName(), 'eset_iso_code2', 'CHAR(2) DEFAULT NULL');
        $this->execute("UPDATE {{%country}} SET eset_iso_code2='RU' WHERE iso_code='RUS'");
        $this->execute("UPDATE {{%country}} SET eset_iso_code2='US' WHERE iso_code='USA'");
        $this->execute("UPDATE {{%country}} SET eset_iso_code2='EE' WHERE iso_code='EST'");
        $this->execute("UPDATE {{%country}} SET eset_iso_code2='LV', iso_code='LVA' WHERE iso_code IN ('LV', 'LVA')");
    }
    
    public function safeDown()
    {
        $sql = 'UPDATE {{%event_handlers}} SET handler_class_name = :to WHERE handler_class_name = :from';
        $this->execute($sql,[
            ':from' => 'app\theme\module\helpers\EsetOrderStageHandlers',
            ':to' => 'app\modules\shop\helpers\BaseOrderStageHandlers'
        ]);

        //$this->execute("ALTER TABLE {{%country}} DROP COLUMN eset_iso_code2;");
    }
}
