<?php

use app\theme\migrations\CustomMigration;
use app\theme\module\events\OrderInitEvent;
use app\modules\core\models\Events;
use app\modules\core\models\EventHandlers;
use app\modules\shop\models\OrderStage;
use app\modules\shop\models\OrderStageLeaf;

class m151013_155500_fix_eset_invoice_create_at extends CustomMigration
{
    public function safeUp()
    {
        $this->alterColumn(
            \app\theme\models\Invoice::tableName(),
            'created_at',
            \yii\db\Schema::TYPE_INTEGER . ' DEFAULT NULL'
        );

        return true;
    }

    public function safeDown()
    {
        return true;
    }
}
