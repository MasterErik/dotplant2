<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000040_transaction extends CustomMigration
{
    public function safeUp()
    {
        $this->createTable('{{%eset_transaction}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . "(30) NOT NULL",
            'created_at' => Schema::TYPE_INTEGER,
        ]);


        $this->createTable('{{%eset_transaction_detail}}', [
            'id' => Schema::TYPE_PK,
            'eset_transaction_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'name' => Schema::TYPE_STRING . "(256) NOT NULL",
            'IBAN' => Schema::TYPE_STRING . "(256) NOT NULL",
            'regkood' => Schema::TYPE_INTEGER . " NULL COMMENT 'Isikukood'",
            'bank' => Schema::TYPE_INTEGER . " NOT NULL",
            'transactiondate' => Schema::TYPE_INTEGER . " NOT NULL",
            'typecode' => Schema::TYPE_STRING . "(2) NULL",
            'BankReference' => Schema::TYPE_STRING . "(30) NOT NULL COMMENT 'MakseID'",
            'DocumentNumber' => Schema::TYPE_STRING . "(30) NULL",
            'ReferenceNumber' => Schema::TYPE_STRING . "(30) NULL COMMENT 'Vitenr'",
            'Amount' => Schema::TYPE_DECIMAL . "(8, 2) NOT NULL COMMENT 'Summa'",
            'Description' => Schema::TYPE_STRING . "(100) NULL",
            'PayDate' => Schema::TYPE_TIMESTAMP . " NULL",
        ]);

        $this->addForeignKey('fk_eset_transaction_detail_transaction_id', '{{%eset_transaction_detail}}', 'eset_transaction_id', '{{%eset_transaction}}', 'id', 'cascade', 'cascade');

    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_transaction_detail}}');
        $this->dropTable('{{%eset_transaction}}');
    }
}
