<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150403_000000_translate_backend_menu extends CustomMigration
{
    public function up()
    {
        $this->insert('{{%backend_menu}}', [
            'parent_id' => 18,
            'name'  => 'I18n Models',
            'route' => '/eset/backend/translate/index',
            'icon'  => 'language',
            'added_by_ext' => 'core',
            'rbac_check' => 'setting manage',
            'translation_category' => 'eset'
        ]);
    }

    public function down()
    {
        $this->delete('{{%backend_menu}}', ['route'=>'/eset/backend/translate/index']);
    }
}
