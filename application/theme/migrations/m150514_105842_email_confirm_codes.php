<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150514_105842_email_confirm_codes extends CustomMigration
{
    public function up()
    {
        $this->createTable('{{%eset_email_confirm_code}}', [
            'email' => Schema::TYPE_STRING . "(255) NOT NULL",
			'code' => Schema::TYPE_STRING . "(32)",
            'ins_ts' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        // indexes
        $this->createIndex('idx_eset_email_confirm__code', '{{%eset_email_confirm_code}}', 'code', true);
        $this->createIndex('idx_eset_email_confirm__email', '{{%eset_email_confirm_code}}', 'email', true);
    }

    public function down()
    {
        $this->dropTable('{{%eset_email_confirm_code}}');
    }
}
