<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150908_000000_payment extends CustomMigration
{
    public function safeUp()
    {
        $this->dropTable('{{%eset_bank_transaction}}');
        $this->dropTable('{{%eset_payment_link}}');
        $this->dropTable('{{%eset_payment}}');
        $this->dropTable('{{%eset_payment_type}}');


        $this->createTable('{{%eset_payment_type}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . "(50) NOT NULL",
            'type' => "enum('bank','e-money') NOT NULL DEFAULT 'bank'",
            'code' => Schema::TYPE_STRING . "(30) NOT NULL",
            'iban' => Schema::TYPE_STRING . "(34) NOT NULL",
            'swift' => Schema::TYPE_STRING . "(30) NOT NULL",

            'email' => Schema::TYPE_STRING . "(255) NOT NULL",
            'payment_type_id' => Schema::TYPE_INTEGER . " UNSIGNED",
            'partner_id' => Schema::TYPE_INTEGER ." UNSIGNED  NOT NULL",

            'bdate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
            'edate' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '2037-01-01 00:00:00'",
            'created_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'updated_at' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'changer_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
            'creater_id' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT '0'",
        ]);
        $this->addForeignKey('fk_eset_payment_type__payment_type_id', '{{%eset_payment_type}}', 'payment_type_id', '{{%payment_type}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_type__partner_id', '{{%eset_payment_type}}', 'partner_id', '{{%user}}', 'id', 'restrict', 'cascade');

        $this->createTable('{{%eset_payment}}', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_SMALLINT."(1) NOT NULL DEFAULT 0",
            'partner_id' => Schema::TYPE_INTEGER ." UNSIGNED",
            'user_id' => Schema::TYPE_INTEGER ." UNSIGNED",
            'eset_payment_type_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'type' => "enum('web','invoice') NOT NULL DEFAULT 'web'",
            'viitenumber' => Schema::TYPE_INTEGER . " NULL",
            'summa' => Schema::TYPE_DECIMAL . "(8,2) NOT NULL",
            'created_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->addForeignKey('fk_eset_payment__payment_type_id', '{{%eset_payment}}', 'eset_payment_type_id', '{{%eset_payment_type}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment__user_id', '{{%eset_payment}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment__partner_id', '{{%eset_payment}}', 'partner_id', '{{%user}}', 'id', 'restrict', 'cascade');


        $this->createTable('{{%eset_payment_link}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . " UNSIGNED", //TODO для чего это поле ?
            'eset_payment_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'invoice_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'created_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->addForeignKey('fk_eset_payment_link__invoice_id', '{{%eset_payment_link}}', 'invoice_id', '{{%eset_invoice}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_link__eset_payment_id', '{{%eset_payment_link}}', 'eset_payment_id', '{{%eset_payment}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_payment_link__user_id', '{{%eset_payment_link}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');


        $this->createTable('{{%eset_bank_transaction}}', [
            'id' => Schema::TYPE_PK,
            'eset_payment_id' => Schema::TYPE_INTEGER . " DEFAULT NULL",
            'eset_payment_type_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'partner_id' => Schema::TYPE_INTEGER ." UNSIGNED NOT NULL",
            'iban' => Schema::TYPE_STRING . "(34)", // Konto
            'payer' => Schema::TYPE_STRING . "(255)", // Maksja
            'summa' => Schema::TYPE_DECIMAL . "(8,2) NOT NULL",
            'viitenumber' => Schema::TYPE_INTEGER . " NULL",
            'description' => Schema::TYPE_STRING . "(255)", // Selgitus
            'created_at' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP",
        ]);
        $this->addForeignKey('fk_eset_bank_transaction__eset_payment_id', '{{%eset_bank_transaction}}', 'eset_payment_id', '{{%eset_payment}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_bank_transaction__eset_payment_type_id', '{{%eset_bank_transaction}}', 'eset_payment_type_id', '{{%eset_payment_type}}', 'id', 'restrict', 'cascade');
        $this->addForeignKey('fk_eset_bank_transaction__partner_id', '{{%eset_bank_transaction}}', 'partner_id', '{{%user}}', 'id', 'restrict', 'cascade');
    }

    public function safeDown()
    {
        $this->dropTable('{{%eset_bank_transaction}}');
        $this->dropTable('{{%eset_payment_link}}');
        $this->dropTable('{{%eset_payment}}');
        $this->dropTable('{{%eset_payment_type}}');
    }
}
