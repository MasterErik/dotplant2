<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m150902_000000_product_attribute_fix extends CustomMigration
{
    public function safeUp()
    {
        $this->alterColumn(\app\theme\models\ProductAttribute::tableName(), 'updater_id', Schema::TYPE_INTEGER . ' NULL DEFAULT NULL');
        $this->alterColumn(\app\theme\models\ProductAttribute::tableName(), 'creator_id', Schema::TYPE_INTEGER . ' NULL DEFAULT NULL');
    }

    public function safeDown()
    {
    }
}
