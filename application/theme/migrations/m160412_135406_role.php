<?php

use app\theme\migrations\CustomMigration;


class m160412_135406_role extends CustomMigration
{
    public function safeUp()
    {
        $this->batchInsert('{{%auth_item}}',
            ['name', 'type', 'description'],
            [
                ['base', 1, 'Base role'],
            ]
        );

    }

    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' => 'base']);
    }
}
