<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;

class m150401_000130_product_eset_fields extends CustomMigration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'eset_product_code', 'INT DEFAULT NULL');
        $this->addColumn('{{%product}}', 'eset_quantity_params', 'VARCHAR(255) DEFAULT NULL');
        $this->addColumn('{{%product}}', 'eset_period_params', 'VARCHAR(255) DEFAULT NULL');

        //TODO это убрать потом
        $this->execute("UPDATE {{%product}} SET eset_product_code=107");
        $this->execute("UPDATE {{%product}} SET eset_quantity_params='1 - 5'");
        $this->execute("UPDATE {{%product}} SET eset_period_params='12 => 1 year, 24 => 2 years, 36 => 3 years'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'eset_product_code');
        $this->dropColumn('{{%product}}', 'eset_quantity_params');
        $this->dropColumn('{{%product}}', 'eset_period_params');
    }
}
