<?php

use app\theme\migrations\CustomMigration;
use yii\db\Schema;


class m151015_215700_eset_event_planning_fix extends CustomMigration
{
    public function safeUp()
    {
        $this->alterColumn(\app\theme\models\EventPlanning::tableName(), 'creator_id', Schema::TYPE_INTEGER . ' NULL DEFAULT NULL');
        $this->alterColumn(\app\theme\models\EventPlanning::tableName(), 'updater_id', Schema::TYPE_INTEGER . ' NULL DEFAULT NULL');
    }

    public function safeDown()
    {
    }
}
