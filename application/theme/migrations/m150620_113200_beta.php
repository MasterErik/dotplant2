<?php

use app\theme\migrations\CustomMigration;
use app\theme\models\PurchaseType;

class m150620_113200_beta extends CustomMigration
{

    public function safeUp()
    {
        // дополнительные поля которые были в корзине перенеосим в Order и OrderItem
        $this->addColumn('{{%order}}', 'eset_sector', "enum('Business', 'Government', 'Education') DEFAULT NULL");
        $this->addColumn('{{%order}}', 'eset_discount_code', 'INT DEFAULT NULL');
        $this->addColumn('{{%order}}', 'eset_custom_discount', 'INT DEFAULT NULL');
        $this->addColumn('{{%order}}', 'eset_discount_document', 'VARCHAR(50) DEFAULT NULL');

        $this->addColumn('{{%order_item}}', 'eset_parent_license_id', 'INTEGER DEFAULT NULL');
        $this->addColumn('{{%order_item}}', 'eset_period', 'INTEGER NOT NULL');
        $this->addColumn('{{%order_item}}', 'eset_purchase_type', "ENUM('" . implode("','", PurchaseType::getKey()) . "') NOT NULL DEFAULT '" . PurchaseType::PURCHASE_TYPE_NEW . "'");
        $this->addColumn('{{%order_item}}', 'eset_note', 'VARCHAR(255) DEFAULT NULL');
        $this->addColumn('{{%order_item}}', 'packet', 'INTEGER NOT NULL DEFAULT 1');

        //fix eset_product_attribute
        $sql = "UPDATE {{%eset_product_attribute}} SET attribute='eset_period', value=REPLACE(value, '\"period\"', '\"eset_period\"') WHERE attribute='period'";
        $this->execute($sql);

        // правим стартовую стадию оформления заказа
        $sql = "UPDATE {{%order_stage}} SET is_initial=IF(name='payment',1,0)";
        $this->execute($sql);

    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'eset_sector');
        $this->dropColumn('{{%order}}', 'eset_discount_code');
        $this->dropColumn('{{%order}}', 'eset_custom_discount');
        $this->dropColumn('{{%order}}', 'eset_discount_document');

        $this->dropColumn('{{%order_item}}', 'eset_parent_license_id');
        $this->dropColumn('{{%order_item}}', 'eset_period');
        $this->dropColumn('{{%order_item}}', 'eset_purchase_type');
        $this->dropColumn('{{%order_item}}', 'eset_note');

        $sql = "UPDATE {{%eset_product_attribute}} SET attribute='period', value=REPLACE(value, '\"eset_period\"', '\"period\"') WHERE attribute='eset_period'";
        $this->execute($sql);

        $sql = "UPDATE {{%order_stage}} SET is_initial=IF(name='customer',1,0)";
        $this->execute($sql);
    }
}
