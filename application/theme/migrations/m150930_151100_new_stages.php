<?php

use app\theme\migrations\CustomMigration;
use app\theme\module\events\OrderInitEvent;
use app\modules\core\models\Events;
use app\modules\core\models\EventHandlers;
use app\modules\shop\models\OrderStage;
use app\modules\shop\models\OrderStageLeaf;

class m150930_151100_new_stages extends CustomMigration
{
    public function safeUp()
    {
        return true;
    }

    public function safeDown()
    {
        return true;
    }
}
