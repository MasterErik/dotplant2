<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 21.07.2015
 * Time: 13:09
 */

namespace app\theme\widgets\catalog;

use app\theme\models\Category;
use yii\base\Widget;

class CatalogWidget extends Widget
{
    public $viewFile = 'catalog';

    public function init()
    {
        parent::init();

        //LoginAsset::register($this->view);
    }


    public function run()
    {
        $model = new Category();

        echo $this->render(
            $this->viewFile,
            [
                'model' => $model,
            ]
        );
    }

}