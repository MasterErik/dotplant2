<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 8.06.2015
 * Time: 15:54
 */
use yii\bootstrap\Nav;
?>

<nav class="navbar navbar-catalog">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#catalog-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="catalog-collapse">
            <?=
            Nav::widget([
                'id' => 'catalogMenu',
                'options' => ['class' => 'nav navbar-nav', ],
                'items' => $model->getMenuItemsLine(), // getMenuItems(1, 2, false),
            ]);
            ?>
        </div>
</nav>


