<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 25.09.2015
 * Time: 10:00
 */

namespace app\theme\widgets\controllers;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\theme\models\search\Select2Search;

class WidgetController extends Controller
{
    /**
     * @param $model
     * @param null String $search
     * @param null integer $page
     * @param string $key
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionGetData($model, $search = null, $page = null, $key = 'id')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException;
        }
        $key = $this->clean($key);

        /** @var ActiveRecord $model */
        $model = str_replace('/', '\\', $model);
        return Select2Search::getData($model::className(), $search, $page, $key);
    }

    protected function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces.

        return preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
    }
}