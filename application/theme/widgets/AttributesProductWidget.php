<?php

namespace app\theme\widgets;

use app\theme\models\ProductAttributesForm;
use yii\base\Widget;
use yii\widgets\ActiveField;

class AttributesProductWidget extends Widget
{
    public $viewFile = 'attributes-product';
    public $item;
    public $mode;
    public $order_attributes_form_id;
    public $form;

    public function run()
    {
        $this->item->initAttributes();
        if ($this->mode == 'form') {
            $this->item->validate();
        }

        echo $this->render(
            $this->viewFile . '-' . $this->mode,
            [
                'model' => $this->item,
                'form' => $this->form,
                'order_attributes_form_id' => $this->order_attributes_form_id
            ]
        );
    }

    /**
     * @param $field ActiveField
     * @param $options
     * @return mixed
     */
    public function setupField($field, $options)
    {
        if (isset($options['type'])) {
            switch ($options['type']) {
                case 'select':
                    $field->dropDownList(isset($options['data']) ? $options['data'] : []);
                    break;
                default:
                    break;
            }
        }

        if (isset($options['hint'])) {
            $field->hint($options['hint']);
        }

        return $field;
    }
}
