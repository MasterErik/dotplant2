<?php

use app\modules\core\helpers\ContentBlockHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
/** @var app\components\WebView $this */
/** @var bool $useFontAwesome */
/** @var \app\extensions\DefaultTheme\Module $theme */
/** @var integer $rootNavigationId */
?>

<footer class="footer">
    <div class="container">
        <div class="row">
        </div>
    </div>
</footer>
<footer class="post-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <a href="/">
                    <img src="/theme/images/eset_logo_image_only.svg" alt="ESET LLC" height="50">
                </a>
            </div>
            <div class="col-md-6">
                <?= ContentBlockHelper::fetchChunkByKey('Info_block') ?>
            </div>
            <div class="col-md-3">
                <a href="http://www.facebook.com/eset/" target="_blank">
                    <img src="/theme/images/facebook_circle_gray.svg" alt="ESET LLC" height="50">
                </a>
                <a href="https://twitter.com/eset/" target="_blank">
                    <img src="/theme/images/twitter_circle_gray.svg" alt="ESET LLC" height="50">
                </a>
                <a href="http://www.youtube.com/esetusa/" target="_blank">
                    <img src="/theme/images/youtube_circle_gray.svg" alt="ESET LLC" height="50">
                </a>
                <a href="http://www.linkedin.com/company/esetnorthamerica/" target="_blank">
                    <img src="/theme/images/linkedin_circle_gray.svg" alt="ESET LLC" height="50">
                </a>
            </div>
        </div>
    </div>
</footer>
