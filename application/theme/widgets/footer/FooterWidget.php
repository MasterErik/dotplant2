<?php

namespace app\theme\widgets\Footer;

use Yii;
use yii\base\Widget;

class FooterWidget extends Widget
{
    public $rootNavigationId = 1;
    /**
     * Actual run function for all widget classes extending BaseWidget
     *
     * @return mixed
     */
    public function run()
    {
        return $this->render(
            'footer',
            [
                'rootNavigationId' => $this->rootNavigationId,
            ]
        );
    }
}