<?php

namespace app\theme\widgets\header;

use Yii;
use app\extensions\DefaultTheme\assets\BootstrapHoverDropdown;
use app\theme\models\Order;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class HeaderWidget extends Widget
{
    public $collapseOnSmallScreen = true;
    /**
     * Actual run function for all widget classes extending BaseWidget
     *
     * @return mixed
     */
    public function run()
    {
        // this header needs this plugin
        BootstrapHoverDropdown::register($this->view);

        $order = Order::getOrder(false);

        return $this->render(
            'header',
            [
                'order' => $order,
                'collapseOnSmallScreen' => $this->collapseOnSmallScreen,
            ]
        );
    }

    public function getCacheTags()
    {
        $tags = ArrayHelper::merge(parent::getCacheTags(), [
            'Session:'.Yii::$app->session->id,
        ]);
        return $tags;
    }
}