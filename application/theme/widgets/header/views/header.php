<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
/** @var yii\web\View $this */
/**
 * @var \app\modules\shop\models\Order $order
 */
/** @var bool $collapseOnSmallScreen */
/** @var bool $useFontAwesome */
/** @var \app\extensions\DefaultTheme\Module $theme */

$mainCurrency = \app\modules\shop\models\Currency::getMainCurrency();
if (is_null($order) || !Yii::$app->session->has('orderId')) {
    $itemsCount = 0;
} else {
    $itemsCount = $order->items_count;
}

$navStyles = '';


$useFontAwesome = false;
?>

<header class="header one-row-header-with-cart">
    <div class="container">

        <nav class="navbar-main navbar" role="navigation">
                <div class="navbar-header">
                    <a id="logo" class="navbar-brand" href="<?=\yii\helpers\Url::to('/')?>">
                        <?php if (Yii::$app->user->isGuest): ?>
                            <img src="<?=Yii::$app->urlManager->baseUrl?>/theme/images/eset_logo_image_only.svg" alt="ESET LLC" class="img-responsive"/>
                        <?php elseif(\app\theme\models\UserProfile::iPartner()): ?>
                            <img src="<?=Yii::$app->urlManager->baseUrl?>/theme/images/eset_logo_image_only.svg" alt="ESET LLC" class="img-responsive"/>
                        <?php else: ?>
                            <img src="<?=Yii::$app->urlManager->baseUrl?>/theme/images/eset_logo_image_only.svg" alt="ESET LLC" class="img-responsive"/>
                        <?php endif; ?>
                    </a>

                    <button id="smallScreen" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="header-navbar-collapse">
                    <?php
                    $appendItems = [];
                    /*
                    if( YII_DEBUG && !Yii::$app->user->isGuest && Yii::$app->user->identity->username == 'admin' ) {
                        $appendItems[] = [
                            'label' => 'debugAPI',
                            'url' => '/eset/default/rest-debug',
                        ];
                    }*/
                    if (Yii::$app->user->isGuest || \app\theme\models\UserProfile::iReseller()) {
                        $appendItems[] = [
                            'label' => Yii::t('app', 'Sign up'),
                            'url' => '/signup',
                        ];
                    }
                    $appendItems[] = [
                        'label' => Yii::$app->user->isGuest
                            ? Yii::t('app', 'Login')
                            : Yii::t('app', 'Logout'),
                        'url' => Yii::$app->user->isGuest ? ['/login'] : ['/logout'],
                    ];

                    echo \app\widgets\navigation\NavigationWidget::widget([
                        'appendItems' => $appendItems,
                        'options' => [
                            'class' => 'nav navbar-nav',
                        ],
                    ])
                    ?>

                    <div class="navbar-text navbar-right cart-area">

                        <a href="<?= \yii\helpers\Url::toRoute(['/eset/cart']) ?>" class="btn-show-cart">
                            <i class="fa fa-shopping-cart cart-icon"></i>
                            <span class="badge items-count"><?= $itemsCount ?></span>
                        </a>

                        <?php if (is_array(Yii::$app->session->get('comparisonProductList')) && count(Yii::$app->session->get('comparisonProductList')) > 0): ?>
                            <a href="<?=Url::to(['/shop/product-compare/compare'])?>" class="btn-compare" title="<?=Yii::t('app', 'Compare products')?>">
                                <?= Icon::show('tags') ?> <?=count(Yii::$app->session->get('comparisonProductList'))?>
                            </a>
                        <?php endif; ?>
                    </div>

                    <?php if (Yii::$app->user->isGuest === true): ?>
                        <?php ?>

                        <?php else: ?>

                            <div class="navbar-text navbar-right">

                            <?= Yii::t('app', 'Hi') ?>,

                            <span class="dropdown">
                                <a href="<?= \yii\helpers\Url::toRoute(['/shop/cabinet']) ?>" class="link-cabinet" data-toggle="dropdown" data-hover="dropdown"><?= Html::encode(Yii::$app->user->identity->username) ?></a>!
                                <?php echo \yii\widgets\Menu::widget([
                                    'items' => array_merge(
                                        Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'base') ?
                                        [
                                            [
                                                'label' => Yii::t('app', 'Personal cabinet'),
                                                'url' => ['/shop/cabinet'],
                                                'options' => [
                                                    'class' => 'shop-cabinet-link',
                                                ]
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Your profile'),
                                                'url' => ['/eset/user/cabinet/profile'],
                                                'options' => [
                                                    'class' => 'user-profile-link',
                                                ]
                                            ],
                                            [
                                                'options' => [
                                                    'role' => 'separator',
                                                    'class' => 'divider',
                                                ],
                                            ],
                                            [
                                                'label' => Yii::t('app', 'License list'),
                                                'url' => ['/eset/license'],
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Orders list'),
                                                'url' => ['/shop/orders/list'],
                                                'options' => [
                                                    'class' => 'shop-orders-list',
                                                ]
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Invoice list'),
                                                'url' => ['/eset/invoice'],
                                            ],
                                        ] : [],
                                        Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'base') && (\app\theme\models\UserProfile::iReseller() || \app\theme\models\UserProfile::iPartner()) ?
                                        [
                                            [
                                                'label' => Yii::t('app', 'The administration section'),
                                                'options' => [
                                                    'class' => 'dropdown-header',
                                                ],
                                            ],
                                            [
                                                'label' => Yii::t('app', 'My Users'),
                                                'url' => ['/eset/user/cabinet/index'],
                                            ],
                                            [
                                                'label' => Yii::t('app', 'Reseller statuses'),
                                                'url' => ['/eset/reseller-status'],
                                            ],
                                            [
                                                'options' => [
                                                    'role' => 'separator',
                                                    'class' => 'divider',
                                                ],
                                            ],
                                        ] : [],
                                        [
                                            [
                                                'label' => Yii::t('app', 'Logout'),
                                                'url' => ['/user/user/logout'],
                                                'options' => [
                                                    'data-action' => 'post',
                                                    'class' => 'logout-link',
                                                ],
                                            ]
                                        ]
                                    ),
                                    'options' => [
                                        'class' => 'dropdown-menu personal-menu',
                                    ],
                                ]) ?>
                            </span>

                            </div>

                        <?php endif; ?>


                    <div class="search-area navbar-right">
                    <?= \app\theme\widgets\header\ExpandableSearchField::widget([
                        //'useFontAwesome' => $useFontAwesome,
                    ]) ?>
                    </div>
                </div>
        </nav>
    </div>
</header>

<div style="border-bottom: 2px solid #f0f0f0; margin-bottom: 16px; background-color: #f7f7f7">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <?= \app\theme\widgets\catalog\CatalogWidget::widget() ?>
            </div>
            <div class="col-md-2">
                <div class="pull-right">
                    <?= \app\theme\widgets\LanguageSwitcherWidget::widget() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (Yii::$app->user->isGuest === false) {
    $js = <<<JS
$('.link-cabinet').dropdownHover();
JS;
    $this->registerJs($js);

}