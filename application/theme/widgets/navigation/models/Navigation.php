<?php

namespace app\theme\widgets\navigation\models;

/**
 * @inheritdoc
 */
class Navigation extends \app\widgets\navigation\models\Navigation
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => \app\theme\components\behaviors\TranslateModelBehavior::className(),
                'languages' => \app\theme\components\MultiLangHelper::getLanguages(),
                'attributes' => ['name']
            ]]
        );
    }
}
