<?php

namespace app\theme\widgets\navigation;

use app\theme\widgets\navigation\models\Navigation;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


class NavigationWidget extends \app\widgets\navigation\NavigationWidget
{
    public function run()
    {
        $items = null;
        $cacheKey = implode(
            ':',
            [
                'Navigation',
                $this->rootId,
                $this->viewFile
            ]
        );
        if ($this->useCache) {
            if (false === $items = \Yii::$app->cache->get($cacheKey)) {
                $items = null;
            }
        }
        if (null === $items) {
            $root = Navigation::findOne($this->rootId);
            $items = [];
            foreach ($root->children as $child) {
                $items[] = self::getTree($child);
            }
            if (count($items) > 0) {
                \Yii::$app->cache->set(
                    $cacheKey,
                    $items,
                    86400,
                    new TagDependency([
                        'tags' => [
                            \devgroup\TagDependencyHelper\ActiveRecordHelper::getCommonTag(Navigation::className())
                        ]
                    ])
                );
            }
        }

        return $this->render(
            $this->viewFile,
            [
                'widget' => $this->widget,
                'items' => ArrayHelper::merge((array)$this->prependItems, $items, (array)$this->appendItems),
                'options' => $this->options,
            ]
        );
    }

    /**
     * @param Navigation $model
     * @return array
     */
    private static function getTree($model)
    {
        if (trim($model->url)) {
            $url = [trim($model->url)];
        } else {
            $params = (trim($model->route_params)) ? Json::decode($model->route_params) : [];
            $url = ArrayHelper::merge([$model->route], $params);
        }
        $tree = [
            'label' => $model->name,
            'url' => $url,
            'options' => ['class' => $model->advanced_css_class],
            'items' => [],
        ];
        foreach ($model->children as $child) {
            $tree['items'][] = self::getTree($child);
        }
        return $tree;
    }

}
