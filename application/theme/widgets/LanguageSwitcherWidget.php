<?php

namespace app\theme\widgets;

use app\theme\components\MultiLangHelper;
use yii\base\Widget;

class LanguageSwitcherWidget extends Widget
{
    public $viewFile = 'language-switcher';

    public function run()
    {
        echo $this->render(
            $this->viewFile,
            [
                'languages' => MultiLangHelper::getLanguages(),
                'currentLanguage' => MultiLangHelper::getCurrentLanguage(),
            ]
        );
    }
}
