<?php

namespace app\theme\widgets\login;


use app\modules\user\models\LoginForm;
use yii\base\Widget;

class LoginWidget extends Widget
{
    public $viewFile = 'login';
    public $model;

    public function init()
    {
        parent::init();

        LoginAsset::register($this->view);
    }


    public function run()
    {
        if( !$this->model ) {
            $this->model = new LoginForm();
        }

        echo $this->render(
            $this->viewFile,
            [
                'model' => $this->model,
            ]
        );
    }
}
