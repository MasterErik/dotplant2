<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.03.15
 * Time: 17:09
 */

namespace app\theme\widgets\login;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
    public $sourcePath = __DIR__;
    public $css = [
        'css/login.css',
    ];
}
?>