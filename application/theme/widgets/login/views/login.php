<?php

/**
 * @var $model \app\models\LoginForm
 * @var $this \yii\web\View
 */

use kartik\widgets\ActiveForm;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;

?>

<div id="loginWidget">

    <div class="panel panel-default">
        <div class="panel-body">

            <h3><?= Html::encode(Yii::t('eset', 'User login')) ?></h3>

            <?php
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'type' => ActiveForm::TYPE_VERTICAL,
                'action' => \yii\helpers\Url::to(['/login'])
            ]);
            ?>

            <?= $form->field($model, 'username', [
                'template' => "<div class=\"input-group\"><span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span></span>{input}</div>\n{hint}\n{error}",
                'inputOptions' => [
                    'placeholder' => $model->getAttributeLabel('username'),
                    'class' => 'form-control'
                ]
            ]) ?>

            <?= $form->field($model, 'password', [
                'template' => "<div class=\"input-group\"><span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span></span>{input}</div>\n{hint}\n{error}",
                'inputOptions' => [
                    'placeholder' => $model->getAttributeLabel('password'),
                    'class' => 'form-control'
                ]
            ])->passwordInput() ?>

            <div class="text-reset-pass" style="margin-bottom: ">
                <?= Yii::t('app', 'If you forgot your password you can') ?>
                <?= Html::a(Yii::t('app', 'reset it'), ['/user/user/request-password-reset']) ?>.
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'rememberMe', ['options'=>['class'=>'pull-left']])->checkbox() ?>
                    <div class="pull-right">
                        <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

            <?= AuthChoice::widget([
                'baseAuthUrl' => ['/user/user/auth']
            ]) ?>
        </div>
    </div>
</div>