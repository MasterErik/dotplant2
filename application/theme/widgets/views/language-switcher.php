<?php
use app\theme\components\MultiLangHelper;
use yii\helpers\Html;

?>

<div id="language-switcher-widget">
    <?php
    foreach ($languages as $lang => $lang_name) {
        echo ' ';
        if ($lang == $currentLanguage) {
            echo Html::tag('span', $lang, ['title' => $lang_name, 'class' => 'label label-default']);
        } else {
            echo Html::a($lang, MultiLangHelper::addLangInUrl(\Yii::$app->request->getUrl(), $lang), ['title' => $lang_name]);
        }
    }
    ?>
</div>

