<?php

use app\theme\models\DiscountCodeList;
use app\theme\models\UserProfile;
use app\theme\module\helpers\PeriodHelper;
use yii\widgets\DetailView;

/** @var $model app\theme\models\License */
?>

<div class="license-view">

    <?= DetailView::widget([
        'model' => $model,
        'template' => '<tr><th class="col-xs-4">{label}</th><td>{value}</td></tr>',
        'attributes' => [
            [
                'attribute' => 'user_id',
                'value' => $model->client->client_name,
                'label' => Yii::t('eset', 'License registered to user'),
            ],
            [
                'attribute' => 'user_id',
                'value' => $model->client->email,
                'label' => Yii::t('eset', 'Customer email'),
            ],
            [
                'attribute' => 'reseller_id',
                'value' => $model->reseller ? $model->reseller->client_name : null,
                'visible' => UserProfile::iPartner() || UserProfile::iReseller(),
                'label' => Yii::t('eset', 'Reseller name'),
            ],
            [
                'attribute' => 'reseller_id',
                'value' => $model->reseller ? $model->reseller->email : null,
                'visible' => UserProfile::iPartner() || UserProfile::iReseller(),
                'label' => Yii::t('eset', 'Reseller email'),
            ],
            [
                'attribute' => 'reseller_id',
                'value' => $model->reseller ? $model->reseller->resellerStatus : null,
                'visible' => UserProfile::iPartner() || UserProfile::iReseller(),
                'label' => Yii::t('eset', 'Reseller status'),
            ],
            [
                'attribute' => 'product_code',
                'value' => $model->productName,
            ],
            'purchase_type',
            [
                'attribute' => 'period',
                'value' => PeriodHelper::format($model->period),
            ],
            'quantity',
            [
                'attribute' => 'bdate',
                'value' => $model->bdate,
                'label' => Yii::t('eset', 'Start date of the license'),
            ],
            [
                'attribute' => 'edate',
                'value' => $model->edate,
                'label' => Yii::t('eset', 'Expiration date'),
            ],
            [
                'attribute' => 'discountType',
            ],
            'custom_discount',

            'register_state',
            'state',

            'username',
            'password',
            'license_key',
            'public_license_key',
            [
                'attribute' => 'epli',
                'visible' => \app\theme\models\UserProfile::iPartner(),
            ],
            [
                'attribute' => 'common_tag',
                'visible' => \app\theme\models\UserProfile::iPartner(),
            ],

            //'note',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>
</div>
