<?php
use app\theme\models\PurchaseType;
use yii\helpers\Html;
use yii\web\JsExpression;

?>

<?php
/**
 * @var $model \app\theme\models\OrderItem
 * @var $order_attributes_form_id integer
 */

//$form = ActiveForm::begin([
//    'type' => ActiveForm::TYPE_VERTICAL,
//    //'enableAjaxValidation' => true,
//    'enableClientValidation' => true,
//    'options' => [
//        'class' => 'product-attributes-form',
//    ],
//]);

//echo $form->errorSummary($model);

//echo Html::activeHiddenInput($model, 'id');

echo $form->field($model, '['.$model->id.']product_id')->widget(\app\theme\widgets\Select2::className(), [
    'id' => Html::getInputId($model, '['.$model->id.']product_id'),
    'initValueText' => $model->product ? $model->product->name : '',
    'pluginOptions' => ['allowClear' => false],
    'selectModel' => \app\theme\models\Product::className(),
    'options' => [
        'class' => 'relation-price',
        'data-item-id' => $model->id,
    ],
]);

echo $form->field($model, '['.$model->id.']eset_purchase_type')->dropDownList($model->getPurchaseTypeList(), [
    'class' => 'relation-price field-eset_purchase_type',
    'data-item-id' => $model->id,
]);

echo $form->field($model, '['.$model->id.']eset_parent_license_id', [
    'options' => [
        'style' => $model->isShowParentLicense() ? '' : 'display: none;',
        'class' => 'form-group relation-price',
        'data-item-id' => $model->id,
    ]
])->widget(\app\theme\widgets\Select2::classname(), [
    'initValueText' => $model->parentLicense ? $model->parentLicense->toString() : '',
    'pluginOptions' => [
        'allowClear' => false,
        'ajax' => [
            'url' => '/eset/cart/get-parent-license',
            'data' => new JsExpression('function(params) { return "search=" + (params.term ? params.term : "") + "&item_id=' . $model->id . '" + "&" + $("#shop-stage").serialize(); }'),
            'method' => 'post',
        ]
    ],
]);

/*
echo $form->field($model, '['.$model->id.']packet', [
    'options' => [
        'class' => 'form-group relation-price',
        'data-item-id' => $model->id,
    ]
]);
*/
//Show additional attributes
foreach (['quantity', 'eset_period'] as $attribute) {
    $field = $form->field($model, '['.$model->id.']'.$attribute, ['options' => [
        'class' => 'form-group relation-price',
        'data-item-id' => $model->id,
    ]]);
    $this->context->setupField($field, $model->getAttributeParams($attribute));
    echo $field;
}
echo $form->field($model, '['.$model->id.']eset_note')->textarea(['class' => 'relation-price', 'data-item-id' => $model->id]);

//ActiveForm::end();

$this->registerJs("jQuery('#orderitem-" . $model->id . "-eset_purchase_type').change(function(){
    //var form = jQuery(this).parents('form:first');
    switch(jQuery(this).val()) {
        case '" . PurchaseType::PURCHASE_TYPE_NEW . "':
        case '" . PurchaseType::PURCHASE_TYPE_NFR . "':
            jQuery('.field-orderitem-" . $model->id . "-eset_parent_license_id').hide();
            break;
        case '" . PurchaseType::PURCHASE_TYPE_RENEW . "':
        case '" . PurchaseType::PURCHASE_TYPE_UPGRADE . "':
        case '" . PurchaseType::PURCHASE_TYPE_ENLARGE . "':
        case '" . PurchaseType::PURCHASE_TYPE_DOWNGRADE . "':
            jQuery('.field-orderitem-" . $model->id . "-eset_parent_license_id').show();
            break;
        default:
            break;
    }
});
");