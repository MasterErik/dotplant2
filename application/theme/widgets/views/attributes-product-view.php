<?php
/**
 * @var $model \app\theme\models\OrderItem
 */
?>

<?php

//$period_translate = Yii::t('eset',
//    '{years, plural, =0{} =1{one year} one{# year} few{# years} many{# years} other{# years}} {months, plural, =0{} =1{one month} one{# month} few{# months} many{# months} other{# months}}',
//    ['years' => $model->years, 'months' => $model->months]);

echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'eset_purchase_type',
        [
            'attribute' => 'eset_parent_license_id',
            'value' => $model->parentLicense ? $model->parentLicense->toString() : null,
            'visible' => $model->isShowParentLicense()
        ],
        'quantity',
        [
            'attribute' => 'eset_period',
            'value' => \app\theme\module\helpers\PeriodHelper::format($model->eset_period),
        ],
        'eset_note',
    ],
    'options' => [
        'class' => 'table table-hover table-bordered detail-view'
    ],
]);


