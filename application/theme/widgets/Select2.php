<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 24.09.2015
 * Time: 17:11
 */

namespace app\theme\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use app\theme\models\search\Select2Search;



class Select2 extends \kartik\widgets\Select2
{
    public $key = 'id';
    public $ajax_data;
    public $selectModel;

    /**
     * initialization widget Select2
     */
    public function init()
    {
        $class_name = str_replace('\\', '/', $this->selectModel);
        $sendJs = <<< JS
function (params) {
    return {model: '{$class_name}', key: '{$this->key}', search: params.term, page: params.page };
}
JS;

        $pageSize = Select2Search::PAGE_SIZE;
        $resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * {$pageSize}) < data.total_count
        }
    };
}
JS;

        $pluginOptions = [
            'minimumInputLength' => 0,
            'placeholder' => ['id' => ''],
            'ajax' => [
                'url' => Url::to('/eset/widget/get-data'),
                'delay' => 250,
                'method' => 'get',
                'cache' => true,
                'dataType' => 'json',
                'data' => new JsExpression($sendJs),
                'processResults' => new JsExpression($resultsJs),
            ],
//            'templateResult' => new JsExpression('function(item) {
//                /* FIX */
//                alert(item.placeholder);
//                if (item.placeholder) return item.placeholder;
//                return item.text;
//            }'),
//            'templateSelection' => new JsExpression('function (item) {
//                /* FIX */
//                if (item.placeholder) return item.placeholder;
//                return item.text;
//            }')
        ];

        $this->pluginOptions = ArrayHelper::merge($pluginOptions, $this->pluginOptions);
        //$this->pluginEvents = ["change" => "function() { console.log('change' + $(this).val()); }"];

        $this->options['id'] = $this->getId();

        parent::init();
    }
}