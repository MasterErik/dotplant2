<?php

namespace app\theme\widgets;

use yii\base\Widget;

class LicenseWidget extends Widget
{
    public $model;

    public function run()
    {
        echo $this->render(
            'license',
            [
                'model' => $this->model,
            ]
        );
    }
}
