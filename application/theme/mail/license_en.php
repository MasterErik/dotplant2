<?php

/**
 * @var $this \yii\web\View view component instance
 * @var $model \app\theme\models\License
 * @var $site_url string
 */
?>


<table style="-webkit-font-smoothing: antialiased; border: 0; font-family: 'Segoe UI',Helvetica,Arial,sans-serif;" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td align="center" valign="top"><!--[if mso]>
            <table cellspacing="0" cellpadding="0" border="0" align="center" width="600">
            <![endif]--> <!--[if !mso]><!-- -->
            <table style="max-width: 600px;" border="0" cellspacing="0" cellpadding="0" align="center"><!--<![endif]-->
                <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" align="left" width="500">
                                    <div class="heading" style="font-size: 20px;"><strong>Tere!</strong></div>
                                    <br />
                                    <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;">Täname, et valisid <strong><span style="color: #008ba0;"><?= $model->productName ?></span></strong>&nbsp;oma Interneti-kasutust kaitsma. Me anname endast kõik, et vastata Sinu ootustele ja et neid koguni ületada.</div>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" align="left" width="500">
                                    <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;"><strong>Sa oled vaid paari sammu kaugusel kaitse nautimisest.</strong></div>
                                    <br />
                                    <table style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="border-right: 1px solid #d0d2d5; color: #a1a6ab; font-weight: 600; font-size: 13px; padding-right: 15px;" align="right" width="25" height="35">1.</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="455">
                                                <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;">Desinstalleeri oma arvutist muu viirusetõrjetarkvara.</div>
                                                <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;"><a style="color: #005599; text-decoration: underline; font-weight: 600;" href="http://support.eset.com/en_EN/kb146/">Uuri välja, kuidas</a>.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                            <td height="20">&nbsp;</td>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #d0d2d5; color: #a1a6ab; font-weight: 600; font-size: 13px; padding-right: 15px;" align="right" width="25" height="35">2.</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="455">
                                                <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;"><a style="color: #005599; text-decoration: underline; font-weight: 600;" href="http://www.eset.com/int/download/home/detail/family/2/">Laadi alla</a> oma ESETi tarkvara, kui Sa pole seda veel teinud.</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20">&nbsp;</td>
                                            <td height="20">&nbsp;</td>
                                            <td height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="border-right: 1px solid #d0d2d5; color: #a1a6ab; font-weight: 600; font-size: 13px; padding-right: 15px;" align="right" width="25" height="35">3.</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="455">
                                                <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;">Jätka lihtsa installeerimisprotsessiga oma litsentsiandmeid (vt altpoolt) kasutades. Leiad vajadusel siit <a style="color: #005599; text-decoration: underline; font-weight: 600;" href="http://support.eset.com/en_EN/kb3418/">detailsed juhised</a>.</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#e4ecf0">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56; font-size: 7px;" align="left" width="500">
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; font-weight: 600; color: #5a656d;">Siin on Sinu litsentsi detailid:</div>
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; margin-top: 15px;"><span style="font-size: 12px; color: #939ca3;">Litsentsi võti:</span><br /> <span style="font-weight: bold;"><?= $model->license_key ?></span></div>
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; margin-top: 15px;"><span style="font-size: 12px; color: #939ca3;">Kasutajanimi:</span><br /> <span style="font-weight: bold;"><?= $model->username ?></span></div>
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; margin-top: 15px;"><span style="font-size: 12px; color: #939ca3;">Parool:</span><br /> <span style="font-weight: bold;"><?= $model->password ?></span></div>
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; margin-top: 10px;"><span style="font-size: 12px; color: #939ca3;">Aegumiskuupäev:</span><br /> <span style="font-weight: bold;"><?= $model->edate ?></span></div>
                                    <div style="font-size: 15px; line-height: 133%; mso-line-height-rule: exactly; margin-top: 10px;"><span style="font-size: 12px; color: #939ca3;">Kogus:</span><br /> <span style="font-weight: bold;"><?= $model->quantity ?></span></div>
                                    <div style="font-size: 14px; line-height: 147%; mso-line-height-rule: exactly; color: #5a656d; margin-top: 20px;"><em>Hoia litsentsiinfot salajasena ja arhiveeri see e-mail tuleviku tarvis.</em></div>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 3px solid #dbdcdb;" bgcolor="#f4f5f4">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" align="left" width="500">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td width="35">
                                                <div style="padding-right: 11px;"><img style="display: block;" src="http://static3.esetstatic.com/fileadmin/Mail/Conv/ico_support_2x.png" alt="" width="24" height="24" /></div>
                                            </td>
                                            <td style="font-size: 14px; line-height: 133%; mso-line-height-rule: exactly; font-family: 'Segoe UI',sans-serif; color: #424d56; font-weight: bold;" width="465">VAJAD ABI INSTALLEERIMISEGA VÕI MIS TAHES AJAL HILJEM?</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <div style="font-size: 13px; line-height: 147%; mso-line-height-rule: exactly;">Need ESETi tugitiimi koostatud <a style="color: #005599; text-decoration: underline; font-weight: 600;" href="http://support.eset.com/?segment=home">kasulikud nõuanded </a> on Sinu käsutuses 24/7. <br /> Tähelepanu! Tegu on automaatteatega, millele vastates Sinu kiri klienditoeni ei jõua. Helista meile numbrile <strong>+372 66 17 950</strong> või saada e-kiri <a style="color: #005599; text-decoration: underline; font-weight: 600;" href="mailto:">info@eset.ee</a>.</div>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px dashed #9ba1a5;" bgcolor="#f4f5f4">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" align="left" width="500">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td width="35">
                                                <div style="padding-right: 11px;"><img style="display: block;" src="http://static3.esetstatic.com/fileadmin/Mail/Conv/ico_doc_2x.png" alt="" width="24" height="24" /></div>
                                            </td>
                                            <td style="font-size: 14px; line-height: 133%; mso-line-height-rule: exactly; font-family: 'Segoe UI',sans-serif; color: #424d56; font-weight: bold;" width="465">KAS TEADSID?</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <div style="font-size: 13px; line-height: 147%; mso-line-height-rule: exactly;">Saad kanda täiesti tasuta oma olemasoleva litsentsi uuele arvutile üle. Soovid kaitsta muud opsüsteemi? Rakenda oma litsentsi maksimaalselt ESETi <a style="color: #005599; text-decoration: underline; font-weight: 600;" href="http://www.eset.com/int/home/unilicense/">Unilicense’iga.</a></div>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                                <td height="45">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td style="font-family: 'Segoe UI',Helvetica,Arial,sans-serif; color: #424d56;" align="left" width="500">
                                    <div class="text" style="font-size: 15px; line-height: 147%; mso-line-height-rule: exactly;">Jätka meie pakutava maailma parima Interneti-turbe nautimist! <br /><br /> – <strong><em>Sinu ESET-i meeskond</em></strong></div>
                                </td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right"><img style="border: 0; display: block;" src="http://static1.esetstatic.com/fileadmin/Mail/Conv/android_2x.png" alt="" width="280" height="180" /></td>
                </tr>
                <tr>
                    <td>
                        <table style="border: 0px;" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f5f4">
                            <tbody>
                            <tr>
                                <td height="20">&nbsp;</td>
                                <td height="20">&nbsp;</td>
                                <td height="20">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="50">&nbsp;</td>
                                <td align="center" width="500"><a href="http://www.eset.com/"><img src="http://static1.esetstatic.com/fileadmin/Mail/Conv/tagline-eset_2x.png" alt="ESET" width="231" height="51" /></a> <br /><br /> <a style="display: inline-block; padding: 0 2px;" href="http://plus.google.com/+esetglobal/posts"><img src="http://static3.esetstatic.com/fileadmin/Mail/Conv/soc_gplus_2x.png" alt="" width="32" height="32" /></a> <a style="display: inline-block; padding: 0 2px;" href="http://twitter.com/ESETglobal"><img src="http://static3.esetstatic.com/fileadmin/Mail/Conv/soc_tw_2x.png" alt="" width="32" height="32" /></a> <a style="display: inline-block; padding: 0 2px;" href="http://www.facebook.com/eset"><img src="http://static3.esetstatic.com/fileadmin/Mail/Conv/soc_fb_2x.png" alt="" width="32" height="32" /></a> <br /><br /></td>
                                <td width="50">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20">&nbsp;</td>
                                <td height="20">&nbsp;</td>
                                <td height="20">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</html>
<p style="font-size: 13px;"> Copyright ©&nbsp;1992&nbsp;–&nbsp;© 1992-2015 ESET spol. s r. o. ESET, ESETi logo, NOD32, ThreatSense, ThreatSense.Net ja/või muud nimetatud ESET spol. s r. o. tooted on ESET spol. s r. o. registreeritud kaubamärgid. Windows® on Microsoft grupi kaubamärk. Muud siin nimetatud ettevõtted või tooted võivad olla nende omanike registreeritud kaubamärgid. Toodetud vastavalt kvaliteedistandardi ISO 9001:2000 nõuetele.</p>