<?php

/**
 * @var $this \yii\web\View view component instance
 * @var $message \yii\mail\BaseMessage instance of newly created mail message
 * @var $code string
 * @var $site_url string
 */
?>

<p>You are receiving this email because you have registered on the site <?= $site_url ?>. If you are not registered on this site, delete the letter.</p>

<p>Code for checking e-mail: <?= $code ?></p>



