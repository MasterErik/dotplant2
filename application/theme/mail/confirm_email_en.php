<?php

/**
 * @var $this \yii\web\View view component instance
 * @var $message \yii\mail\BaseMessage instance of newly created mail message
 * @var $confirm_url string
 * @var $site_url string
 */
?>

<p>Dear <?= $client_name?>,</p>

<p>This is an automated response confirming You have been registered on the http://shop.eset.ee website.</p>

<p>To confirm registration, please follow the link below: <?= $confirm_url?></p>

<p>Your login: <?= $username?>
<br>Your Password: <?= $password?> </p>


<p>If you did not initiate registration on this site, you can disregard this email.</p>