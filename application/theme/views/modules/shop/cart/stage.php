<?php
/**
 * @var \app\theme\models\OrderStage $stage
 * @var \yii\web\View $this
 * @var \app\theme\models\Order $order
 */

use app\theme\module\helpers\OrderStageHelper;
use kartik\helpers\Html;
use kartik\form\ActiveForm;

//yii\caching\TagDependency::invalidate(Yii::$app->cache, ['Session:'.Yii::$app->session->id]);
$this->title = Html::encode($stage->name_frontend);
?>

<h1><?= Html::encode($stage->name_frontend); ?></h1>

<?php
$form = ActiveForm::begin([
    'id' => 'shop-stage',
    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
    'type' => ActiveForm::TYPE_HORIZONTAL,
//    'layout' => 'horizontal',
]);
$stageView = Yii::getAlias($stage->view);
if (is_file($stageView)) {
    $eventData = empty($eventData) ? [] : $eventData;
    echo $this->renderFile($stageView, array_merge($eventData, ['form' => $form]) );
}
?>

    <div class="row">
        <div class="col-md-6">
            <ul class="list-unstyled list-stage-buttons pull-left">
                <?=
                array_reduce(
                    OrderStageHelper::getPreviousButtons($stage, $order),
                    function ($result, $item) {
                        $result .= '<li>'.Html::a($item['label'], $item['url'], ['data-action' => $item['url'], 'class' => $item['css']]).'</li>';
                        return $result;
                    },
                    ''
                );
                ?>
            </ul>
        </div>
        <div class="col-md-6">
            <?php if ($order->items_count > 0): ?>
                <ul class="list-unstyled list-stage-buttons pull-right">
                    <?=
                    array_reduce(
                        OrderStageHelper::getNextButtons($stage, $order),
                        function ($result, $item) {
                            $result .= '<li>'.Html::submitButton($item['label'], ['data-action' => $item['url'], 'class' => $item['css'] . ' ajaxSubmit' ]).'</li>';
                            //$result .= '<li>'.Html::a($item['label'], $item['url'], ['data-action' => $item['url'], 'class' => $item['css'] . ' ajaxSubmit' ]).'</li>';
                            return $result;
                        },
                        ''
                    );
                    ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>

<?php
$form->end();

$js = <<<JS
        $('form#shop-stage .ajaxSubmit').on('click', function(event) {
            event.preventDefault();
            var selectedElements = $('form#shop-stage');
            selectedElements.attr('action', $(this).attr('data-action'));
            selectedElements.submit();
        });
JS;
$this->registerJs($js);

?>

<style>
    .list-stage-buttons li {
        list-style-type: none;
        padding: 5px 0;
    }
</style>
