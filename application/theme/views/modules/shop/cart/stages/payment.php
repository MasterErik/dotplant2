<?php
/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\shop\models\PaymentType[] $paymentTypes
 * @var integer $totalPayment
 */

    $currency = \app\modules\shop\models\Currency::getMainCurrency();

//print_r($paymentTypes); exit;

    $items = array_reduce($paymentTypes, function($result, $item)
    {
        /** @var \app\theme\models\PaymentType $item */
        $result[$item->id] = ['id'=>$item->id, 'name'=>$item->name, 'commission'=>$item->commission, 'logo'=>$item->logo];
        return $result;
    }, []);

//print_r($items); exit;

?>
<div class="row">
    <div class="col-md-3">
        <?= \yii\helpers\Html::radioList('PaymentType', $paymentType, $items,
            [
                'item' => function ($index, $label, $name, $checked, $value) {
                        $html = '<div class="radio">'
                            . '<label>'
                            . \yii\helpers\Html::radio($name, $checked, ['value'=>$value])
                            . '<span class="img-container">'
                            . \yii\helpers\Html::img($label['logo'] )
                            . '</span>'
                            . '</label>'
                            . '</div>';
                        return $html;
                },
            ]
        ) ?>
    </div>
</div>