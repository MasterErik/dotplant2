<?php

use app\theme\models\UserProfile;
use app\theme\module\helpers\PeriodHelper;
use yii\helpers\Html;

/** @var $model app\theme\models\License */
?>
<hr class="soft">
<div class="row">
    <div class="col-md-2">
        <label><b><?=Yii::t('eset', 'License for')?></b></label><br>
        <?= $model->client->client_name?>
    </div>
    <div class="col-md-2">
        <label><?= Yii::t('eset', 'Customer email') ?></label><br>
        <?= $model->client->email ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['product_code'] ?></label><br>
        <?= $model->productName ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['purchase_type'] ?></label><br>
        <?= $model->purchase_type ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['period'] ?></label><br>
        <?= PeriodHelper::format($model->period) ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['quantity'] ?></label><br>
        <?= $model->quantity ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['edate'] ?></label><br>
        <?= $model->edate ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['username'] ?></label><br>
        <?= $model->username ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['password'] ?></label><br>
        <?= $model->password ?>
    </div>
    <div class="col-md-4">
        <label><?= $model->attributeLabels()['license_key'] ?></label><br>
        <?= $model->license_key ?>
    </div>
    <div class="col-md-2">
        <label><?= $model->attributeLabels()['public_license_key'] ?></label><br>
        <?= $model->public_license_key ?>
    </div>
</div>
<hr class="soft">