<?php
/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\modules\shop\models\PaymentType $paymentType
 * @var \app\theme\models\Order $order
 * @var \app\modules\shop\models\OrderTransaction $orderTransaction
 */
use app\modules\shop\models\OrderTransaction;

?>

    <h2><?= Yii::t('app', 'Order items') ?></h2>
    <?=
    $this->render(
        'final_items',
        [
            'model' => $order,
            'items' => $order->items,
        ]
    )
    ?>

    <hr class="soft">

    <div class="col-md-7">
        <?php
            if (!empty($paymentType) && (
                OrderTransaction::TRANSACTION_START === $transaction->status ||
                OrderTransaction::TRANSACTION_ROLLBACK === $transaction->status ||
                OrderTransaction::TRANSACTION_TIMEOUT === $transaction->status ||
                OrderTransaction::TRANSACTION_ERROR === $transaction->status )
            ) {
                echo $paymentType->getPayment($order, $transaction)->content();
            }
        ?>
    </div>
