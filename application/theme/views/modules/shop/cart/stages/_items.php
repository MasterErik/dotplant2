<?php

/**
 * @var array $items
 * @var float $totalPrice Total price
 * @var boolean $immutable If we should hide controls like plus, minus, delete item
 * @var integer $order_attributes_form_id
 * @var \yii\web\View $this
 */
use app\theme\models\UserProfile;
use app\theme\widgets\AttributesProductWidget;
use yii\helpers\Url;

$immutable = isset($immutable) && $immutable;
$mainCurrency = \app\modules\shop\models\Currency::getMainCurrency();
$subItems = [];
foreach ($items as $i => $item) {
    if ($item->parent_id != 0) {
        if (isset($subItems[$item->parent_id])) {
            $subItems[$item->parent_id][] = $item;
        } else {
            $subItems[$item->parent_id] = [$item];
        }
        unset($items[$i]);
    }
}
?>

<hr class="soft">

<?php foreach ($items as $item): ?>
    <div class="row">
        <div class="col-md-3 hidden-xs">
            <?=
            \app\modules\image\widgets\ObjectImageWidget::widget([
                'limit' => 1,
                'model' => $item->product,
                'thumbnailOnDemand' => true,
                'viewFile' => '/image/img',
            ])
            ?>
        </div>

        <div class="col-md-9">

			<div class="form-group">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <?php if (!$immutable) { ?>
                                <button class="btn btn-danger" type="button" data-action="delete" data-url="<?= Url::toRoute([
                                    '/eset/cart/delete',
                                    'id' => $item->id
                                ]) ?>"><i class="fa fa-trash-o"></i></button>
                            <?php } ?>
                        </div>

                        <div class="col-md-10">
                            <div >
                                <span class="item-price pull-right" data-item-id="<?= $item->id ?>">
                                    <?= $mainCurrency->format($item->total_price) ?>
                                </span>
                                <span class='pull-right'><?= yii::t('eset', 'Price without tax ') ?>&nbsp;</span>
                            </div>
                        </div>
                    </div>
				</div>
			</div>

            <?= AttributesProductWidget::widget(['item' => $item, 'mode' => $immutable ? 'view' : 'form', 'form' => $form, 'order_attributes_form_id' => isset($order_attributes_form_id) ? $order_attributes_form_id : null]) ?>
        </div>
    </div>
    <hr class="soft">

<?php endforeach; ?>

<div class="row">
    <div class="col-md-12">
        <span class="pull-left">
            <?php if (UserProfile::iReseller()) {
                echo $form->field($model, 'dexterSuppressEmail')->checkbox(['options' => ['class' => 'form-group']]);
            }?>
        </span>
		<span class="pull-right">
            <?= Yii::t('eset', 'Tax') ?>
            <span class="total-tax ">
				<?= $mainCurrency->format($model->eset_tax) ?>
			</span>
		</span>

        <br>

		<span class="pull-right">
            <?php if (UserProfile::iReseller()) {
                echo Yii::t('eset', 'Suggested end-user price with tax');
            } else {
                echo Yii::t('eset', 'End-user price with tax');
            }?>
			<span class="total-price">
				<?= $mainCurrency->format($model->total_price) ?>
			</span>
		</span>
    </div>
</div>
<hr class="soft">
