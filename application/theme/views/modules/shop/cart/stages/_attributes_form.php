<?php
/**
 * @var $this yii\web\View
 * @var $model \app\theme\models\Order
 * @var $form_id integer
 */

use app\theme\models\DiscountCodeList;
use app\theme\models\Position;
use app\theme\models\UserProfile;
use app\theme\widgets\Select2;
?>

<?php
if (UserProfile::iReseller()) {

    echo $form->field($model, 'user_id', ['enableClientValidation' => false])->widget(Select2::className(), [
        'id' => 'cartUserId',
        'initValueText' => $model->clientUserProfile ? $model->clientUserProfile->client_name : '',
        'pluginOptions' => ['allowClear' => true],
        'options' => [
            'placeholder' => Yii::t('eset', 'Select user'),
            'class' => 'relation-price',
        ],
        'selectModel' => \app\theme\models\User::className()
    ]);

//    echo $form->field($model, 'eset_sector')->dropDownList($model->getSectorList(), ['class' => 'relation-price']);

    echo $form->field($model, 'eset_discount_code', [
        'inputOptions' => [
            'id' => 'discountCodeInput',
        ]
    ])->dropDownList($model->getDiscountCodeList(), ['class' => 'relation-price']);

    echo $form->field($model, 'eset_custom_discount', [
        'options' => [
            'id' => 'productDiscountDocumentField',
            'class' => 'form-group relation-price',
            'format' => 'raw',
            'style' => $model->isShowDiscountCustom() ? '' : 'display: none;'
        ],
    ]);
}

$js_client = Position::POSITION_CLIENT;
$js_business = Position::POSITION_BUSINESS;
$js = <<<JS

    var cart_user_change = function(){
        console.log($('#cartUserId').val());
        $('#cartUserId').val() == undefined || $('#cartUserId').val() == "" ? $('.relation-registration').show() : $('.relation-registration').hide();
    };
    $('#cartUserId').change(cart_user_change);
    
    $('#orderDiscountCodeInput').change(function(){
        $(this).val()==". DiscountCodeList::CODE_FOR_DOCUMENT ." ? $('#orderDiscountDocumentField').show() : $('#orderDiscountDocumentField').hide();
    });
        
    var cart_position_change = function(){
        /*console.log($('#registrationform-position').val());*/
        switch( $('#registrationform-position').val() ) {
            case '{$js_client}':
                $('.legal-entity').hide();
                break;
            case '{$js_business}':
                $('.legal-entity').show();
                break;
        }
    };
    $('#registrationform-position').change(cart_position_change);
    
    cart_position_change();
    cart_user_change();
JS;
$this->registerJs($js, \yii\web\View::POS_READY, 'product-discount-fields');


