<?php

/**
 * @var $model \app\theme\models\RegistrationForm
 * @var $this \yii\web\View
 */

use app\theme\models\Country;
use app\theme\models\UserProfile;
use yii\helpers\Html;
?>

<?= $form->errorSummary($model, ['class'=>'alert alert-danger']); ?>

<?php /*= $form->field($model, 'username')->textInput(['autocomplete' => 'off'])*/ ?>
<?php /*= $form->field($model, 'email')->textInput(['autocomplete' => 'off'])*/ ?>

<?php
$input = '<div class="col-md-9"><div class="input-group">'
    . Html::activeTextInput($model, 'email', ['class' => "form-control", 'autocomplete' => "off"])
    . '<span class="input-group-btn">'
    . '<button class="btn btn-primary" type="button" id="btn-send-code">' . \Yii::t('eset', 'Send verification code') . '</button>'
    . '</span>'
    . '</div></div>';
echo $form->field($model, 'email', [
    'template' => "{label}\n{$input}\n{hint}\n{error}",
]) ?>

<?= $form->field($model, 'confirmEmailCode')->textInput(['autocomplete' => 'off']) ?>

<?= $form->field($model, 'password')->passwordInput(['autocomplete' => 'off']) ?>
<?= $form->field($model, 'confirmPassword')->passwordInput(['autocomplete' => 'off']) ?>

<?= $form->field($model, 'position')->dropDownList(UserProfile::getAvailablePositions()) ?>

<?= $form->field($model, 'country_id')->dropDownList(Country::getList()) ?>
<?php /*= $form->field($model, 'locale')->dropDownList(\app\theme\components\MultiLangHelper::getLanguages())*/ ?>
<?= $form->field($model, 'telefon')->textInput() ?>

<?php
$this->registerJs("jQuery('#btn-send-code').click(function(){
    var email = jQuery('#registrationform-email').val();
    if( email == '' ) {
        alert('" . Yii::t('eset', $model->getAttributeLabel('email').' empty') . "');
        return false;
    }
    jQuery.ajax('" . \yii\helpers\Url::toRoute('/eset/cart/send-code')."', {
        data: {email: email},
        dataType: 'json',
        type: 'get',
        success: function(data) {
            if(data.success) {
                alert(data.message);
            } else {
                alert(data.message);
            }
        },
//        error: function (jqXHR, textStatus, errorThrown) {
//            alert(jqXHR.responseText);
//        },
    });



});
", \yii\web\View::POS_END, 'cart-send-email-code');

?>

<?php /*
    echo $this->render(
        '_attributes',
        [
            'model' => $model_order,
        ]
    );

    echo $this->render(
        '_items',
        [
            'model' => $model_order,
            'items' => $model_order->items,
            'immutable' => true,
        ]
    ); */
?>
