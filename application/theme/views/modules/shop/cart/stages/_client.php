<?php
    use app\theme\models\Position;
?>
<h3><label class="relation-registration"><?=Yii::t('eset', 'Create new user')?></label></h3>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($registration, 'position',['autoPlaceholder' => true])->dropDownList(Position::getUnregistered(), [
            'class' => 'relation-registration',
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($registration, 'email', [
            'enableClientValidation' => true,
            'autoPlaceholder' => true,
            'options' => [
                'class' => 'form-group relation-registration',
            ],
            'feedbackIcon' => [
                'default' => 'envelope',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
            'addon' => [
                'prepend' => [ 'content' => '<i class="glyphicon glyphicon-envelope"></i>']
            ]
        ])
        ?>
    </div>
</div>
<div class="row not-legal-entity">
    <div class="col-md-6">
        <?= $form->field($registration, 'first_name', [
            'enableClientValidation' => true,
            'autoPlaceholder' => true,
            'options' => [
                'class' => 'form-group relation-registration',
            ],
            'feedbackIcon' => [
                'default' => 'user',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
        ])->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($registration, 'last_name', [
            'enableClientValidation' => true,
            'autoPlaceholder' => true,
            'options' => [
                'class' => 'form-group relation-registration',
            ],
        ])->textInput() ?>
    </div>
</div>
<div class="row legal-entity">
    <div class="col-md-12">
        <?= $form->field($registration, 'client_name', [
            'enableClientValidation' => true,
            'autoPlaceholder' => true,
            'options' => [
                'class' => 'form-group relation-registration',
            ],
            'feedbackIcon' => [
                //'default' => 'user',
                'success' => 'ok',
                'error' => 'exclamation-sign',
                'defaultOptions' => ['class' => 'text-primary']
            ],
        ])->textInput() ?>
    </div>
</div>
