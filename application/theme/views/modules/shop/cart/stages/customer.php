<?php
/**
 * @var \app\theme\models\Order $order
 * @var \app\modules\shop\models\OrderCode $orderCode
 * @var \yii\web\View $this
 */

use app\theme\models\Position;
use app\theme\models\UserProfile;
use kartik\helpers\Html;
?>

<div class="cart">
<?php if (!is_null($order) && $order->items_count > 0): ?>
    <?= $this->render('_attributes_form', ['model' => $order, 'registration' => $registration, 'form' => $form, 'form_id' => $formId]) ?>
    <?php if (UserProfile::iReseller() || Yii::$app->user->isGuest) {
        echo $this->render('_client', ['model' => $order, 'registration' => $registration, 'form' => $form]);
    }?>
    <?= $this->render('_items', ['model' => $order, 'items' => $order->items, 'form' => $form, 'order_attributes_form_id' => $formId]) ?>
    


    <?php if ($orderCode): ?>
        <div class="pull-right">
            <div class="discount-code">
                <?php if ($orderCode->isNewRecord): ?>
                    <?= $form->errorSummary($orderCode); ?>
                    <?= $form->field($orderCode, 'code') ?>
                    <?= Html::submitButton(Yii::t('app', 'Apply code'), ['class' => 'btn btn-success']); ?>
                <?php else: ?>
                    <?= Yii::t('app', 'Applied discount code:') ?>
                    <div class="applied-discount-code">
                        <?= $orderCode->discountCode->code ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
    <?= $form->errorSummary($order, ['class'=>'alert alert-danger']); ?>
    <div class="clearfix"></div>
<?php else: ?>
    <p><?= Yii::t('app', 'Your cart is empty') ?></p>
<?php endif; ?>
</div>

<?php
$js = <<<JS

    $('.relation-price').change(function(){
    var form = jQuery(this).parents('form:first');

    //var item_id = form.find('.item-id').val();
    var item_id = jQuery(this).data('item-id');
    var els;
    if( item_id ) {
        els = jQuery('.item-price[data-item-id='+ item_id +']');
    } else {
        els = jQuery('.item-price');
    }
    els.html('<img src=\"/theme/images/loading-block.gif\"/>');

    Shop.getPrice('item_id=' + item_id + '&' + form.serialize(), function(data) {
        jQuery('form#shop-stage button[type=submit]').addClass('disabled');
        if( data.success ) {
            for(el in data.prices) {
                jQuery('.item-price[data-item-id='+ el +']').text(data.prices[el]);
            }
            var f_success = true;
            for(el in data.errors) {
                jQuery('.item-price[data-item-id='+ el +']').html('<div class=\"alert alert-danger\" role=\"alert\">' + data.errors[el] + '</div>');
                f_success = false;
            }
            jQuery('.total-tax').text(data.totalTax);
            jQuery('.total-price').text(data.totalPrice);
            if( f_success ) {
                jQuery('form#shop-stage button[type=submit]').removeClass('disabled');
            }
        } else {
            //console.error(data.message);
            var html = '<div class=\"alert alert-danger\" role=\"alert\">' + data.message + '</div>';
            if( item_id ) {
                jQuery('.item-price[data-item-id='+ item_id +']').html(html);
            } else {
                jQuery('.item-price').html(html);
            }
        }
        jQuery('.item-price img').remove();
    });
});

Shop.getPrice = function(data, callback) {
    jQuery.ajax({
        'data' : data,
        'dataType' : 'json',
        'success' : function(data) {
            if (typeof(callback) === 'function') {
                callback(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.error(jqXHR.responseText);
        },
        'type' : 'post',
        'url' : '/eset/cart/get-price'
    });
}

JS;
$this->registerJs($js, \yii\web\View::POS_END, 'calculation-price');
?>

