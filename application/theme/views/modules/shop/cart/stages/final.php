<?php
/**
 * @var \app\modules\shop\models\Order $order
 * @var \yii\web\View $this
 */

$this->title = Yii::t('app', 'Order #{order}', ['order' => $order->id]);

    $orderIsImmutable = Yii::$app->user->isGuest
        ? true
        : $order->getImmutability(\app\theme\models\Order::IMMUTABLE_USER);
?>

    <h1><?= $this->title ?></h1>


    <?= \yii\widgets\DetailView::widget([
            'model' => $order,
            'attributes' => [
                [
                    'attribute' => 'start_date',
                    'value' => $order->start_date,
                    'label' => Yii::t('eset', 'Order date'),
                ],

                [
                    'attribute' => 'order_stage_id',
                    'value' => $order->stage->name_frontend
                ],
                [
                    'attribute' => 'payment_type_id',
                    'value' => $order->paymentType ? $order->paymentType->name : null,
                ],

            ],
        ]);
    ?>

    <h2><?= Yii::t('app', 'Order items') ?></h2>
    <?=
        $this->render(
            'final_items',
            [
                'model' => $order,
                'items' => $order->items,
            ]
        )
    ?>

    <h2><?= Yii::t('app', 'Licenses') ?></h2>

    <?php
    $licenses = \app\theme\models\License::find()->byOrder($order->id)->all();
    foreach($licenses as $license) {
        echo $this->render('_final_license', ['model' => $license]);
        //echo \app\theme\widgets\LicenseWidget::widget(['model' => $license]);
    }
    ?>

    <?= \app\modules\shop\widgets\OrderTransaction::widget([
            'model' => $order,
            'immutable' => $orderIsImmutable,
        ]);
    ?>


