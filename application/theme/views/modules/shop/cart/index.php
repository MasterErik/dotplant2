<?php
/**
 * @var \app\modules\shop\models\Order $model
 * @var \app\modules\shop\models\OrderCode $orderCode
 * @var \yii\web\View $this
 */

use app\theme\module\helpers\OrderStageHelper;

$this->title = Yii::t('app', 'Cart');

?>
    <h1><?= Yii::t('app', 'Cart') ?></h1>
<div class="cart">
<?php if (!is_null($model) && $model->items_count > 0): ?>

    <?php $orderAttributesFormId = 'orderAttributesFormId'; ?>
    <?= $this->render('_attributes_form', ['model' => $model, 'form'=>$form, 'form_id' => $orderAttributesFormId]) ?>
    <?= $this->render('_items', ['model' => $model, 'items' => $model->items, 'form'=>$form, 'order_attributes_form_id' => $orderAttributesFormId]) ?>

    <div class="pull-right">
        <div class="discount-code">
        <?php if ($orderCode->isNewRecord): ?>
            <?php/* $form = \kartik\form\ActiveForm::begin(
                [
                    'type' => \kartik\form\ActiveForm::TYPE_INLINE,
                ]
            );*/
            ?>
            <?= $form->errorSummary($orderCode); ?>
            <?= $form->field($orderCode, 'code') ?>
            <?= \kartik\helpers\Html::submitButton(Yii::t('app', 'Apply code'), ['class' => 'btn btn-success']); ?>
            <?php /*= \kartik\helpers\Html::endForm(); */ ?>
        <?php else: ?>
            <?= Yii::t('app', 'Applied discount code:') ?>
            <div class="applied-discount-code">
                <?= $orderCode->discountCode->code ?>
            </div>
        <?php endif; ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="pull-right cta">
        <?=
            array_reduce(
                OrderStageHelper::getNextButtons($model->stage, $model, false),
                    function ($result, $item) {
                        $result = \yii\helpers\Html::a($item['label'], [$item['url']], ['class' => $item['css']]); //. ' disabled'
                        return $result;
                    },
                ''
            );
        ?>
    </div>

<?php else: ?>
    <p><?= Yii::t('app', 'Your cart is empty') ?></p>
<?php endif; ?>
</div>

<?php $this->registerJs("
jQuery('.relation-price').change(function(){
    var form = jQuery(this).parents('form:first');
    var item_id = form.find('#orderitem-id').val();
    var els;
    if( item_id ) {
        els = jQuery('.item-price[data-item-id='+ item_id +']');
    } else {
        els = jQuery('.item-price');
    }
    els.html('<img src=\"/theme/images/loading-block.gif\"/>');

    Shop.getPrice(form.serialize(), function(data) {
        jQuery('.btn-primary').addClass('disabled');
        if( data.success ) {
            for(el in data.prices) {
                jQuery('.item-price[data-item-id='+ el +']').text(data.prices[el]);
            }
            var f_success = true;
            for(el in data.errors) {
                jQuery('.item-price[data-item-id='+ el +']').html('<div class=\"alert alert-danger\" role=\"alert\">' + data.errors[el] + '</div>');
                f_success = false;
            }
            jQuery('.total-tax').text(data.totalTax);
            jQuery('.total-price').text(data.totalPrice);
            if( f_success ) {
                jQuery('.btn-checkout').removeClass('disabled');
            }
        } else {
            console.error(data.message);
        }
        jQuery('.item-price img').remove();
    });
});

Shop.getPrice = function(data, callback) {
    jQuery.ajax({
        'data' : data,
        'dataType' : 'json',
        'success' : function(data) {
            if (typeof(callback) === 'function') {
                callback(data);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.error(jqXHR.responseText);
        },
        'type' : 'post',
        'url' : '/eset/cart/get-price'
    });
}
", \yii\web\View::POS_END, 'calculation-price'); ?>
