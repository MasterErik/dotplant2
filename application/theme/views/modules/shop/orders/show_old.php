<?php
/**
 * @var \app\modules\shop\models\Order $order
 * @var \yii\web\View $this
 */

use kartik\helpers\Html;

    $this->title = Yii::t('app', 'Order #{order}', ['order' => $order->id]);

    $this->params['breadcrumbs'] = [
        [
            'label' => Yii::t('app', 'Personal cabinet'),
            'url' => ['/shop/cabinet/']
        ],
        [
            'label' => Yii::t('app', 'Orders list'),
            'url' => ['/shop/orders/list']
        ],
        $this->title,
    ];

    $orderIsImmutable = Yii::$app->user->isGuest
        ? true
        : $order->getImmutability(\app\modules\shop\models\Order::IMMUTABLE_USER);
?>

    <h1><?= $this->title ?></h1>



<?= \yii\widgets\DetailView::widget([
        'model' => $order,
        'attributes' => [
            'start_date',
            [
                'attribute' => 'order_stage_id',
                'value' => $order->stage->name_frontend
            ],
            [
                'attribute' => 'payment_type_id',
                'value' => $order->paymentType ? $order->paymentType->name : null,
            ],

        ],
    ]);
?>

    <h2><?= Yii::t('app', 'Order items') ?></h2>

    <?=
        $this->render(
            '_items',
            [
                'model' => $order,
                'items' => $order->items,
            ]
        )
    ?>

    <h2><?= Yii::t('app', 'Licenses') ?></h2>

    <?php
    $licenses = \app\theme\models\License::find()->byOrder($order->id)->all();
    foreach($licenses as $license) {
        echo \app\theme\widgets\LicenseWidget::widget(['model' => $license]);
    }
    ?>

    <?= \app\modules\shop\widgets\OrderTransaction::widget([
            'model' => $order,
            'immutable' => $orderIsImmutable,
        ]);
    ?>


