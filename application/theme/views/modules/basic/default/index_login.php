<?php if( Yii::$app->user->isGuest ) { ?>
    <div class="row">
        <div class="col-md-offset-7 col-md-5">
            <?= \app\theme\widgets\login\LoginWidget::widget() ?>
        </div>
    </div>
<?php } ?>
