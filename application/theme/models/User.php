<?php

namespace app\theme\models;

use app\backgroundtasks\helpers\BackgroundTasks;
use app\models\Object;
use app\theme\services\Synchronization;
use Yii;
use yii\filters\RateLimiter;
use yii\filters\RateLimitInterface;
use yii\web\IdentityInterface;

/**
 * @inheritdoc
 */
class User extends \app\modules\user\models\User implements IdentityInterface //, RateLimitInterface
{
    const STATUS_EMAIL_NOT_CONFIRMED = 9;

    private $object = null;

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('eset', 'Active'),
            self::STATUS_DELETED => \Yii::t('eset', 'Deleted'),
            self::STATUS_EMAIL_NOT_CONFIRMED => \Yii::t('eset', 'Not confirmed'),
        ];
    }

    public function getObject()
    {
        if ($this->object === null) {
            $this->object = Object::getForClass(\app\modules\user\models\User::className());
            if ($this->object === null) {
                throw new \Exception("Can't find Object row for " . get_class($this->owner));
            }
        }
        return $this->object;
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString(12);
    }

    /*
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return [
            $behaviors['rateLimiter'] => [
                'class' => RateLimiter::className(),
                'user' => new IpLimiter(),
            ],
        ];
    }

    public function getRateLimit($request, $action)
    {
        return [100, 60]; //не более 100 запросов в течении 60 секунд
    }
    public function loadAllowance($request, $action)
    {
        //$count - считаем сколько уже запросов совершил юзер, например по записям в некой таблице логов
        return [100-$count, time()];
    }
    public function saveAllowance($request, $action, $allowance, $timestamp)
    {

        //записываем результат запроса, например в некоторую таблицу логов
    }
    */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'except' => ['registerService', 'signup']],
            ['email', 'email'],
            ['email', 'unique', 'message' => \Yii::t('app', 'This email address has already been taken.')],
            ['email', 'exist', 'message' => \Yii::t('app', 'There is no user with such email.'), 'on' => 'requestPasswordResetToken'],

            ['password', 'required', 'on' => ['adminSignup', 'changePassword']],
            ['password', 'string', 'min' => 8],
            [['first_name', 'last_name',], 'string', 'max' => 255],

            // change password
            [['newPassword', 'confirmPassword'], 'required'],
            [['newPassword', 'confirmPassword'], 'string', 'min' => 8],
            [['confirmPassword'], 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['registerService'] = ['username', 'password', 'email', 'first_name', 'last_name'];
        return $scenarios;
    }
    public static function find()
    {
        return new \app\theme\models\query\UserQuery(get_called_class());
    }


    public function getUniqueUsername($username)
    {
        $username = trim($username);
        while (self::find()->where(['username' => $username])->one()) {
            $username .= rand(0, 9);
        }
        return $username;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }
    public static function AfterLogin()
    {
        if (Synchronization::isNeedQuery(Yii::$app->user->id)) {
            $ok = BackgroundTasks::addTask(
                [
                    'name' => 'Synchronization',
                    'description' => 'Synchronization license by user',
                    'action' => 'eset/rest/sync-license',
                    'params' => (string)Yii::$app->user->id,
                    'init_event' => 'User',
                ]
                //['create_notification' => false]
            );
            if (!$ok) {
                Yii::error(yii::t('eset', 'Failed to add tasks: Synchronization license by user'), 'BackgroundTasks');
            }
        }
    }
}
