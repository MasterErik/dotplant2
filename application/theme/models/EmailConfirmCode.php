<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_email_confirm_code}}".
 *
 * @property string $email
 * @property string $code
 * @property string $ins_ts
 */
class EmailConfirmCode extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_email_confirm_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['ins_ts'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('eset', 'Email'),
            'code' => Yii::t('eset', 'Code'),
            'ins_ts' => Yii::t('eset', 'Ins Ts'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\EmailConfirmCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EmailConfirmCodeQuery(get_called_class());
    }

    public static function sendCode($email)
    {
        self::deleteAll(['email' => $email]);

        $confirm = new self;
        $confirm->email = $email;
        $confirm->code = Yii::$app->getSecurity()->generateRandomString(4) . '-' . Yii::$app->getSecurity()->generateRandomString(4);

        if ($confirm->save()) {
            MailHelper::Send($email, '@app/theme/mail/confirm_email_code',
                [
                    'code' => $confirm->code,
                    'site_url' => \Yii::$app->urlManager->createAbsoluteUrl(''),
                ],
                \Yii::t('eset', \Yii::$app->name) . ': ' . \Yii::t('eset', 'confirm email')
            );

            return true;
        } else {
            return false;
        }

    }

    public static function validateEmail($email, $code)
    {
        return self::find()->where(['email' => $email, 'code' => $code])->exists();
    }

}
