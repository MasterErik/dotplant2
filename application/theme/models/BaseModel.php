<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 6.02.2015
 * Time: 10:03
 */

namespace app\theme\models;

use app\theme\components\behaviors\ShopBlameableBehavior;
use app\theme\components\behaviors\ShopTimestampBehavior;
use devgroup\TagDependencyHelper\ActiveRecordHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

abstract class BaseModel extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'blame' => [
                'class' => ShopBlameableBehavior::className(),
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'updater_id'
            ],
            'timestamp' => [
                'class' => ShopTimestampBehavior::className(),
            ],
            ['class' => ActiveRecordHelper::className()],
        ];

    }

    /**
     * @inheritdoc
     * @return integer thit is primary key.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

}