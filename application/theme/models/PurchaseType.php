<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 24.03.2015
 * Time: 14:51
 */

namespace app\theme\models;

use Yii;


class PurchaseType extends CustomMemoryModel
{
    public $purchase_type;

    const PURCHASE_TYPE_NEW = 'New';
    const PURCHASE_TYPE_NFR = 'NFR';
    const PURCHASE_TYPE_TRIAL = 'Trial';
    const PURCHASE_TYPE_BUSINESSTRIAL = 'BusinessTrial';

    const PURCHASE_TYPE_RENEW = 'Renew';
    const PURCHASE_TYPE_ENLARGE = 'Enlarge';
    const PURCHASE_TYPE_UPGRADE = 'Upgrade';
    const PURCHASE_TYPE_DOWNGRADE = 'Downgrade';

    public static function getKey()
    {
        return [
            static::PURCHASE_TYPE_NEW,
            static::PURCHASE_TYPE_RENEW,
            static::PURCHASE_TYPE_ENLARGE,
            static::PURCHASE_TYPE_NFR,
            static::PURCHASE_TYPE_UPGRADE,
            static::PURCHASE_TYPE_DOWNGRADE,
        ];
    }

    public function rules()
    {
        return [
            ['purchase_type', 'validateExists'],
        ];
    }

    protected function fullData()
    {
        return [
            ["Id" => 1, self::key => self::PURCHASE_TYPE_NEW, self::desc => Yii::t("eset", "New licence")],
            ["Id" => 2, self::key => self::PURCHASE_TYPE_RENEW, self::desc => Yii::t("eset", "Renew licence")],
            ["Id" => 3, self::key => self::PURCHASE_TYPE_ENLARGE, self::desc => Yii::t("eset", "Enlarge licence")],
            ["Id" => 4, self::key => self::PURCHASE_TYPE_NFR, self::desc => Yii::t("eset", "NFR licence")],
            ["Id" => 9, self::key => self::PURCHASE_TYPE_UPGRADE, self::desc => Yii::t("eset", "Upgrade licence")],
            ["Id" => 12, self::key => self::PURCHASE_TYPE_DOWNGRADE, self::desc => Yii::t("eset", "Downgrade")],
            ["Id" => 13, self::key => self::PURCHASE_TYPE_TRIAL, self::desc => Yii::t("eset", "Trial")],
            ["Id" => 13, self::key => self::PURCHASE_TYPE_BUSINESSTRIAL, self::desc => Yii::t("eset", "BusinessTrial")],


        ];
    }

}