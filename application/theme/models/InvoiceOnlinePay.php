<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_invoice_online_pay}}".
 *
 * @property integer $invoice_id
 * @property integer $order_id
 * @property string $key
 * @property string $created_at
 *
 * @property Order $order
 * @property EsetInvoice $invoice
 */
class InvoiceOnlinePay extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_invoice_online_pay}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'order_id', 'key'], 'required'],
            [['invoice_id', 'order_id'], 'integer'],
            [['created_at'], 'safe'],
            [['key'], 'string', 'max' => 32],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('eset', 'Invoice ID'),
            'order_id' => Yii::t('eset', 'Order ID'),
            'key' => Yii::t('eset', 'Key'),
            'created_at' => Yii::t('eset', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(EsetInvoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\InvoiceOnlinePayQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\InvoiceOnlinePayQuery(get_called_class());
    }
}
