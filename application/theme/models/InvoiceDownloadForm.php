<?php

namespace app\theme\models;

use app\theme\components\invoice\Export\Export;
use app\theme\models\query\InvoiceQuery;
use Yii;
use yii\base\Model;
use yii\web\BadRequestHttpException;

/**
 * @property array $formats
 * @property array $periods
 */
class InvoiceDownloadForm extends Model
{
    const FORMAT_PERIOD = 'yyyy/mm';

    public $format;
    public $email;
    public $period;

    public $date_from;
    public $date_to;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['format', 'period'], 'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email', 'checkDNS' => true],
            [['type', 'period'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => Yii::t('eset', 'Format'),
            'email' => Yii::t('app', 'E-mail'),
            'period' => Yii::t('eset', 'Period'),
        ];
    }

    public function getFormats()
    {
        return Export::getFormats();
    }


    /**
     * @return InvoiceQuery
     * @throws BadRequestHttpException
     */
    public function getInvoiceQuery()
    {
        if( !$this->parsePeriod() ) {
            throw new BadRequestHttpException('The format of the period is not known.');
        }
        return Invoice::find()->dateFrom($this->date_from)->dateTo($this->date_to);
    }

    private function parsePeriod()
    {
        // self::FORMAT_PERIOD == 'yyyy/mm'
        if( !preg_match('@[0-9]{4}/[0-9]{2}@', $this->period) ) {
            return false;
        }

        list($y,$m) = explode('/', $this->period);
        $time_from = strtotime('first day of ' . $y . '-' . $m . '-01');
        $time_to = strtotime('last day of ' . $y . '-' . $m . '-01');
        if( !$time_from || !$time_to ) {
            return false;
        }

        $this->date_from = date('Y-m-d 00:00:00', $time_from);
        $this->date_to = date('Y-m-d 23:59:59', $time_to);

        return true;
    }

    public function genFilename()
    {
        return 'invoice-'.str_replace(array('/', '.'), '-', $this->period) . '.csv';
    }
}
