<?php

namespace app\theme\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%eset_discount_code}}".
 *
 * @property integer $id
 * @property integer $code
 * @property string $name
 * @property string $cost
 * @property string $note
 * @property string $state
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $creator_id
 * @property integer $updater_id
 */
class DiscountCode extends BaseModel
{

    protected static $_dropDownItems = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_discount_code}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => \app\theme\components\behaviors\TranslateModelBehavior::className(),
                'languages' => \app\theme\components\MultiLangHelper::getLanguages(),
                'attributes' => ['name', 'note']
            ]]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'cost', 'state'], 'required'],
            [['code', 'created_at', 'updated_at', 'creator_id', 'updater_id'], 'integer'],
            [['cost'], 'number'],
            [['note'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['state'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'code' => Yii::t('eset', 'Code'),
            'name' => Yii::t('eset', 'Name'),
            'cost' => Yii::t('eset', 'Discount Percentage'),
            'note' => Yii::t('eset', 'Note'),
            'state' => Yii::t('eset', 'State'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'creator_id' => Yii::t('eset', 'Creator ID'),
            'updater_id' => Yii::t('eset', 'Updater ID'),
        ];
    }

    public static function getDiscountCodeList()
    {
        if (self::$_dropDownItems === false) {
            $condition = 'state="active"';
            //. ', ' . self::CODE_NO_DISCOUNT
            if (UserProfile::iReseller()) {
                $condition .= ' AND code NOT IN (' . DiscountCodeList::CODE_CUSTOM . ')';
            } else if (UserProfile::iPartner()) {
                $condition = 'state="active"';
            } else {
                $condition .= ' AND code IN (' . DiscountCodeList::CODE_NO_DISCOUNT . ')';
            }
            self::$_dropDownItems = ArrayHelper::map(self::find()
                ->select(['code', 'name' => 'CONCAT(name, IF(cost < 100 AND cost > 0, CONCAT(": ", cost, "%"), ""))'])
                ->orderBy('name')
                ->where($condition)
                ->all(),
                'code', 'name');
        }

        return self::$_dropDownItems;
    }
}
