<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_bank_transaction}}".
 *
 * @property integer $id
 * @property integer $eset_payment_id
 * @property integer $eset_payment_type_id
 * @property integer $partner_id
 * @property string $iban
 * @property string $payer
 * @property string $summa
 * @property integer $viitenumber
 * @property string $description
 * @property string $created_at
 *
 * @property User $partner
 * @property EsetPayment $esetPayment
 * @property EsetPaymentType $esetPaymentType
 */
class EsetBankTransaction extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_bank_transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eset_payment_id', 'eset_payment_type_id', 'partner_id', 'viitenumber'], 'integer'],
            [['eset_payment_type_id', 'partner_id', 'summa'], 'required'],
            [['summa'], 'number'],
            [['created_at'], 'safe'],
            [['iban'], 'string', 'max' => 34],
            [['payer', 'description'], 'string', 'max' => 255],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['eset_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetPayment::className(), 'targetAttribute' => ['eset_payment_id' => 'id']],
            [['eset_payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetPaymentType::className(), 'targetAttribute' => ['eset_payment_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'eset_payment_id' => Yii::t('eset', 'Eset Payment ID'),
            'eset_payment_type_id' => Yii::t('eset', 'Eset Payment Type ID'),
            'partner_id' => Yii::t('eset', 'Partner ID'),
            'iban' => Yii::t('eset', 'Iban'),
            'payer' => Yii::t('eset', 'Payer'),
            'summa' => Yii::t('eset', 'Summa'),
            'viitenumber' => Yii::t('eset', 'Viitenumber'),
            'description' => Yii::t('eset', 'Description'),
            'created_at' => Yii::t('eset', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPayment()
    {
        return $this->hasOne(EsetPayment::className(), ['id' => 'eset_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPaymentType()
    {
        return $this->hasOne(EsetPaymentType::className(), ['id' => 'eset_payment_type_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\EsetBankTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EsetBankTransactionQuery(get_called_class());
    }


    /**
     * @return EsetPayment
     */
    public function create_payment()
    {
        $payment = new EsetPayment();
        $payment->setAttributes($this->getAttributes([
            'partner_id',
            'eset_payment_type_id',
            'viitenumber',
            'summa',
            'created_at'
        ]));
        $payment->type = 'invoice';

        if( $payment->save() ) {
            $this->eset_payment_id = $payment->id;
            $this->save(true, ['eset_payment_id']);
        }

        return $payment;
    }

}
