<?php

namespace app\theme\models;

use Yii;

/**
 * @inheritdoc
 */
class Category extends \app\modules\shop\models\Category
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => \app\theme\components\behaviors\TranslateModelBehavior::className(),
                'languages' => \app\theme\components\MultiLangHelper::getLanguages(),
                'attributes' => ['name', 'title', 'h1', 'breadcrumbs_label', 'content', 'announce']
            ]]
        );
    }

    private static function removeEmptyArray(&$items)
    {
        foreach ($items as $key => $item) {
            if (is_array($item)) {
                if (0 == count($item)) {
                    unset($items[$key]);
                } else {
                    static::removeEmptyArray($items[$key]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public static function getMenuItemsLine()
    {

        $result = self::getMenuItems(1, 2, false);
        static::removeEmptyArray($result);

        return $result;
    }
}
