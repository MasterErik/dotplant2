<?php

namespace app\theme\models;

use app\theme\components\license\proxy\OrderItemProxy;
use app\theme\components\validators\ReferenceValidator;
use Yii;


/**
 * This is the model class for table "{{%eset_license}}".
 *
 * @property integer $id
 * @property integer $order_detail_id
 * @property integer $child_license_id
 * @property integer $user_id
 * @property integer $reseller_id
 * @property integer $product_code
 * @property integer $country_code
 * @property integer $quantity
 * @property string $purchase_type
 * @property string $register_state
 * @property string $state
 * @property string $license_type_global
 * @property string $license_type
 * @property string $sector
 * @property string $bdate
 * @property string $edate
 * @property integer $period
 * @property integer $discount_code
 * @property string $custom_discount
 * @property string $discount_document
 * @property string $username
 * @property string $password
 * @property string $epli
 * @property string $license_key
 * @property string $public_license_key
 * @property string $common_tag
 * @property string $note
 * @property string $note_own
 * @property integer $created_at
 * @property integer $updated_at
  *
 * @property string $productName
 * @property string $parent_license_epli
 * Relations:
 * @property OrderItem $orderDetail
 * @property Country $country
 * @property Product $product
 * @property UserProfile $reseller
 * @property UserProfile $client
 * @property User $user
 */
class License extends BaseModel
{
    const REGISTER_STATE_REGISTERED = 'registered';
    const REGISTER_STATE_UNREGISTERED = 'unregistered';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_license}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_detail_id', 'user_id', 'product_code', 'country_code', 'quantity', 'period', 'discount_code'], 'required'],
            [['order_detail_id', 'child_license_id', 'user_id', 'reseller_id', 'product_code', 'country_code', 'quantity', 'period', 'discount_code'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
            [['purchase_type', 'register_state', 'state', 'license_type_global', 'license_type', 'sector', 'discount_document'], 'string'],
            ['state', ReferenceValidator::className(), 'targetClass' => LicenseStatus::className()],
            [['bdate', 'edate'], 'safe'],
            [['custom_discount'], 'number'],
            [['username', 'password', 'epli', 'license_key', 'public_license_key', 'common_tag'], 'string', 'max' => 50],
            [['note', 'note_own'], 'string', 'max' => 255],
            ['sector', ReferenceValidator::className(), 'targetClass' => Sector::className()],
            ['purchase_type', ReferenceValidator::className(), 'targetClass' => PurchaseType::className()],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'rest' => [
                'user_id', 'product_code', 'country_code', 'quantity', 'purchase_type', 'register_state', 'state', 'sector', 'bdate', 'edate',
                'period', 'discount_code', 'custom_discount', 'username', 'password', 'epli', 'license_key', 'public_license_key', 'common_tag', 'note',
            ],
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'order_detail_id' => Yii::t('eset', 'Order Detail ID'),
            'child_license_id' => Yii::t('eset', 'Child License'),
            'user_id' => Yii::t('eset', 'User'),
            'reseller_id' => Yii::t('eset', 'Reseller'),
            'product_code' => Yii::t('eset', 'Product'),
            'country_code' => Yii::t('eset', 'Country'),
            'quantity' => Yii::t('eset', 'Quantity'),
            'purchase_type' => Yii::t('eset', 'Purchase Type'),
            'register_state' => Yii::t('eset', 'Register States'),
            'state' => Yii::t('eset', 'Status'),
            'license_type_global' => Yii::t('eset', 'License Type Global'),
            'license_type' => Yii::t('eset', 'License Type'),
            'sector' => Yii::t('eset', 'Sector'),
            'bdate' => Yii::t('eset', 'Start date'),
            'edate' => Yii::t('eset', 'Expiration'),
            'period' => Yii::t('eset', 'Period'),
            'discount_code' => Yii::t('eset', 'Discount Code'),
            'custom_discount' => Yii::t('eset', 'Discount'),
            'discount_document' => Yii::t('eset', 'Discount Document'),
            'username' => Yii::t('eset', 'Username'),
            'password' => Yii::t('eset', 'Password'),
            'epli' => Yii::t('eset', 'Epli'),
            'license_key' => Yii::t('eset', 'Serial key'),
            'public_license_key'  => Yii::t('eset', 'Public ID'),
            'common_tag' => Yii::t('eset', 'Common tag'),
            'note' => Yii::t('eset', 'Note'),
            'note_own' => Yii::t('eset', 'Our note'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'client_name'           => Yii::t('eset', 'Client name'),
            'reseller_name'         => Yii::t('eset', 'Reseller name'),
            'product_name'          => Yii::t('eset', 'Product'),
            'total_price'           => Yii::t('eset', 'Price'),
            'discountType'          => Yii::t('eset', 'Discount type'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\LicenseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\LicenseQuery(get_called_class());
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetail()
    {
        return $this->hasOne(OrderItem::className(), ['id' => 'order_detail_id'])
            ->from(['order_item' => OrderItem::tableName() ]);
    }

    public function getClient()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id'])
            ->from(['client' => UserProfile::tableName() ]);
    }

    public function getReseller()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'reseller_id'])
            ->from(['reseller' => UserProfile::tableName() ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildLicense()
    {
        return $this->hasOne(self::className(), ['id' => 'child_license_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     *
     */
    public function getParentLicense()
    {
        return $this->hasOne(self::className(), ['child_license_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['code' => 'country_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['eset_product_code' => 'product_code']);
    }

    public function getClientName()
    {
        return $this->client ? $this->client->client_name : null;
    }
    public function getResellerName()
    {
        $this->reseller ? $this->reseller->client_name : null;
    }
    public function getTotalPrice()
    {
        return $this->orderDetail ? $this->orderDetail->total_price : null;
    }

    public function getProductName()
    {
        return $this->product ? $this->product->name : $this->product_code;
    }
    public function getDiscountType()
    {
        return DiscountCodeList::getDesc($this->discount_code);
    }
    public function getEmails()
    {
        $emails = [];
        if ($this->client) {
            $emails[] = [$this->client->email => $this->client->email];
        }
        if ($this->reseller) {
            $emails[] = [$this->reseller->email => $this->reseller->email];
        }
        return $emails;
    }
    /**
     * @param $order Order
     * @param $item OrderItem
     * @return bool
     */
    public static function createFromOrderItem($order, $item)
    {
        $license = License::findOne(['order_detail_id' => $item->id]);
        if (empty($license)) {
            $license = new License();
            $proxy = new OrderItemProxy([
                'owner' => $item,
                'owner_order' => $order,
            ]);

            $license->setAttributes($proxy->getAttributes());
            if ($license->save()) {
                if ($proxy->parentLicense) {
                    $proxy->parentLicense->child_license_id = $license->id;
                    $proxy->parentLicense->state = LicenseStatus::STATUS_INACTIVE;
                    $proxy->parentLicense->save(false, ['child_license_id', 'state']);
                }
            }
        }
        return $license;
    }


    /**
     * @param $user_id
     * @param $productCode
     * @param array $condition
     * @return License
     * @internal param $product_code
     */
    public static function findForRenew($user_id, $productCode, $condition = [])
    {
        return self::find()
            ->where([
                'user_id' => $user_id,
                'child_license_id' => null,
                'register_state' => self::REGISTER_STATE_REGISTERED,
                'state' => LicenseStatus::STATUS_ACTIVE,
                'product_code' => $productCode,
            ])
            ->andWhere('edate > NOW() + INTERVAL 2 MONTH')
            ->andWhere($condition)
            ->orderBy('edate DESC')
            ->one();
    }

    public function toString()
    {
        return $this->product->getAttributeLabel('name') . ': ' . $this->product->name
        . '; ' . $this->getAttributeLabel('edate') . ': ' . Yii::$app->formatter->format($this->edate, 'date')
        . '; ' . $this->getAttributeLabel('license_key') . ':' . $this->license_key;
    }

    /**
     * @param integer $detail_id
     * @return bool
     * @throws \Exception
     */
    public static function cleanTrash($detail_id)
    {
        $license = self::findOne(['order_detail_id' => $detail_id]);
        if (!empty($license)) {
            return (self::REGISTER_STATE_UNREGISTERED == $license->register_state) && $license->delete();
        } else {
            return true;
        }
    }
}
