<?php

namespace app\theme\models;

use Yii;
use yii\helpers\Json;

/**
 * @inheritdoc
 * @property integer $eset_period
 * @property integer $eset_parent_license_id
 * @property integer $eset_purchase_type
 * @property integer $eset_sector
 * @property integer $eset_note
 * @property integer $packet
 *
 * @property License $parentLicense
 * @property Order $order
 */
class OrderItem extends \app\modules\shop\models\OrderItem
{
    private $_additional_rules = [];
    private $_attribute_params = [];
    private $_parent_license = false;

    public static $_PurchaseTypes = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                ['eset_purchase_type', 'default', 'value' => PurchaseType::PURCHASE_TYPE_NEW],
                ['eset_purchase_type', 'in', 'range' => array_keys($this->getPurchaseTypeList())],
                ['eset_purchase_type', 'required'],
                ['packet', 'default', 'value' => 1],
                ['packet', 'integer', 'max' => 1000],
                ['eset_parent_license_id', 'exist', 'targetClass' => License::className(), 'targetAttribute' => 'id'],
                ['eset_note', 'string', 'max' => 1024],
                ['eset_period', 'safe'],
            ],
            $this->_additional_rules
        );
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function beforeValidate()
    {
        $total_price = $this->total_price;
        $price_per_pcs = $this->packet > 0 ? $this->total_price / $this->packet : $this->total_price;

        $res = parent::beforeValidate();

        $this->total_price = $total_price;
        $this->price_per_pcs = $price_per_pcs;
        $this->total_price_without_discount = $total_price;
        $this->discount_amount = 0;

        return $res;
    }

    public function addRule($rule)
    {
        $this->_additional_rules[] = $rule;
    }

    public function setAttributeParams($attribute, $params)
    {
        $this->_attribute_params[$attribute] = $params;
    }

    public function getAttributeParams($attribute)
    {
        return isset($this->_attribute_params[$attribute]) ? $this->_attribute_params[$attribute] : [];
    }

    public function initAttributes()
    {
        $attributes = ProductAttribute::find()->where(['product_id' => $this->product_id])->all();
        foreach ($attributes as $attribute) {
            foreach ($attribute->params['rules'] as $rule) {
                $this->addRule($rule);
            }
            $this->setAttributeParams($attribute->attribute, $attribute->params);
        }

    }

    /**
     * @return array
     */
    public function getPurchaseTypeList()
    {
        if (static::$_PurchaseTypes === false) {
            $PurchaseType = new PurchaseType();
            static::$_PurchaseTypes = $PurchaseType->getList();
            if (UserProfile::iNormalUser()) {
                unset(static::$_PurchaseTypes[PurchaseType::PURCHASE_TYPE_NFR]);
            } elseif (Yii::$app->user->isGuest) {
                foreach (static::$_PurchaseTypes as $key => $value) {
                    if ($key == PurchaseType::PURCHASE_TYPE_NEW) {
                        continue;
                    } else {
                        unset(static::$_PurchaseTypes[$key]);
                    }
                }
            }

        }

        return static::$_PurchaseTypes;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::rules(),
            [
                'eset_period' => Yii::t('eset', 'Duration'),
                'eset_parent_license_id' => Yii::t('eset', 'Parent license'),
                'eset_purchase_type' => Yii::t('eset', 'Purchase type'),
                'eset_sector' => Yii::t('eset', 'Sector'),
                'eset_note' => Yii::t('eset', 'Additional description'),
                'product_id' => Yii::t('eset', 'Product'),
                'packet' => Yii::t('eset', 'Packet quantity'),
                'quantity' => Yii::t('eset', 'Quantity'), //Number of seats
            ]
        );
    }

    public function isShowParentLicense()
    {
        switch ($this->eset_purchase_type) {
            case PurchaseType::PURCHASE_TYPE_NEW:
            case PurchaseType::PURCHASE_TYPE_NFR:
                return false;
                break;
            case PurchaseType::PURCHASE_TYPE_RENEW:
            case PurchaseType::PURCHASE_TYPE_UPGRADE:
            case PurchaseType::PURCHASE_TYPE_ENLARGE:
            case PurchaseType::PURCHASE_TYPE_DOWNGRADE:
                return true;
                break;
            default:
                break;
        }
    }

    public function getParentLicense()
    {
        if( $this->_parent_license === false ) {
            if ($this->eset_parent_license_id) {
                $this->_parent_license = License::findOne($this->eset_parent_license_id);
            } else {
                $this->_parent_license = null;
            }
        }
        return  $this->_parent_license;
    }
    public function normalizationType()
    {
        switch ($this->eset_purchase_type) {
            case PurchaseType::PURCHASE_TYPE_NEW:       break;
            case PurchaseType::PURCHASE_TYPE_NFR:       break;
            case PurchaseType::PURCHASE_TYPE_RENEW:     $this->restoreParams(['packet', 'eset_period', 'quantity', 'product_id']); break;
            case PurchaseType::PURCHASE_TYPE_UPGRADE:   $this->restoreParams(['packet', 'product_id']); break;
            case PurchaseType::PURCHASE_TYPE_ENLARGE:   $this->restoreParams(['packet', 'eset_period', 'product_id']); break;
            case PurchaseType::PURCHASE_TYPE_DOWNGRADE: $this->restoreParams(['packet']); break;
            default: break;
        }

    }
    private function restoreParams($fields)
    {
        if (!empty($this->parentLicense) && !empty($this->parentLicense->orderDetail)) {
            $this->setAttributes($this->parentLicense->orderDetail->getAttributes($fields));
        }
    }
}
