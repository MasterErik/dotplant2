<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_payment_type}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $code
 * @property string $iban
 * @property string $swift
 * @property string $email
 * @property integer $payment_type_id
 * @property integer $partner_id
 * @property string $bdate
 * @property string $edate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $changer_id
 * @property integer $creater_id
 *
 * @property EsetBankTransaction[] $esetBankTransactions
 * @property EsetPayment[] $esetPayments
 * @property User $partner
 * @property PaymentType $paymentType
 */
class EsetPaymentType extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_payment_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'iban', 'swift', 'email', 'partner_id'], 'required'],
            [['type'], 'string'],
            [['payment_type_id', 'partner_id', 'created_at', 'updated_at', 'changer_id', 'creater_id'], 'integer'],
            [['bdate', 'edate'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['code', 'swift'], 'string', 'max' => 30],
            [['iban'], 'string', 'max' => 34],
            [['email'], 'string', 'max' => 255],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'name' => Yii::t('eset', 'Name'),
            'type' => Yii::t('eset', 'Type'),
            'code' => Yii::t('eset', 'Code'),
            'iban' => Yii::t('eset', 'Iban'),
            'swift' => Yii::t('eset', 'Swift'),
            'email' => Yii::t('eset', 'Email'),
            'payment_type_id' => Yii::t('eset', 'Payment Type ID'),
            'partner_id' => Yii::t('eset', 'Partner ID'),
            'bdate' => Yii::t('eset', 'Bdate'),
            'edate' => Yii::t('eset', 'Edate'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'changer_id' => Yii::t('eset', 'Changer ID'),
            'creater_id' => Yii::t('eset', 'Creater ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetBankTransactions()
    {
        return $this->hasMany(EsetBankTransaction::className(), ['eset_payment_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPayments()
    {
        return $this->hasMany(EsetPayment::className(), ['eset_payment_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\EsetPaymentTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EsetPaymentTypeQuery(get_called_class());
    }
}
