<?php

namespace app\theme\models;

use app\theme\module\events\CustomStageEvent;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "{{%order_stage}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_frontend
 * @property string $name_short
 * @property integer $is_initial
 * @property integer $is_buyer_stage
 * @property integer $become_non_temporary
 * @property integer $is_in_cart
 * @property integer $immutable_by_user
 * @property integer $immutable_by_manager
 * @property integer $immutable_by_assigned
 * @property string $reach_goal_ym
 * @property string $reach_goal_ga
 * @property string $event_name
 * @property string $event_stage_name
 * @property string $view
 * Relations:
 * @property OrderStageLeaf[] $nextLeafs
 * @property OrderStageLeaf[] $prevLeafs
 */
class OrderStage extends \app\modules\shop\models\OrderStage
{
    const ORDER_STATE_FINISH = 0;
    const ORDER_STATE_IN_PROCESS = 1;
    const ORDER_STATE_IN_INIT = 2;

    const STAGE_LICENSE = 'license';
    const STAGE_REGISTRATION = 'registration';
    const STAGE_PAYMENT = 'payment';
    const STAGE_CUSTOMER = 'customer';
    const STAGE_PAY = 'payment pay';

    const ORDER_FINAL = 70;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['event_stage_name'], 'string'],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'event_stage_name' => Yii::t('eset', 'event stage name'),
            ]
        );
    }
    /**
     * @return OrderStage|null
     */
    public static function getFinalStage()
    {
        return static::findOne(['immutable_by_user' => 1, 'immutable_by_manager' => 1, 'immutable_by_assigned' => 1]);
    }

    public static function getNameToStage($value) {
        return self::findOne(['name' => $value]);
    }
    /**
     * @return OrderStageLeaf[]
     */
    public function getNextLeafs()
    {
        /** @var \yii\db\ActiveQuery $record */
        $record = $this->hasMany(OrderStageLeaf::className(), ['stage_from_id' => 'id'])->addOrderBy(['sort_order' => SORT_ASC, 'id' => SORT_ASC]);
        if ($this->name == self::STAGE_CUSTOMER) {
//            if (Yii::$app->user->isGuest === true) {
//                $stage = self::getNameToStage(self::STAGE_REGISTRATION);
//            } else {
                /** @var UserProfile $user_profile */
                $user_profile = UserProfile::myProfile();

                if ($user_profile && YesNo::YES === $user_profile->auto_license) {
                    $stage = self::getNameToStage(self::STAGE_LICENSE);
                } else {
                    $stage = self::getNameToStage(self::STAGE_PAYMENT);
                }
//            }
            if ($stage) {
                $record->where(['stage_to_id' => $stage->id]);
            }
        }
        return $record;
    }

    /**
     * @return OrderStageLeaf[]
     */
    public function getPrevLeafs()
    {
        $record = $this->hasMany(OrderStageLeaf::className(), ['stage_to_id' => 'id'])->addOrderBy(['sort_order' => SORT_ASC, 'id' => SORT_ASC]);
        $stage = null;
        if ($this->name == self::STAGE_PAYMENT) {
            $stage = self::findOne(['name'=> self::STAGE_CUSTOMER]);
        }
//        if ($this->name == self::STAGE_LICENSE) {
//            $record->andFilterWhere(['stage_from_id' => 9999999]); //Empty record
//        }
        if ($stage) {
            $record->where(['stage_from_id' => $stage->id]);
        }
        return $record;
    }

    /**
     * @return int
     */
    public function getStageStatus()
    {
        if (1 === $this->is_initial) {
            return self::ORDER_STATE_IN_INIT;
        } else if (1 === ($this->immutable_by_user & $this->immutable_by_manager & $this->immutable_by_assigned)) {
            return self::ORDER_STATE_FINISH;
        } else {
            return self::ORDER_STATE_IN_PROCESS;
        }
    }

    /**
     * @return bool
     */
    public function isModify()
    {
        return !(self::ORDER_STATE_FINISH === $this->getStageStatus());
    }


    /**
     * @param $eventData
     * @param bool $stage
     * @return bool
     * @throws BadRequestHttpException
     */
    public function executeEventStage(&$eventData, $stage = false)
    {
        /** @var EventHandlers $eventClass */
        $eventClass = EventHandlers::findOne(['function_stage_name' => $this->event_name]);
        if (empty($eventClass) ) {
            throw new BadRequestHttpException(Yii::t('eset', 'Incorrect stage event:' ) . $this->event_name);
        }

        /** @var CustomStageEvent $event */
        $event = new CustomStageEvent;
        $event->setEventData($eventData);
        $handler = ($stage) ?  $eventClass->function_stage_name : $eventClass->function_name;

        if (!empty($handler)) {
            $handler = $eventClass->class_name . '::' . $handler;
            call_user_func($handler, $event);
        }

        $eventData = $event->eventData();
        return $event->getStatus();
    }
}
