<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 8.3.2016
 * Time: 15:41
 */

namespace app\theme\models;

use Yii;

class YesNo extends CustomMemoryModel
{
    public $yes_no;

    const NO = 0;
    const YES = 1;

    public function getId()
    {
        return $this->yes_no;
    }

    public function rules()
    {
        return [
            ['yes_no', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::NO, self::desc => Yii::t("eset", 'No')],
            [self::key => self::YES, self::desc => Yii::t("eset", 'Yes')],
        ];
    }
}