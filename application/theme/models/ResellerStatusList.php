<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 2.2.2016
 * Time: 15:56
 */

namespace app\theme\models;

use Yii;


class ResellerStatusList  extends CustomMemoryModel
{
    public $reseller_status;

    const RESELLER_STATUS_GOLD = 'Gold';
    const RESELLER_STATUS_SILVER = 'Silver';
    const RESELLER_STATUS_BRONZE = 'Bronze';
    const RESELLER_STATUS_REG = 'Registered';

    public function getId()
    {
        return $this->reseller_status;
    }

    public function rules()
    {
        return [
            ['reseller_status', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::RESELLER_STATUS_GOLD, self::desc => Yii::t("eset", 'Gold')],
            [self::key => self::RESELLER_STATUS_SILVER, self::desc => Yii::t("eset", 'Silver')],
            [self::key => self::RESELLER_STATUS_BRONZE, self::desc => Yii::t("eset", 'Bronze')],
            [self::key => self::RESELLER_STATUS_REG, self::desc => Yii::t("eset", 'Registered')],
        ];
    }

}