<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 11.3.2016
 * Time: 16:58
 */

namespace app\theme\models;


use Yii;

class DiscountCodeList extends CustomMemoryModel
{
    public $discount_code;

    const CODE_NO_DISCOUNT = 7;
    const CODE_CUSTOM = 0;
    const CODE_FOR_DOCUMENT = 1;

    public function getId()
    {
        return $this->discount_code;
    }

    public function rules()
    {
        return [
            ['discount_code', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::CODE_NO_DISCOUNT, self::desc => Yii::t("eset", 'No discount')],
            [self::key => self::CODE_CUSTOM, self::desc => Yii::t("eset", 'Custom discount')],
            [self::key => self::CODE_FOR_DOCUMENT, self::desc => Yii::t("eset", 'Document discount')],
        ];
    }
}