<?php

namespace app\theme\models;

use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 */
class Page extends \app\modules\page\models\Page
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => \app\theme\components\behaviors\TranslateModelBehavior::className(),
                'languages' => \app\theme\components\MultiLangHelper::getLanguages(),
                'attributes' => ['name']
            ]]
        );
    }

    public static function getList()
    {
        return ArrayHelper::map(Page::find()->where('published=1')->orderBy('name')->all(), 'slug_compiled', 'name');
    }
}
