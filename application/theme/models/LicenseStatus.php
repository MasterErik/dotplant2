<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 27.04.2015
 * Time: 17:35
 */

namespace app\theme\models;

use Yii;
use yii\helpers\ArrayHelper;

class LicenseStatus extends CustomMemoryModel {

    public $state;

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';
    const STATUS_CANCELED = 'Canceled';
    const STATUS_SUSPENDED = 'Suspended';

    public function getId()
    {
        return $this->state;
    }
    public function rules()
    {
        return [
            ['state', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }
    protected function fullData()
    {
        return [
            [self::key => static::STATUS_ACTIVE,   self::desc => Yii::t("eset", 'Active')],
            [self::key => static::STATUS_INACTIVE, self::desc => Yii::t("eset", 'Inactive')],
            [self::key => static::STATUS_CANCELED, self::desc => Yii::t("eset", 'Canceled')],
            [self::key => static::STATUS_SUSPENDED, self::desc => Yii::t("eset", 'Suspended')],
        ];
    }

    public static function getIcon($key)
    {
        switch ($key) {
            case self::STATUS_ACTIVE:
                return 'text-success glyphicon glyphicon-ok-sign fa-lg';
            case self::STATUS_INACTIVE:
                return 'glyphicon glyphicon-remove-sign fa-lg'; //text-warning
            case self::STATUS_CANCELED:
                return 'text-danger glyphicon glyphicon-minus-sign fa-lg';
            case self::STATUS_SUSPENDED:
                return 'text-danger glyphicon glyphicon-question-sign fa-lg';
        }
    }
}