<?php

namespace app\theme\models;

use app\theme\module\helpers\SequencesHelper;
use Yii;

/**
 * This is the model class for table "{{%eset_invoice}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $number
 * @property integer $user_id
 * @property string $type
 * @property string $tax
 * @property string $sum
 * @property string $iban
 * @property string $swift
 * @property integer $viitenumber
 * @property string $bill_to
 * @property string $due_date
 * @property integer $created_at
 *
 * @property User $user
 * @property UserProfile $userProfile
 * @property User $partner
 * @property UserProfile $partnerProfile
 * @property InvoiceDetail[] $invoiceDetails
 * @property InvoiceOnlinePay[] $invoiceOnlinePays
 */
class Invoice extends BaseModel
{
    const TYPE_PAYMENT = 'payment';
    const TYPE_PREPAYMENT = 'prepayment';
    const TYPE_INVOICE = 'invoice';
    const TYPE_CREDIT = 'credit';

    public static function getInvoiceTypes()
    {
        return [
            self::TYPE_PAYMENT => Yii::t('eset', 'Payment'),
            self::TYPE_PREPAYMENT => Yii::t('eset', 'Prepayment'),
            self::TYPE_INVOICE => Yii::t('eset', 'Invoice'),
            self::TYPE_CREDIT => Yii::t('eset', 'Credit'),
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_invoice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'number', 'user_id', 'tax', 'sum', 'iban', 'bill_to', 'due_date'], 'required'],
            [['partner_id', 'user_id', 'viitenumber', 'created_at'], 'integer'],
            [['type'], 'string'],
            [['tax', 'sum'], 'number'],
            [['due_date'], 'safe'],
            [['number', 'swift'], 'string', 'max' => 30],
            [['iban'], 'string', 'max' => 34],
            [['bill_to'], 'string', 'max' => 1024],
            [['number'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'partner_id' => Yii::t('eset', 'Partner'),
            'number' => Yii::t('eset', 'Number'),
            'user_id' => Yii::t('eset', 'Client'),
            'type' => Yii::t('eset', 'Type'),
            'tax' => Yii::t('eset', 'Tax'),
            'sum' => Yii::t('eset', 'Sum'),
            'iban' => Yii::t('eset', 'Iban'),
            'swift' => Yii::t('eset', 'Swift'),
            'viitenumber' => Yii::t('eset', 'Viitenumber'),
            'bill_to' => Yii::t('eset', 'Bill To'),
            'due_date' => Yii::t('eset', 'Due Date'),
            'created_at' => Yii::t('eset', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceDetails()
    {
        return $this->hasMany(InvoiceDetail::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceOnlinePays()
    {
        return $this->hasMany(InvoiceOnlinePay::className(), ['invoice_id' => 'id']);
    }
    /**
     * @inheritdoc
     * @return \app\theme\models\query\InvoiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\InvoiceQuery(get_called_class());
    }

    /**
     * @param $user_profile UserProfile
     * @param string $invoice_type
     * @return Invoice
     */
    public static function create($user_profile, $invoice_type = self::TYPE_PREPAYMENT)
    {
        $partner = UserProfile::getParentPartner($user_profile->user_id);

        $invoice = new Invoice();

        $invoice->partner_id = $partner->user_id;
        $invoice->user_id = $user_profile->user_id;

        $invoice->tax = 0;
        $invoice->sum = 0;


        $invoice->iban = Yii::$app->getModule('financial')->iban;
        $invoice->swift = ''; //Currently there are not using

        $invoice->bill_to = $user_profile->client_name;
        $invoice->due_date = date('Y-m-d H:i:s', strtotime('now + 1 month'));

        $invoice->type = $invoice_type;
        $invoice->number = SequencesHelper::getNext($invoice->type);

        $genNumber = SequencesHelper::getNext(SequencesHelper::COUNTER_VIITENUMBER);
        $invoice->viitenumber = self::genViitenumber($genNumber);

        $invoice->save();
        return $invoice;
    }

    /**
     * @link http://www.pangaliit.ee/en/settlements-and-standards/example-of-the-use-of-method-7-3-1
     * @param $nr
     * @return string
     */
    public static function genViitenumber($nr)
    {
        $nr = (string)$nr;
        $kaal = array(7, 3, 1);
        $sl = $st = strlen($nr);
        $total = 0;
        while ($sl > 0 and substr($nr, --$sl, 1) >= '0') {
            $total += substr($nr, ($st - 1) - $sl, 1) * $kaal[($sl % 3)];
        }
        $kontrollnr = ((ceil(($total / 10)) * 10) - $total);

        return $nr . $kontrollnr;
    }

    /**
     * @param $order Order
     * @return InvoiceDetail
     */
    public function addOrder($order)
    {
        $invoice_detail = new InvoiceDetail();
        $invoice_detail->invoice_id = $this->id;
        $invoice_detail->order_id = $order->id;
        $invoice_detail->type = 'order';
        $invoice_detail->tax = $order->eset_tax;
        $invoice_detail->sum = $order->total_price;

        if( $invoice_detail->save() ) {
            $this->sum += $invoice_detail->sum;
            $this->tax += $invoice_detail->tax;
            $this->save();
        }

        return $invoice_detail;
    }

    /**
     * @param $order_item OrderItem
     * @param $discount Discount
     * @param $type string
     * @return InvoiceDetail
     */
    public function addDiscount($discount, $type, $order_item)
    {
        $invoice_detail = new InvoiceDetail();
        $invoice_detail->invoice_id = $this->id;
        $invoice_detail->order_id = $order_item->order_id;
        $invoice_detail->discount_id = $discount->id;
        $invoice_detail->type = $type;
        $invoice_detail->tax = 0;
        $invoice_detail->sum = $order_item->total_price * ($discount->cost/100) * ($discount->sign == '-' ? -1 : 1);

        if( $invoice_detail->save() ) {
            $this->sum += $invoice_detail->sum;
            $this->save();
        }

        return $invoice_detail;
    }

    public function sumAsSpellOut($sum)
    {
        // todo Язык
        $integer_part = floor($sum);
        $fractional_part = round(($sum - $integer_part) * 100);
        $language = Yii::$app->language;
        Yii::$app->language = 'et';
        $str = Yii::$app->formatter->asSpellout($integer_part) . ' eurot ja ' . $fractional_part . ' senti';
        Yii::$app->language = $language;
        return $str;
    }
}
