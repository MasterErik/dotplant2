<?php

namespace app\theme\models;

use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 * @property string $code
 * @property integer $tax
 *
 */
class Country extends \app\models\Country
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => \app\theme\components\behaviors\TranslateModelBehavior::className(),
                'languages' => \app\theme\components\MultiLangHelper::getLanguages(),
                'attributes' => ['name']
            ]]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                ['code', 'integer'],
                ['tax', 'number'],
                ['tax', 'default', 'value' => 20],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'code' => \Yii::t('eset', 'Code'),
                'tax' => \Yii::t('eset', 'Tax'),
            ]
        );
    }

    public static function getList($id = 'id')
    {
        return ArrayHelper::map(self::find()->orderBy('name')->all(), $id, 'name');
    }

    public static function getDropDownList($skip = false, $id = 'id')
    {
        if ($skip) {
            return ArrayHelper::merge([''=>''], self::getList($id));
        } else {
            return self::getList();
        }
    }


}
