<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 22.1.2016
 * Time: 09:33
 */

namespace app\theme\models;


/**
 * Class EventHandlers
 * @package app\theme\models
 * This is the model class for table "{{%eset_event_handlers}}".
 *
 * @property integer $id
 * @property string $class_name
 * @property string $function_name
 * @property string $function_stage_name
 *
 */
class EventHandlers extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_event_handlers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['class_name', 'function_name', 'function_stage_name'], 'required'],
            [['class_name', 'function_name', 'function_stage_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'class_name' => Yii::t('app', 'Handler Class Name'),
            'function_name' => Yii::t('app', 'Handler Function Name'),
            'function_stage_name' => Yii::t('app', 'Handler Function Stage Name'),
        ];
    }

}