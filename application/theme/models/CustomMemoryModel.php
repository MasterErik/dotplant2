<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 6.10.2015
 * Time: 11:57
 */

namespace app\theme\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class CustomMemoryModel extends Model
{
    private $data;

    const key = 'Name';
    const desc = 'Desc';

    public function __construct()
    {
        $this->data = $this->fullData();
        parent::__construct();
    }

    protected function  fullData()
    {
    }

    public function getId()
    {
    }

    public function all()
    {
        return ArrayHelper::index($this->data, self::key);
    }

    public function validateExists($attribute, $params)
    {
        if (!$this->find($this->$attribute)) {
            $message = isset($params['message']) ? $params['message'] : Yii::t('yii', $attribute . ' does not exist');
            $this->addError($attribute, $message);
        }
    }

    public function find($value, $attribute = self::key)
    {
        $column = ArrayHelper::getColumn($this->data, $attribute);
        return in_array($value, $column);
    }

    public function getList()
    {
        return ArrayHelper::map($this->data, self::key, self::desc);
    }

    public static function getKey()
    {
        $class = new static();
        return ArrayHelper::getColumn($class->data, self::key);
    }
    public static function getEnum()
    {
        return "enum('" . implode("','", self::getKey()) . "') ";
    }
    public static function getDesc($key)
    {
        if (is_numeric($key)) {
            $class = new static();
            $index = array_search($key, $class->data);
            return $class->data[$index][self::desc];
        } else {
            return null;
        }
    }
    public static function getDropDownList($skip = false)
    {
        /** @var CustomMemoryModel $class */
        $class = new static();
        if ($skip) {
            return ArrayHelper::merge([''=>''], $class->getList());
        } else {
            return $class->getList();
        }
    }

}