<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_invoice_detail}}".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $order_id
 * @property integer $discount_id
 * @property string $type
 * @property string $tax
 * @property string $sum
 *
 * @property EsetDiscount $discount
 * @property EsetInvoice $invoice
 * @property Order $order
 */
class InvoiceDetail extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_invoice_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'tax', 'sum'], 'required'],
            [['invoice_id', 'order_id', 'discount_id'], 'integer'],
            [['type'], 'string'],
            [['tax', 'sum'], 'number'],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discount::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'invoice_id' => Yii::t('eset', 'Invoice ID'),
            'order_id' => Yii::t('eset', 'Order ID'),
            'discount_id' => Yii::t('eset', 'Discount ID'),
            'type' => Yii::t('eset', 'Type'),
            'tax' => Yii::t('eset', 'Tax'),
            'sum' => Yii::t('eset', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\InvoiceDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\InvoiceDetailQuery(get_called_class());
    }
}
