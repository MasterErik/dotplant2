<?php

namespace app\theme\models;

use app\modules\user\models\UserService;
use app\theme\components\validators\ReferenceValidator;
use app\theme\module\helpers\MailHelper;
use Yii;
use yii\base\Model;
use app\theme\components\validators\ConfirmEmailCodeValidator;
use yii\base\Security;

/**
 * RegistrationForm is the model behind the login form.
 */
class RegistrationForm extends Model
{
    public $username;
    public $email;
    public $confirmEmailCode;
    public $password;
    public $confirmPassword;
    public $first_name;
    public $last_name;
    public $client_name;

    public $position;
    public $country_id;
    public $locale;
    public $address;
    public $comment;
    public $page;
    public $contact;
    public $url;
    public $telefon;
    public $invoice_mode;
    public $auto_license;
    public $gender;
    public $birthday;
    public $parent_id;
    public $user_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [


            ['password', 'string', 'min' => 8],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
            [['password', 'confirmPassword'], 'required', 'when' => function (RegistrationForm $model) {
                return Yii::$app->user->isGuest;
            }],

            [['first_name', 'last_name', 'client_name'], 'string', 'max' => 30],
            [['first_name', 'last_name', 'client_name'], 'filter', 'filter' => 'trim'],
/*
 * 'when' => function (RegistrationForm $model) {
                    return empty($model->user_id) || (0 == $model->user_id);
                },
 */
            [['username','first_name', 'last_name', 'email'], 'required',
                'whenClient' =>  "function (attribute, value) {
                    return $('#' + attribute.id).is(':visible');
                }",
            ],
            ['client_name', 'required', 'when' => function (RegistrationForm $model) {
                    return in_array($model->position, [Position::POSITION_RESELLER, Position::POSITION_BUSINESS]);
                },
                'whenClient' =>  "function (attribute, value) {
                    return $('#' + attribute.id).is(':visible') && ( $('#registrationform-position').val() == '" .Position::POSITION_BUSINESS. "' || $('#position').value == '" . Position::POSITION_RESELLER. "');
                }",
            ],

            [['country_id', 'auto_license', 'gender'], 'integer'],
            ['country_id', 'default', 'value' => 3],

            [['invoice_mode'], 'string'],
            [['birthday'], 'safe'],
            [['locale'], 'string', 'max' => 32],
            [['address'], 'string', 'max' => 1024],
            [['comment'], 'string', 'max' => 2048],
            [['page', 'contact', 'url'], 'string', 'max' => 255],

            ['url', 'url'],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('eset', 'This username has already been taken.')],
            ['username', 'string', 'min' => 3, 'max' => 255],
            ['username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Yii::t('eset', '{attribute} can contain only letters, numbers, and "_"')],
/*
            ['email', 'required', 'when' => function (RegistrationForm $model) {
                return Yii::$app->user->isGuest;
            }],
*/
            ['email', 'email', 'checkDNS' => true],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('eset', 'This email address has already been taken.')],

            ['telefon', 'filter', 'filter' => 'trim'],
            ['telefon', 'string', 'max' => 30],
            ['telefon', 'default', 'value' => null],
            ['telefon', 'unique', 'targetClass' => UserProfile::className(), 'message' => Yii::t('eset', 'This phone has already been taken.')],

            ['position', 'required'],
            ['position', ReferenceValidator::className(), 'targetClass' => Position::className()],

            ['auto_license', 'default', 'value' => YesNo::NO],
            ['auto_license', ReferenceValidator::className(), 'targetClass' => YesNo::className(), 'targetAttribute' => 'yes_no'],

            ['invoice_mode', 'default', 'value' => InvoiceMode::INVOICE_IMMEDIATELY],
            ['invoice_mode', ReferenceValidator::className(), 'targetClass' => InvoiceMode::className()],

            ['gender', ReferenceValidator::className(), 'targetClass' => Gender::className()],

            ['confirmEmailCode', 'required', 'except' => 'default'],
            ['confirmEmailCode', ConfirmEmailCodeValidator::className(), 'emailAttribute' => 'email', 'except' => 'default', 'validator' => function($email, $code) {
                return EmailConfirmCode::validateEmail($email, $code);
            }],

        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['cart'] = ['email', 'confirmEmailCode', 'password', 'confirmPassword', 'position', 'country_id', 'telefon'];

        return $scenarios;
    }


    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'E-mail'),
            'confirmEmailCode' => Yii::t('eset', 'Verification code'),
            'password' => Yii::t('app', 'Password'),
            'confirmPassword' => Yii::t('app', 'Confirm Password'),
            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),

            'client_name' => Yii::t('eset', 'Business name'),
            'position' => Yii::t('eset', 'Position'),
            'user_id' => Yii::t('eset', 'User ID'),
            'country_id' => Yii::t('eset', 'Country'),
            'locale' => Yii::t('eset', 'Locale'),
            'address' => Yii::t('eset', 'Address'),
            'comment' => Yii::t('eset', 'Comment'),
            'page' => Yii::t('eset', 'Page'),
            'contact' => Yii::t('eset', 'Contact'),
            'url' => Yii::t('eset', 'Website'),
            'telefon' => Yii::t('eset', 'Phone'),
            'invoice_mode' => Yii::t('eset', 'Invoce Mode'),
            'auto_license' => Yii::t('eset', 'Auto License'),
            'gender' => Yii::t('eset', 'Gender'),
            'birthday' => Yii::t('eset', 'Birthday'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),

        ];
    }

    public function init()
    {
        parent::init();
        $this->position = Position::POSITION_CLIENT;
    }

    public function buildClientName()
    {
        if (empty($this->client_name)) {
            $this->client_name = trim($this->first_name . ' ' . $this->last_name);
        }
    }
    /**
     * @return UserProfile
     * @throws \Exception
     */
    public function getParentReseller(){
        if (Yii::$app->user->isGuest) {
            return UserProfile::getParentReseller();
        } else {
            return UserProfile::getParentReseller(Yii::$app->user->id);
        }
    }
    /**
     * Registration user and login
     * @return bool
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        $email_is_valid = $this->confirmEmailCode && EmailConfirmCode::validateEmail($this->email, $this->confirmEmailCode);

        $transaction = Yii::$app->db->beginTransaction();

        //$user = new \app\modules\user\models\User(['scenario' => 'signup']);

        $user = new User(['scenario' => 'signup']);

        $this->passwordGen();
        $user->setAttributes(
            [
                'username' => $this->username,
                'password' => $this->password,
                'email' => $this->email,
                'status' => (UserProfile::iReseller() || $email_is_valid) ? User::STATUS_ACTIVE : User::STATUS_EMAIL_NOT_CONFIRMED,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name
            ]
        );

        $user->generateAuthKey();

        if (!$user->save()) {
            $this->addErrors($user->getErrors());
        }

        $this->parent_id = $this->getParentReseller()->user_id;

        $user_profile = new UserProfile();
        $user_profile->setAttributes([
            'user_id' => $user->id,
            'parent_id' => $this->parent_id,
            'position' => $this->position,
            'client_name' => $this->buildClientName(),
            'country_id' => $this->country_id,
            'locale' => $this->locale,
            'address' => $this->address,
            'comment' => $this->comment,
            'page' => $this->page,
            'contact' => $this->contact,
            'url' => $this->url,
            'telefon' => $this->telefon,
            'invoice_mode' => $this->invoice_mode,
            'auto_license' => $this->auto_license,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'confirm_registration_key' => $email_is_valid ? '' : null
        ]);

        if (!$user_profile->save()) {
            $this->addErrors($user_profile->getErrors());
        }

        if ($this->hasErrors()) {
            $transaction->rollBack();
            return false;
        }

        $transaction->commit();

        if( $this->confirmEmailCode && EmailConfirmCode::validateEmail($this->email, $this->confirmEmailCode)) {

            $user = User::findOne($user->id);
            if ($user && Yii::$app->user->login($user, 0)) {
                \Yii::$app->session->setFlash('success', Yii::t('eset', 'Registration completed'));
            } else {
                \Yii::$app->session->setFlash('error', Yii::t('eset', 'Registration failed'));
            }

        } else {
            $this->sendConfirmRegistration($user, $user_profile);
        }


        return true;
    }

    private function sendConfirmRegistration($user, $user_profile)
    {
        if ($user->email) {
            MailHelper::Send($user->email, '@app/theme/mail/confirm_email',
                [
                    'site_url' => \Yii::$app->urlManager->createAbsoluteUrl(''),
                    'confirm_url' => \Yii::$app->urlManager->createAbsoluteUrl(['eset/user/user/confirm', 'code' => $user_profile->confirm_registration_key]),
                    'password' => $this->password,
                    'username' => $user->username,
                    'client_name' => $user_profile->client_name,
                ],
                \Yii::t('eset', \Yii::$app->name) . ': ' . \Yii::t('eset', 'confirm registration')
            );
        }
    }
    public function signupService($serviceType, $serviceId)
    {
        if (!$this->validate()) {
            return false;
        }
        $user = $this->getUser();
        if ($user !== null) {
            $this->addError('username', 'Choose other name');
            return false;
        }
        $user = new \app\modules\user\models\User(['scenario' => 'signup']);
        $user->setAttributes(
            [
                'username' => $this->username,
                'email' => $this->email,
                'password' => $this->password,
            ]
        );
        if (!$user->save()) {
            return false;
        }
        $userService = new UserService();
        $userService->setAttributes(
            [
                'user_id' => $user->id,
                'service_type' => $serviceType,
                'service_id' => $serviceId,
            ]
        );
        $userService->save();
        Yii::$app->user->login($user, 0);
        return true;
    }

    public function passwordGen() {
        if (empty($this->password)) {
            $chars = '123456789aabcdeefghiijkmnoopqrstuuvwxyzAABCDEEFGHIIJKLMNPQRSTUUVWXYYZ';
            $this->password = '';
            srand((double)microtime()*1000000);
            while (strlen($this->password) < 8)
                $this->password .= substr($chars, (rand()%(strlen($chars))), 1);
        }
        return $this->password;
    }

    public function FastSaveUser()
    {
        $security = new Security();
        $this->password = $security->generateRandomString();

        $this->username = $this->email;
        $this->locale = Yii::$app->language;
        $this->buildClientName();

        $reseller = $this->getParentReseller();
        $this->country_id = $reseller->country_id;
        $this->parent_id = $reseller->user_id;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User(['scenario' => 'adminSignup']);
            $user->setAttributes($this->getAttributes([
                'username', 'password', 'email', 'first_name', 'last_name'
            ]));
            $user->status = User::STATUS_ACTIVE;
            $user->generateAuthKey();
            if (!$user->save()) {
                $this->addErrors($user->getErrors());
                return null;
            }

            $this->user_id = $user->id;


            $user_profile = new UserProfile();
            $user_profile->setAttributes($this->getAttributes([
                'user_id', 'parent_id', 'position', 'client_name', 'country_id'
            ]));

            if (!$user_profile->save()) {
                $this->addErrors($user_profile->getErrors());
                return null;
            }
            $this->sendConfirmRegistration($user, $user_profile);
        } catch (Exception $e) {
            $this->addError($e->getMessage());
            $transaction->rollBack();
            return null;
        } finally {
            if (!empty($this->getErrors())) {
                $transaction->rollBack();
                return null;
            } else {
                $transaction->commit();
                return $user;
            }
        }
    }
    /**
     * @return User|null|static
     */
    public function AutoRegistration()
    {
        /** @var User $user */
        $user = User::findOne(['email' => $this->email]);
        if (empty($user)) {
            $user = $this->FastSaveUser();
            if (!empty($user)) {

            }
        }
        return $user;
    }

}
