<?php

namespace app\theme\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\ResellerStatus;

/**
 * ResellerStatusSearch represents the model behind the search form about `app\theme\models\ResellerStatus`.
 */
class ResellerStatusSearch extends ResellerStatus
{
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id', 'created_at'], 'integer'],
            [['reseller_status', 'date_from', 'date_to'], 'safe'],
            [['discount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ResellerStatus::find()->my();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
            'discount' => $this->discount,
            'reseller_status' => $this->reseller_status,
        ]);

        if($this->date_from) {
            $query->andFilterWhere(['>=', 'created_at', strtotime($this->date_from . ' 00:00:00')]);
        }
        if($this->date_to) {
            $query->andFilterWhere(['<=', 'created_at', strtotime($this->date_to . ' 23:59:59')]);
        }

        return $dataProvider;
    }
}
