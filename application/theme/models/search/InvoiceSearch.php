<?php

namespace app\theme\models\search;

use app\theme\models\EsetPayment;
use app\theme\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\Invoice;

/**
 * InvoiceSearch represents the model behind the search form about `app\theme\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    public $date_range;
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'user_id', 'viitenumber', 'created_at'], 'integer'],
            [['type', 'iban', 'swift', 'due_date', 'date_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find()->my();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $this->parseDateRange();

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
            'user_id' => $this->user_id,
        ]);

        if($this->date_from) {
            $query->andWhere('created_at >= :date_from', [':date_from' => $this->date_from . ' 00:00:00']);
        }

        if($this->date_to) {
            $query->andWhere('created_at <= :date_to', [':date_to' => $this->date_to . ' 23:59:59']);
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPayments($params)
    {
        $query = EsetPayment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $this->parseDateRange();

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
        ]);

        if($this->date_from) {
            $query->andWhere('created_at >= :date_from', [':date_from' => $this->date_from . ' 00:00:00']);
        }

        if($this->date_to) {
            $query->andWhere('created_at <= :date_to', [':date_to' => $this->date_to . ' 23:59:59']);
        }

        return $dataProvider;
    }


    public function getPartners()
    {
        $items = [];

        $profiles = UserProfile::find()->partners()->all();
        foreach( $profiles as $profile ) {
            $items[$profile->user_id] = $profile->client_name;
        }

        return $items;
    }

    private function parseDateRange()
    {
        if( $this->date_range ) {
            list($this->date_from, $this->date_to) = explode(' - ', $this->date_range);
            $this->date_from = trim($this->date_from);
            $this->date_to = trim($this->date_to);
        } else {
            $this->date_from = null;
            $this->date_to = null;
        }
    }

    public function getTypes()
    {
        return Invoice::getInvoiceTypes();
    }


}
