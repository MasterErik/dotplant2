<?php

namespace app\theme\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\InvoiceDetail;

/**
 * InvoiceDetailSearch represents the model behind the search form about `app\theme\models\InvoiceDetail`.
 */
class InvoiceDetailSearch extends InvoiceDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'order_id', 'discount_id'], 'integer'],
            [['type'], 'safe'],
            [['tax', 'sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvoiceDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'order_id' => $this->order_id,
            'discount_id' => $this->discount_id,
            'tax' => $this->tax,
            'sum' => $this->sum,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
