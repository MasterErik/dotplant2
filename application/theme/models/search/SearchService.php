<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 8.3.2016
 * Time: 15:08
 */
namespace app\theme\models\search;

use app\theme\module\helpers\DateUtils;
use Yii;
use yii\db\ActiveQuery;

trait SearchService
{

    private function separate($value)
    {
        $value = str_replace('+', '', $value);
        return explode(DateUtils::$DATE_RANGE_SEPARATOR, $value);

    }
    private function combine($value1, $value2)
    {
        return  $value1 . DateUtils::$DATE_RANGE_SEPARATOR . $value2;
    }
    private function formatItem(&$item, $format, $setTime = null)
    {
        if (isset($item)) {
            if (!empty($setTime)) {
                $item = DateUtils::formatDate($item, $format, $setTime);
            } else {
                $item = DateUtils::formatDateTime($item, $format);
            }
        }
    }

    /**
     * @param $range_date string
     * @return array|null
     */
    private function separateDate($range_date)
    {
        if( !empty($range_date) ) {
            $range = $this->separate($range_date);
        } else {
            $range = null;
        }
        return $range;
    }
    public function addDateFilter($query, $field, $value)
    {
        if (!empty($value)) {
            $this->addRangeDateFilter($query, $field, $this->combine($value, $value));
        }
    }

    /**
     * @param $query ActiveQuery
     * @param $field string
     * @param $range_date string
     * @param int $format
     * @param bool $clearTime
     */
    public function addRangeDateFilter($query, $field, $range_date, $format = DateUtils::FMT_TIMESTAMP, $clearTime = true)
    {
        $range = $this->separateDate($range_date, $format, $clearTime);
        $this->formatItem($range[0], $format, $clearTime ? DateUtils::$TIME_FROM : null);
        $this->formatItem($range[1], $format, $clearTime ? DateUtils::$TIME_TO : null);

        if(isset($range[0])) {
            $query->andFilterWhere(['>=', $field, $range[0]]);
        }
        if(isset($range[1])) {
            $query->andFilterWhere(['<=', $field, $range[1]]);
        }
    }
}