<?php

namespace app\theme\models\search;


use app\theme\models\query\UsersFilter;
use app\theme\models\ResellerStatus;
use app\theme\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\theme\models\UserProfile`.
 */
class UserProfileSearch extends UserProfile
{

    public $date_from;
    public $date_to;

    public $username;
    public $email;
    public $status;
    public $create_time;
    public $update_time;
    public $first_name;
    public $last_name;

    use UsersFilter;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'parent_id', 'created_at', 'updated_at', 'birthday'], 'integer'],

            [['client_name'], 'string'],
            [['position', 'invoice_mode', 'auto_license', 'reseller_status_id', 'gender', 'birthday', 'gender', 'date_from', 'date_to'], 'safe'],
            [['telefon', 'locale'], 'string'],

            [['username', 'email', 'status', 'create_time', 'update_time', 'first_name', 'last_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::myUsers(self::className()); //::find();


        $query->joinWith(['user']);
        if ($this->reseller_status_id) {
            $query->innerJoin(['resellerStatus']);
        } else {
            $query->joinWith(['resellerStatus']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => [
//                'pagesize' => 25,
//            ],
        ]);

        $dataProvider->sort->attributes['username'] = ['asc' => ['user.username' => SORT_ASC], 'desc' => ['user.username' => SORT_DESC]];
        $dataProvider->sort->attributes['email'] = ['asc' => ['user.email' => SORT_ASC], 'desc' => ['user.email' => SORT_DESC]];
        $dataProvider->sort->attributes['status'] = ['asc' => ['user.username' => SORT_ASC], 'desc' => ['user.username' => SORT_DESC]];
        $dataProvider->sort->attributes['create_time'] = ['asc' => ['user.create_time' => SORT_ASC], 'desc' => ['user.create_time' => SORT_DESC]];
//        $dataProvider->sort->attributes['update_time'] = ['asc' => ['user.update_time' => SORT_ASC], 'desc' => ['user.update_time' => SORT_DESC]];
//        $dataProvider->sort->attributes['first_name'] = ['asc' => ['user.first_name' => SORT_ASC], 'desc' => ['user.first_name' => SORT_DESC]];
//        $dataProvider->sort->attributes['last_name'] = ['asc' => ['user.last_name' => SORT_ASC], 'desc' => ['user.last_name' => SORT_DESC]];


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($this->date_from) {
            $query->andFilterWhere(['>=', 'user.create_time', strtotime($this->date_from . ' 00:00:00')]);
        }
        if($this->date_to) {
            $query->andFilterWhere(['<=', 'user.create_time', strtotime($this->date_to . ' 23:59:59')]);
        }

        $query->andFilterWhere([
            'position' => $this->position,
            'status' => $this->status,
            ResellerStatus::tableName() .'.reseller_status' => $this->reseller_status_id,
            'auto_license' => $this->auto_license,
        ]);

        $query->andFilterWhere(['like', 'user.username', $this->username])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'telefon', $this->telefon])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
