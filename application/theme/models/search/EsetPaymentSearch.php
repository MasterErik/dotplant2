<?php

namespace app\theme\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\EsetPayment;

/**
 * EsetPaymentSearch represents the model behind the search form about `app\theme\models\EsetPayment`.
 */
class EsetPaymentSearch extends EsetPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id', 'user_id', 'eset_payment_type_id', 'viitenumber'], 'integer'],
            [['type', 'created_at'], 'safe'],
            [['summa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EsetPayment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
            'user_id' => $this->user_id,
            'eset_payment_type_id' => $this->eset_payment_type_id,
            'viitenumber' => $this->viitenumber,
            'summa' => $this->summa,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
