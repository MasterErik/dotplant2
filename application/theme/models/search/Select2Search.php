<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 25.09.2015
 * Time: 10:37
 */

namespace app\theme\models\search;

use app\theme\models\Product;
use yii\base\Model;
use yii\db\ActiveRecord;

class Select2Search extends Model
{
    const PAGE_SIZE = 15;

    public $search = '';

    public function attributeLabels()
    {
        return [
            'search' => \Yii::t('app', 'Do Search') . '...'
        ];
    }

    public function rules()
    {
        return [
            ['search', 'string', 'min' => 3, 'skipOnEmpty' => false],
        ];
    }

    /**
     * @param ActiveRecord|Product $model
     * @param String $search
     * @param integer $page
     * @param String $key
     * @return mixed
     */
    public static function getData($model, $search, $page, $key)
    {
        $page = $page ? (int) $page : 1;
        $query = $model::find()->select2($key)->addParams([':search' => '%' . $search . '%']);

        $result['items'] = $query
            ->limit(self::PAGE_SIZE)
            ->offset(($page-1) * self::PAGE_SIZE)
            ->asArray()
            ->all();
        $result['total_count'] = $query->count();

        return $result;
    }
}