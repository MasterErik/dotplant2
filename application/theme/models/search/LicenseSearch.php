<?php

namespace app\theme\models\search;

use app\theme\models\YesNo;
use app\theme\module\helpers\DateUtils;
use DateTime;
use DateTimeImmutable;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\License;
use app\theme\models\query\UsersFilter;
use yii\db\ActiveQuery;

/**
 * LicenseSearch represents the model behind the search form about `app\theme\models\License`.
 */
class LicenseSearch extends License
{
    use SearchService;

    public $search_bdate_range;
    public $search_edate_range;
    public $search_purchase_type;
    public $search_child;
    public $search_user_id;
    public $search_reseller_id;


    public $client_name;
    public $reseller_name;
    public $product_name;
    public $total_price;

    //public $discountType;

    use UsersFilter;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_detail_id', 'user_id', 'reseller_id', 'country_code', 'quantity'], 'integer'],
            [['product_code', 'purchase_type', 'register_state', 'state', 'custom_discount', 'discount_code', 'bdate', 'edate', 'period',
                'note', 'username', 'password', 'epli', 'license_key', 'public_license_key'], 'safe'],
            [['note_own'], 'string', 'max' => 255],

            [['search_bdate_range', 'search_edate_range', 'search_purchase_type', 'search_user_id', 'search_child', 'search_reseller_id',
                'created_at',], 'safe'],
            [['client_name', 'reseller_name', 'total_price', 'product_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'search_bdate_range'    => Yii::t('eset', 'Begin date'),
                'search_edate_range'    => Yii::t('eset', 'Expiration date'),
                'search_user_id'        => Yii::t('eset', 'Client name'),
                'search_reseller_id'    => Yii::t('eset', 'Reseller name'),
                'search_child'          => Yii::t('eset', 'Is child'),
            ]
        );
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var ActiveQuery $query */
        $query = self::myUsers(self::className(), 'client');
        $query->joinWith(['client', 'reseller', 'orderDetail', 'product']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['client_name'] = ['asc' => ['client.client_name' => SORT_ASC], 'desc' => ['client.client_name' => SORT_DESC]];
        $dataProvider->sort->attributes['reseller_name'] = ['asc' => ['reseller.client_name' => SORT_ASC], 'desc' => ['reseller.client_name' => SORT_DESC]];
        $dataProvider->sort->attributes['product_name'] = ['asc' => ['product.name' => SORT_ASC], 'desc' => ['product.name' => SORT_DESC]];
        $dataProvider->sort->attributes['total_price'] = ['asc' => ['order_item.total_price' => SORT_ASC], 'desc' => ['order_item.total_price' => SORT_DESC]];

        $tbl = License::tableName() . '.';

        $this->addRangeDateFilter($query, 'bdate', $this->search_bdate_range);
        $this->addRangeDateFilter($query, 'edate', $this->search_edate_range);
        $this->addDateFilter($query, 'bdate', $this->bdate);
        $this->addDateFilter($query, 'edate', $this->edate);
        $this->addRangeDateFilter($query, $tbl . 'created_at', $this->created_at, DateUtils::FMT_INTEGER, false);

        $query->andFilterWhere([
            'id' => $this->id,
            'order_detail_id' => $this->order_detail_id,
            'reseller_id' => $this->search_reseller_id,
            $tbl . 'user_id' => $this->search_user_id,
            'product_code' => $this->product_name,
            'country_code' => $this->country_code,
            'purchase_type' => $this->purchase_type,
            'period' => $this->period,
            'quantity' => $this->quantity,
            'custom_discount' => $this->custom_discount,
            'discount_code' =>  $this->discount_code,
            'state' => $this->state,
            'username' => $this->username,
            'password' => $this->password,
            'license_key' => $this->license_key,
            'public_license_key' => $this->public_license_key,
            'epli' => $this->epli,
        ]);
/*
        if($this->created_at) {
            $query->andFilterWhere(['>=', 'created_at', strtotime($this->date_from . ' 00:00:00')]);
        }
        if($this->date_to) {
            $query->andFilterWhere(['<=', 'created_at', strtotime($this->date_to . ' 23:59:59')]);
        }
*/
        if ($this->total_price != '') {
            $query->andFilterWhere(['between', 'order_item.total_price', $this->total_price-0.01, $this->total_price+0.01]);
        }

        if ($this->search_child != '') {
            if ($this->search_child == YesNo::NO) {
                $query->andWhere(['not', ['child_license_id' => null]]);
            } elseif ($this->search_child == YesNo::YES) {
                $query->andWhere(['child_license_id' => null]);
            }
        }
        $query->andFilterWhere(['like', 'note', $this->note_own])
            ->andFilterWhere(['like', 'client.client_name', $this->client_name])
            ->andFilterWhere(['like', 'reseller.client_name', $this->reseller_name]);

        return $dataProvider;
    }
}
