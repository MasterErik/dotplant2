<?php

namespace app\theme\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use app\theme\models\SystemLog;

/**
 * SystemLogSearch represents the model behind the search form about `app\theme\models\SystemLog`.
 */
class SystemLogSearch extends SystemLog
{

    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'log_time'], 'integer'],
            [['message'], 'string'],
            [['date_from', 'date_to'], 'safe'],
            [['category', 'prefix', 'level'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
//            'log_time' => $this->log_time,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'message', $this->message]);
        if($this->date_from) {
            $query->andFilterWhere(['>=', 'log_time', strtotime($this->date_from . ' 00:00:00')]);
        }
        if($this->date_to) {
            $query->andFilterWhere(['<=', 'log_time', strtotime($this->date_to . ' 23:59:59')]);
        }

        return $dataProvider;
    }
}
