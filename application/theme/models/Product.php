<?php

namespace app\theme\models;

use Yii;
use yii\db\Exception;
use yii\db\Query;

use app\theme\components\behaviors\TranslateModelBehavior;
use app\theme\components\MultiLangHelper;
use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 */
class Product extends \app\modules\shop\models\Product
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            ['translate' => [
                'class' => TranslateModelBehavior::className(),
                'languages' => MultiLangHelper::getLanguages(),
                'attributes' => ['name', 'title', 'h1', 'breadcrumbs_label', 'content', 'announce']
            ]]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'eset_product_code' => 'Eset product code'
            ]
        );
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\ProductQuery(get_called_class());
    }


    /**
     * @return ProductAttribute
     */
    public function getProductAttributeQuantity()
    {
        return $this->hasOne(ProductAttribute::className(), ['product_id' => 'id'])->where([ProductAttribute::tableName().'.attribute'=>'quantity']);
    }

    /**
     * @return ProductAttribute
     */
    public function getProductAttributePeriod()
    {
        return $this->hasOne(ProductAttribute::className(), ['product_id' => 'id'])->where([ProductAttribute::tableName().'.attribute'=>'eset_period']);
    }

    public static function updateTrigger($event)
    {
        $model = $event->sender;
        if( isset($event->changedAttributes) ) {
            $changedAttributes = $event->changedAttributes;
            if ($event->name == self::EVENT_AFTER_INSERT) {
                self::generateAttributePeriod($model);
                self::generateAttributeQuantity($model);
            }
            if ($event->name == self::EVENT_AFTER_UPDATE) {
                //print_r($changedAttributes); exit;
                if (isset($changedAttributes['eset_period_params'])) {
                    self::generateAttributePeriod($model);
                }
                if (isset($changedAttributes['eset_quantity_params'])) {
                    self::generateAttributeQuantity($model);
                }
            }
        }
    }

    public static function generateAttributes($model)
    {
        self::generateAttributePeriod($model);
        self::generateAttributeQuantity($model);
    }


    private static function generateAttributePeriod($model)
    {
        $period = ProductAttribute::findOne(['product_id'=>$model->id, 'attribute'=>'eset_period']);
        if( is_null($period) ) {
            $period = new ProductAttribute();
            $period->product_id = $model->id;
            $period->attribute = 'eset_period';
        }

        $data = [];
        $els = explode(',', $model->eset_period_params);
        foreach( $els as $el) {
            if( strpos($el, '=>') !== false ) {
                list($key, $value) = explode('=>', $el);
                $data[(int)$key] = trim($value);
            }
        }

        reset($data);
        $period->value = json_encode([
            'rules' => [
                ['eset_period', 'default', 'value' => key($data)],
                ['eset_period', 'required'],
                ['eset_period', 'in', 'range' => array_keys($data)],
            ],
            'type' => 'select',
            'data' => $data
        ]);

        if (!$period->save()) {
            throw new Exception(print_r($period->getErrors(),1));
        }
    }

    private static function generateAttributeQuantity($model)
    {
        $quantity = ProductAttribute::findOne(['product_id'=>$model->id, 'attribute'=>'quantity']);
        if( is_null($quantity) ) {
            $quantity = new ProductAttribute();
            $quantity->product_id = $model->id;
            $quantity->attribute = 'quantity';
        }

        if( strpos($model->eset_quantity_params, '-') !== false ) {
            list($min, $max) = explode('-', str_replace(' ', '', $model->eset_quantity_params));
        } else {
            $min = $max = (int) str_replace(' ', '', $model->eset_quantity_params);
        }
        $quantity->value = json_encode([
            'rules' => [
                ['quantity', 'default', 'value' => (int) $min],
                ['quantity', 'required'],
                ['quantity', 'integer', 'min' => (int) $min, 'max' => (int) $max]
            ],
            'type' => 'text',
            'hint' => Yii::t('eset', 'Enter a value in the range from'). ' ' . $min .' '. Yii::t('eset', 'to') .' '. $max
        ]);
        if (!$quantity->save()) {
            throw new Exception(print_r($quantity->getErrors(),1));
        }
    }

    public static function getDefaultValue($product_id, $attribute)
    {
        $product_attribute = ProductAttribute::findOne(['product_id'=>$product_id, 'attribute'=>$attribute]);
        if( $product_attribute && isset($product_attribute->params['rules'])) {
            foreach ($product_attribute->params['rules'] as $rule) {
                if( $rule[0] == $attribute && $rule[1] == 'default' ) {
                    return isset($rule['value']) ? $rule['value'] : null;
                }
            }
        }
        return null;
    }

    /**
     * @param $id integer
     * @return string
     */
    public static function getIdentifyById($id) {
        return self::findById($id)->name;
    }

    public static function getListCode()
    {
        return ArrayHelper::map(self::find()->orderBy(['sort_order' => SORT_ASC, 'name'  => SORT_ASC])->all(), 'eset_product_code', 'name');
    }

}
