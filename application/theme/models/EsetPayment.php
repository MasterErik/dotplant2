<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_payment}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $partner_id
 * @property integer $user_id
 * @property integer $eset_payment_type_id
 * @property string $type
 * @property integer $viitenumber
 * @property string $summa
 * @property string $created_at
 *
 * @property EsetBankTransaction[] $esetBankTransactions
 * @property EsetBankTransaction $esetBankTransaction
 * @property User $partner
 * @property UserProfile $partnerProfile
 * @property EsetPaymentType $esetPaymentType
 * @property User $user
 * @property UserProfile $userProfile
 * @property EsetPaymentLink[] $esetPaymentLinks
 */
class EsetPayment extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'user_id', 'eset_payment_type_id', 'viitenumber', 'status'], 'integer'],
            [['eset_payment_type_id', 'summa'], 'required'],
            [['type'], 'string'],
            [['summa'], 'number'],
            [['created_at'], 'safe'],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['eset_payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetPaymentType::className(), 'targetAttribute' => ['eset_payment_type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'status' => Yii::t('eset', 'Status'),
            'partner_id' => Yii::t('eset', 'Partner'),
            'user_id' => Yii::t('eset', 'Client'),
            'eset_payment_type_id' => Yii::t('eset', 'Eset Payment Type'),
            'type' => Yii::t('eset', 'Type'),
            'viitenumber' => Yii::t('eset', 'Viitenumber'),
            'summa' => Yii::t('eset', 'Sum'),
            'created_at' => Yii::t('eset', 'Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetBankTransactions()
    {
        return $this->hasMany(EsetBankTransaction::className(), ['eset_payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetBankTransaction()
    {
        return $this->hasOne(EsetBankTransaction::className(), ['eset_payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(User::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPaymentType()
    {
        return $this->hasOne(EsetPaymentType::className(), ['id' => 'eset_payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPaymentLinks()
    {
        return $this->hasMany(EsetPaymentLink::className(), ['eset_payment_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\EsetPaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EsetPaymentQuery(get_called_class());
    }

    public function autoLink()
    {
        // 1. Viitenumber
        $invoice = Invoice::find()->viitenumber($this->viitenumber)->one();

        // 2. Sum & fio
        if(!$invoice) {

            $sum1 = floor($this->summa);
            $sum2 = floor($this->summa) + 1;

            //todo fio
            $invoice = Invoice::find()->betweenSum($sum1, $sum2)->clientName($this->esetBankTransaction->payer)->one();
        }

        if( $invoice ) {

            $transaction = Yii::$app->db->beginTransaction();

            $link = new EsetPaymentLink();
            $link->invoice_id = $invoice->id;
            $link->eset_payment_id = $this->id;
            $link->save();

            $this->status = 1;
            $this->save(false, ['status']);

            $transaction->commit();
        }



    }

}
