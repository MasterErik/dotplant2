<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 29.1.2016
 * Time: 15:57
 */

namespace app\theme\models;

use Yii;
use yii\helpers\ArrayHelper;

class Position extends CustomMemoryModel
{
    public $position;

    const POSITION_PARTNER = 'partner';
    const POSITION_RESELLER = 'reseller';
    const POSITION_BUSINESS = 'business';
    const POSITION_CLIENT = 'client';
    const POSITION_EMPLOYEE = 'employee';

    public function getId()
    {
        return $this->position;
    }

    public function rules()
    {
        return [
            ['position', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::POSITION_PARTNER,   self::desc => Yii::t("eset", 'Partner')],
            [self::key => self::POSITION_RESELLER,  self::desc => Yii::t("eset", 'Reseller')],
            [self::key => self::POSITION_BUSINESS,  self::desc => Yii::t("eset", 'Business')],
            [self::key => self::POSITION_CLIENT,    self::desc => Yii::t("eset", 'Private')],
            [self::key => self::POSITION_EMPLOYEE,  self::desc => Yii::t("eset", 'Employee')],
        ];
    }

    public function getChild($parent)
    {
        switch ($parent) {
            case self::POSITION_PARTNER:
                $allowedKeys = [
                    self::POSITION_RESELLER,
                    self::POSITION_BUSINESS,
                    self::POSITION_CLIENT,
                    self::POSITION_EMPLOYEE
                ];
                break;
            case self::POSITION_RESELLER:
                $allowedKeys = [
                    self::POSITION_BUSINESS,
                    self::POSITION_CLIENT,
                    self::POSITION_EMPLOYEE
                ];
                break;
            default:
                $allowedKeys = [
                    self::POSITION_BUSINESS,
                    self::POSITION_CLIENT,
                ];
        }
        return ArrayHelper::map(array_intersect_key($this->all(), array_flip($allowedKeys)), 'Name', 'Desc');
    }

    public static function getUnregistered()
    {
        $position = New static;
        return $position->getChild(self::POSITION_BUSINESS);
    }
    public static function isGoParent($value)
    {
        switch ($value) {
            case self::POSITION_EMPLOYEE:
            case self::POSITION_BUSINESS:
            case self::POSITION_CLIENT:
                return true;
                break;
            case self::POSITION_PARTNER:
                return false;
                break;
            default:
                return false;
        }
    }
    /**
     * @param $value
     * @return bool
     */
    public static function isPartner($value)
    {
        return self::POSITION_PARTNER == $value;
    }

    /**
     * @param $value
     * @return bool
     */
    public static function isReseller($value)
    {
        return self::POSITION_EMPLOYEE == $value || self::POSITION_RESELLER == $value || self::POSITION_PARTNER == $value;
    }

    /**
     * @param $value
     * @return bool
     */
    public static function isEmployee($value)
    {
        return self::POSITION_EMPLOYEE == $value;
    }

    /**
     * @param $value
     * @return bool
     */
    public static function isNormalUser($value)
    {
        return self::POSITION_BUSINESS == $value || self::POSITION_CLIENT == $value;
    }

}