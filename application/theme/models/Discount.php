<?php

namespace app\theme\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eset_discount}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property string $sign
 * @property string $cost
 * @property string $bdate
 * @property string $edate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $creator_id
 * @property integer $updater_id
 *
 * @property Product $product
 * @property User $user
 */
class Discount extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_discount}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id', 'created_at', 'updated_at', 'creator_id', 'updater_id'], 'integer'],
            [['sign', 'cost'], 'required'],
            [['cost'], 'number'],
            [['bdate', 'edate'], 'safe'],
            [['sign'], 'string', 'max' => 1],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'user_id' => Yii::t('eset', 'User ID'),
            'product_id' => Yii::t('eset', 'Product ID'),
            'sign' => Yii::t('eset', 'Sign'),
            'cost' => Yii::t('eset', 'Cost'),
            'bdate' => Yii::t('eset', 'Bdate'),
            'edate' => Yii::t('eset', 'Edate'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'creator_id' => Yii::t('eset', 'Creator ID'),
            'updater_id' => Yii::t('eset', 'Updater ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\DiscountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\DiscountQuery(get_called_class());
    }

    public static function changeDiscountByUserId($user_id, $new_discount_value, $from_date)
    {

        foreach (self::find()->user($user_id)->product(null)->active()->all() as $discount) {
            $discount->stop($from_date);
        }
        $new_discount = new self;
        $new_discount->user_id = $user_id;
        //
        $new_discount->bdate = $from_date;
        $new_discount->sign = $new_discount_value < 0 ? '-' : '+';
        $new_discount->cost = abs($new_discount_value);

        return $new_discount->save();
    }

    public static function changeDiscountAllResellers($reseller_status_id, $new_discount_value, $from_date)
    {

        foreach (self::find()->product(null)->reseller_status($reseller_status_id)->active()->all() as $discount) {
            $discount->stop($from_date);
        }

        foreach (UserProfile::find()->reseller_status($reseller_status_id)->all() as $user) {
            $new_discount = new self;
            $new_discount->user_id = $user->user_id;
            $new_discount->bdate = $from_date;
            $new_discount->sign = $new_discount_value < 0 ? '-' : '+';
            $new_discount->cost = abs($new_discount_value);
            if( !$new_discount->save() ) {
                return false;
            }
        }

        return true;
    }


    public function stop($from_date)
    {
        $this->edate = $from_date;
        $this->save();
    }
}
