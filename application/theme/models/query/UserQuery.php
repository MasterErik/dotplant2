<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 12.10.2015
 * Time: 16:32
 */

namespace app\theme\models\query;

use app\theme\models\User;
use app\theme\models\UserProfile;
use app\theme\models\Position;
use \yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{

    /**
     * @param $key
     * @return mixed
     */
    public function select2($key)
    {
        return $this->select('u.' . $key . ' as id, up.client_name AS text')
            ->from('{{%user}} AS u')
            ->join('JOIN', '{{%eset_user_profile}} AS up', 'up.user_id = u.id')
            ->where('u.status = :status', [':status' => User::STATUS_ACTIVE])
            ->andWhere('(u.username LIKE :search OR up.client_name LIKE :search)')
            ->andWhere("
                (
                    up.parent_id IN
                        (SELECT est.user_id
                           FROM eset_user_profile AS est
                          WHERE est.parent_id = :parent_id
                            AND est.position = :position
                        )
                    OR up.parent_id = :parent_id
                )",
                    [':parent_id' => UserProfile::getParentReseller(\Yii::$app->user->identity->id)->user_id, ':position' => Position::POSITION_PARTNER]
                )
            ->orderBy('u.username');
   }

    /**
     * @return $this
     */
    public function addActive()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }

}