<?php

namespace app\theme\models\query;

use app\theme\models\License;
use app\theme\models\LicenseStatus;
use app\theme\models\PurchaseType;

/**
 * This is the ActiveQuery class for [[\app\theme\models\Product]].
 *
 * @see \app\theme\models\Product
 */
class LicenseQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byOrder($order_id)
    {
        return $this->andWhere('order_detail_id IN (SELECT id FROM order_item WHERE order_id=:order_id)', [':order_id'=>$order_id]);
    }

    /**
     *
     * @param $proxy OrderFormProxy
     * @return LicenseQuery
     */
    public function select2($proxy = null, $search = null)
    {
        $this->joinWith('product');

        $this->where([
            'user_id' => $proxy->userId,
            'child_license_id' => null,
            'register_state' => License::REGISTER_STATE_REGISTERED,
            'state' => LicenseStatus::STATUS_ACTIVE,
        ]);

        if( $search ) {
            $this->andWhere('license_key LIKE :search OR product.name LIKE :search',
                [':search' => '%' . $search . '%']);
        }

        switch ($proxy->purchaseType) {

            case PurchaseType::PURCHASE_TYPE_NEW:
            case PurchaseType::PURCHASE_TYPE_NFR:
                $this->andWhere('1=0');
                break;

            case PurchaseType::PURCHASE_TYPE_RENEW:
                $this->andWhere([
                    'quantity' => $proxy->quantity,
                    'product_code' => $proxy->productCode
                ]);
                $this->andWhere('edate > NOW() + INTERVAL 2 MONTH');
                break;

            case PurchaseType::PURCHASE_TYPE_ENLARGE:
                $this->andWhere(['product_code' => $proxy->productCode]);
                $this->andWhere('edate > NOW() + INTERVAL 2 MONTH');
                break;

            case PurchaseType::PURCHASE_TYPE_UPGRADE:
                $this->andWhere('edate > NOW() + INTERVAL 2 MONTH');
                break;

            case PurchaseType::PURCHASE_TYPE_DOWNGRADE:
                $this->andWhere('edate > NOW() + INTERVAL 2 MONTH');
                break;
        }

        return $this;
    }
}