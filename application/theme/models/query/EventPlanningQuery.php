<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 12.10.2015
 * Time: 16:32
 */

namespace app\theme\models\query;

use app\theme\models\EventPlanning;
use yii\db\ActiveQuery;

class EventPlanningQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return \app\theme\models\EventPlanning[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\EventPlanning|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function nextTask()
    {
        return $this->andWhere('
            bdate <= NOW()
            AND edate IS NULL
            AND errors < 15
            AND (last_run_ts IS NULL OR last_run_ts < NOW() - INTERVAL 5 + POW(2, errors) MINUTE)');
    }

    /**
     * Get tasks such as license from the order and license
     * @param $order_id
     * @return $this
     */
    public function byOrderLicense($order_id)
    {
        return $this
            ->join('INNER JOIN', '{{%eset_license}} AS license', "license.id = {{%eset_event_planning}}.table_id")
            ->join('INNER JOIN', '{{%order_item}} AS order_item', "order_item.id = license.order_detail_id")
            ->andWhere('order_item.order_id = :order_id', [':order_id'=>$order_id])
            ->andWhere("{{%eset_event_planning}}.name='" . EventPlanning::EVENT_TYPE_LICENSE ."'" )
            ->andWhere('{{%eset_event_planning}}.edate IS NULL');
    }

    /**
     * Get tasks such as invoice from the order
     * @param $order_id
     * @return $this
     */
    public function byOrderInvoice($order_id)
    {
        return $this
            ->join('INNER JOIN', '{{%order}} AS order', "order.id = {{%eset_event_planning}}.table_id")
            ->andWhere('order.id = :order_id', [':order_id' => $order_id])
            ->andWhere("{{%eset_event_planning}}.name='" . EventPlanning::EVENT_TYPE_INVOICE ."'" )
            ->andWhere('{{%eset_event_planning}}.edate IS NULL');
    }

}