<?php

namespace app\theme\models\query;

use app\theme\models\UserProfile;

/**
 * This is the ActiveQuery class for [[\app\theme\models\Invoice]].
 *
 * @see \app\theme\models\Invoice
 */
class InvoiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\theme\models\Invoice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\Invoice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $date
     * @return $this
     */
    public function dateFrom($date)
    {
        return $this->andWhere('created_at >= :date_from', [':date_from' => $date]);
    }

    /**
     * @param $date
     * @return $this
     */
    public function dateTo($date)
    {
        return $this->andWhere('created_at <= :date_to', [':date_to' => $date]);
    }

    /**
     * @param $viitenumber
     * @return $this
     */
    public function viitenumber($viitenumber)
    {
        return $this->andWhere('viitenumber = :viitenumber', [':viitenumber' => $viitenumber]);
    }

    /**
     * @param $viitenumber
     * @return $this
     */
    public function betweenSum($sum1, $sum2)
    {
        return $this->andWhere('sum >= :sum1 AND sum <= :sum2', [':sum1' => $sum1, ':sum2' => $sum2]);
    }

    /**
     * @param $name
     * @return $this
     */
    public function clientName($name)
    {
        //
        return $this->andWhere('user_id IN (SELECT id FROM {{%eset_user_profile}} WHERE client_name LIKE :name)', [':name' => $name]);
    }

    /**
     * Return my or child invoices.
     * @return $this
     */
    public function my()
    {
        if( UserProfile::iPartner() ) {
            return $this->andWhere('user_id IN (SELECT user_id FROM {{%eset_user_profile}} WHERE parent_id IN (SELECT user_id FROM eset_user_profile WHERE parent_id=:partner_id OR user_id=:partner_id))', [':partner_id'=>\Yii::$app->user->id]);
        } elseif( UserProfile::iReseller() ) {
            return $this->andWhere('user_id IN (SELECT user_id FROM {{%eset_user_profile}} WHERE parent_id=:partner_id)', [':partner_id'=>\Yii::$app->user->id]);
        } else {
            return $this->andWhere(['user_id' => \Yii::$app->user->id]);
        }
    }

}