<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 8.2.2016
 * Time: 11:28
 */

namespace app\theme\models\query;

use app\theme\models\UserProfile;
use Yii;
use yii\db\ActiveQuery;

trait UsersFilter
{
    /**
     * @param $className
     * @param $clientTable string
     * @return $this
     * @internal param ActiveQuery $query
     */
    public static function myUsers($className, $clientTable = null)
    {
        $id = UserProfile::getLeader(Yii::$app->user->id)->user_id;
        if (empty($clientTable)) {
            $clientTable = UserProfile::tableName();
        }
        $aliasUser = UserProfile::tableName();
        /** @var ActiveQuery $query */
        $query = new $className();

        if( UserProfile::iPartner() ) {
            return $query::find()->andWhere($clientTable . '.user_id = :partner_id OR ' . $clientTable . '.user_id IN
                (SELECT '.$aliasUser.'.user_id FROM '.$aliasUser. ' WHERE parent_id IN
                    (SELECT '.$aliasUser.'.user_id FROM '.$aliasUser.' WHERE parent_id=:partner_id OR '. $aliasUser.'.user_id=:partner_id))', [':partner_id'=> $id]);
//        } elseif( UserProfile::iReseller() ) {
//            return $this->andWhere('user_id IN (SELECT user_id FROM {{%eset_user_profile}} WHERE parent_id=:partner_id)', [':partner_id'=> $id]);
        } else {
            return $query::find()->andWhere($clientTable . '.user_id = :partner_id OR '. $clientTable . '.user_id IN
                (SELECT user_id FROM '.$aliasUser.' WHERE parent_id=:partner_id)', [':partner_id'=> $id]);
//            return $this->andWhere(['user_id' => $id]);
        }
    }

}