<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\Discount]].
 *
 * @see \app\theme\models\Discount
 */
class DiscountQuery extends \yii\db\ActiveQuery
{
    public function product($id)
    {
        if( $id ) {
            $this->andWhere('product_id=:product_id', [':product_id' => $id]);
        } else {
            $this->andWhere('product_id IS NULL');
        }
        return $this;
    }

    public function user($id)
    {
        $this->andWhere('user_id=:user_id', [':user_id' => $id]);
        return $this;
    }

    public function date($date)
    {
        $this->andWhere('bdate <= :active_date AND edate > :active_date', [':active_date' => $date]);
        return $this;
    }

    public function active()
    {
        $this->andWhere('edate > NOW()');
        return $this;
    }

    public function reseller_status($reseller_status_id)
    {
        $this->andWhere('user_id IN (SELECT user_id FROM {{%eset_user_profile}} WHERE reseller_status_id=:reseller_status_id)', [':reseller_status_id'=>$reseller_status_id]);
        return $this;
    }

    public function best()
    {
        $this->orderBy(['cost' => SORT_DESC]);
        return $this;
    }


    /**
     * @inheritdoc
     * @return \app\theme\models\Discount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\Discount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


}