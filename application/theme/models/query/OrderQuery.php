<?php

namespace app\theme\models\query;

use app\theme\models\InvoiceDetail;
use app\theme\models\Order;
use app\theme\models\OrderStage;

/**
 * This is the ActiveQuery class for [[\app\theme\models\Order]].
 *
 * @see \app\theme\models\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function noInvoice()
    {
        //return $this->andWhere("id NOT IN (SELECT order_id FROM {{%eset_invoice_detail}})");
        return $this->leftJoin(InvoiceDetail::tableName(), InvoiceDetail::tableName() . '.order_id = ' . Order::tableName() . '.id'  )
            ->andWhere(InvoiceDetail::tableName() . '.order_id IS NULL');

    }

    /**
     * @return $this
     */
    public function userInvoiceAuto()
    {
        return $this->andWhere("user_id IN (SELECT user_id FROM {{%eset_user_profile}} WHERE invoice_mode='auto')");
    }

    /**
     * @return $this
     */
    public function finish()
    {
        return $this->andWhere("order_stage_id IN (SELECT id FROM order_stage WHERE immutable_by_user=1 AND immutable_by_manager=1 AND immutable_by_assigned=1)");
        //return $this->andFilterWhere(['order_stage_id' => OrderStage::ORDER_FINISH]);
    }


    /**
     * @return $this
     */
    public function period($date)
    {
        return $this->andWhere("start_date <= :date", [':date' => $date]);
    }
}