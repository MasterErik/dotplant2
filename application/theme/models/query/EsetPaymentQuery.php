<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\EsetPayment]].
 *
 * @see \app\theme\models\EsetPayment
 */
class EsetPaymentQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\EsetPayment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\EsetPayment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function fresh()
    {
        $this->andWhere('status=0');
        return $this;
    }

}