<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\EsetBankTransaction]].
 *
 * @see \app\theme\models\EsetBankTransaction
 */
class EsetBankTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\theme\models\EsetBankTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\EsetBankTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function duplicate($iban, $created_at, $summa, $payer, $description)
    {
        $this->andWhere('iban=:iban AND created_at=:created_at AND summa=:summa AND payer=:payer AND description=:description', [
            ':iban' => $iban,
            ':created_at' => $created_at,
            ':summa' => (float) $summa,
            ':payer' => $payer,
            ':description' => $description
        ]);

        return $this;
    }

    public function fresh()
    {
        $this->andWhere('eset_payment_id IS NULL');

        return $this;
    }

}