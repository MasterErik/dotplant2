<?php

namespace app\theme\models\query;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[EmailConfirmCode]].
 *
 * @see EmailConfirmCode
 */
class EmailConfirmCodeQuery extends ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    public function byEmail($email)
    {
        $this->andWhere(['email'=>$email]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return EmailConfirmCode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmailConfirmCode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}