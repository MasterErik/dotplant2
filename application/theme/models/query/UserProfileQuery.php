<?php

namespace app\theme\models\query;

use app\theme\models\UserProfile;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\theme\models\UserProfile]].
 *
 * @see \app\theme\models\UserProfile
 */
class UserProfileQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\UserProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\UserProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function partners()
    {
        return $this->andWhere("position='partner'");
    }



    /**
     * @return $this
     */
    public function addByPk($id)
    {
        return $this->andWhere("user_id=:pk", [':pk'=>$id]);
    }

    /**
     * @return $this
     */
    public function reseller_status($reseller_status_id)
    {
        return $this->andWhere("reseller_status_id=:reseller_status_id", [':reseller_status_id'=>$reseller_status_id]);
    }

 }