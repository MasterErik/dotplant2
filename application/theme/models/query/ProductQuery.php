<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\Product]].
 *
 * @see \app\theme\models\Product
 */
class ProductQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param string $key
     * @return $this
     */
    public function select2($key)
    {
        return $this->select($key . ' as id, `name` as text')
            ->orWhere('`name` LIKE :search')
            ->orWhere('`h1` LIKE :search')
            ->orWhere('`content` LIKE :search')
            ->orWhere('`sku` LIKE :search')
            ->andWhere(
                [
                    'active' => 1,
                ]
            )
            ->orderBy('sort_order, name');
    }
}