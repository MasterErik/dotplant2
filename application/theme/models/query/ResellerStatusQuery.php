<?php

namespace app\theme\models\query;

use app\theme\models\UserProfile;

/**
 * This is the ActiveQuery class for [[\app\theme\models\ResellerStatus]].
 *
 * @see \app\theme\models\ResellerStatus
 */
class ResellerStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\theme\models\ResellerStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\ResellerStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function my()
    {
        return $this->andWhere('partner_id=:partner_id', [':partner_id' => UserProfile::myProfile()->myParentPartner()->user_id]);
    }

    /**
     * @param $reseller_status
     * @return \app\theme\models\ResellerStatus|array|null
     */
    public function getCurrentStatus($reseller_status) {
        return $this->my()
            ->andFilterWhere(['reseller_status' => $reseller_status])
            ->andFilterWhere(['<=', 'created_at', strtotime('tomorrow') - 1])
            ->addOrderBy(['created_at' => SORT_DESC])
            ->one();
    }
}