<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\EsetPaymentType]].
 *
 * @see \app\theme\models\EsetPaymentType
 */
class EsetPaymentTypeQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\theme\models\EsetPaymentType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\EsetPaymentType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function email($email)
    {
        $this->andWhere('email=:email', [':email' => $email]);

        return $this;
    }

    public function partner($partner_id)
    {
        $this->andWhere('partner_id=:partner_id', [':partner_id' => $partner_id]);

        return $this;
    }

    public function iban($iban)
    {
        $this->andWhere('iban=:iban', [':iban' => $iban]);

        return $this;
    }

    public function active()
    {
        $this->andWhere('edate >= NOW() AND bdate <= NOW()');
        return $this;
    }


}