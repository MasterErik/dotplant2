<?php

namespace app\theme\models\query;

/**
 * This is the ActiveQuery class for [[\app\theme\models\InvoiceOnlinePay]].
 *
 * @see \app\theme\models\InvoiceOnlinePay
 */
class InvoiceOnlinePayQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\theme\models\InvoiceOnlinePay[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\InvoiceOnlinePay|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}