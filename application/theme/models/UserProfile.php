<?php

namespace app\theme\models;

use app\theme\components\validators\ReferenceValidator;
use Exception;
use Yii;
use yii\db\Expression;
use yii\db\Transaction;

/**
 * This is the model class for table "{{%eset_user_profile}}".
 *
 * @property integer $user_id
 * @property integer $country_id
 * @property string $locale
 * @property string $address
 * @property string $comment
 * @property string $page
 * @property string $contact
 * @property string $url
 * @property string $telefon
 * @property string $oldpass
 * @property integer $parent_id
 * @property string $position
 * @property string $invoice_mode
 * @property integer $auto_license
 * @property integer $gender
 * @property string $birthday
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $confirm_registration_key
 * @property string $client_name
 * @property integer $reseller_status_id
 * @property string reseller_status
 *
 * @property string email
 * @property string username
 * @property integer status
 *
 * @property Country $country
 * @property User $user
 * @property UserProfile $parent
 * @property ResellerStatus $resellerStatus
 *
 */
class UserProfile extends BaseModel
{
    const DefaultCountry = 'ee';

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;


    protected static $_myProfile = null;
    protected static $_myParentPartner = null;
    protected static $_myParentReseller = null;
    protected static $_iPartner = null;
    protected static $_iReseller = null;
    protected static $_iNormal = null;
    protected static $_iEmployee = null;


    public $reseller_status;
    private $reseller_status_old;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_user_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id', 'parent_id', 'auto_license', 'gender', 'created_at', 'updated_at', 'reseller_status_id'], 'integer'],
            [['user_id', 'position'], 'required'],
            [['invoice_mode', 'position', 'client_name'], 'string'],
            [['birthday', 'reseller_status'], 'safe'],
            [['locale'], 'string', 'max' => 32],
            ['locale', 'default', 'value' => 'en'],
            [['address'], 'string', 'max' => 1024],
            [['comment'], 'string', 'max' => 2048],
            [['page', 'contact', 'oldpass', 'url'], 'string', 'max' => 255],
            [['telefon'], 'string', 'max' => 30],
            ['telefon', 'unique', 'message' => Yii::t('eset', 'This phone has already been taken.')],

            ['position', ReferenceValidator::className(), 'targetClass' => Position::className()],
            ['auto_license', 'default', 'value' => YesNo::NO],
            ['auto_license', ReferenceValidator::className(), 'targetClass' => YesNo::className(), 'targetAttribute' => 'yes_no'],
            ['invoice_mode', 'default', 'value' => InvoiceMode::INVOICE_IMMEDIATELY],
            ['invoice_mode', ReferenceValidator::className(), 'targetClass' => InvoiceMode::className()],
            ['gender', 'default', 'value' => Gender::GENDER_MALE],
            ['gender', ReferenceValidator::className(), 'targetClass' => Gender::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('eset', 'User ID'),
            'country_id' => Yii::t('eset', 'Country ID'),
            'locale' => Yii::t('eset', 'Locale'),
            'address' => Yii::t('eset', 'Address'),
            'comment' => Yii::t('eset', 'Comment'),
            'page' => Yii::t('eset', 'Page'),
            'contact' => Yii::t('eset', 'Contact'),
            'url' => Yii::t('eset', 'Website'),
            'telefon' => Yii::t('eset', 'Phone'),
            'oldpass' => Yii::t('eset', 'Oldpass'),
            'parent_id' => Yii::t('eset', 'Parent ID'),
            'invoice_mode' => Yii::t('eset', 'Invoice Mode'),
            'auto_license' => Yii::t('eset', 'Auto License'),
            'gender' => Yii::t('eset', 'Gender'),
            'birthday' => Yii::t('eset', 'Birthday'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'client_name' => Yii::t('eset', 'Client Name'),
            'reseller_status_id' => Yii::t('eset', 'Reseller Status'),

            'reseller_status' => Yii::t('eset', 'Reseller Status'),

            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'E-mail'),
            'status' => Yii::t('app', 'Status'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\UserProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\UserProfileQuery(get_called_class());
    }

    public function afterFind()
    {
        if (Position::POSITION_EMPLOYEE == $this->position && !empty($this->parent_id)) {
            $user_profile = self::findOne($this->parent_id);
            $this->auto_license = $user_profile->auto_license;
        }
        parent::afterFind();
        $this->reseller_status = $this->resellerStatus ? $this->resellerStatus->reseller_status : null;
        $this->reseller_status_old = $this->reseller_status;
    }

    public static function getUserCountry()
    {
        $profile = static::myProfile();
        return mb_strtolower(!empty($profile) ? $profile->country->eset_iso_code2 : static::DefaultCountry);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResellerStatus()
    {
        return $this->hasOne(ResellerStatus::className(), ['id' => 'reseller_status_id']);
    }
    public function getUsername() {
        return $this->user->username;
    }
    public function getEmail() {
        return $this->user->email;
    }
    public function getStatus() {
        return $this->user->status;
    }
    public function getCreate_time() {
        return $this->user->create_time;
    }
    public function getUpdate_time() {
        return $this->user->update_time;
    }
    public function getFirst_name() {
        return $this->user->first_name;
    }
    public function getLast_name() {
        return $this->user->last_name;
    }
    /*    public function setBirthday($value)
        {
            $this->birthday = Yii::$app->formatter->asDate($value, 'YYYY-MM-DD');
        }
    */
    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {

        if (empty($this->client_name)) {
            $this->client_name = trim(implode(' ', [$this->user->first_name, $this->user->last_name]));
        }

        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->reseller_status_old !== $this->reseller_status) {
            //$this->reseller_status_id = reseller_status
            if ($resellerStatus = ResellerStatus::find()->getCurrentStatus($this->reseller_status)) {
                $this->reseller_status_id = $resellerStatus->id;
            }

        }
        if ($insert && $this->confirm_registration_key === null) {
            $this->confirm_registration_key = Yii::$app->getSecurity()->generateRandomString();
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (isset($changedAttributes['reseller_status_id'])) {
            $result = Discount::changeDiscountByUserId($this->user_id, $this->resellerStatus->discount, new Expression('NOW()'));
            if (!$result) {
                $this->addError('reseller_status_id', 'Failed to set the status of a reseller.');
                return false;
            }
        }
    }

    public static function userDelete($id)
    {
        /** @var User $model */
        $model = User::findOne($id);
        $model->scenario = 'admin';
        $model->status = User::STATUS_DELETED;
        $model->save(true, ['status']);
    }
    public static function userRestore($id)
    {
        /** @var User $model */
        $model = User::findOne($id);
        $model->scenario = 'admin';
        $model->status = User::STATUS_ACTIVE;
        $model->save(true, ['status']);
    }


    /**
     *
     * @return array
     */
    public static function getAvailablePositions()
    {
        $value = null;

        if (!Yii::$app->user->isGuest) {
            $user_profile = self::myProfile();
            if ($user_profile && $user_profile->position == Position::POSITION_EMPLOYEE) {
                $user_profile = self::findOne($user_profile->parent_id);
            }
            if ($user_profile) {
                $value = $user_profile->position;
            }
        }

        $position = new Position();

        return $position->getChild($value);
    }

    public static function myProfile()
    {
        if (is_null(self::$_myProfile)) {
            self::$_myProfile = isset(Yii::$app->user) ? self::findOne(Yii::$app->user->id) : null;
        }

        return self::$_myProfile;
    }

    public static function getLeader($id = null) {
        if (empty($id)) {
            $user_profile = self::myProfile();
            $id = $user_profile ? $user_profile->user_id : null;
        }

        if (!empty($id)) {
            $user_profile = self::findOne($id);
            if (Position::isEmployee($user_profile->position)) {
                return self::findOne($user_profile->parent_id);
            } else {
                return self::findOne($id);
            }
        } else {
            return null;
        }
    }
    public static function iEmployee()
    {
        if (is_null(self::$_iEmployee)) {

            if (Yii::$app->user->isGuest) {
                self::$_iEmployee = false;

                return self::$_iEmployee;
            }

            $user_profile = self::myProfile();
            self::$_iEmployee = $user_profile ? Position::isEmployee($user_profile->position) : false;
        }

        return self::$_iEmployee;
    }
    /**
     * @return bool
     */
    public static function iPartner()
    {
        if (is_null(self::$_iPartner)) {

            if (Yii::$app->user->isGuest) {
                self::$_iPartner = false;

                return self::$_iPartner;
            }

            $user_profile = self::myProfile();
            self::$_iPartner = $user_profile ? $user_profile->isPartner() : false;
        }

        return self::$_iPartner;
    }

    public static function checkBase() {
        return Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'base');
    }
    public static function setRole($role = 'base')
    {
        $manager = Yii::$app->authManager;
        $id = Yii::$app->user->id;
        if (!$manager->checkAccess($id, $role)) {
            $userRole = $manager->getRole($role);
            $manager->assign($userRole, $id);
        }
    }
    public static function revokeRole($role = 'base')
    {
        $manager = Yii::$app->authManager;
        $id = Yii::$app->user->id;

        if ($manager->checkAccess($id, $role)) {
            $userRole = $manager->getRole($role);
            $manager->revoke($userRole, $id);
        }
    }
    /**
     * @return bool
     */
    public static function iReseller()
    {
        if (is_null(self::$_iReseller)) {
            if (Yii::$app->user->isGuest) {
                self::$_iReseller = false;

                return self::$_iReseller;
            }

            $user_profile = self::myProfile();
            self::$_iReseller = $user_profile ? Position::isReseller($user_profile->position) : false;

        }

        return self::$_iReseller;
    }

    /**
     * @return bool
     */
    public static function iNormalUser()
    {
        if (is_null(self::$_iNormal)) {
            if (Yii::$app->user->isGuest) {
                self::$_iNormal = false;

                return self::$_iNormal;
            }

            $user_profile = self::myProfile();
            if (!$user_profile) {
                self::$_iNormal = false;

                return self::$_iNormal;
            }

            self::$_iNormal = $user_profile ? Position::isNormalUser($user_profile->position) : false;
        }

        return self::$_iNormal;
    }

    /**
     * @return bool
     */
    public function isPartner()
    {
        $user_profile = $this;

        if (!self::checkBase()) {
            return false;
        }

        if (Position::isEmployee($user_profile->position)) {
            $user_profile = self::findOne($user_profile->parent_id);
        }

        return Position::isPartner($user_profile->position);
    }

    /**
     * @return UserProfile|null
     */
    public function myParentPartner()
    {
        if (is_null(self::$_myParentPartner)) {
            self::$_myParentPartner = self::getParentPartner($this->user_id);
        }

        return self::$_myParentPartner;
    }

    /**
     * @return UserProfile
     */
    public function myParentReseller()
    {
        if (is_null(self::$_myParentReseller)) {
            self::$_myParentReseller = self::getParentReseller($this->user_id);
        }

        return self::$_myParentReseller;
    }

    /**
     * @param null $user_id
     * @return UserProfile
     * @throws Exception
     */
    public static function getParentReseller($user_id = null)
    {
        if (!$user_id) {
            return self::findOne(\Yii::$app->params['defaultResellerId']);
        }
        $user_profile = self::findOne($user_id);

        if ($user_profile) {
            if (Position::isGoParent($user_profile->position)) {
                return self::findOne($user_profile->parent_id);
            } else {
                return $user_profile;
            }
        } else {
            throw new Exception(Yii::t('eset', 'Not found user profile'));
        }
    }

    /**
     * @param integer $user_id
     * @return UserProfile user id
     */
    public static function getParentPartner($user_id = null)
    {
        if (!$user_id) {
            return self::findOne(\Yii::$app->params['defaultResellerId']);
        }

        $user_profile = self::findOne($user_id);

        if (Position::isPartner($user_profile->position)) {
            return $user_profile;
        } else {
            return self::getParentPartner($user_profile->parent_id);
        }
    }

    public static function getUserRoot($user_id = null)
    {
        $user_profile = self::findOne($user_id);
        if (self::POSITION_EMPLOYEE == $user_profile->position) {
            return self::findOne($user_profile->parent_id);
        }

    }

}
