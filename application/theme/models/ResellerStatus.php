<?php

namespace app\theme\models;

use app\theme\components\validators\ReferenceValidator;
use Yii;
use yii\db\Expression;
use yii\db\Transaction;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%eset_reseller_status}}".
 *
 * @property integer $id
 * @property string $reseller_status
 * @property integer $image
 * @property string $discount
 * @property string $created_at
 * @property string $partner_id
 *
 * @property UserProfile[] $resellerProfiles
 * @property UserProfile $partnerProfile
 */
class ResellerStatus extends BaseModel
{
    /**
     * @var Transaction
     */
    private $_save_transaction;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_reseller_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount', 'partner_id', 'reseller_status', 'created_at'], 'required'],
            [['discount'], 'number'],
            [['partner_id', 'created_at'], 'integer'],

            ['reseller_status', ReferenceValidator::className(), 'targetClass' => ResellerStatusList::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'reseller_status' => Yii::t('eset', 'Name'),
            'image' => Yii::t('eset', 'Image'),
            'discount' => Yii::t('eset', 'Discount'),
            'created_at' => Yii::t('eset', 'Created At'),
            'partner_id' => Yii::t('eset', 'Partner'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResellerProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['reseller_status_id' => 'id']);
    }

    /**
     * @return UserProfile|null
     */
    public function getPartner()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'partner_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\ResellerStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\ResellerStatusQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->_save_transaction = self::getDb()->beginTransaction();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (isset($changedAttributes['discount'])) {
            $result = Discount::changeDiscountAllResellers($this->id, $this->discount, new Expression('NOW()'));
            if (!$result) {
                $this->_save_transaction->rollBack();
                $this->addError('discount', 'Failed to set the discount of a reseller.');
                return false;
            }
        }
        $this->_save_transaction->commit();
    }

    public function getImageUrl()
    {
        switch ($this->reseller_status) {
            case ResellerStatusList::RESELLER_STATUS_REG: return Url::to('/theme/images/statuses/partner_left_al.svg'); break;
            case ResellerStatusList::RESELLER_STATUS_BRONZE: return Url::to('/theme/images/statuses/bronze_left_al.svg'); break;
            case ResellerStatusList::RESELLER_STATUS_SILVER: return Url::to('/theme/images/statuses/silver_left_al.svg'); break;
            case ResellerStatusList::RESELLER_STATUS_GOLD: return Url::to('/theme/images/statuses/gold_left_al.svg');
            default: return Url::to('/theme/images/statuses/partner_left_al.svg');
        }
    }

}
