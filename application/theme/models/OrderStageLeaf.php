<?php

namespace app\theme\models;

use app\modules\core\helpers\EventTriggeringHelper;
use app\modules\core\models\Events;
use app\theme\module\events\CustomStageEvent;
use app\theme\module\events\CustomStageLeafEvent;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * Class OrderStageLeaf
 * @package app\theme\models
 */
class OrderStageLeaf extends \app\modules\shop\models\OrderStageLeaf
{
    public static function getStageLeaf($prev, $next) {
        if($stageLeaf = self::findOne(['stage_from_id' => $prev, 'stage_to_id' => $next])) {
            return $stageLeaf;
        } else {
            return null;
        }

    }

}
?>