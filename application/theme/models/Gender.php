<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 1.2.2016
 * Time: 10:45
 */

namespace app\theme\models;

use Yii;

class Gender extends CustomMemoryModel
{
    public $gender;

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    public function getId()
    {
        return $this->gender;
    }

    public function rules()
    {
        return [
            ['gender', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::GENDER_MALE, self::desc => Yii::t('eset', 'male')],
            [self::key => self::GENDER_FEMALE, self::desc => Yii::t('eset', 'female')],
        ];

    }
}