<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_payment_link}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $eset_payment_id
 * @property integer $invoice_id
 * @property string $created_at
 *
 * @property User $user
 * @property EsetPayment $esetPayment
 * @property EsetInvoice $invoice
 */
class EsetPaymentLink extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_payment_link}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'eset_payment_id', 'invoice_id'], 'integer'],
            [['eset_payment_id', 'invoice_id'], 'required'],
            [['created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['eset_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetPayment::className(), 'targetAttribute' => ['eset_payment_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => EsetInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'user_id' => Yii::t('eset', 'User ID'),
            'eset_payment_id' => Yii::t('eset', 'Eset Payment ID'),
            'invoice_id' => Yii::t('eset', 'Invoice ID'),
            'created_at' => Yii::t('eset', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEsetPayment()
    {
        return $this->hasOne(EsetPayment::className(), ['id' => 'eset_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(EsetInvoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\EsetPaymentLinkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EsetPaymentLinkQuery(get_called_class());
    }
}
