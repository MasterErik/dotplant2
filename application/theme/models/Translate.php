<?php

namespace app\theme\models;

use Yii;

/**
 * This is the model class for table "{{%eset_translate}}".
 *
 * @property string $table
 * @property integer $model_id
 * @property string $attribute
 * @property string $language
 * @property string $translate
 */
class Translate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_translate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table', 'model_id', 'attribute', 'language', 'translate'], 'required'],
            [['model_id'], 'integer'],
            [['translate'], 'string'],
            [['table'], 'string', 'max' => 255],
            [['attribute'], 'string', 'max' => 256],
            [['language'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model' => Yii::t('eset', 'Model'),
            'model_id' => Yii::t('eset', 'Model ID'),
            'attribute' => Yii::t('eset', 'Attribute'),
            'language' => Yii::t('eset', 'Language'),
            'translate' => Yii::t('eset', 'Translate'),
        ];
    }
}
