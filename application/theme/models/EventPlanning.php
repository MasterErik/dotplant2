<?php

namespace app\theme\models;

use app\theme\components\commands\Processor;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%eset_event_planning}}".
 *
 * @property integer $id
 * @property integer $table_id
 * @property string $name
 * @property string $bdate
 * @property string $edate
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $creator_id
 * @property integer $updater_id
 * @property integer $errors
 * @property string $last_run_ts
 */
class EventPlanning extends BaseModel
{
    const EVENT_TYPE_INVOICE = 'create_invoice';
    const EVENT_TYPE_INVOICE_PRE = 'create_invoice_pre';
    const EVENT_TYPE_LICENSE = 'create_license';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_event_planning}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_id', 'name'], 'required'],
            [['table_id', 'created_at', 'updated_at', 'creator_id', 'updater_id', 'errors'], 'integer'],
            [['bdate', 'edate', 'last_run_ts'], 'safe'],
            [['name'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'table_id' => Yii::t('eset', 'Table ID'),
            'name' => Yii::t('eset', 'Name'),
            'bdate' => Yii::t('eset', 'Bdate'),
            'edate' => Yii::t('eset', 'Edate'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'creator_id' => Yii::t('eset', 'Creator ID'),
            'updater_id' => Yii::t('eset', 'Updater ID'),
            'errors' => Yii::t('eset', 'Errors'),
            'last_run_ts' => Yii::t('eset', 'Last Run Ts'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\theme\models\query\EventPlanningQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\theme\models\query\EventPlanningQuery(get_called_class());
    }

//    /**
//     * @return null|self
//     */
//    public static function giveMeTask()
//    {
//        return self::find()->where('
//            bdate <= NOW() AND edate IS NULL
//            AND errors < 15
//            AND (last_run_ts IS NULL OR last_run_ts < NOW() - INTERVAL 5 + POW(2, errors) MINUTE)')->one();
//    }

    /**
     * @return string
     */
    public function getLockName()
    {
        return $this->className() . '_' . $this->id;
    }

    /**
     * @param int $timeout
     * @return int
     */
    public function getLock($timeout = 5)
    {
        $sql = "SELECT GET_LOCK(:lock, :timeout);";

        return (int)Yii::$app->db->createCommand($sql, [
            'lock' => $this->getLockName(),
            ':timeout' => $timeout
        ])->queryScalar();
    }

    public function releaseLock()
    {
        $sql = "SELECT RELEASE_LOCK(:lock);";

        return (int)Yii::$app->db->createCommand($sql, [
            'lock' => $this->getLockName()
        ])->queryScalar();
    }

    /**
     * @param integer $id License->id
     * @param bool $overtake
     * @return bool
     */
    public static function addCreateLicense($id, $overtake = false)
    {
        // проверим наличие этой команды в БД
        $model = self::findOne(['name' => self::EVENT_TYPE_LICENSE, 'table_id' => $id]);
        if (!is_null($model)) {
            return false;
        } else {
            $model = new EventPlanning();
            $model->table_id = $id;
            $model->name = self::EVENT_TYPE_LICENSE;
            if ($overtake) {
                $model->bdate = new Expression("NOW() + INTERVAL 2 MINUTE");
            }
            return $model->save();
        }
    }
    /**
     * @param $order_id integer
     */
    public static function forceCreateLicense($order_id)
    {
        $processor = new Processor();
        foreach(EventPlanning::find()->byOrderLicense($order_id)->all() as $command ) {
            $processor->run($command);
        }
    }

    /**
     * @param integer $id License->id
     * @param $cmd
     * @return bool
     * @internal param int $type
     */
    public static function addImmediatelyCmd($id, $cmd)
    {
        // проверим наличие этой команды в БД
        $model = self::findOne(['name' => $cmd, 'table_id' => $id]);
        if (!is_null($model)) {
            return false;
        } else {
            $model = new EventPlanning();
            $model->table_id = $id;
            $model->name = $cmd;
            return $model->save();
        }
    }
    public function completeAsSuccess()
    {
        $this->edate = new Expression('NOW()');
        return $this->save(false, ['edate']);
    }

    public function completeAsError()
    {
        $this->errors++;
        $this->last_run_ts = new Expression('NOW()');
        return $this->save(false, ['errors', 'last_run_ts']);
    }

    /**
     * @param integer $order_id Order->id
     * @return bool
     */
    public static function checkExecuteLicense($order_id)
    {
        $result = false;
        if ($order_id && !empty($order_id)) {
            $result = 0 == EventPlanning::find()->byOrderLicense($order_id)->count();
        }
        return $result;
    }
}
