<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 1.2.2016
 * Time: 10:22
 */

namespace app\theme\models;

use Yii;

class InvoiceMode extends CustomMemoryModel
{
    public $invoice_mode;

    const INVOICE_MANUAL = 'manual';
    const INVOICE_AUTO = 'auto';
    const INVOICE_IMMEDIATELY = 'immediately';

    public function getId()
    {
        return $this->invoice_mode;
    }

    public function rules()
    {
        return [
            ['invoice_mode', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::INVOICE_MANUAL, self::desc => Yii::t('eset', 'manual')],
            [self::key => self::INVOICE_AUTO, self::desc => Yii::t('eset', 'auto')],
            [self::key => self::INVOICE_IMMEDIATELY, self::desc => Yii::t('eset', 'immediately')],
        ];
    }

}