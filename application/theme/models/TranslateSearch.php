<?php

namespace app\theme\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\theme\models\Translate;

/**
 * TranslateSearch represents the model behind the search form about `app\theme\models\Translate`.
 */
class TranslateSearch extends Translate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table', 'attribute', 'language', 'translate'], 'safe'],
            [['model_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Translate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'model_id' => $this->model_id,
        ]);

        $query->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'attribute', $this->attribute])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'translate', $this->translate]);

        return $dataProvider;
    }
}
