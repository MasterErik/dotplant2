<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 14.04.2015
 * Time: 11:58
 */

namespace app\theme\models;

use Yii;


class LogCategory extends CustomMemoryModel
{
    public $category;

    const LOG_CATEGORY_DEXTOR = 'dextor';
    const LOG_CATEGORY_PRODUCT = 'product';
    const LOG_CATEGORY_EVENTS = 'events';
    const LOG_CATEGORY_INVOICE = 'invoice';
    const LOG_CATEGORY_PAYMENTS = 'payments';


    public function getId()
    {
        return $this->category;
    }

    public function rules()
    {
        return [
            ['category', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::LOG_CATEGORY_DEXTOR, self::desc => Yii::t("eset", 'dextor')],
            [self::key => self::LOG_CATEGORY_PRODUCT, self::desc => Yii::t("eset", 'product')],
            [self::key => self::LOG_CATEGORY_EVENTS, self::desc => Yii::t("eset", 'events')],
            [self::key => self::LOG_CATEGORY_INVOICE, self::desc => Yii::t("eset", 'invoice')],
            [self::key => self::LOG_CATEGORY_PAYMENTS, self::desc => Yii::t("eset", 'payments')],
        ];
    }

}