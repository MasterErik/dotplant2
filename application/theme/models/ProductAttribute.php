<?php

namespace app\theme\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%product_attribute}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $attribute
 * @property integer $value
 * @property string $created_at
 * @property string $updated_at
 * @property integer $creator_id
 * @property integer $updater_id
 *
 * @property User $updater
 * @property Product $product
 * @property User $creator
 */
class ProductAttribute extends BaseModel
{
    public $params;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%eset_product_attribute}}';
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->params = Json::decode($this->value);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute', 'value'], 'required'],
            [['product_id', 'creator_id', 'updater_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attribute'], 'string', 'max' => 255],
            [['value'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('eset', 'ID'),
            'product_id' => Yii::t('eset', 'Product ID'),
            'attribute' => Yii::t('eset', 'Attribute'),
            'value' => Yii::t('eset', 'Value'),
            'created_at' => Yii::t('eset', 'Created At'),
            'updated_at' => Yii::t('eset', 'Updated At'),
            'creator_id' => Yii::t('eset', 'Creator ID'),
            'updater_id' => Yii::t('eset', 'Updater ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updater_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }
}
