<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 14.04.2015
 * Time: 11:58
 */

namespace app\theme\models;

use Yii;


class Sector extends CustomMemoryModel
{
    public $sector;

    const SECTOR_BUSINESS = 'Business';
    const SECTOR_GOVERNMENT = 'Government';
    const SECTOR_EDUCATION = 'Education';

    public function getId()
    {
        return $this->sector;
    }

    public function rules()
    {
        return [
            ['sector', 'validateExists'], //'params' => ['message' => Yii::t('eset', '{atttibute} does not exist')]
        ];
    }

    protected function fullData()
    {
        return [
            [self::key => self::SECTOR_BUSINESS, self::desc => Yii::t("eset", 'Business')],
            [self::key => self::SECTOR_GOVERNMENT, self::desc => Yii::t("eset", 'Government')],
            [self::key => self::SECTOR_EDUCATION, self::desc => Yii::t("eset", 'Education')],
        ];
    }

}