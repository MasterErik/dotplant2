<?php
/**
 * Created by PhpStorm.
 * User: Erik
 * Date: 15.05.2015
 * Time: 15:55
 */

namespace app\theme\models;

use WebToPay;
use Yii;

class PaymentType extends \app\modules\shop\models\PaymentType
{
    /**
     * @param int $isActive
     * @param float $total
     * @return PaymentType[]|array
     */
    public static function getPaymentTypes($isActive = 1, $total = 0.0)
    {
        $models = static::find()->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC]);
        if (null !== $isActive) {
            $models->andWhere(
                'active = :active AND payment_available = :available AND min <= :min AND (country_id = :country_id OR country_id IS NULL)',
                [
                    ':active' => 1,
                    ':available' => 1,
                    ':min' => $total,
                    ':country_id' => UserProfile::myProfile()->country_id
                ]
            );
        }

        return $models->all();
    }


}